let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .js('resources/assets/js/pages/login.js', 'public/js/pages')
   .js('resources/assets/js/pages/register.js', 'public/js/pages')
   .sass('resources/assets/sass/app.scss', 'public/css/bootstrap-4.x.css')
   .sass('resources/assets/sass/theme-styles.scss', 'public/css/theme-styles.css')
   .sass('resources/assets/sass/blocks.scss', 'public/css/blocks.css')
   .sass('resources/assets/sass/widgets.scss', 'public/css/widgets.css')
   .copyDirectory('resources/assets/js/app', 'public/js')
   .copyDirectory('resources/assets/css', 'public/css')
   .copyDirectory('resources/assets/fonts', 'public/fonts')
   .copyDirectory('resources/assets/img', 'public/img')
   .copyDirectory('resources/assets/svg', 'public/svg')
   .copyDirectory('resources/assets/svg-icons', 'public/svg-icons')
   .copyDirectory('resources/assets/js/commonFcns.js', 'public/js/commonFcns.js')

   // pages
   .js('resources/assets/js/pages/channel/create.js', 'public/js/pages/channel')
   .js('resources/assets/js/pages/channel/show.js', 'public/js/pages/channel')
   .js('resources/assets/js/pages/channel/edit.js', 'public/js/pages/channel')
   .js('resources/assets/js/pages/channel/index.js', 'public/js/pages/channel')
   .js('resources/assets/js/pages/video/upload.js', 'public/js/pages/video')
   .js('resources/assets/js/pages/tutorial/create.js', 'public/js/pages/tutorial')
   .js('resources/assets/js/pages/tutorial/show.js', 'public/js/pages/tutorial')
   .js('resources/assets/js/pages/tutorial/edit.js', 'public/js/pages/tutorial')
   .js('resources/assets/js/pages/tutorial/index.js', 'public/js/pages/tutorial')
   .js('resources/assets/js/pages/video/viewcount.js', 'public/js/pages/video')
   .js('resources/assets/js/pages/video/vedit.js', 'public/js/pages/video')
   .js('resources/assets/js/pages/user/mywave.js', 'public/js/pages/user')
   .js('resources/assets/js/pages/user/wave.js', 'public/js/pages/user')
   .js('resources/assets/js/pages/user/messagebox.js', 'public/js/pages/user')
   .js('resources/assets/js/pages/search/search.js', 'public/js/pages/search')
   .js('resources/assets/js/pages/main/index.js', 'public/js/pages/main')


   // Plugins
   .copyDirectory('resources/assets/plugins/cropperjs', 'public/plugins/cropperjs')
   .copyDirectory('resources/assets/plugins/froala_editor_2.8.4/', 'public/plugins/froala_editor_2.8.4')
   .copyDirectory('resources/assets/plugins/codemirror.min.css', 'public/plugins/codemirror.min.css')
   .copyDirectory('resources/assets/plugins/codemirror.min.js', 'public/plugins/codemirror.min.js')
   .copyDirectory('resources/assets/plugins/xml.min.js', 'public/plugins/xml.min.js')
   .copyDirectory('resources/assets/plugins/bootstrap_3.3.7/bootstrap.min.css', 'public/plugins/bootstrap.min.css')
   .copyDirectory('resources/assets/plugins/bootstrap_3.3.7/bootstrap.min.js', 'public/plugins/bootstrap.min.js')
   .copyDirectory('resources/assets/plugins/vimeo/player.js', 'public/plugins/player.js')
   .copyDirectory('resources/assets/plugins/highlightjs/highlight.css', 'public/plugins/highlight.css')
   .copyDirectory('resources/assets/plugins/font-awesome-5/all.min.css', 'public/css/fontawesome-all.css')
   .copyDirectory('resources/assets/plugins/devicon', 'public/plugins/devicon')
   .copyDirectory('resources/assets/plugins/ion.rangeSlider', 'public/plugins/ion.rangeSlider')
   .copyDirectory('resources/assets/plugins/jquery.pwstrength.bootstrap', 'public/plugins/jquery.pwstrength.bootstrap')
   .copyDirectory('resources/assets/plugins/sharer.js-master/sharer.min.js', 'public/plugins/sharerjs/sharer.min.js')
   .copyDirectory('resources/assets/plugins/vue-multiselect/vue-multiselect.min.css', 'public/plugins/vue-multiselect/vue-multiselect.min.css')
   .copyDirectory('resources/assets/plugins/toastr-master', 'public/plugins/toastr-master');
