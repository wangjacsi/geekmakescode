<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTutorialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tutorials', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('slug');
            $table->integer('channel_id')->unsigned();
            $table->text('description')->nullable();
            // 채널 관리자 중 강좌 관리 가능
            $table->enum('visibility', ['public', 'unlisted', 'private']);
            $table->enum('cost_type', ['free', 'once', 'divide']);
            $table->float('cost', 8, 2)->default(0);
            $table->boolean('allow_votes')->default(false);
            $table->boolean('allow_comments')->default(false);
            //$table->integer('processed_percentage')->nullable();
            $table->enum('progress_status', ['prepare', 'publishing', 'pause', 'stop', 'complete']);
            $table->string('thumbnail')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('channel_id')->references('id')->on('channels')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tutorials');
    }
}
