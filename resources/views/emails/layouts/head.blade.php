<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width,initial-scale=1" />
  <meta name="x-apple-disable-message-reformatting" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <!--[if mso]>
  <xml>
    <o:OfficeDocumentSettings>
      <o:AllowPNG/>
      <o:PixelsPerInch>96</o:PixelsPerInch>
    </o:OfficeDocumentSettings>
  </xml>
  <style>
    .spacer, .divider {mso-line-height-rule: exactly;}
    td,th,div,p,a,h1,h2,h3,h4,h5,h6 {font-size:13px; line-height:23px; font-family:"Segoe UI",Helvetica,Arial,sans-serif;}
  </style>
  <![endif]-->

  <style type="text/css">

    @import url('https://fonts.googleapis.com/css?family=Montserrat:400,700|Open+Sans:400,700');
    @media only screen {
      .serif {font-family: 'Montserrat', sans-serif!important;}
      .sans-serif {font-family: 'Open Sans', sans-serif!important;}
      .column, th, td, div, p {font-family: -apple-system,system-ui,BlinkMacSystemFont,"Segoe UI","Roboto",Helvetica,Arial,sans-serif;}
    }

    #outlook a {padding: 0;}
    a {text-decoration: none;}
    table {border-collapse: collapse;}
    img {border: 0; display: block; line-height: 100%;}
    .column, th, td, div, p {font-size: 13px; line-height: 23px;}

    .wrapper {min-width: 700px;}
    .row {margin: 0 auto; width: 700px;}
    .row .row, th .row {width: 100%;}

    @media only screen and (max-width: 699px) {

      .wrapper {min-width: 100% !important;}
      .row {width: 90% !important;}
      .row .row {width: 100% !important;}

      .column {
        box-sizing: border-box;
        display: inline-block !important;
        line-height: inherit !important;
        width: 100% !important;
        word-break: break-word;
        -webkit-text-size-adjust: 100%;
      }
      .mobile-1  {max-width: 8.33333%;}
      .mobile-2  {max-width: 16.66667%;}
      .mobile-3  {max-width: 25%;}
      .mobile-4  {max-width: 33.33333%;}
      .mobile-5  {max-width: 41.66667%;}
      .mobile-6  {max-width: 50%;}
      .mobile-7  {max-width: 58.33333%;}
      .mobile-8  {max-width: 66.66667%;}
      .mobile-9  {max-width: 75%;}
      .mobile-10 {max-width: 83.33333%;}
      .mobile-11 {max-width: 91.66667%;}
      .mobile-12 {
        padding-right: 30px !important;
        padding-left: 30px !important;
      }

      .mobile-offset-1  {margin-left: 8.33333% !important;}
      .mobile-offset-2  {margin-left: 16.66667% !important;}
      .mobile-offset-3  {margin-left: 25% !important;}
      .mobile-offset-4  {margin-left: 33.33333% !important;}
      .mobile-offset-5  {margin-left: 41.66667% !important;}
      .mobile-offset-6  {margin-left: 50% !important;}
      .mobile-offset-7  {margin-left: 58.33333% !important;}
      .mobile-offset-8  {margin-left: 66.66667% !important;}
      .mobile-offset-9  {margin-left: 75% !important;}
      .mobile-offset-10 {margin-left: 83.33333% !important;}
      .mobile-offset-11 {margin-left: 91.66667% !important;}

      .has-columns {
        padding-right: 20px !important;
        padding-left: 20px !important;
      }

      .has-columns .column {
        padding-right: 10px !important;
        padding-left: 10px !important;
      }

      .mobile-collapsed .column {
        padding-left: 0 !important;
        padding-right: 0 !important;
      }

      img {
        width: 100% !important;
        height: auto !important;
      }

      .mobile-center {
        display: table !important;
        float: none;
        margin-left: auto !important;
        margin-right: auto !important;
      }
      .mobile-left {
        float: none;
        margin: 0 !important;
      }
      .mobile-text-center {text-align: center !important;}
      .mobile-text-left   {text-align: left !important;}
      .mobile-text-right  {text-align: right !important;}

      .mobile-valign-top  {vertical-align: top !important;}

      .mobile-full-width {
        display: table;
        width: 100% !important;
      }

      .mobile-first {display: table-header-group !important;}
      .mobile-intermediate {display: table-row !important;}
      .mobile-last {display: table-footer-group !important;}

      .mobile-first th,
      .mobile-intermediate th,
      .mobile-last th {
        padding-left: 30px !important;
        padding-right: 30px !important;
      }

      .spacer                     {height: 30px; line-height: 100% !important; font-size: 100% !important;}
      .divider th                 {height: 60px;}
      .mobile-padding-top         {padding-top: 30px !important;}
      .mobile-padding-top-mini    {padding-top: 10px !important;}
      .mobile-padding-bottom      {padding-bottom: 30px !important;}
      .mobile-padding-bottom-mini {padding-bottom: 10px !important;}
      .mobile-margin-top          {margin-top: 30px !important;}
      .mobile-margin-top-mini     {margin-top: 10px !important;}
      .mobile-margin-bottom       {margin-bottom: 30px !important;}
      .mobile-margin-bottom-mini  {margin-bottom: 10px !important;}
    }
  </style>
</head>
