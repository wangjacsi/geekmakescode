<!-- Footer 1 -->
<table class="row" align="center" bgcolor="#1F2225" cellpadding="0" cellspacing="0">
  <tr>
    <th class="column has-columns" width="640" style="padding-left: 30px; padding-right: 30px;">

      <table class="row" cellpadding="0" cellspacing="0">
        <tr>
          <td class="spacer" height="80" colspan="2" style="font-size: 80px; line-height: 80px;">&nbsp;</td>
        </tr>
        <tr valign="top" style="vertical-align: top;">
          <th class="column mobile-padding-bottom" width="310" style="padding-right: 10px; text-align: left;">
              <a target="_blank" href="{{url('/')}}">
                <img src="{{ asset("img/logo/logoSB_w.png") }}" width="100" alt="{{env('APP_NAME')}}" style="border: 0; width: 100%; max-width: 100px;">
              </a>
          </th>
          <th class="column mobile-text-right" width="300" style="padding-left: 10px; padding-right: 10px; color: #969AA1; font-weight: 400; text-align: right;">
            <div class="sans-serif" style="font-size: 14px; font-weight: 700; line-height: 100%; margin-bottom: 15px;">{{ env('APP_NAME')}}</div>
            <div class="sans-serif">
                함께 즐겁게 미래를 향해
                <br> 배우고 가르치고 협업하는
                <br> 핫한 개발자들의 커뮤니티
            </div>
          </th>
        </tr>
      </table>

      <table class="row divider" cellpadding="0" cellspacing="0" width="100%">
        <tr>
          <th height="81">
            <div style="border-top: 1px solid #2B2E32; font-size: 0; line-height: 0;">&nbsp;</div>
          </th>
        </tr>
      </table>

      <table class="row" cellpadding="0" cellspacing="0">
        <tr>
          <th class="column" width="640" style="text-align: left;">
            <table cellpadding="10" cellspacing="0">
              <tr>
                  <td style="padding-left: 0;">
                    <a target="_blank" href="https://twitter.com/codewwwave" style="text-decoration: none;">
                      <img src="{{ asset("img/email/icons/social/circle/twitter-circle.png") }}" width="24" alt="Twitter" style="border: 0; width: 100%; max-width: 24px;">
                    </a>
                  </td>
                  <td>
                    <a target="_blank" href="https://www.facebook.com/Codewwwave-2170298399910863" style="text-decoration: none;">
                      <img src="{{ asset("img/email/icons/social/circle/facebook-circle.png")}}" width="24" alt="Facebook" style="border: 0; width: 100%; max-width: 24px;">
                    </a>
                  </td>
                  <td>
                    <a target="_blank" href="https://www.instagram.com/codewwwave/" style="text-decoration: none;">
                      <img src="{{ asset("img/email/icons/social/circle/instagram-circle.png")}}" width="24" alt="instagram" style="border: 0; width: 100%; max-width: 24px;">
                    </a>
                  </td>
                  <td style="padding-right: 0;">
                    <a target="_blank" href="https://www.youtube.com/channel/UClzuFd0vGnRwWN9w2xtcPDA"  style="text-decoration: none;">
                      <img src="{{ asset("img/email/icons/social/circle/youtube-circle.png")}}" width="24" alt="youtube" style="border: 0; width: 100%; max-width: 24px;">
                    </a>
                  </td>
              </tr>
            </table>
          </th>
        </tr>
      </table>

      <div class="spacer" style="font-size: 20px; line-height: 20px;">&nbsp;</div>

      <table class="row" cellpadding="0" cellspacing="0">
        <tr>
          <th class="column" width="640" style="color: #969AA1; font-weight: 400; text-align: left;">
            <div class="sans-serif">&copy; {{ env('APP_NAME')}}. All Rights Reserved.</div>
          </th>
        </tr>
        <tr>
          <td class="spacer" height="30" style="font-size: 30px; line-height: 30px;">&nbsp;</td>
        </tr>
      </table>

    </th>
  </tr>
</table>
