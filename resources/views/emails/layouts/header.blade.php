<!-- Header Left -->
<table class="row" align="center" bgcolor="#FFFFFF" cellpadding="0" cellspacing="0">
  <tr>
    <td class="spacer" height="40" style="font-size: 40px; line-height: 40px; mso-line-height-rule: exactly;">&nbsp;</td>
  </tr>
  <tr>
    <th class="column" width="640" style="padding-left: 30px; padding-right: 30px; text-align: left;">
      <a target="_blank" href="{{url('/')}}" style="text-decoration: none;">
        <img class="mobile-center" src="{{ asset("img/logo/logo_bc.png") }}" width="200" alt="{{env('APP_NAME')}}" style="border: 0; width: 100%; max-width: 200px;">
      </a>
    </th>
  </tr>
  <tr>
    <td class="spacer" height="40" style="font-size: 40px; line-height: 40px; mso-line-height-rule: exactly;">&nbsp;</td>
  </tr>
</table>
<!-- /Header Left -->
