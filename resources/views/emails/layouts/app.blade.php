<!DOCTYPE html>
<html lang="en" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">

  @include('emails.layouts.head')

  <body style="box-sizing:border-box;margin:0;padding:0;width:100%;-webkit-font-smoothing:antialiased;">

    <table class="wrapper" align="center" bgcolor="#EEEEEE" cellpadding="0" cellspacing="0" width="100%">
      <tr>
        <td style="padding: 30px 0;">

          @include('emails.layouts.header')

          @yield('content')

          @include('emails.layouts.footer')


        </td>
      </tr>
    </table>

  </body>
</html>
