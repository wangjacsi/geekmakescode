@component('mail::message')
# 안녕하세요

{{ config('app.name') }}에 가입해 주셔서 감사합니다. 당신은 {{ $service }} 를 이용하여 등록하였습니다.

@if($user->social->count() > 1)
***
### 당신이 사용하고 있는 소셜 로그인 리스트:
    @foreach ($user->social as $social)
        * {{ config("services.{$social->service}.name") }}
    @endforeach

@endif

감사합니다,<br>
{{ config('app.name') }}
@endcomponent
