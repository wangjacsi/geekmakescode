@extends('emails.layouts.app')

@section('content')
<!-- Intro Basic -->
<table class="row" align="center" bgcolor="#F8F8F8" cellpadding="0" cellspacing="0">
<tr>
  <td class="spacer" height="80" style="font-size: 80px; line-height: 80px; mso-line-height-rule: exactly;">&nbsp;</td>
</tr>
<tr>
  <th class="column sans-serif" width="640" style="padding-left: 30px; padding-right: 30px; font-weight: 400; text-align: left;">
    <div style="color: #969AA1; font-size: 18px; line-height: 28px; margin-bottom: 50px;">강좌
        <span class="serif" style="color: #1F2225; font-size: 18px; font-weight: 700; line-height: 50px; margin-bottom: 30px;">{{$tutorial->title}}
        </span>에 새로운 수강 신청이 있습니다
    </div>
    <div style="color: #969AA1; font-size: 18px; line-height: 28px; margin-bottom: 10px;">신청자: <span style="font-weight:bold; color:#1F2225;"><a target="_blank" href="{{ url('/wave/'.$user->slug)}}">{{$user->name}}</a></span></div>

  </th>
</tr>
<tr>
  <td class="spacer" height="80" style="font-size: 80px; line-height: 80px; mso-line-height-rule: exactly;">&nbsp;</td>
</tr>
</table>
<!-- /Intro Basic -->
@endsection
