@component('mail::message')
# 등록하신 이메일 확인

안녕하세요~ 서비스 가입해주셔서 감사합니다. 서비스 이용을 위해서 아래의 버튼을 누르시고 이메일 확인 작업을 완료해주세요.

@component('mail::button', ['url' => route('activate', [
    'token' => $user->activation_token,
    'email' => $user->email
    ])])
이메일 확인
@endcomponent

감사합니다,<br>
{{ config('app.name') }}
@endcomponent
