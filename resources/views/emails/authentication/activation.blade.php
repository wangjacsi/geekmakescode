@extends('emails.layouts.app')

@section('content')
<!-- Intro Basic -->
<table class="row" align="center" bgcolor="#F8F8F8" cellpadding="0" cellspacing="0">
<tr>
  <td class="spacer" height="80" style="font-size: 80px; line-height: 80px; mso-line-height-rule: exactly;">&nbsp;</td>
</tr>
<tr>
  <th class="column" width="640" style="padding-left: 30px; padding-right: 30px; font-weight: 400; text-align: left;">
    <div class="serif" style="color: #1F2225; font-size: 28px; font-weight: 700; line-height: 50px; margin-bottom: 30px;">Hi {{$user->name}},</div>
    <div class="sans-serif" style="color: #969AA1; font-size: 18px; line-height: 28px; margin-bottom: 40px;">안녕하세요~ 서비스에 가입해주셔서 감사합니다. 서비스 이용을 위해서 아래의 버튼을 클릭하여 이메일 확인을 완료해주세요.</div>
    <table class="mobile-text-center" bgcolor="#3FB58B" cellpadding="0" cellspacing="0" style="border-radius: 3px;">
      <tr>
        <th class="sans-serif">
          <a href="{{ route('activate', [
              'token' => $user->activation_token,
              'email' => $user->email
              ]) }}" style="border: 0 solid #3FB58B; color: #FFFFFF; display: inline-block; font-size: 14px; font-weight: 400; padding: 15px 50px 15px 50px; text-decoration: none;">이메일 확인</a>
        </th>
      </tr>
    </table>
    <div style="color: #969AA1; font-size: 13px; margin-top: 40px;">감사합니다 <br><strong>{{env('APP_NAME')}} Team</strong></div>
  </th>
</tr>
<tr>
  <td class="spacer" height="80" style="font-size: 80px; line-height: 80px; mso-line-height-rule: exactly;">&nbsp;</td>
</tr>
</table>
<!-- /Intro Basic -->
@endsection
