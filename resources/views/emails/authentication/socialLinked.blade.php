@extends('emails.layouts.app')

@section('content')

<!-- Intro Basic -->
<table class="row" align="center" bgcolor="#F8F8F8" cellpadding="0" cellspacing="0">
<tr>
  <td class="spacer" height="80" style="font-size: 80px; line-height: 80px; mso-line-height-rule: exactly;">&nbsp;</td>
</tr>
<tr>
  <th class="column sans-serif" width="640" style="padding-left: 30px; padding-right: 30px; font-weight: 400; text-align: left;">
    <div class="serif" style="color: #1F2225; font-size: 28px; font-weight: 700; line-height: 50px; margin-bottom: 30px;">Hi {{$user->name}},</div>
    <div style="color: #969AA1; font-size: 18px; line-height: 28px; margin-bottom: 40px;">{{ config('app.name') }}에 가입해 주셔서 감사합니다. <br />
        당신은
        <span style="color:blue; font-weight:bold;">{{ $service }}</span>
        를 이용하여 등록하였습니다.
        </div>
  </th>
</tr>
<tr>
  <td class="spacer" height="80" style="font-size: 80px; line-height: 80px; mso-line-height-rule: exactly;">&nbsp;</td>
</tr>
</table>
<!-- /Intro Basic -->

<!-- Cart Items Headline -->
@if($user->social->count() > 1)
<table class="row" align="center" bgcolor="#FFFFFF" cellpadding="0" cellspacing="0">
<tr>
  <td class="spacer" height="80" style="font-size: 80px; line-height: 80px; mso-line-height-rule: exactly;">&nbsp;</td>
</tr>
<tr>
  <th class="column" width="640" style="padding-left: 30px; padding-right: 30px; font-weight: 400; text-align: left;">
    <div class="serif" style="color: #1F2225; font-size: 18px;">사용중인 소셜 로그인 리스트</div>
  </th>
</tr>
<tr>
  <td class="spacer" height="50" style="font-size: 50px; line-height: 50px; mso-line-height-rule: exactly;">&nbsp;</td>
</tr>
</table>

<!-- Product Row -->
<table class="row" align="center" bgcolor="#FFFFFF" cellpadding="0" cellspacing="0">
  @foreach ($user->social as $social)
      <tr>
        <th class="column" width="640" style="padding-left: 30px; padding-right: 30px;">
          <table class="mobile-center" cellpadding="0" cellspacing="0" style="border-radius: 3px;">
            <tr>
              <th class="sans-serif">
                  <span style="color:blue; font-weight:bold; font-size: 16px; ">{{ config("services.{$social->service}.name") }}</span>
              </th>
            </tr>
          </table>
        </th>
      </tr>

      <tr>
        <th class="column" width="640" style="padding-left: 30px; padding-right: 30px;">
          <table class="divider" cellpadding="0" cellspacing="0" width="100%">
            <tr>
              <th height="80">
                <div style="border-top: 1px solid #EEEEEE; font-size: 0; line-height: 0; mso-line-height-rule: exactly;">&nbsp;</div>
              </th>
            </tr>
          </table>
        </th>
      </tr>

@endforeach
</table>
<!-- /Product Row -->
@endif
<!-- /Cart Items Headline -->
@endsection
