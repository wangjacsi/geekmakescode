@extends('emails.layouts.app')

@section('content')
<!-- Intro Basic -->
<table class="row" align="center" bgcolor="#F8F8F8" cellpadding="0" cellspacing="0">
<tr>
  <td class="spacer" height="80" style="font-size: 80px; line-height: 80px; mso-line-height-rule: exactly;">&nbsp;</td>
</tr>
<tr>
  <th class="column sans-serif" width="640" style="padding-left: 30px; padding-right: 30px; font-weight: 400; text-align: left;">
    <div class="serif" style="color: #1F2225; font-size: 28px; font-weight: 700; line-height: 50px; margin-bottom: 30px;">Hi {{$name}},</div>
    <div style="color: #969AA1; font-size: 18px; line-height: 28px; margin-bottom: 40px;">사용자 비밀번호 재설정을 위한 이메일입니다. 아래 링크를 클릭하시고 비밀번호를 새롭게 설정하세요.</div>
    <table align="center" cellpadding="0" cellspacing="0" width="100%" style="margin: 0 auto; word-break: break-all;">
      <tr>
        <th class="sans-serif" bgcolor="#FFFFFF" style="padding: 20px; border-radius: 3px;">
         <a href="{{ route('password.reset', $token) }}" style="color: #3FB58B; font-weight: 400; font-size: 14px; text-decoration: none;">비밀번호 재설정</a>
        </th>
      </tr>
    </table>
    <div style="color: #969AA1; font-size: 13px; margin-top: 40px;">비밀번호 재설정을 원하지 않을 시 필요한 작업은 없습니다.</div>
  </th>
</tr>
<tr>
  <td class="spacer" height="80" style="font-size: 80px; line-height: 80px; mso-line-height-rule: exactly;">&nbsp;</td>
</tr>
</table>
<!-- /Intro Basic -->
@endsection
