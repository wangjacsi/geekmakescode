@extends('emails.layouts.app')

@section('content')

<!-- Intro Basic -->
<table class="row" align="center" bgcolor="#F8F8F8" cellpadding="0" cellspacing="0">
<tr>
  <td class="spacer" height="80" style="font-size: 80px; line-height: 80px; mso-line-height-rule: exactly;">&nbsp;</td>
</tr>
<tr>
  <th class="column sans-serif" width="640" style="padding-left: 30px; padding-right: 30px; font-weight: 400; text-align: left;">
    <div style="color: #969AA1; font-size: 18px; line-height: 28px; margin-bottom: 50px;">채널
        <span class="serif" style="color: #1F2225; font-size: 18px; font-weight: 700; line-height: 50px; margin-bottom: 30px;">{{$channel->name}}
        </span>에 새로운 메세지가 도착하였습니다
    </div>
    <div style="color: #969AA1; font-size: 18px; line-height: 28px; margin-bottom: 10px;">보낸이: <span style="font-weight:bold; color:#1F2225;"><a target="_blank" href="{{ url('/wave/'.$user->slug)}}">{{$user->name}}</a></span></div>
    <div style="color: #969AA1; font-size: 18px; line-height: 28px; margin-bottom: 10px;">제 목: <span style="font-weight:bold; color:#1F2225;">{{$messageM->title}}</span></div>
    <div style="color: #969AA1; font-size: 18px; line-height: 28px; margin-bottom: 10px;">메세지 내용: </div>
    <table align="center" cellpadding="0" cellspacing="0" width="100%" style="margin: 0 auto; word-break: break-all;">
      <tr>
        <th class="sans-serif" bgcolor="#FFFFFF" style="padding: 20px; border-radius: 3px;">
            @markdown($messageM->message)
        </th>
      </tr>
    </table>

  </th>
</tr>
<tr>
  <td class="spacer" height="80" style="font-size: 80px; line-height: 80px; mso-line-height-rule: exactly;">&nbsp;</td>
</tr>
</table>
<!-- /Intro Basic -->
@endsection
