@extends('emails.layouts.app')

@section('content')

<!-- Intro Basic -->
<table class="row" align="center" bgcolor="#F8F8F8" cellpadding="0" cellspacing="0">
<tr>
  <td class="spacer" height="80" style="font-size: 80px; line-height: 80px; mso-line-height-rule: exactly;">&nbsp;</td>
</tr>
<tr>
  <th class="column" width="640" style="padding-left: 30px; padding-right: 30px; font-weight: 400; text-align: left;">
    <div class="serif" style="color: #1F2225; font-size: 28px; font-weight: 700; line-height: 50px; margin-bottom: 30px;">Hi, 새로운 강좌가 오픈되었습니다</div>
    <div class="sans-serif" style="color: #969AA1; font-size: 18px; line-height: 28px; margin-bottom: 40px;">강좌 {{$tutorial->title}}가 새롭게 오픈되었습니다. 강좌 내용을 살펴보시고 신청해주세요.</div>
  </th>
</tr>
<tr>
    <th class="column mobile-last" width="310" style="padding-left: 30px; padding-right: 50px;">
      <table cellpadding="0" cellspacing="0">
        <tr>
          <th style="color: #969AA1; font-weight: 400; text-align: left;">
            <div class="sans-serif" style="font-size: 20px; margin-bottom: 30px;">{{$tutorial->title}}</div>
            <div class="sans-serif" style="margin-bottom: 20px;">{{str_limit(strip_tags($tutorial->description), 300)}}</div>
            <a href="{{url('/tutorial/'.$tutorial->slug)}}" class="sans-serif" style="color: #3FB58B; text-decoration: none;">Read More</a>
          </th>
        </tr>
      </table>
    </th>
  <th class="column mobile-first" width="310">
    <table cellpadding="0" cellspacing="0" width="100%">
      <tr>
        <th class="mobile-padding-bottom" style="text-align: right;">
          <a href="{{url('/tutorial/'.$tutorial->slug)}}" style="text-decoration: none;">
              @if($tutorial->thumbnail)
            <img src="{{ $tutorial->getThumbnail('thumbnail')}}" width="310" alt="Feature image" style="border: 0; width: 100%; max-width: 310px;">
            @else
                <img src="{{ asset('img/tutorials/thumbnail.jpg') }}" width="310" alt="Feature image" style="border: 0; width: 100%; max-width: 310px;">
            @endif
          </a>
        </th>
      </tr>
    </table>
  </th>
</tr>
<tr>
  <th class="column" width="640" style="padding-left: 30px; padding-right: 30px; font-weight: 400; text-align: left;">
    <div style="color: #969AA1; font-size: 13px; margin-top: 40px;">감사합니다 <br><strong>From <a href="{{url('/channel/'.$channel->slug)}}">{{$channel->name}}</a> Team</strong></div>
  </th>
</tr>
<tr>
  <td class="spacer" height="80" style="font-size: 80px; line-height: 80px; mso-line-height-rule: exactly;">&nbsp;</td>
</tr>
</table>
<!-- /Intro Basic -->
@endsection
