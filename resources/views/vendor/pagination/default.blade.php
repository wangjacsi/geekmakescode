@if ($paginator->hasPages())
    <nav class="navigation" role="navigation">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <a class="disabled page-numbers" aria-aria-disabled="true" aria-label="@lang('pagination.previous')">
                <span class="xi-angle-left"></span>
            </a>
        @else
            <a class="page-numbers" href="{{ $paginator->previousPageUrl() }}" rel="prev" aria-label="@lang('pagination.previous')"><span class="xi-angle-left"></span></a>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <a class="disabled page-numbers" aria-disabled="true"><span>{{ $element }}</span></a>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <a class="active current page-numbers" aria-current="page"><span>{{ $page }}</span></a>
                    @else
                        <a class="page-numbers" href="{{ $url }}">{{ $page }}</a>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <a class="page-numbers" href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')"><span class="xi-angle-right"></span></a>
        @else
            <a class="page-numbers disabled" aria-disabled="true" aria-label="@lang('pagination.next')">
                <span class="xi-angle-right"></span>
            </a>
        @endif
    </nav>
@endif
