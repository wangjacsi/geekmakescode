@extends('layouts.app')

@section('styles')
    <style>
        .contact-form .with-icon i{
            color:#6987ab;
        }
    </style>
@endsection

@section('content')
<div class="container">
  <div class="row" style="margin: 75px 0 75px 0">
      <div class="col-lg-8" style="float: none; margin: 0 auto">
          <div class="crumina-module crumina-heading align-center">
              <h6 class="heading-sup-title">환영해요 ~~~</h6>
              <h2 class="h1 heading-title">{{ env('APP_NAME') }}</h2>
              <div class="heading-text">코드웨이브는 개발자들의 커뮤니티입니다. 함께 서비스를 만들어가요.
              </div>

              <form class="contact-form">
                  <div class="row" style="margin: 50px 0;">
                      <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" style="float: none; margin: 0 auto">

                        <div class="with-icon">
                            <input name="email" placeholder="User Name" id="name" type="text" required v-model="form.name">
                            <i class="fas fa-user-astronaut utouch-icon"></i>
                        </div>
                          <div class="with-icon">
                              <input name="email" placeholder="Email" id="email" type="email" required v-model="form.email">
                              <i class="fas fa-at utouch-icon "></i>
                          </div>
                          <div class="with-icon">
                              <input name="password" placeholder="Password" id=
                              "password" type="password" required v-model="form.password">
                              <i class="fas fa-key utouch-icon"></i>
                          </div>

                          <div class="with-icon">
                              <input name="password_confirmation" placeholder="Password Confirm" id=
                              "password_confirmation" type="password" required v-model="form.password_confirmation">
                              <i class="fas fa-key utouch-icon"></i>
                          </div>

                          <a @click.prevent="register" class="btn btn-border btn--with-shadow c-orange-light full-width" style="margin-bottom: 10px; ">
                              가입하기
                          </a>

                          <div class="login-links">
                            <a class="btn btn-link c-gray" href="{{ route('login') }}">로그인</a>
                            <a class="btn btn-link c-gray" href="{{ route('password.request') }}">비밀번호 찾기?</a>

                            <a class="btn btn-link c-gray" href="{{ route('auth.activate.resend') }}">이메일확인</a>
                          </div>


                          <div v-if="this.errorMessage != ''" class="summit-message" style="margin-top:50px;">
                              <p class="summit-error" v-html="this.errorMessage"></p>
                          </div>

                          <div class="or"></div>
                          <div class="">
                              <a href="{{ url('/login/github') }}" class="btn btn-social-icon btn-github"><span class="fab fa-github"></span></a>
                              <a href="{{ url('/login/bitbucket') }}" class="btn btn-social-icon btn-bitbucket"><span class="fab fa-bitbucket"></span></a>
                              <a href="{{ url('/login/twitter') }}" class="btn btn-social-icon btn-twitter"><span class="fab fa-twitter"></span></a>
                              <a href="{{ url('/login/facebook') }}" class="btn btn-social-icon btn-facebook"><span class="fab fa-facebook"></span></a>
                              <a href="{{ url('/login/linkedin') }}" class="btn btn-social-icon btn-linkedin"><span class="fab fa-linkedin"></span></a>
                              <a href="#" class="btn btn-social-icon btn-google"><span class="fab fa-google"></span></a>
                          </div>


                      </div>
                  </div>

              </form>

          </div>
      </div>
  </div>
</div>



@endsection

@section('popups')
    @component('components.sendMessagePopup')
    @endcomponent

    @component('components.searchPopup')
    @endcomponent
@endsection

@section('svgs')
  @component('components.svgIcons')
  @endcomponent
@endsection

@section('scripts-files')
    <script src="{{ asset('js/commonFcns.js') }}"></script>
  <script src="{{ asset('js/pages/register.js') }}"></script>
@endsection

@section('scripts-code')
@endsection
