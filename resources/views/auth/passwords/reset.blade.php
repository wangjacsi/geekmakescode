@extends('layouts.app')

@section('styles')
@endsection

@section('content')
<div class="container">
  <div class="row" style="margin: 75px 0 75px 0">
      <div class="col-lg-8" style="float: none; margin: 0 auto">
          <div class="crumina-module crumina-heading align-center">
              <h6 class="heading-sup-title">User Interface</h6>
              <h2 class="h1 heading-title">Heading Style-center</h2>
              <div class="heading-text">Mirum est notare quam littera gothica, quam nunc putamus parum claram,
                  anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima.
              </div>

              <form class="" method="POST" action="{{ route('password.request') }}">
                  @csrf

                  <input type="hidden" name="token" value="{{ $token }}">

                  <div class="row" style="margin: 50px 0;">
                      <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" style="float: none; margin: 0 auto">

                          <div class="with-icon">
                              <input name="email" placeholder="Email" id="email" type="email" required autofocus value="{{ $email ?? old('email') }}" >
                              <svg class="utouch-icon utouch-icon-user"><use xlink:href="#utouch-icon-user"></use></svg>
                          </div>


                          <div class="with-icon">
                              <input name="password" placeholder="Password" id=
                              "password" type="password" required>
                              <svg class="utouch-icon utouch-icon-message-closed-envelope-1"><use xlink:href="#utouch-icon-message-closed-envelope-1"></use></svg>
                          </div>

                          <div class="with-icon">
                              <input name="password_confirmation" placeholder="Password Confirm" id=
                              "password_confirmation" type="password" required >
                              <svg class="utouch-icon utouch-icon-message-closed-envelope-1"><use xlink:href="#utouch-icon-message-closed-envelope-1"></use></svg>
                          </div>

                          <button type="summit" class="btn btn-border btn--with-shadow c-orange-light full-width" style="margin-bottom: 10px; ">
                              변경하기
                          </button>

                          <div class="login-links">
                            <a class="btn btn-link c-gray" href="{{ route('register') }}">가입하기</a>
                            <a class="btn btn-link c-gray" href="{{ route('password.request') }}">비밀번호 찾기?</a>

                            <a class="btn btn-link c-gray" href="{{ route('auth.activate.resend') }}">이메일확인</a>
                          </div>

                          @if ($errors->has('email') || $errors->has('password'))
                          <div class="summit-message" style="margin-top:50px;">
                              <p class="summit-error">
                              @if ($errors->has('email'))
                              {{ $errors->first('email') }}
                              <br />
                              @endif
                              @if ($errors->has('password'))
                              {{ $errors->first('password') }}
                              @endif
                              </p>
                          </div>
                          @endif

                      </div>
                  </div>

              </form>

          </div>
      </div>
  </div>
</div>



@endsection

@section('popups')
    @component('components.sendMessagePopup')
    @endcomponent

    @component('components.searchPopup')
    @endcomponent
@endsection

@section('svgs')
  @component('components.svgIcons')
  @endcomponent
@endsection

@section('scripts-code')
@endsection
