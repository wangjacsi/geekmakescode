@extends('layouts.app')

@section('styles')
    <style>
        .contact-form .with-icon i{
            color:#6987ab;
        }
    </style>
@endsection

@section('content')
<div class="container">
  <div class="row" style="margin: 75px 0 75px 0">
      <div class="col-lg-8" style="float: none; margin: 0 auto">
          <div class="crumina-module crumina-heading align-center">
              <h6 class="heading-sup-title">안녕하세요 ~~~</h6>
              <h2 class="h1 heading-title">{{ env('APP_NAME') }}</h2>
              <div class="heading-text">새로운 강좌와 소식을 살펴보세요.
              </div>

              <form class="contact-form">
                  <div class="row" style="margin: 50px 0;">
                      <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" style="float: none; margin: 0 auto">

                          <div class="with-icon">
                              <input name="email" placeholder="Email" id="email" type="email" required v-model="loginForm.email" v-on:keyup.enter="login">
                              <i class="fas fa-at utouch-icon "></i>
                          </div>
                          <div class="with-icon">
                              <input name="password" placeholder="Password" id=
                              "password" type="password" required v-model="loginForm.password" v-on:keyup.enter="login">
                              <i class="fas fa-key utouch-icon"></i>
                          </div>

                          <div class="checkbox" style="text-align:left; padding-left:20px;">
                              <label>
                                  <input v-model="loginForm.remember" type="checkbox" name="remember" {{ Auth::viaRemember() ? 'checked' : '' }}>
                                  <span style="padding-left:10px;">{{ __('Remember Me') }}</span>
                              </label>
                          </div>

                          <a @click.prevent="login" class="btn btn-border btn--with-shadow c-orange-light full-width" style="margin-bottom: 10px; ">
                              로그인
                          </a>

                          <div class="login-links">
                            <a class="btn btn-link c-gray" href="{{ route('register') }}">가입하기</a>
                            <a class="btn btn-link c-gray" href="{{ route('password.request') }}">비밀번호 찾기?</a>

                            <a class="btn btn-link c-gray" href="{{ route('auth.activate.resend') }}">이메일확인</a>
                          </div>


                          <div v-if="this.loginError != ''" class="summit-message" style="margin-top:50px;">
                              <p class="summit-error">@{{ this.loginError }}</p>
                          </div>



                          <div class="or"></div>
                          <div class="">
                              <a href="{{ url('/login/github') }}" class="btn btn-social-icon btn-github"><span class="fab fa-github"></span></a>
                              <a href="{{ url('/login/bitbucket') }}" class="btn btn-social-icon btn-bitbucket"><span class="fab fa-bitbucket"></span></a>
                              <a href="{{ url('/login/twitter') }}" class="btn btn-social-icon btn-twitter"><span class="fab fa-twitter"></span></a>
                              <a href="{{ url('/login/facebook') }}" class="btn btn-social-icon btn-facebook"><span class="fab fa-facebook"></span></a>
                              <a href="{{ url('/login/linkedin') }}" class="btn btn-social-icon btn-linkedin"><span class="fab fa-linkedin"></span></a>
                              <a href="#" class="btn btn-social-icon btn-google"><span class="fab fa-google"></span></a>
                          </div>



                      </div>
                  </div>

              </form>

          </div>
      </div>
  </div>
</div>



@endsection

@section('popups')
    @component('components.sendMessagePopup')
    @endcomponent

    @component('components.searchPopup')
    @endcomponent
@endsection

@section('svgs')
  @component('components.svgIcons')
  @endcomponent
@endsection

@section('scripts-files')
  <script src="{{ asset('js/pages/login.js') }}"></script>
@endsection

@section('scripts-code')
@endsection
