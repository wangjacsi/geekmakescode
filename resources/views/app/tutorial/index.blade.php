@extends('layouts.app')

@section('styles')
    <style>
        #items-grid .tutorial-item{
            margin-bottom:50px;
        }
        .cat-list-bg-style .cat-list__item{
            background-color: #EAEAEA;
        }
    </style>
@endsection

@section('content')
<div class="content-wrapper">
    <section class="medium-padding120">
		<div class="container">
            <div class="row mb60">
                <div class="crumina-module crumina-module-img-bottom">
                    <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
                        <div class="crumina-module crumina-heading">
                            <h6 class="heading-sup-title">강좌리스트</h6>
                            <h2 class="heading-title">다양한 기술 강좌를 살펴보세요!</h2>
                            <div class="h6 heading-text">프로트엔드 부터 백엔드 까지 풀스택 개발을 위한 웹앱 개발, 클라우드 서비스 및 인프라 구축, 지속적인 통합 및 배포 자동화 등 다양한 기술을 배우고 또 공유해주세요. 
                            </div>
                        </div>
                    </div>

                    <!--<div class="col-lg-4 col-lg-offset-1 col-md-5 col-sm-4 col-sm-offset-0 col-xs-12">
                        <a href="02_company.html" class="btn btn--green btn--with-shadow full-width mb30">
                            Get in Touch Now!
                        </a>
                    </div>-->
                </div>
            </div>

            <div class="case-item-wrap">
                <div class="row align-center">
                    <div class="col-md-12">
                        <ul class="cat-list-bg-style sorting-menu">
    						<li class="cat-list__item" :class="{'active' : orderType == 'recent'}"><a href="" @click.prevent="orderBy('recent')" class="">최신날짜</a></li>
    						<li class="cat-list__item" :class="{'active' : orderType == 'votes'}"><a href="" @click.prevent="orderBy('votes')" class="">인 기</a></li>
    						<li class="cat-list__item" :class="{'active' : orderType == 'review'}"><a href="" @click.prevent="orderBy('review')" class="">리 뷰</a></li>
    						<li class="cat-list__item" :class="{'active' : orderType == 'registration'}"><a href="" @click.prevent="orderBy('registration')" class="">수강자</a></li>
    					</ul>
                    </div>
                </div>
				<div class="row" id="items-grid">
                    <div class="curriculum-event-wrap">
                        <tutoriallist channel-logo-default="/img/channels/logo_image.png"  thumbnail-default="/img/tutorials/thumbnail.jpg" :view-options-prop="tutorialsOptions" comp-name='searchtutorials' :url-path="getSearchTutorialUrl" background-colors-prop="{{ json_encode($backgroundColors) }}" progress-status-prop="{{ json_encode($progressStatus) }}"></tutoriallist>
                    </div>
                </div>
            </div>
        </div>
    </section>

</div>



@endsection

@section('popups')
    @component('components.sendMessagePopup')
    @endcomponent

    @component('components.searchPopup')
    @endcomponent
@endsection

@section('svgs')
  @component('components.svgIcons')
  @endcomponent
@endsection

@section('scripts-files')
@endsection

@section('scripts-code')
    <script>

    window.onload = function () {
    }
    </script>

    <script src="{{ asset('js/pages/tutorial/index.js') }}"></script>
@endsection
