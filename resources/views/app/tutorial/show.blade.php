@extends('layouts.app')


@section('styles')
    <link rel="stylesheet" href="{{ asset("plugins/devicon/devicon.min.css") }}">
    <style>
    .course-details .w-tags .tags-list{
        margin-left: 0px;
        margin-bottom: 0px;
    }
    .course-details .w-tags a{
        padding: 10px 14px;
        text-transform: none;
        border: 1px solid #d6dfeb;
    }
    .course-details .w-tags .tags-list li{
        float: none;
        display: inline-block;
        vertical-align: middle;
        line-height: 1;
        margin-left:0;
        margin-right:5px;
    }
    </style>
@endsection

@section('meta_tags')
    @php
    $meta = [
        'title' => $tutorial->title,
        //'description' => $tutorial->description,
        'image' => $tutorial->getThumbnail('thumbnail'),
        'tags' => ''
    ];
    @endphp
    @isset($tags)
        @php
        $meta['tags'] = implode(',', $tags);
        @endphp
    @endisset
@endsection


@section('content')
<div class="content-wrapper">

	<!-- Stunning Header -->
	<div class="crumina-stunning-header stunning-header--content-center stunning-header--bg-photo stunning-header--min640 custom-color c-white fill-white" style="background-image:url({{ $tutorial->getThumbnail('head_image') }});">
		<div class="container">
			<div class="stunning-header-content">
				<h6 class="category-link c-lime-light">{{ App\Http\Helper::transTutorialStatusToKorean($tutorial->progress_status) }}</h6>
				<h2 class="h1 stunning-header-title">{{$tutorial->title}}</h2>
				<div class="inline-items">
					<div class="author-block inline-items">
						<div class="author-avatar">
							<img src="{{ $channel->getThumbnail('logo_image') }}" alt="author">
						</div>
						<div class="author-info">
							<div class="author-prof">Channel</div>
							<h6 class="author-name"><a href="/channel/{{$channel->slug}}">{{$channel->name }}</a></h6>
						</div>
					</div>
					<div class="icon-text-item inline-items">
						<div class="text"><i class="fas fa-calendar-alt"></i> Last Update:  @{{ moment(tutorial.updated_at).format("MMM YYYY") }}</div>
					</div>


					<a v-if="!isRegistered && !owner && tutorial.visibility == 'public' && tutorial.progress_status != 'prepare' && tutorial.progress_status != 'stop'" @click.prevent="registTutorial" href="" style="font-size:16px; width:200px;" class="btn btn--large btn--orange-light btn--with-shadow f-right">
						수강하기
					</a>

				</div>
			</div>
		</div>

		<div class="overlay-standard overlay--dark"></div>
	</div>
	<!-- ... end Stunning Header -->


	<!-- Course Details -->
	<section class="negative-margin-top80">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="course-details">
						<ul class="course-details-control" role="tablist">

							<li role="presentation" class="tab-control active">
								<a href="#description" role="tab" data-toggle="tab" class="control-item">강좌소개</a>
							</li>

							<li role="presentation" class="tab-control">
								<a href="#curriculum" role="tab" data-toggle="tab" class="control-item">커리큘럼</a>
							</li>

							<li role="presentation" class="tab-control">
								<a href="#instructors" role="tab" data-toggle="tab" class="control-item">강사</a>
							</li>

							<li v-if="tutorial.allow_reviews" role="presentation" class="tab-control">
								<a href="#reviews" role="tab" data-toggle="tab" class="control-item">리뷰 @{{totalReviews ? '('+totalReviews +')' : ''}}</a>
							</li>

                            <li class="tab-control" style="float:right;">
								<a @click.prevent="voteToTutorial(!voted)" href="" class="control-item" :title="voted ? '좋아요 취소': '좋아요'" v-bind:class="{'voted' : voted}" style="display:inline-block;"><i class="fas fa-thumbs-up"></i> @{{upVotesTotal}}</a>

                                <a @click.prevent="interestModal(!interested)" href="" class="control-item" title="관심 강좌 설정" v-bind:class="{'interested' : interested}" style="display:inline-block;"><i class="fas fa-folder-open"></i> @{{ interested ? '취소' : '관심' }}</a>
							</li>

						</ul>

						<div class="tab-content">
							<div role="tabpanel" class="tab-pane fade in active" id="description">
								<div class="row">
									<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
										<h3>{{$tutorial->title}}</h3>
										{!! $tutorial->description !!}
									</div>
									<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
										<div class="course-features">
											<h5 class="title">Course Features</h5>
											<ul class="course-features-list">
												<li>
													<div class="feature-item">
														<svg class="utouch-icon utouch-icon-copy"><use xlink:href="#utouch-icon-copy"></use></svg>
														Lectures
													</div>
													<div class="value">{{$totalVideos}}</div>
												</li>
												<li>
													<div class="feature-item">
														<svg class="utouch-icon utouch-icon-clock-1"><use xlink:href="#utouch-icon-clock-1"></use></svg>
														Play Time
													</div>
                                                    @if($totalPlayTime == '')
													<div class="value">0</div>
                                                    @else
                                                        <div class="value">{{$totalPlayTime}}</div>
                                                    @endif
												</li>
												<li>
													<div class="feature-item">
														<svg class="utouch-icon utouch-icon-upward-arrow"><use xlink:href="#utouch-icon-upward-arrow"></use></svg>
														Skill level
													</div>
													<div class="value">{{ucfirst($tutorial->skill)}}</div>
												</li>
												<li>
													<div class="feature-item">
														<svg class="utouch-icon utouch-icon-users"><use xlink:href="#utouch-icon-users"></use></svg>
														Listeners
													</div>
													<div class="value">{{$totalRegistrations}}</div>
												</li>
                                                <li v-if="tutorial.allow_reviews">
													<div class="feature-item">
														<i class="fas fa-star utouch-icon" style="color:#0083ff;"></i>
														Review
													</div>
													<div class="value">@{{ reviewAvg }}</div>
												</li>
                                                <li v-if="tutorial.allow_votes">
													<div class="feature-item">
														<i class="far fa-thumbs-up utouch-icon" style="color:#0083ff;"></i>
														Likes
													</div>
													<div class="value">@{{ upVotesTotal }}</div>
												</li>
                                                <li>
													<div class="feature-item">
														<svg class="utouch-icon utouch-icon-worlwide"><use xlink:href="#utouch-icon-worlwide"></use></svg>
														Open
													</div>
													<div class="value">@{{ moment(tutorial.created_at).format("MMM YYYY") }}</div>
												</li>


											</ul>

                                            @if($owner)
                                            <a style="margin-top:20px;" href="/tutorial/{{$tutorial->slug}}/edit" class="btn btn-small btn--grey full-width btn--with-shadow">
                  								수정하기
                  							</a>
                                            @endif
										</div>
									</div>
								</div>
							</div>
							<div role="tabpanel" class="tab-pane fade" id="curriculum">
								<h3>커리큘럼</h3>
								<p>
								</p>
								<curriculumlist :records-prop="{{ $records }}" :curriculum-prop="videos" :view-options-prop="curriculumViewOptions" v-bind:access-allow-prop="{{$owner || $isRegistered ? 'true' : 'false' }}"></curriculumlist>
							</div>
							<div role="tabpanel" class="tab-pane fade" id="instructors">
								<h3>강좌 운영진</h3>
								<userlist :view-options-prop="userListViewOptions" user-photo-default="/img/profile/default.png" from-comp='userlist' url-path='/getChannelAdminUsers/{{ $channel->slug }}'></userlist>
							</div>
							<div v-if="tutorial.allow_reviews" role="tabpanel" class="tab-pane fade" id="reviews">
								<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
									<h3>리뷰</h3>
									<p class="weight-bold">여러 사용자의 리뷰를 살펴보세요. 강좌를 들으신 후에는 리뷰를 남겨주세요.
									</p>

									<reviews url-path="/tutorial/{{$tutorial->slug}}/getReviewsByTutorial"></reviews>

                                    <div v-if="isRegistered && !owner" class="">
                                        <reviewform url-path="/tutorial/{{$tutorial->slug}}/storeReview"></reviewform>
                                    </div>


								</div>
								<div class="col-lg-3 col-lg-offset-1 col-md-4 col-sm-12 col-xs-12">
									<div class="raiting-details">
										<h2 class="overall-rating c-yellow">@{{reviewAvg}}</h2>
										<h6 class="title">Average Rating</h6>
										<ul class="rate-stars" v-html="starRate(reviewAvg)">
										</ul>
										<p class="text">@{{reviewAvg}} average based on @{{totalReviews}} ratings.</p>

										<div class="skills">
											<div class="skills-item">
												<div class="skills-item-info">
													<span class="skills-item-title">5 Stars</span>
													<span class="skills-item-count"><span class="count-animate" data-speed="1000" data-refresh-interval="50" :data-to="reviewGroup[4]" data-from="0"></span></span>
												</div>
												<div class="skills-item-meter">
													<span class="skills-item-meter-active bg-yellow" :style="{width: reviewGroup[4] / totalReviews * 100 + '%'}"></span>
												</div>
											</div>

											<div class="skills-item">
												<div class="skills-item-info">
													<span class="skills-item-title">4 Stars</span>
													<span class="skills-item-count"><span class="count-animate" data-speed="1000" data-refresh-interval="50" :data-to="reviewGroup[3]" data-from="0"></span></span>
												</div>

												<div class="skills-item-meter">
													<span class="skills-item-meter-active bg-yellow" :style="{width: reviewGroup[3] / totalReviews * 100 + '%'}"></span>
												</div>
											</div>

											<div class="skills-item">
												<div class="skills-item-info">
													<span class="skills-item-title">3 Stars</span>
													<span class="skills-item-count"><span class="count-animate" data-speed="1000" data-refresh-interval="50" :data-to="reviewGroup[2]" data-from="0"></span></span>
												</div>

												<div class="skills-item-meter">
													<span class="skills-item-meter-active bg-yellow" :style="{width: reviewGroup[2] / totalReviews * 100 + '%'}"></span>
												</div>
											</div>

											<div class="skills-item">
												<div class="skills-item-info">
													<span class="skills-item-title">2 Stars</span>
													<span class="skills-item-count"><span class="count-animate" data-speed="1000" data-refresh-interval="50" :data-to="reviewGroup[1]" data-from="0"></span></span>
												</div>

												<div class="skills-item-meter">
													<span class="skills-item-meter-active bg-yellow" :style="{width: reviewGroup[1] / totalReviews * 100 + '%'}"></span>
												</div>
											</div>

											<div class="skills-item">
												<div class="skills-item-info">
													<span class="skills-item-title">1 Star</span>
													<span class="skills-item-count"><span class="count-animate" data-speed="1000" data-refresh-interval="50" :data-to="reviewGroup[0]" data-from="0"></span></span>											</div>

												<div class="skills-item-meter">
													<span class="skills-item-meter-active bg-yellow" :style="{width: reviewGroup[0] / totalReviews * 100 + '%'}"></span>
												</div>
											</div>
										</div>

                                        <div v-if="reviewed" class="myReview" style="margin-top:50px;">
                                            <h6 class="title">My Review</h6>
    										<ul class="rate-stars" v-html="starRate(myReview.review / 2)">
    										</ul>
    										<p style="text-align:left; margin-top:20px; max-height:150px; overflow:overlay;" class="text" v-html="$sanitize(myReview.content)"></p>

                                            <a @click.prevent="editMyReview" style="margin-bottom:10px;" href="" class="btn btn-small btn--primary btn--with-shadow full-width">
                                                수 정
                                            </a>
                                            <div class=""></div>
                                            <a @click.prevent="deleteMyReviewConfirm" href="" class="btn btn-small btn--secondary btn--with-shadow full-width">
                                                삭 제
                                            </a>
                                        </div>

									</div>
								</div>

							</div>


                            <div class="post-details-shared">

                                <div class="widget w-follow">
                                    <ul class="socials socials-share socials--round">
                                        <li>Share:</li>
                                        <li>
                                            <a class="social__item ttt" style="background-color: #1da1f2;" data-sharer="twitter" data-title="{{$tutorial->title}}" data-hashtags="codewwwave,코드웨이브," data-url="https://codewwwave.test/tutorial/{{$tutorial->slug}}" data-via="@codewwwave">
                                                <span class="utouch-icon fab fa-twitter"></span>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="social__item ttt" style="background-color: #3b5998;" data-sharer="facebook" data-url="https://codewwwave.test/tutorial/{{$tutorial->slug}}">
                                                <span class="utouch-icon fab fa-facebook-f"></span>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="social__item ttt" style="background-color: #dd4b39;" data-sharer="googleplus" data-url="https://codewwwave.test/tutorial/{{$tutorial->slug}}">
                                                <span class="utouch-icon fab fa-google-plus-g"></span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>

                                @if(count($tags))
                                <div class="widget w-tags">
                                    <ul class="tags-list">
                                        <li>Tags:</li>
                                        @foreach($tags as $tag)
                                            <li>
                                                <a>{{$tag}}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
                            </div>
    					</div>
					</div>

				</div>
			</div>
		</div>
	</section>
	<!-- ... end Course Details -->

	<!-- Curriculum Events -->
	<section class="crumina-module crumina-module-slider medium-padding100">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<h3>추천 강좌</h3>

                    @include('components.recommendTutorials')
				</div>
			</div>
		</div>
	</section>
	<!-- ... end Curriculum Events -->

</div>

<confirm modal-id='deleteMyReview' :content-prop="deleteMyReviewContent" event-emit-name='deleteMyReview' event-on-name='deleteMyReviewConfirm'></confirm>

<interest :categories-prop="userInterestCategories" event-emit-name='interestTutorial' event-on-name='interestTutorialModalOn'></interest>
@endsection


@section('popups')
    @component('components.alarm', ['title'=>'', 'content'=>''])
    @endcomponent

    @component('components.sendMessagePopup')
    @endcomponent

    @component('components.searchPopup')
    @endcomponent
@endsection

@section('svgs')
  @component('components.svgIcons')
  @endcomponent
@endsection


@section('scripts-code')
<script type="text/javascript" src="{{ asset('js/jquery.countdown.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('plugins/sharerjs/sharer.min.js')}}"></script>

<script>
var videos = {!! $videos !!};
var tutorial = {!! $tutorial !!};
var owner = {!! $owner ? 'true' : 'false' !!};
var reviewGroup = {!! $reviewGroup !!};
var upVotesTotal = {!! $upVotesTotal !!};
var voted = {!! $voted ? 'true' : 'false' !!};
var interested = {!! $interested ? 'true' : 'false' !!};
@if($userInterestCategories)
var userInterestCategories = {!! $userInterestCategories !!};
@else
var userInterestCategories = null;
@endif
var reviewAvg = {!! $reviewAvg !!};
var reviewed = {!! $reviewed ? 'true' : 'false' !!};
var isRegistered = {!! $isRegistered ? 'true' : 'false' !!};
@if($reviewed)
var myReview = {!! $myReview !!};
@else
var myReview = null;
@endif
var recommendTutorials = {!! $suggestions !!};
var backgroundColors = {!! json_encode($backgroundColors) !!};
var progressStatus = {!! json_encode($progressStatus) !!};

$(document).ready( function() {
    window.Sharer.init();
});
</script>

<script src="{{ asset('js/pages/tutorial/show.js') }}"></script>
@endsection
