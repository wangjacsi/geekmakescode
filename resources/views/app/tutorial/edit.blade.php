@extends('layouts.app')


@section('styles')
    <link rel="stylesheet" href="{{ asset("plugins/cropperjs/cropper.min.css") }}">

    <!-- Froala Editor -->
    <link rel="stylesheet" href="{{ asset('plugins/codemirror.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/froala_editor_2.8.4/css/froala_editor.pkgd.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/froala_editor_2.8.4/css/froala_style.min.css') }}">

    <!-- Vue-multiselect -->
    <link rel="stylesheet" href="{{ asset('plugins/vue-multiselect/vue-multiselect.min.css') }}">

    <style>

    </style>
@endsection

@section('content')
<div class="content-wrapper">

	<!-- Stunning Header -->
	<div class="crumina-stunning-header stunning-header--content-center stunning-header--bg-photo stunning-header--min640 custom-color c-white fill-white" style="background-image:url({{ $tutorial->getThumbnail('head_image') }});">
		<div class="container">
			<div class="stunning-header-content">
				<h6 class="category-link c-lime-light">{{ App\Http\Helper::transTutorialStatusToKorean($tutorial->progress_status) }}</h6>
				<h2 class="h1 stunning-header-title">@{{tutorial.title}}</h2>
				<div class="inline-items">
					<div class="author-block inline-items">
						<div class="author-avatar">
							<img :src="channel.logo_image ? channel.logo_image : '/img/channels/logo_image.png'" alt="author">
						</div>
						<div class="author-info">
							<div class="author-prof">Channel</div>
							<h6 class="author-name">@{{channel.name }}</h6>
						</div>
					</div>
					<div class="icon-text-item inline-items">
						<div class="text"><i class="fas fa-calendar-alt"></i> Last Update:  @{{ moment(tutorial.updated_at).format("MMM YYYY") }}</div>
					</div>

				</div>
			</div>
		</div>

		<div class="overlay-standard overlay--dark"></div>
	</div>
	<!-- ... end Stunning Header -->


	<!-- Course Details -->
	<section class="negative-margin-top80">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="course-details">
						<ul class="course-details-control" role="tablist">

							<li role="presentation" class="tab-control ">
								<a href="#description" role="tab" data-toggle="tab" class="control-item">강좌정보</a>
							</li>

                            <li role="presentation" class="tab-control active">
								<a href="#image" role="tab" data-toggle="tab" class="control-item">강좌이미지</a>
							</li>

							<li role="presentation" class="tab-control">
								<a href="#curriculum" role="tab" data-toggle="tab" class="control-item">커리큘럼</a>
							</li>

							<li role="presentation" class="tab-control">
								<a href="#settings" role="tab" data-toggle="tab" class="control-item">강좌설정</a>
							</li>

						</ul>

						<div class="tab-content">
							<div role="tabpanel" class="tab-pane fade " id="description">
                                <tutorialinfo></tutorialinfo>
							</div>
                            <div role="tabpanel" class="tab-pane fade in active" id="image">
                                <tutorialimage></tutorialimage>
                            </div>

							<div role="tabpanel" class="tab-pane fade" id="curriculum">
								<h3>커리큘럼</h3>
                                <div class="" style="margin-bottom:50px;">
                                    <curriculumlist :curriculum-prop="videos" :view-options-prop="curriculumViewOptions" v-bind:access-allow-prop="true"></curriculumlist>
                                </div>
							</div>
							<div role="tabpanel" class="tab-pane fade" id="settings">
                                <tutorialsetting></tutorialsetting>
							</div>
    					</div>
					</div>

				</div>
			</div>
		</div>
	</section>
	<!-- ... end Course Details -->
</div>

<confirm modal-id='deleteVideo' :content-prop="deleteVideoContent" event-emit-name='deleteVideo' event-on-name='deleteVideoConfirm'></confirm>

@endsection


@section('popups')
    @component('components.alarm', ['title'=>'', 'content'=>''])
    @endcomponent

    @component('components.sendMessagePopup')
    @endcomponent

    @component('components.searchPopup')
    @endcomponent
@endsection

@section('svgs')
  @component('components.svgIcons')
  @endcomponent
@endsection


@section('scripts-code')

<script>
var videos = {!! $videos !!};
var tutorial = {!! $tutorial !!};
var channel = {!! $channel !!};
var channels = {!! $channels !!};
var skills = {!! json_encode($skills) !!};
var tags = {!! json_encode($tags) !!};
var visibilities = {!! json_encode($visibility) !!};
var progressStatus = {!! json_encode($progressStatus) !!};

$(document).ready( function() {
});
</script>

<script src="{{ asset('plugins/cropperjs/cropper.min.js')}}"></script>

<!-- Froala Editor -->
<!-- Include external JS libs. -->
<script src="{{ asset('plugins/codemirror.min.js') }}"></script>
<script src="{{ asset('plugins/xml.min.js') }}"></script>

<!-- Include Editor JS files. -->
<script src="{{ asset('plugins/froala_editor_2.8.4/js/froala_editor.pkgd.min.js') }}"></script>
<script src="{{ asset('plugins/froala_editor_2.8.4/js/languages/ko.js') }}"></script>

<script src="{{ asset('js/pages/tutorial/edit.js') }}"></script>
@endsection
