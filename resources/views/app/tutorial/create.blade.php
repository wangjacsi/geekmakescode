@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{ asset("plugins/cropperjs/cropper.min.css") }}">

    <!-- Froala Editor -->
    <link rel="stylesheet" href="{{ asset('plugins/codemirror.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/froala_editor_2.8.4/css/froala_editor.pkgd.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/froala_editor_2.8.4/css/froala_style.min.css') }}">

    <!-- Vue-multiselect -->
    <link rel="stylesheet" href="{{ asset('plugins/vue-multiselect/vue-multiselect.min.css') }}">
@endsection

@section('content')
<div class="crumina-stunning-header stunning-header--breadcrumbs-bottom-left stunning-bg-clouds stunning-header--content-inline " style="margin-bottom: 100px">
    <div class="container">
        <div class="stunning-header-content">
            <div class="inline-items">
                <h4 class="stunning-header-title">강좌 만들기</h4>
                 <!--<a href="15_pricing_tables.html" class="btn btn--green btn--with-shadow f-right">
                     Get trial version
                 </a>-->
            </div>
            <div class="breadcrumbs-wrap inline-items">
                <a href="#" class="btn btn--primary btn--round">
                    <svg class="utouch-icon utouch-icon-home-icon-silhouette"><use xlink:href="#utouch-icon-home-icon-silhouette"></use></svg>
                </a>

                <ul class="breadcrumbs breadcrumbs--rounded">
                    <li class="breadcrumbs-item">
                        <a href="08_events.html">Events</a>
                        <svg class="utouch-icon utouch-icon-media-play-symbol"><use xlink:href="#utouch-icon-media-play-symbol"></use></svg>
                    </li>
                    <li class="breadcrumbs-item active">
                        <span>Presentation of the Utouch app</span>
                        <svg class="utouch-icon utouch-icon-media-play-symbol"><use xlink:href="#utouch-icon-media-play-symbol"></use></svg>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="container">
  <div class="row" style="margin: 0 0 75px 0">
      <div class="col-lg-8" style="float: none; margin: 0 auto">
          <div class="crumina-module crumina-heading">
              <h5 class="heading-sup-title">채널에 강좌를 추가해보세요. 좋은 강좌를 통해 서로 협력할 수 있어요.</h5>
              <hr />

              <form class="channel-form">
                  <div class="form-group">
                    <label for="title">강좌 이름</label>
                    <input type="text" class="" placeholder="강좌 이름" v-model="form.title" >
                    <input type="text" style="display: none;" hidden />
                  </div>


                  <div class="form-group">
                      <label for="channel">채널 선택</label>
                      <p>강좌가 속하는 채널을 선택해 주세요.</p>
                      <select id="channel" class="" name="channel" >
                          <option value="">선택해주세요</option>
                          <option v-bind:value="channel.id" v-for="(channel, index) in channels" :key="channel.id">@{{channel.name}}</option>
                      </select>
                  </div>


                  <div class="form-group">
                      <label for="headImage">강좌 상단 이미지</label>
                      <div class="img-container">
                            <img src="/img/tutorials/head_image.jpg" class="crop-image">
                      </div>
                      <div class="btn-group image-button-group">
                          <a @click.prevent="imageCancel(0)" class="btn btn-small btn--orange-light" style="margin-bottom: 10px; ">이미지 취소
                          </a>
                          <label title="Upload image file" for="headImage" class="btn btn-small btn--primary">
                              <input type="file" accept="image/*" name="headImage" id="headImage" class="hide input-image-file">
                              이미지 선택
                          </label>
                      </div>
                      <p class="help-block">옵션사항입니다. 미지정 시 기본 이미지가 보여집니다.</p>
                  </div>


                  <div class="form-group">
                    <label for="thumbnailImage">강좌 대표 이미지</label>
                    <div class="img-container">
                        <img src="/img/tutorials/thumbnail.jpg" class="crop-image">
                    </div>
                    <div class="btn-group image-button-group">
                        <a @click.prevent="imageCancel(1)" class="btn btn-small btn--orange-light" style="margin-bottom: 10px; ">이미지 취소
                        </a>
                        <label title="Upload image file" for="thumbnailImage" class="btn btn-small btn--primary">
                            <input type="file" accept="image/*" name="thumbnailImage" id="thumbnailImage" class="hide input-image-file">
                            이미지 선택
                        </label>
                    </div>
                    <p class="help-block">옵션사항입니다. 미지정 시 기본 이미지가 보여집니다.</p>
                </div>

                <div class="form-group">
                    <label for="skill">강좌 레벨 선택</label>
                    <p>원리와 언어 등 기초적인 강좌의 경우 Beginner, 기본을 넘어 다양한 기술적 설명이 있는 강좌는 Intermediate, 프로젝트 레벨이나 전문가 수준의 강좌는 Expert, 기초부터 핵심까지 모든 레벨을 포함한다면 All Levels를 선택해주세요</p>
                    <select class="" id="skill" name="skill" >
                        <option value="">선택해주세요</option>
                        <option v-bind:value="skill" v-for="(skill, index) in skills" :key="index">@{{skill | capitalize}}</option>
                    </select>
                </div>

                  <div class="form-group">
                      <label for="description">강좌 소개</label>
                      <froala :tag="'textarea'" :config="froalaConfig" v-model="form.description"></froala>
                  </div>

                  <div class="form-group">
                      <label for="description">태그</label>
                      <p style="margin-bottom:5px;">특수 문자는 제외됩니다 &#40;&#38;&#91;&#92;&#93;
&#94;&#47;&#35;&#44;&#43;&#40;&#41;&#36;&#126;&#37;&#39;&#34;&#58;&#59;&#42;&#63;&#33;&#38;&#60;&#61;&#62;&#123;&#124;&#125;&#41;</p>
                      <multiselect :multiple="true" :searchable="true" :loading="isLoading" :internal-search="false" :clear-on-select="false" :close-on-select="false" :options-limit="50" :limit="20" :limit-text="limitText" @search-change="asyncFind" id="ajax" v-model="tag" :options="tags" :taggable="true" @tag="addTag" tag-placeholder="새로운 태그를 추가하세요" :max="maxTags" placeholder="태그를 검색하세요">
                          <template slot="maxElements">
                              최대 20개까지 태그를 입력하실 수 있습니다.
                          </template>
                      </multiselect>
                </div>


                  <div class="form-group">
                      <label for="visibility">강좌 공개 선택</label>
                      <p>강좌의 공개를 원하면 public으로 비공개 설정 시 private으로 선택해주세요. 강좌의 세부 내용 및 개별 비디오를 모두 업로드 후 강좌를 오픈 하고 싶을 때는 비공개로 설정 후 전체 완료후에 공개로 전환해주세요. 연재 형태의 규칙적인 일정에 따라 강좌를 업로드 하실 때는 공개로 설정하시기 바랍니다. <span style="font-weight:bold; color:red;">공개 시 채널을 구독중인 유저들에게 알림 메일이 전송됩니다.</span></p>
                      <select class="" id="visibility" name="visibility" >
                          <option value="">선택해주세요</option>
                          <option v-bind:value="visibility" v-for="(visibility, index) in visibilities" :key="index">@{{visibility}}</option>
                      </select>
                  </div>

                  <div class="form-group">
                      <label for="cost_type">강좌 가격 정책 선택</label>
                      <p>강좌 가격 정책은 현재 계획중에 있습니다. 추후 업데이트 될 예정입니다.</p>
                      <!--<p>가격 타입은 무료 강좌, 전체 강좌, 강좌 세부 영상별 비용 세가지가 있습니다.</p>
                      <ul style="margin-bottom:20px;">
                          <li>무료 강좌: 누구나 들을 수 있습니다</li>
                          <li>전체 강좌: 강좌 전체를 하나의 비용으로 처리합니다</li>
                          <li>세부 영상별: 세부 영상별 비용을 따로 구분하여 영상별 접근이 가능합니다. 단, 영상별 비용은 동일합니다.</li>
                      </ul>
                      <select class="" id="cost_type" name="cost_type" >
                          <option value="">선택해주세요</option>
                          <option v-bind:value="costType" v-for="(costType, index) in costTypes" :key="index">@{{costType}}</option>
                      </select>-->
                  </div>

                  <!--
                  <div class="form-group" v-if="form.cost_type != 'free' && form.cost_type != ''">
                        <label for="cost">강좌 가격 (단위: 원)</label>
                        <div class="" v-if="costSelect=='self'">
                            <input type="number" class="" placeholder="1000 원" v-model.number="costSelf" :disabled="costSelf==0 ? true : false">
                            <a @click.prevent="costChangeToSel" class="btn btn--green-light btn--with-shadow" style="margin-bottom: 10px; ">
                                옵션 고르기
                            </a>
                        </div>

                        <select v-else class="" id="costSelect"  v-on:change="costSelectChange">
                            <option value="">선택해주세요</option>
                            <option value="self">직접 쓰기</option>
                            <option v-bind:value="cost" v-for="(cost, index) in costSelectOptions" :key="index">@{{cost}} 원</option>
                        </select>
                  </div>
                    -->
                    <div class="form-group">
                        <label for="cost_type">강좌 리뷰</label>
                        <p>구독자들이 강좌 리뷰를 남길 수 있습니다</p>
                        <select class="" id="allow_reviews" name="allow_reviews" >
                            <option value="">선택해주세요</option>
                            <option value="1" selected>사용</option>
                            <option value="0">미사용</option>
                        </select>
                    </div>

                  <div class="form-group">
                      <label for="cost_type">강좌 좋아요</label>
                      <p>구독자들이 강좌 좋아요를 남길 수 있습니다</p>
                      <select class="" id="allow_votes" name="allow_votes" >
                          <option value="">선택해주세요</option>
                          <option value="1" selected>사용</option>
                          <option value="0">미사용</option>
                      </select>
                  </div>


                  <div class="form-group">
                      <label for="cost_type">강좌 댓글</label>
                      <p>구독자들이 댓글을 남길 수 있습니다</p>
                      <select class="" id="allow_comments" name="allow_comments" >
                          <option value="">선택해주세요</option>
                          <option value="1" selected>사용</option>
                          <option value="0">미사용</option>
                      </select>
                  </div>


                  <a @click.prevent="upload" class="btn btn-border btn--with-shadow c-orange-light full-width" style="margin-bottom: 10px; ">
                      생성하기
                  </a>


                  <div v-if="this.errorMessage != ''" class="summit-message" style="margin-top:50px;">
                      <p class="summit-error" v-html="this.errorMessage"></p>
                  </div>

              </form>

          </div>
      </div>
  </div>
</div>
@endsection

@section('popups')
    @component('components.alarm', ['title'=>'', 'content'=>''])
    @endcomponent

    @component('components.sendMessagePopup')
    @endcomponent

    @component('components.searchPopup')
    @endcomponent
@endsection

@section('svgs')
  @component('components.svgIcons')
  @endcomponent
@endsection


@section('scripts-code')
    <script>
        var channels = {!! $channels !!};
        var skills = {!! json_encode($skills) !!};
        var visibilities = {!! json_encode($visibility) !!};
        var costTypes = {!! json_encode($costType) !!};
    </script>
    <script src="{{ asset('plugins/cropperjs/cropper.min.js')}}"></script>

    <!-- Froala Editor -->
    <!-- Include external JS libs. -->
    <script src="{{ asset('plugins/codemirror.min.js') }}"></script>
    <script src="{{ asset('plugins/xml.min.js') }}"></script>

    <!-- Include Editor JS files. -->
    <script src="{{ asset('plugins/froala_editor_2.8.4/js/froala_editor.pkgd.min.js') }}"></script>
    <script src="{{ asset('plugins/froala_editor_2.8.4/js/languages/ko.js') }}"></script>
    <script src="{{ asset('js/pages/tutorial/create.js') }}"></script>
@endsection
