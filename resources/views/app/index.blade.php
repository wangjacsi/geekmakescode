@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset("plugins/devicon/devicon.min.css") }}">
<style>

.crumina-clients{
    padding: 70px 0;
}
.clients-item .clients-images{
    font-size: 90px;
    color:#fff;
}
.main-slider .swiper-wrapper .swiper-slide {
    height:820px;
}

@media (max-width: 1920px) {
  .main-slider .swiper-wrapper, .main-slider .swiper-slide {
	height:820px !important;
  }
}

@media (max-width: 1200px) {
  .main-slider .swiper-wrapper,.main-slider .swiper-slide {
	height:720px !important;
  }
}

@media (max-width: 992px) {
    .main-slider .swiper-wrapper,.main-slider .swiper-slide {
  height:520px !important;
    }
}

@media (max-width: 768px) {
    .main-slider .swiper-wrapper,.main-slider .swiper-slide {
  	height:320px !important;
    }
}

</style>
@endsection

@section('content')
  <div class="content-wrapper">

    <!-- Main Slider -->

    <div class="crumina-module crumina-module-slider container-full-width">
        <div class="swiper-container main-slider navigation-center-both-sides" data-effect="fade">

          <!-- Additional required wrapper -->
          <div class="swiper-wrapper">
            <!-- Slides -->
            <div class="swiper-slide main-slider-bg-light" style="background-image:url('/img/contents/test/image17.jpg');">

              <div class="container">

                <div class="row">

                <!--  <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-sm-offset-0 col-xs-12">-->
                <div class="col-lg-12 col-md-12 col-sm-12 col-sm-offset-0 col-xs-12">

                    <div class="slider-content align-center">

                      <h1 class="slider-content-title with-decoration" data-swiper-parallax="-100">
                        Show Me The Code !! 플리즈 ~

                        <!--<svg class="first-decoration utouch-icon utouch-icon-arrow-left"><use xlink:href="#utouch-icon-arrow-left"></use></svg>

                        <svg class="second-decoration utouch-icon utouch-icon-arrow-left"><use xlink:href="#utouch-icon-arrow-left"></use></svg>-->

                      </h1>
                      <h6 class="slider-content-text" data-swiper-parallax="-200">시작이 어려운 코딩 그러나 쉽게 할 수 있어요~ 코드웨이브를 통해 지식을 공유하고 함께 성장해나가요~!!
                      </h6>

                      <div class="main-slider-btn-wrap" data-swiper-parallax="-300">

                        <a href="#" class="btn btn--yellow btn--with-shadow">
                          코드웨이브는 ?
                        </a>

                        <a href="{{ url('/register') }}" class="btn btn-border btn--with-shadow c-primary">
                          지금 바로 시작하기
                        </a>
                      </div>
                    </div>
                  </div>
                  <!--<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="slider-thumb" data-swiper-parallax="-400" data-swiper-parallax-duration="600">
                      <img src="/img/slides1.png" alt="slider">
                    </div>
                </div>-->

                </div>
              </div>
            </div>

            <div class="swiper-slide main-slider-bg-light" style="background-image:url('/img/contents/test/image34.jpg')">

              <div class="container table">
                <div class="row table-cell">

                  <div class="col-lg-6 col-lg-offset-6 col-md-12 col-sm-12 col-xs-12">
                    <div class="slider-content align-both ">
                      <h2 class="slider-content-title c-white Glow" data-swiper-parallax="-100">
                        <span class="">라라벨 프레임워크 시작부터 프로젝트 완료까지 모든것</span>
                      </h2>
                      <h6 class="slider-content-text c-white Glow" data-swiper-parallax="-200">라라벨은 강력한 기능과 확장성을 가진 우아한 프레임워크입니다. 프로젝트가 빠른 개발을 필요로하나요? 프로젝트가 스케일이 큰 규모인가요? 견고함이 필요한가요? 모든 요구사항을 쉽고 빠르게 해결할 수 있는 모던 프레임워크 라라벨을 시작해보세요.    
                      </h6>

                      <div class="main-slider-btn-wrap" data-swiper-parallax="-300">

                        <a href="#"   class="btn btn-market btn--with-shadow">
                            <i style="font-size: 26px;" class="utouch-icon fab fa-laravel"></i>
                          <div style="padding-left: 10px;" class="text">
                            <span class="sup-title">강좌 보기</span>
                            <span class="title">라라벨 프레임워크 따라하기</span>
                          </div>
                        </a>

                        <!--<a href="#"   class="btn btn-market btn--with-shadow">
                          <img class="utouch-icon" src="/svg-icons/google-play.svg" alt="google">
                          <div class="text">
                            <span class="sup-title">Download on the</span>
                            <span class="title">Google Play</span>
                          </div>
                      </a>-->

                    </div>

                    </div>
                  </div>

                </div>
              </div>
            </div>
            <div class="swiper-slide thumb-left main-slider-bg-light" style="background-image:url('/img/contents/test/image4.jpg')">

              <div class="container table full-height">
                <div class="row table-cell">
                  <div class="col-lg-6 col-sm-12 table-cell">

                    <div class="slider-content align-both">

                      <h2 class="slider-content-title c-white Glow" data-swiper-parallax="-100">현재 가장 핫한 프론트 기술은 ~ VueJS !!</h2>

                      <h6 class="slider-content-text c-white Glow" data-swiper-parallax="-200">효율적인 뷰렌더링 및 데이터 바인딩, Vuex를 이용한 상태 저장 및 관리, Router의 효율적인 라우팅 기능 등 다양한 핵심 기술을 토대로 모듈화된 기능, 컴포넌트의 구성등을 진행하며 빠른 프로토 개발을 진행해봅니다
                      </h6>

                      <div class="main-slider-btn-wrap" data-swiper-parallax="-300">

                        <a href="02_company.html" class="btn btn--lime btn--with-shadow">
                          VueJS 시작 !!
                        </a>

                      </div>

                    </div>

                  </div>

                  <!--<div class="col-lg-6 col-sm-12 table-cell">
                    <div class="slider-thumb" data-swiper-parallax="-300" data-swiper-parallax-duration="500">
                      <img src="/img/slides2.png" alt="slider">
                    </div>
                </div>-->

                </div>
              </div>
            </div>
          </div>

          <!--Prev next buttons-->

          <div class="btn-prev with-bg">
            <svg class="utouch-icon icon-hover utouch-icon-arrow-left-1"><use xlink:href="#utouch-icon-arrow-left-1"></use></svg>
            <svg class="utouch-icon utouch-icon-arrow-left1"><use xlink:href="#utouch-icon-arrow-left1"></use></svg>
          </div>

          <div class="btn-next with-bg">
            <svg class="utouch-icon icon-hover utouch-icon-arrow-right-1"><use xlink:href="#utouch-icon-arrow-right-1"></use></svg>
            <svg class="utouch-icon utouch-icon-arrow-right1"><use xlink:href="#utouch-icon-arrow-right1"></use></svg>
          </div>

        </div>
    </div>

    <!-- ... end Main Slider -->

    <!-- Info Boxes -->

    <section class="medium-padding100">
        <div class="container">
          <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
              <div class="crumina-module crumina-info-box info-box--standard-hover">

                <div class="info-box-image">
                  <img class="utouch-icon" src="/svg-icons/smartphone.svg" alt="smartphone">
                  <img class="cloud" src="/img/clouds8.png" alt="cloud">
                </div>

                <div class="info-box-content">
                  <a href="#" class="h5 info-box-title">코딩 강좌</a>
                  <p class="info-box-text">새로운 코딩을 쉽고 빠르게 배워봐요. 그리고 내가 가진 기술을 필요한 사람들한테 전해줄 수 있어요. 같이 가르치고 배워가요 ~ 코딩을 즐겁게 개발을 자신있게 
                  </p>
                </div>

                <a href="#" class="btn-next">
                  <svg class="utouch-icon icon-hover utouch-icon-arrow-right-1"><use xlink:href="#utouch-icon-arrow-right-1"></use></svg>
                  <svg class="utouch-icon utouch-icon-arrow-right1"><use xlink:href="#utouch-icon-arrow-right1"></use></svg>
                </a>

              </div>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
              <div class="crumina-module crumina-info-box info-box--standard-hover">

                <div class="info-box-image">
                  <img class="utouch-icon" src="/svg-icons/music%20(1).svg" alt="smartphone">
                  <img class="cloud" src="/img/clouds9.png" alt="cloud">
                </div>

                <div class="info-box-content">
                  <a href="#" class="h5 info-box-title">함께하는 프로젝트</a>
                  <p class="info-box-text">새로운 아이디어 같이 만들어봐요. 혼자서는 힘들지만 같이하면 할 수 있어요! 함께 기획하고 개발하고 구현하는 프로젝트. 열정과 긍정의 에너지를 나누며 함께 만들어가는 프로젝트
                  </p>
                </div>

                <a href="#" class="btn-next">
                  <svg class="utouch-icon icon-hover utouch-icon-arrow-right-1"><use xlink:href="#utouch-icon-arrow-right-1"></use></svg>
                  <svg class="utouch-icon utouch-icon-arrow-right1"><use xlink:href="#utouch-icon-arrow-right1"></use></svg>
                </a>

              </div>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
              <div class="crumina-module crumina-info-box info-box--standard-hover">

                <div class="info-box-image">
                  <img class="utouch-icon" src="/svg-icons/settings%20(4).svg" alt="smartphone">
                  <img class="cloud" src="/img/clouds10.png" alt="cloud">
                </div>

                <div class="info-box-content">
                  <a href="#" class="h5 info-box-title">우리들의 커뮤니티</a>
                  <p class="info-box-text">새로운 기술과 아이디어 구현에 관심있는 우리들을 위한 공간과 모임. 재밌는 아이디어, 공통의 관심사, 명확한 목표와 배움을 위해, 서로를 응원하기 위한 모든 모임을 응원합니다.  
                  </p>
                </div>

                <a href="#" class="btn-next">
                  <svg class="utouch-icon icon-hover utouch-icon-arrow-right-1"><use xlink:href="#utouch-icon-arrow-right-1"></use></svg>
                  <svg class="utouch-icon utouch-icon-arrow-right1"><use xlink:href="#utouch-icon-arrow-right1"></use></svg>
                </a>

              </div>
            </div>
          </div>
        </div>
    </section>

    <!-- ... end Info Boxes -->


    <!-- Slider with vertical tabs -->

    <section class="crumina-module crumina-module-slider slider-tabs-vertical-line">

        <div class="swiper-container" data-show-items="1">
          <div class="swiper-wrapper">
            <div class="swiper-slide bg-primary-color bg-5" data-mh="slide">
              <div class="container">
                <div class="row">
                  <div class="col-lg-4 col-lg-offset-1 col-md-5 col-md-offset-0 col-sm-12 col-xs-12">
                    <div class="slider-tabs-vertical-thumb">
                      <img src="/img/contents/test/image8.jpg" alt="iphone">
                    </div>
                  </div>
                  <div class="col-lg-6 col-lg-offset-1 col-md-7 col-md-offset-0 col-sm-12 col-xs-12">
                    <div class="crumina-module crumina-heading custom-color c-white">
                      <!--<h6 class="heading-sup-title">코드웨이브는 ~ ?!</h6>-->
                      <h2 class="heading-title">오픈 소스와 기술 공유</h2>
                      <div class="heading-text">기술은 빠르게 발전하고 있고 단순한 것에서 이제는 복합적이고 상호작용하는 형태로 변하고 있어요. 개발자들이 배워야 하는 것은 더욱 많아지고 고려해야 하는 것들도 많아지고 있어요. 기술은 이러한 시대에 맞춰 자유롭게 공유되며 발전하고 있습니다. 코드웨이브에서는 서로서로가 기술을 가르치고 공유하고 배우는 곳이에요. 끝이 없는 항해와 같은 이 길을 즐겁게 갈수 있도록 동료가 되고 길잡이가 되고자 합니다. 혼자가 아닌 여럿이 힘을 함께하는 오픈 소스와 기술공유로 발전을 꿈꾸는 곳, 코드웨이브와 함께해요!
                      </div>
                    </div>          
                  </div>
                </div>
              </div>
            </div>

            <div class="swiper-slide bg-orange-light bg-6" data-mh="slide">
              <div class="container">
                <div class="row">
                  <div class="col-lg-4 col-lg-offset-1 col-md-5 col-md-offset-0 col-sm-12 col-xs-12">
                    <div class="slider-tabs-vertical-thumb">
                      <img src="/img/contents/test/image9.jpg" alt="iphone">
                    </div>
                  </div>
                  <div class="col-lg-6 col-lg-offset-1 col-md-7 col-md-offset-0 col-sm-12 col-xs-12">
                    <div class="crumina-module crumina-heading custom-color c-white">
                      <!--<h6 class="heading-sup-title">User Interface</h6>-->
                      <h2 class="heading-title">함께 하는 오픈 프로젝트</h2>
                      <div class="heading-text">코드웨이브에서는 오픈 프로젝트를 운영합니다. 누구나 프로젝트를 만들고 참여하며 진행할 수 있어요. 멋진 아이디어가 있는 기획자, 독창적인 디자인을 하는 디자이너, 현실로 표현하는 기술을 가진 엔지니어 모두가 참여하고 프로젝트를 발전시킬 수 있어요. 혼자서는 한계에 부딪혀도 함께 한다면 꿈꾸는 목표를 이룰 수 있습니다. 코드웨이브 오픈 프로젝트를 통해서 다양한 분야의 멋진 동료들을 만나고, 새로운 경험을 얻고, 신나는 프로젝트를 만들고, 빛나고 값진 결과물을 만들어 보세요.  
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="swiper-slide bg-red bg-7" data-mh="slide">
              <div class="container">
                <div class="row">
                  <div class="col-lg-4 col-lg-offset-1 col-md-5 col-md-offset-0 col-sm-12 col-xs-12">
                    <div class="slider-tabs-vertical-thumb">
                      <img src="/img/contents/test/image31.jpg" alt="iphone">
                    </div>
                  </div>
                  <div class="col-lg-6 col-lg-offset-1 col-md-7 col-md-offset-0 col-sm-12 col-xs-12">
                    <div class="crumina-module crumina-heading custom-color c-white">
                      <!--<h6 class="heading-sup-title">User Interface</h6>-->
                      <h2 class="heading-title">긍정의 패러다임 커뮤니티</h2>
                      <div class="heading-text">코드웨이브는 다양한 커뮤니티 활동을 지원합니다. 기술이나 관심사를 나누는 가벼운 모임부터 프로젝트 기획부터 완결을 위한 커뮤니티까지 다양한 커뮤니티 활동을 할 수 있습니다. 또한 여러분이 직접 관심있는 분야에 커뮤니티를 만들고 운영해 보세요. 커뮤니티를 통해 여러분의 생각을 발전시키고 많은 사람들과 공유해보세요. 코드웨이브 커뮤니티를 동해서 여러사람이 함께 만들어가는 커뮤니티를 통해 다양성을 경험하고 서로의 생각을 공유하고 긍정적인 피드백들을 나누는 즐거운 커뮤니티를 함께 만들어가요.
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="slider-slides slider-slides--vertical-line">
            <a href="#" class="slides-item">
              <span class="round primary"></span>01.
            </a>

            <a href="#" class="slides-item">
              <span class="round orange"></span>02.
            </a>

            <a href="#" class="slides-item">
              <span class="round red"></span>03.
            </a>

          </div>

        </div>

    </section>

    <!-- ... Slider with vertical tabs -->


    <!-- Info Boxes -->

    <section class="crumina-module crumina-module-slider bg-blue-lighteen background-contain bg-11 medium-padding100">
        <div class="container">
          <div class="row">
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
              <div class="crumina-module crumina-heading">
                <h6 class="heading-sup-title">Screenshots</h6>
                <h2 class="heading-title">Beautiful interface</h2>
                <p class="heading-text">Claritas est etiam processus dynamicus, qui sequitur mutationem
                  consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram,
                  anteposuerit litterarum formas humanitatis per est usus legentis in iis qui facit eorum
                  claritatem.
                </p>
              </div>
              <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12">
                  <div class="crumina-module crumina-info-box info-box--standard">
                    <div class="info-box-image display-flex">
                      <svg class="utouch-icon utouch-icon-checked"><use xlink:href="#utouch-icon-checked"></use></svg>
                      <h6 class="info-box-title">Quick Settings</h6>
                    </div>
                    <p class="info-box-text">Wisi enim ad minim veniam, quis nostrud exerci tation qui
                      nunc nobis videntur parum clari.
                    </p>
                  </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12">
                  <div class="crumina-module crumina-info-box info-box--standard">
                    <div class="info-box-image display-flex">
                      <svg class="utouch-icon utouch-icon-checked"><use xlink:href="#utouch-icon-checked"></use></svg>
                      <h6 class="info-box-title">Looks Perfect</h6>
                    </div>
                    <p class="info-box-text">Sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt.</p>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-lg-5 col-lg-offset-1 col-md-12 col-md-offset-0 col-sm-12 col-xs-12">
              <div class="swiper-container pagination-bottom slider-tripple-right-image" data-show-items="1" data-effect="coverflow" data-centered-slider="false" data-stretch="170" data-depth="195">
                <div class="swiper-wrapper">
                  <div class="swiper-slide">
                    <img src="/img/img-slide1.png" alt="slide">
                  </div>
                  <div class="swiper-slide">
                    <img src="/img/img-slide1.png" alt="slide">
                  </div>
                  <div class="swiper-slide">
                    <img src="/img/img-slide1.png" alt="slide">
                  </div>
                </div>
                <!-- If we need pagination -->
                <div class="swiper-pagination"></div>
              </div>
            </div>
          </div>
        </div>
    </section>

    <!-- ... end Info Boxes -->


    <!-- Clients Block -->

    <section v-if="showTechBrands.length > 0" class="crumina-module crumina-clients background-contain bg-yellow">
        <div class="container">
          <div class="row">
            <div v-for="brand in showTechBrands" class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
              <a class="clients-item">
                <span class="clients-images" v-html="brand">
                </span>
              </a>
            </div>
          </div>
        </div>
    </section>

    <!-- ... end Clients Block -->

    <!-- FAQS Slider -->

    <section class="crumina-module crumina-module-slider pt100">
        <div class="container">
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 mb30">
              <div class="crumina-module crumina-heading">
                <h6 class="heading-sup-title">FAQ</h6>
                <h2 class="heading-title">Six important questions on application</h2>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="swiper-container navigation-bottom" data-effect="fade">
                <div class="slider-slides">
                  <a href="#" class="slides-item">
                    1
                  </a>

                  <a href="#" class="slides-item">
                    2
                  </a>

                  <a href="#" class="slides-item">
                    3
                  </a>

                  <a href="#" class="slides-item">
                    4
                  </a>

                  <a href="#" class="slides-item">
                    5
                  </a>

                  <a href="#" class="slides-item">
                    6
                  </a>
                </div>
                <div class="swiper-wrapper">
                  <div class="swiper-slide">
                    <div class="col-lg-4 col-md-12 col-sm-12" data-swiper-parallax="-100">
                      <div class="slider-faqs-thumb">
                        <img class="utouch-icon" src="/svg-icons/dial.svg" alt="image">
                      </div>
                    </div>

                    <div class="col-lg-8 col-md-12 col-sm-12" data-swiper-parallax="-300">
                      <h5 class="slider-faqs-title">soluta eleifend congue?</h5>

                      <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12">
                          <p>Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est
                            notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas
                            humanitatis.
                          </p>
                          <p>Gest etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum.</p>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                          <p>Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius.</p>
                          <ul class="list list--standard">
                            <li>
                              <svg class="utouch-icon utouch-icon-correct-symbol-1"><use xlink:href="#utouch-icon-correct-symbol-1"></use></svg>
                              <a href="#">Gectores legere me lius quod</a>
                            </li>
                            <li>
                              <svg class="utouch-icon utouch-icon-correct-symbol-1"><use xlink:href="#utouch-icon-correct-symbol-1"></use></svg>
                              <a href="#">Mirum est notare quam</a>
                            </li>
                            <li>
                              <svg class="utouch-icon utouch-icon-correct-symbol-1"><use xlink:href="#utouch-icon-correct-symbol-1"></use></svg>
                              <a href="#">Zril delenit augue duis</a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="col-lg-4 col-md-12 col-sm-12" data-swiper-parallax="-100">
                      <div class="slider-faqs-thumb">
                        <img class="utouch-icon" src="/svg-icons/fingerprint.svg" alt="image">
                      </div>
                    </div>

                    <div class="col-lg-8 col-md-12 col-sm-12" data-swiper-parallax="-300">
                      <h5 class="slider-faqs-title">Mirum quam gothica?</h5>
                      <p>Ilaritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum
                        est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum
                        formas humanitatis. Gest etiam processus dynamicus, qui sequitur mutationem consuetudium
                        lectorum.
                      </p>

                      <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12">
                          <ul class="list list--standard">
                            <li>
                              <svg class="utouch-icon utouch-icon-correct-symbol-1"><use xlink:href="#utouch-icon-correct-symbol-1"></use></svg>
                              <a href="#">Gectores legere me lius quod</a>
                            </li>
                            <li>
                              <svg class="utouch-icon utouch-icon-correct-symbol-1"><use xlink:href="#utouch-icon-correct-symbol-1"></use></svg>
                              <a href="#">Mirum est notare quam</a>
                            </li>
                            <li>
                              <svg class="utouch-icon utouch-icon-correct-symbol-1"><use xlink:href="#utouch-icon-correct-symbol-1"></use></svg>
                              <a href="#">Zril delenit augue duis</a>
                            </li>
                          </ul>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                          <ul class="list list--standard">
                            <li>
                              <svg class="utouch-icon utouch-icon-correct-symbol-1"><use xlink:href="#utouch-icon-correct-symbol-1"></use></svg>
                              <a href="#">Mirum est notare quam</a>
                            </li>
                            <li>
                              <svg class="utouch-icon utouch-icon-correct-symbol-1"><use xlink:href="#utouch-icon-correct-symbol-1"></use></svg>
                              <a href="#">Zril delenit augue duis</a>
                            </li>
                            <li>
                              <svg class="utouch-icon utouch-icon-correct-symbol-1"><use xlink:href="#utouch-icon-correct-symbol-1"></use></svg>
                              <a href="#">Gectores legere me lius quod</a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="col-lg-4 col-md-12 col-sm-12" data-swiper-parallax="-100">
                      <div class="slider-faqs-thumb">
                        <img class="utouch-icon" src="/svg-icons/devices.svg" alt="image">
                      </div>
                    </div>

                    <div class="col-lg-8 col-md-8 col-sm-12" data-swiper-parallax="-100">
                      <h5 class="slider-faqs-title">Investigationes quod lectores?</h5>
                      <p>Gest etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum.  Claritas est
                        etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare
                        quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas
                        humanitatis.
                      </p>

                      <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12">
                          <div class="crumina-module crumina-info-box info-box--standard">
                            <div class="info-box-image display-flex">
                              <svg class="utouch-icon utouch-icon-checked"><use xlink:href="#utouch-icon-checked"></use></svg>
                              <h6 class="info-box-title">Quick Settings</h6>
                            </div>
                            <p class="info-box-text">Wisi enim ad minim veniam, quis nostrud exerci tation qui
                              nunc nobis videntur parum clari.
                            </p>
                          </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                          <div class="crumina-module crumina-info-box info-box--standard">
                            <div class="info-box-image display-flex">
                              <svg class="utouch-icon utouch-icon-checked"><use xlink:href="#utouch-icon-checked"></use></svg>
                              <h6 class="info-box-title">Looks Perfect</h6>
                            </div>
                            <p class="info-box-text">Sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt.</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="col-lg-4 col-md-12 col-sm-12" data-swiper-parallax="-100">
                      <div class="slider-faqs-thumb">
                        <img class="utouch-icon" src="/svg-icons/payment-method.svg" alt="image">
                      </div>
                    </div>

                    <div class="col-lg-8 col-md-12 col-sm-12" data-swiper-parallax="-300">
                      <h5 class="slider-faqs-title">Duis autem vel eum iriure?</h5>
                      <p class="weight-bold">Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum
                        formas humanitatis. Gest etiam processus dynamicus, qui sequitur.
                      </p>
                      <p>Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum
                        est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum
                        formas humanitatis. Gest etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum.
                      </p>
                      <a href="03_products.html" class="btn btn-border btn--with-shadow c-secondary">
                        Learn More
                      </a>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="col-lg-4 col-md-12 col-sm-12" data-swiper-parallax="-100">
                      <div class="slider-faqs-thumb">
                        <img class="utouch-icon" src="/svg-icons/chat1.svg" alt="image">
                      </div>
                    </div>

                    <div class="col-lg-8 col-md-12 col-sm-12" data-swiper-parallax="-300">
                      <h5 class="slider-faqs-title">wisi minim veniam, quis nostrud?</h5>

                      <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12">
                          <p>Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium
                            lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram,
                            anteposuerit litterarum formas humanitatis.
                          </p>
                          <p>Gest etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum.</p>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                          <p>Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius.</p>
                          <div class="play-with-title">
                            <a href="https://www.youtube.com/watch?v=wnJ6LuUFpMo" class="video-control js-popup-iframe">
                              <img src="/img/play.png" alt="play">
                            </a>
                            <h6 class="play-title">Watch the video of instruction</h6>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="col-lg-4 col-md-12 col-sm-12" data-swiper-parallax="-100">
                      <div class="slider-faqs-thumb">
                        <img class="utouch-icon" src="/svg-icons/tap.svg" alt="image">
                      </div>
                    </div>

                    <div class="col-lg-8 col-md-12 col-sm-12" data-swiper-parallax="-300">
                      <h5 class="slider-faqs-title">Eodem typi nunc videntur?</h5>

                      <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12">
                          <p>Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium
                            lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram,
                            anteposuerit litterarum formas humanitatis.
                          </p>
                          <p>Gest etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum.</p>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                          <p>Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius.</p>
                          <ul class="list list--standard">
                            <li>
                              <svg class="utouch-icon utouch-icon-correct-symbol-1"><use xlink:href="#utouch-icon-correct-symbol-1"></use></svg>
                              <a href="#">Mirum est notare quam</a>
                            </li>
                            <li>
                              <svg class="utouch-icon utouch-icon-correct-symbol-1"><use xlink:href="#utouch-icon-correct-symbol-1"></use></svg>
                              <a href="#">Zril delenit augue duis</a>
                            </li>
                            <li>
                              <svg class="utouch-icon utouch-icon-correct-symbol-1"><use xlink:href="#utouch-icon-correct-symbol-1"></use></svg>
                              <a href="#">Gectores legere me lius quod</a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>

                  </div>

                </div>

                <!--Prev next buttons-->

                <div class="btn-slider-wrap navigation-left-bottom">

                  <div class="btn-prev">
                    <svg class="utouch-icon icon-hover utouch-icon-arrow-left-1"><use xlink:href="#utouch-icon-arrow-left-1"></use></svg>
                    <svg class="utouch-icon utouch-icon-arrow-left1"><use xlink:href="#utouch-icon-arrow-left1"></use></svg>
                  </div>

                  <div class="btn-next">
                    <svg class="utouch-icon icon-hover utouch-icon-arrow-right-1"><use xlink:href="#utouch-icon-arrow-right-1"></use></svg>
                    <svg class="utouch-icon utouch-icon-arrow-right1"><use xlink:href="#utouch-icon-arrow-right1"></use></svg>
                  </div>

                </div>

              </div>
            </div>
          </div>
        </div>
    </section>

    <!-- ... end FAQS Slider -->


    <!-- Counters -->

    <section class="bg-secondary-color background-contain" style="background-image:url('/img/contents/test/image5.jpg'); background-size: cover;">

        <div class="container">
          <div class="row">
            <div class="counters">
              <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
                <div class="crumina-module crumina-counter-item">
                  <div class="counter-numbers counter c-yellow">
                    <span data-speed="2000" data-refresh-interval="3" data-to="6237" data-from="2">6237</span>
                  </div>
                  <h5 class="counter-title">Line of codes</h5>
                </div>
              </div>
              <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
                <div class="crumina-module crumina-counter-item">
                  <div class="counter-numbers counter c-yellow">
                    <span data-speed="2000" data-refresh-interval="3" data-to="4000" data-from="2">4000</span>
                  </div>
                  <h5 class="counter-title">Solutions</h5>
                </div>
              </div>
              <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
                <div class="crumina-module crumina-counter-item">
                  <div class="counter-numbers counter c-yellow">
                    <span data-speed="2000" data-refresh-interval="3" data-to="3" data-from="2">3</span>
                    <div class="units">M+</div>
                  </div>
                  <h5 class="counter-title">Active users</h5>
                </div>
              </div>
              <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
                <div class="crumina-module crumina-counter-item">
                  <div class="counter-numbers counter c-yellow">
                    <span data-speed="2000" data-refresh-interval="3" data-to="8" data-from="2">8</span>
                    <div class="units">M+</div>
                  </div>
                  <h5 class="counter-title">Download</h5>
                </div>
              </div>

              <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                <h5 class="c-white">Utouch is an awesome app</h5>
                <p class="c-semitransparent-white">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy euismod.</p>
              </div>

            </div>
          </div>
        </div>

    </section>

    <!-- ... end  Counters -->


    <!-- Testimonials -->

    <section class="crumina-module crumina-module-slider bg-4 cloud-center navigation-center-both-sides medium-padding100">
        <div class="container">
          <div class="row">
            <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-12 col-sm-offset-0">
              <div class="swiper-container" data-effect="fade">
                <div class="swiper-wrapper">
                  <div class="crumina-module crumina-testimonial-item testimonial-item-author-top swiper-slide">

                    <div class="testimonial-img-author" data-swiper-parallax="-100">
                      <img src="/img/author2.png" alt="avatar">
                    </div>

                    <h6 class="testimonial-text" data-swiper-parallax="-300">
                      Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est
                      etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum parum claram.
                    </h6>

                    <div class="author-info-wrap" data-swiper-parallax="-100">

                      <div class="author-info">
                        <a href="#" class="h6 author-name">Doe Simpson</a>
                        <div class="author-company">Student, 23 years old</div>
                      </div>

                    </div>
                  </div>

                  <div class="crumina-module crumina-testimonial-item testimonial-item-author-top swiper-slide">

                    <div class="testimonial-img-author" data-swiper-parallax="-100">
                      <img src="/img/author2.png" alt="avatar">
                    </div>

                    <h6 class="testimonial-text" data-swiper-parallax="-300">
                      Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est
                      etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum parum claram.
                    </h6>

                    <div class="author-info-wrap" data-swiper-parallax="-100">

                      <div class="author-info">
                        <a href="#" class="h6 author-name">Doe Simpson</a>
                        <div class="author-company">Student, 23 years old</div>
                      </div>

                    </div>
                  </div>

                  <div class="crumina-module crumina-testimonial-item testimonial-item-author-top swiper-slide">

                    <div class="testimonial-img-author" data-swiper-parallax="-100">
                      <img src="/img/author2.png" alt="avatar">
                    </div>

                    <h6 class="testimonial-text" data-swiper-parallax="-300">
                      Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est
                      etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum parum claram.
                    </h6>

                    <div class="author-info-wrap" data-swiper-parallax="-100">

                      <div class="author-info">
                        <a href="#" class="h6 author-name">Doe Simpson</a>
                        <div class="author-company">Student, 23 years old</div>
                      </div>

                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>

        <!--Prev next buttons-->

        <div class="btn-prev">
          <svg class="utouch-icon icon-hover utouch-icon-arrow-left-1"><use xlink:href="#utouch-icon-arrow-left-1"></use></svg>
          <svg class="utouch-icon utouch-icon-arrow-left1"><use xlink:href="#utouch-icon-arrow-left1"></use></svg>
        </div>

        <div class="btn-next">
          <svg class="utouch-icon icon-hover utouch-icon-arrow-right-1"><use xlink:href="#utouch-icon-arrow-right-1"></use></svg>
          <svg class="utouch-icon utouch-icon-arrow-right1"><use xlink:href="#utouch-icon-arrow-right1"></use></svg>
        </div>
    </section>

    <!-- ... end Testimonials -->



    <!-- Subscribe Form -->

    <section class="bg-primary-color background-contain bg-14 crumina-module crumina-module-subscribe-form">
        <div class="container">
          <div class="row">
            <div class="subscribe-form">
              <div class="subscribe-main-content">
                <img class="subscribe-img" src="/img/subscribe-img.png" alt="image">

                <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
                  <div class="crumina-module crumina-heading">
                    <h2 class="heading-title">{{env('APP_NAME')}} 소식을 원하세요?<br /> 구독을 신청하세요.</h2>
                    <p class="heading-text">강좌, 기술, 모임, 컨퍼런스 등 다양한 소식들을 메일로 보내드려요.
                    </p>
                  </div>

                  <form class="form-inline">
                    <input name="email" v-model="subscribeEmail" placeholder="Enter your email address" type="email">
                    <button @click.prevent="emailSubscribe" class="btn btn--green-light" :disabled="subscribeFormStatus == 'uploading'">
                      구독하기
                    </button>
                  </form>
                </div>

              </div>
              <div class="subscribe-layer"></div>
            </div>
          </div>
        </div>
    </section>

    <!-- End Subscribe Form -->



  </div>

@endsection

@section('popups')
    @component('components.alarm', ['title'=>'', 'content'=>''])
    @endcomponent

    @component('components.searchPopup')
    @endcomponent
@endsection

@section('svgs')
  @component('components.svgIcons')
  @endcomponent
@endsection


@section('scripts-code')
    <script>
    var userLoginStatus = {{ Auth::check() ? 'true' : 'false' }}
    window.onload = function () {
    }
    </script>

    <script src="{{ asset('js/pages/main/index.js') }}"></script>

@endsection
