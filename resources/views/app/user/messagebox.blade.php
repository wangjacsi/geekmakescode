@extends('layouts.app')

@section('styles')
    <style>
        .message-content :first-child{
            margin-top:0;
        }

        .button-group {
            text-align: right;
            margin-bottom: 50px;
        }
        .button-group .btn + .btn{
            margin-left: 5px;
        }
        .send-message-btn{
            float: right;
        }
        .box-title{
            color:#738CAA;
            opacity:0.5;
        }
        .box-title:hover{
            cursor: pointer;
        }
        .box-title.current-box{
            color:inherit;
            opacity: 1;
        }
    </style>
@endsection

@section('content')
  <div class="content-wrapper">
      <!-- Stunning Header -->

  	<div class="crumina-stunning-header stunning-header--bg-photo stunning-header--breadcrumbs-bottom-left stunning-header--content-padding100 custom-color c-white fill-white" style="background-image:url({{ $user->getHeadImage() }})">
  		<div class="container">
  			<div class="stunning-header-content">
  				<h6 class="category-link c-yellow-light">Training</h6>
  				<h4 class="stunning-header-title">@{{ user.status_message}}</h4>
  				<div class="icon-text-item display-flex">
  					<div class="text"><i class="far fa-calendar-alt"></i> @{{moment(user.created_at).format('MMMM Do YYYY')}}</div>
  				</div>

  				<div class="breadcrumbs-wrap inline-items">
  					<a href="/mywave" class="btn btn--black btn--round breadcrumbs-custom">
  						<svg class="utouch-icon utouch-icon-home-icon-silhouette breadcrumbs-custom"><use xlink:href="#utouch-icon-home-icon-silhouette"></use></svg>
  					</a>

                    <ul class="breadcrumbs breadcrumbs--rounded">
						<li class="breadcrumbs-item">
							<a href="/mywave" class="breadcrumbs-custom">마이웨이브</a>
							<svg class="utouch-icon utouch-icon-media-play-symbol breadcrumbs-custom"><use xlink:href="#utouch-icon-media-play-symbol"></use></svg>
						</li>
						<li class="breadcrumbs-item active">
							<span class="breadcrumbs-custom">메세지 박스</span>
							<svg class="utouch-icon utouch-icon-media-play-symbol breadcrumbs-custom"><use xlink:href="#utouch-icon-media-play-symbol"></use></svg>
						</li>
					</ul>
  				</div>
  			</div>
  		</div>
  		<div class="overlay-standard overlay--yellow"></div>
  	</div>

  	<!-- ... end Stunning Header -->
    <section class="medium-padding100">
      <div class="container">
      	<div class="row">
      		<div class="col-xl-12 col-lg-12 col-md-12 order-md-1 col-sm-12 col-12">
      			<div class="ui-block block-rounded-shadow">
      				<div v-if="myChannelCnt > 0" class="ui-block-title">
                        <h5 class="line-block">
                            <a @click.prevent="messageBoxChange('user')" :class="{'current-box' : currentBox == 'user'}" class="box-title">마이 메세지 박스 </a>
                            <a :class="{'btn--primary' : myTotalMessagesCnt > 0, 'btn--secondary' : myTotalMessagesCnt == 0}" class="items-round-little ">Total @{{myTotalMessagesCnt}}</a>
                            <a v-if="myNewMessagesCnt > 0" class="items-round-little btn--orange-light">New @{{myNewMessagesCnt}}</a> /
                            <a @click.prevent="messageBoxChange('channel')" :class="{'current-box' : currentBox == 'channel'}" class="box-title">채널 메세지 박스</a>
                            <a :class="{'btn--green' : myChTotalMessagesCnt > 0, 'btn--secondary' : myChTotalMessagesCnt == 0}" class="items-round-little ">Total @{{myChTotalMessagesCnt}}</a>
                            <a v-if="myNewChMessageCnt > 0" class="items-round-little btn--orange-light">New @{{myNewChMessageCnt}}</a>
                        </h5>
      				</div>

                    <div v-else class="ui-block-title">
                        <h5 class="line-block">마이 메세지 박스
                            <a :class="{'btn--primary' : myTotalMessagesCnt > 0, 'btn--secondary' : myTotalMessagesCnt == 0}" class="items-round-little current-box" class="box-title ">Total @{{myTotalMessagesCnt}}</a>
                            <a v-if="myNewMessagesCnt > 0" class="items-round-little btn--orange-light">New @{{myNewMessagesCnt}}</a>
                        </h5>
                    </div>

      				<div v-if="loadFlag && messages.length" class="row">
      					<div class="col-xl-5 col-lg-5 col-md-12 col-sm-12  padding-r-0">

      						<!-- Notification List Chat Messages -->

      						<ul class="notification-list chat-message">
      							<li v-for="message in messages" :key="message.id" v-bind:class="{'active' : activeId == message.id, 'message-unread' : !message.read}">



                                    <!-- 보낸 채널 정보 -->
                                    <div v-if="message.channel" class="author-thumb align-center">
      									<a :href="'/channel/'+message.channel.slug"><img :src="message.channel.logo_image ? message.channel.logo_image : '/img/channels/logo_image.png'" alt="author"></a>
                                        <span style="font-size:12px;" class="">채널</span>
      								</div>

                                    <!-- 보낸 사람 정보 -->
                                    <div v-else class="author-thumb">
      									<a :href="'/wave/'+message.user.slug"><img :src="message.user.photo ? message.user.photo : '/img/profile/default.png'" alt="author"></a>
      								</div>



      								<div class="notification-event">
      									<a v-if="message.channel" :href="'/channel/'+message.channel.slug" class="h6 notification-friend" style="margin-top:0;">@{{message.channel.name}}</a>
                                        <a v-else href="'/wave/'+message.user.slug" class="h6 notification-friend" style="margin-top:0;">@{{message.user.name}}</a>
      									<span class="chat-message-item"><a style="cursor:pointer;" @click.prevent="changeMessage(message.id)">@{{message.title}}</a></span>

      									<span class="notification-date">
                                            <time class="entry-date updated" datetime="2004-07-24T18:18">@{{moment(message.created_at).fromNow()}}</time>

                                            <!-- 보낸 사람 정보 -->
                                            <span v-if="message.channel" class="chat-message-item">by <a :href="'/wave/'+message.user.slug">@{{message.user.name}}</a></span>
                                        </span>
      								</div>
      								<span v-if="message.read" class="notification-icon">
										<i class="far fa-envelope-open"></i>
                                        <!-- 받는 채널 정보-->
                                        <div v-if="'rchannel_name' in message" class="channel-thumb align-center">
          									<a :title="message.rchannel_name" :href="'/channel/'+message.rchannel_slug"><img :src="message.rchannel_logo_image ? message.rchannel_logo_image : '/img/channels/logo_image.png'" alt="author"></a>
                                            <span style="font-size:12px;" class=""></span>
          								</div>
									</span>
                                    <span v-else class="notification-icon">
										<i class="far fa-envelope"></i>
                                        <!-- 받는 채널 정보-->
                                        <div v-if="'rchannel_name' in message" class="channel-thumb align-center">
          									<a :title="message.rchannel_name" :href="'/channel/'+message.rchannel_slug"><img :src="message.rchannel_logo_image ? message.rchannel_logo_image : '/img/channels/logo_image.png'" alt="author"></a>
                                            <span style="font-size:12px;" class=""></span>
          								</div>
									</span>
      							</li>
      						</ul>

      						<!-- ... end Notification List Chat Messages -->
                            <template v-if="loadFlag">
                                <pagination for='mymessages' :pagination="pagination" />
                            </template>

      					</div>

      					<div class="col-xl-7 col-lg-7 col-md-12 col-sm-12  padding-l-0">

      						<!-- Chat Field -->

      						<div class="chat-field">
      							<div class="ui-block-title with-icon">

                    				<span style="top: 25px; " class="utouch-icon utouch-icon-user"> 제 목</span>

      								<h6 style="padding-left: 60px;" v-bind:style="{ height: showTitle==null ? '20px' : 'auto' }" class="title" v-html="showTitle" v-highlightjs></h6>
      							</div>
      							<div class="mCustomScrollbar with-icon" data-mcs-theme="dark" style="margin-bottom:50px;">
                                    <span style="top: 25px;" class="utouch-icon utouch-icon-user"> 내 용</span>

      								<ul style="padding-left: 60px;" class="notification-list chat-message chat-message-field">
      									<li>
      										<p class="message-content" v-html="showContent" v-highlightjs></p>
      									</li>
      								</ul>
      							</div>

                                <div class="button-group">
                                    <a @click.prevent="unreadMessage" href="" class="btn btn-small btn--orange-light btn--with-shadow">
          								읽음 해제
          							</a>
                                    <a @click.prevent="deleteMessage" href="" class="btn btn-small btn--grey btn--with-shadow">
          								삭제
          							</a>
                                </div>

      							<form >
                                    <h6 class="">메세지 보내기</h6>
                                    <div class="with-icon">
                        				<input v-model.trim="subject" class="with-icon" name="subject" placeholder="Subject" type="text" required="required">
                        				<svg class="utouch-icon utouch-icon-icon-1"><use xlink:href="#utouch-icon-icon-1"></use></svg>
                        			</div>

                        			<div class="with-icon">
                        				<textarea v-model.trim="message" name="message" required="" placeholder="Your Message" style="height: 180px;"></textarea>
                        				<svg class="utouch-icon utouch-icon-edit"><use xlink:href="#utouch-icon-edit"></use></svg>
                        			</div>
                                    <div style="text-align: initial;font-size: 13px;margin-bottom: 20px;">마크다운(Markdown <i class="fab fa-markdown"></i>) 지원합니다. # ~ ######: 헤더 h1 ~ h6, >: 인용 Blockquotes, ~~~ 코드블록 ~~~, ` 인라인 코드 블럭 `, * 기울여쓰기(italic) *, ** 굵게쓰기(bold) **, --- 수평선, [Google](http://www.google.co.kr “구글”) : 인라인 링크, &lt;http://google.com/&gt;: URl 링크, 1. list item : 리스트, * list item : 리스트, ![alt text](image_URL): 링크 이미지
                                    </div>


                                    <button @click.prevent="sendMessage" class="send-message-btn btn btn--green btn--with-shadow">
                        				메세지 전송
                        			</button>

                                    <div v-if="errorMessage != ''" class="summit-message" style="margin-top:20px;">
                                        <p class="summit-error" v-html="errorMessage"></p>
                                    </div>

      							</form>

      						</div>

      						<!-- ... end Chat Field -->

      					</div>
      				</div>
                    <div v-else class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 align-center">
                            <h6 class="title" style="margin:100px auto;">메세지함이 비었습니다</h6>
                        </div>
                    </div>

      			</div>

      		</div>
      	</div>
      </div>
    </section>
  </div>

@endsection

@section('popups')
    @component('components.alarm', ['title'=>'', 'content'=>''])
    @endcomponent

    @component('components.searchPopup')
    @endcomponent
@endsection

@section('svgs')
  @component('components.svgIcons')
  @endcomponent
@endsection


@section('scripts-code')
    <script>
    var user = {!! $user !!};
    var myChannelCnt = {!! $myChannelCnt !!};
    var myTotalMessagesCnt = {!! $myTotalMessagesCnt !!};
    var myNewMessagesCnt = {!! $myNewMessagesCnt !!};
    var myNewChMessageCnt = {!! $myNewChMessageCnt !!};
    var myChTotalMessagesCnt = {!! $myChTotalMessagesCnt !!};

    </script>
<script src="{{ asset('js/pages/user/messagebox.js') }}"></script>
@endsection
