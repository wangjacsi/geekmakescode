@extends('layouts.app')

@section('styles')
    <style>
        .block-rounded-shadow:not(:first-child){
            margin-top : 50px;
        }
        .social-btn-group {
            margin-bottom:10px;
        }
        .w-author .js-message-popup {
            margin-top: 20px;
        }
        .members-list{
            margin:0;
        }
    </style>
@endsection

@section('content')
<div class="content-wrapper">

	<!-- Stunning Header -->

	<div class="crumina-stunning-header stunning-header--bg-photo stunning-header--breadcrumbs-bottom-left stunning-header--content-padding100 custom-color c-white fill-white" style="background-image:url({{ $user->getHeadImage() }})">
		<div class="container">
			<div class="stunning-header-content">
				<h6 class="category-link c-yellow-light">Training</h6>
				<h4 class="stunning-header-title">@{{ user.status_message}}</h4>
				<div class="icon-text-item display-flex">
					<div class="text"><i class="far fa-calendar-alt"></i> @{{moment(user.created_at).format('MMMM Do YYYY')}}</div>
				</div>

				<div class="breadcrumbs-wrap inline-items">
					<a href="/mywave" class="btn btn--black btn--round breadcrumbs-custom">
						<svg class="utouch-icon utouch-icon-home-icon-silhouette breadcrumbs-custom"><use xlink:href="#utouch-icon-home-icon-silhouette"></use></svg>
					</a>

					<ul class="breadcrumbs breadcrumbs--rounded">
						<li class="breadcrumbs-item">
							<a href="/mywave" class="breadcrumbs-custom">마이웨이브</a>
							<svg class="utouch-icon utouch-icon-media-play-symbol breadcrumbs-custom"><use xlink:href="#utouch-icon-media-play-symbol"></use></svg>
						</li>
						<li class="breadcrumbs-item active">
							<span v-if="views.main=='myChannels'" class="breadcrumbs-custom">운영 채널</span>
                            <span v-else-if="views.main=='myRegisteredTutorials'" class="breadcrumbs-custom">수강 강좌</span>
                            <span v-else-if="views.main=='myInterestTutorials'" class="breadcrumbs-custom">관심 강좌</span>
                            <span v-else-if="views.main=='mySubscribedChannels'" class="breadcrumbs-custom">구독 채널</span>
							<svg class="utouch-icon utouch-icon-media-play-symbol breadcrumbs-custom"><use xlink:href="#utouch-icon-media-play-symbol"></use></svg>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="overlay-standard overlay--yellow"></div>
	</div>

	<!-- ... end Stunning Header -->


	<!-- Main content + sidebar-->

	<section class="medium-padding100">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">

                    <!-- 나의 채널 정보 -->
                    <div v-show="views.main=='myChannels'" class="block-rounded-shadow">
						<h5 class="line-block">마이 웨이브 채널</h5>
                        <div class="list-style-group">
                            <span title="블럭형태 보기"><a @click.prevent="listViewChange('block', 'mychannels')" href=""><i class="fas fa-th-large"></i></a></span>
                            <span title="리스트형태 보기"><a @click.prevent="listViewChange('list', 'mychannels')" href=""><i class="fas fa-th-list"></i></a></span>
                        </div>

                        <div class="case-item-wrap">
            				<div class="row channel-lists" id="items-grid">
                                <div class="curriculum-event-wrap">
                                    <div v-if="myChViewOptions.viewType == 'block'" class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                        <div class="curriculum-event c-secondary" data-mh="mychannels">
                                            <div class="curriculum-event-thumb">
                                                <img src="/img/channels/head_image.png" alt="image">
                                                <a href="/channel/create" title="채널 만들기" class="channel-add-btn"><i class="xi-plus-circle-o"></i></a>
                                                <div class="overlay-standard overlay--blue"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div v-else class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align:center; margin:50px 0;">
                                        <a href="/channel/create" class="btn btn-small btn--green-light btn--with-shadow" style="width:250px;">
                                            채널 만들기
                                        </a>
                                    </div>

                                    <my-channels from-comp='mychannels' :view-options-prop="myChViewOptions" channel-logo-default="/img/channels/logo_image.png" channel-head-default="/img/channels/head_image.png"
                                    user-photo-default="/img/profile/default.png"></my-channels>
                                </div>
                            </div>
                        </div>

                        <div class="">
                            <div class="or none" style="margin-top:50px"></div>
                            <h6><i class="fas fa-broadcast-tower"></i> 마이 웨이브 채널 알림<span style="float:right"><a @click.prevent="refreshNotices('myChNotices')" href="" id="myChNotices"><i class="xi-renew xi-x"></i></a></span></h6>

            				<div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <my-notices notice-name='myChNotices' main-obj='channel'></my-notices>
                                </div>
                            </div>
                        </div>


                        <div v-if="views.myChannelsTotal > 0" class="">
                            <div class="or none" style="margin-top:80px"></div>
                            <h5 class="line-block">운영 강좌</h5>
                            <div class="list-style-group">
                                <span title="블럭형태 보기"><a @click.prevent="listViewChange('block', 'mytutorials')" href=""><i class="fas fa-th-large"></i></a></span>
                                <span title="리스트형태 보기"><a @click.prevent="listViewChange('list', 'mytutorials')" href=""><i class="fas fa-th-list"></i></a></span>
                            </div>
                            <div class="case-item-wrap">
                				<div class="row tutorial-lists" id="items-grid">
                                    <div class="curriculum-event-wrap">
                                        <div v-if="myTutorialsOptions.viewType == 'block'" class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                            <div class="curriculum-event" data-mh="mytutorials">
                                                <div class="curriculum-event-thumb">
                                                    <img src="/img/tutorials/thumbnail.jpg" alt="image">

                                                    <div class="overlay-standard"></div>
                                                </div>
                                                <div class="curriculum-event-content">
                                                    <a title="강좌 만들기" class="tutorial-add-btn"><i class="xi-plus-circle-o"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div v-else class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align:center; margin:50px 0;">
                                            <a href="" class="btn btn-small btn--green-light btn--with-shadow" style="width:250px;">
                                                강좌 만들기
                                            </a>
                                        </div>
                                        <my-tutorials channel-logo-default="/img/channels/logo_image.png" thumbnail-default="/img/tutorials/thumbnail.jpg" :view-options-prop="myTutorialsOptions" comp-name='mytutorials' url-path='/getMyTutorialsInfoByLoginUser' background-colors-prop="{{ json_encode($backgroundColors) }}" progress-status-prop="{{ json_encode($progressStatus) }}"></my-tutorials>
                                    </div>
                                </div>
                            </div>

                            <div class="">
                                <div class="or none" style="margin-top:50px"></div>
                                <h6><i class="fas fa-broadcast-tower"></i> 운영 강좌 알림<span style="float:right"><a @click.prevent="refreshNotices('myTutoNotices')" href="" id="myTutoNotices"><i class="xi-renew xi-x"></i></a></span></h6>

                				<div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <my-notices notice-name='myTutoNotices' main-obj='tutorial'></my-notices>
                                    </div>
                                </div>
                            </div>
                        </div>
					</div>


                    <!-- 나의 수강 강좌 정보 -->
                    <div v-show="views.main=='myRegisteredTutorials'" class="block-rounded-shadow">
						<h5 class="inline-block">나의 수강 강좌</h5>
                        <div class="list-style-group">
                            <span title="블럭형태 보기"><a @click.prevent="listViewChange('block', 'registeredtutorials')" href=""><i class="fas fa-th-large"></i></a></span>
                            <span title="리스트형태 보기"><a @click.prevent="listViewChange('list', 'registeredtutorials')" href=""><i class="fas fa-th-list"></i></a></span>
                        </div>
                        <div class="case-item-wrap">
            				<div class="row tutorial-lists" id="items-grid">
                                <div v-if="views.myRegisteredTutorialsTotal == 0" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <p>수강중인 강좌가 없습니다 </p>
                                </div>
                                <div class="curriculum-event-wrap">
                                    <my-tutorials channel-logo-default="/img/channels/logo_image.png"  thumbnail-default="/img/tutorials/thumbnail.jpg" :view-options-prop="myRegisteredTutorialsOptions" comp-name='registeredtutorials' url-path='/getRegisteredTutorialsInfoByLoginUser' background-colors-prop="{{ json_encode($backgroundColors) }}" progress-status-prop="{{ json_encode($progressStatus) }}"></my-tutorials>
                                </div>
                            </div>
                        </div>

                        <div class="">
                            <div class="or none" style="margin-top:50px"></div>
                            <h6><i class="fas fa-broadcast-tower"></i> 수강 강좌 알림<span style="float:right"><a @click.prevent="refreshNotices('regiTutoNotices')" href="" id="regiTutoNotices"><i class="xi-renew xi-x"></i></a></span></h6>

                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <my-notices notice-name='regiTutoNotices' main-obj='tutorial'></my-notices>
                                </div>
                            </div>
                        </div>
					</div>


                    <!-- 나의 수강 강좌 정보 -->
                    <div v-show="views.main=='myInterestTutorials'" class="block-rounded-shadow">
						<h5 class="inline-block">나의 관심 강좌</h5>
                        @if($myInterestCategories)
                        <div class="">
                            <div class="interest-category-select" style="display: inline-block; width: 250px;">
                                <select v-on:change="costSelectChange" v-model="category" class="" name="" style="padding: 5px 25px;">
                                    <option value="">전체보기</option>
                                    @foreach($myInterestCategories as $category)
                                    <option value="{{$category->category}}">{{$category->category}}</option>
                                    @endforeach
                                </select>
                            </div>
                            @endif
                            <div class="list-style-group">
                                <span title="블럭형태 보기"><a @click.prevent="listViewChange('block', 'interesttutorials')" href=""><i class="fas fa-th-large"></i></a></span>
                                <span title="리스트형태 보기"><a @click.prevent="listViewChange('list', 'interesttutorials')" href=""><i class="fas fa-th-list"></i></a></span>
                            </div>
                        @if($myInterestCategories)
                        </div>
                        @endif

                        <div class="case-item-wrap">
            				<div class="row tutorial-lists" id="items-grid">
                                <div v-if="views.myInterestTutorialsTotal == 0" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <p>관심 강좌가 없습니다 </p>
                                </div>
                                <div class="curriculum-event-wrap">
                                    <my-tutorials channel-logo-default="/img/channels/logo_image.png"  thumbnail-default="/img/tutorials/thumbnail.jpg" :view-options-prop="myInterestTutorialsOptions" comp-name='interesttutorials' url-path='/getInterestTutorialsInfoByLoginUser' background-colors-prop="{{ json_encode($backgroundColors) }}" progress-status-prop="{{ json_encode($progressStatus) }}"></my-tutorials>
                                </div>
                            </div>
                        </div>
					</div>


                    <!-- 구독 채널 정보 -->
                    <div v-show="views.main=='mySubscribedChannels'" class="block-rounded-shadow">
						<h5 class="inline-block">나의 구독 채널</h5>
                        <div class="list-style-group">
                            <span title="블럭형태 보기"><a @click.prevent="listViewChange('block', 'mysubscribedchannels')" href=""><i class="fas fa-th-large"></i></a></span>
                            <span title="리스트형태 보기"><a @click.prevent="listViewChange('list', 'mysubscribedchannels')" href=""><i class="fas fa-th-list"></i></a></span>
                        </div>

                        <div class="case-item-wrap">
            				<div class="row channel-lists" id="items-grid">
                                <div v-if="views.mySubscribedChannelsTotal == 0" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <p>구독중인 채널이 없습니다 </p>
                                </div>
                                <div class="curriculum-event-wrap">
                                    <my-subscribed-channels :view-options-prop="mySubscribedChViewOptions" from-comp='mysubscribedchannels' channel-logo-default="/img/channels/logo_image.png" channel-head-default="/img/channels/head_image.png"
                                    user-photo-default="/img/profile/default.png"></my-subscribed-channels>
                                </div>
                            </div>
                        </div>
                        <div class="">
                            <div class="or none" style="margin-top:50px"></div>
                            <h6><i class="fas fa-broadcast-tower"></i> 구독 채널 알림<span style="float:right"><a @click.prevent="refreshNotices('subsChNotices')" href="" id="subsChNotices"><i class="xi-renew xi-x"></i></a></span></h6>

                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <my-notices notice-name='subsChNotices' main-obj='channel'></my-notices>
                                </div>
                            </div>
                        </div>
					</div>
                    <!-- 구독 채널 정보 -->
				</div>



                <!-- sidebar -->
				<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
					<aside aria-label="sidebar" class="sidebar sidebar-right">

						<aside class="widget w-author">
							<div class="testimonial-img-author">
								<img src="{{ $user->getPhoto() }}" alt="author">
							</div>

							<a href="/wave/{{$user->slug}}" class="h5 title" style="margin-bottom:0;">{{ $user->name }}</a>
                            <p>{{$user->job}}</p>

							<p v-html="sanitizeMarkedBody(user.aboutme)" v-highlightjs></p>

                            <div v-if="options && options.viewoptions.sns && snsLists.length > 0" class="social-btn-group">
                                <a v-for="snsList in snsLists" target="_blank" :href="snsList.link" class="social-icon" v-bind:style="{ color: snsList.color }" :title="snsList.name"><span :class="snsList.class"></span></a>
                            </div>

                            <div v-if="options && options.viewoptions.contacts" class="">
                                <div v-if="options.phone" class="contact-item display-flex">
    								<svg class="utouch-icon utouch-icon-telephone-keypad-with-ten-keys"><use xlink:href="#utouch-icon-telephone-keypad-with-ten-keys"></use></svg>
    								<span class="info">@{{ options.phone }}</span>
    							</div>

    							<div class="contact-item display-flex">
    								<svg class="utouch-icon utouch-icon-message-closed-envelope-1"><use xlink:href="#utouch-icon-message-closed-envelope-1"></use></svg>
    								<span class="info">{{ $user->email }}</span>
    							</div>

                                <div v-if="options.site" class="contact-item display-flex">
    								<i class="fas fa-archway utouch-icon"></i>
    								<span class="info"><a target="_blank" v-bind:href="options.site">@{{ options.site }}</a></span>
    							</div>
                            </div>
						</aside>

                        <aside class="widget w-category">
    						<h5 class="widget-title">마이 웨이브</h5>
    						<ul class="category-list">
    							<li v-if="views.myChannelsTotal > 0">
    								<a @click.prevent="changeMainView('myChannels')" href="" v-bind:class="{active : views.main=='myChannels' }">운영 채널
    									<span class="cat-count">@{{views.myChannelsTotal}}</span>
    								</a>
    							</li>
    							<li>
    								<a @click.prevent="changeMainView('myRegisteredTutorials')" href="" v-bind:class="{active : views.main=='myRegisteredTutorials'}">수강 강좌
    									<span v-if="views.myRegisteredTutorialsTotal > 0" class="cat-count">@{{views.myRegisteredTutorialsTotal}}</span>
    								</a>
    							</li>
                                <li>
    								<a @click.prevent="changeMainView('myInterestTutorials')" href="" v-bind:class="{active : views.main=='myInterestTutorials'}">관심 강좌
    									<span v-if="views.myInterestTutorialsTotal > 0" class="cat-count">@{{views.myInterestTutorialsTotal}}</span>
    								</a>
    							</li>
    							<li>
    								<a @click.prevent="changeMainView('mySubscribedChannels')" href="" v-bind:class="{active : views.main=='mySubscribedChannels'}">구독 채널
    									<span v-if="views.mySubscribedChannelsTotal > 0" class="cat-count">@{{views.mySubscribedChannelsTotal}}</span>
    								</a>
    							</li>
    							<li>
    								<a href="/wave/{{$user->slug}}">개인 정보
    								</a>
    							</li>
    							<!--<li>
    								<a href="#">마이 웨이브 프렌즈
    								</a>
    							</li>-->
                                <li>
    								<a href="/messagebox">메세지 박스
                                        <span v-if="totalNewMessages > 0" class="cat-count">NEW @{{totalNewMessages}}</span>
    								</a>
    							</li>

    						</ul>
    					</aside>

						<aside v-if="views.myChannelsTotal == 0" class="widget w-contacts">
							<h5 class="widget-title">채널 만들기</h5>

							<p class="contacts-text" style="margin-bottom:0">나만의 채널을 만들어보세요</p>

                            <div class="" style="margin-top:20px">
                                <img src="/img/tutorials/thumbnail.jpg" alt="image">
                                <a href="/channel/create" class="btn btn--large full-width btn--primary btn--with-shadow cd-nav-trigger">
                                    채널 만들기
                                </a>
                            </div>
						</aside>

						<aside v-if="chAdminUsers.length > 0" class="widget w-latest-members">
							<h5 class="widget-title">마이 웨이브 채널 관리자</h5>
							<p>채널을 운영중인 멤버리스트입니다</p>

							<ul class="members-list">
								<li v-for="admin in chAdminUsers">
									<a href="#" v-bind:title="admin.name">
										<img style="width:100%;" :src="admin.photo ? admin.photo : userPhotoDefault" alt="avatar">
									</a>
								</li>
							</ul>
						</aside>

                        <aside v-if="followUsers.length > 0" class="widget w-latest-news">
    						<h5 class="widget-title">마이 프렌즈</h5>
    						<div class="tab-content">
    							<div role="tabpanel" class="tab-pane fade in active">
    								<ul class="latest-news-list">
    									<li v-for="follow in followUsers" >
    										<article class="latest-news-item">
    											<header>
    												<div class="post-thumb">
    													<img :src="follow.follow_user.photo ? follow.follow_user.photo : userPhotoDefault" alt="news">
    												</div>
    												<div class="post-additional-info">
    													<h6 class="post__title entry-title" itemprop="name">
    														<a href="17_news_details.html">@{{follow.follow_user.name}}</a>
    													</h6>
    													<span class="post__date">
                                                            <ul class="friends-harmonic">
                                                                <li v-for="channel in follow.follow_user.channels" :key="channel.id">
                                    								<a v-bind:href="'/channel/'+ channel.slug" v-bind:title="channel.name">
                                    									<img v-bind:src="channel.logo_image" alt="image">
                                    								</a>
                                    							</li>
                                                            </ul>
    													</span>
    												</div>
    											</header>
    										</article>
    									</li>
    								</ul>
    							</div>
    						</div>
    					</aside>

					</aside>
				</div>
			</div>
		</div>
	</section>

	<!-- ... end Main content + sidebar-->

</div>
@endsection

@section('popups')
    @component('components.sendMessagePopup')
    @endcomponent

    @component('components.searchPopup')
    @endcomponent
@endsection

@section('svgs')
  @component('components.svgIcons')
  @endcomponent
@endsection

@section('scripts-files')
@endsection

@section('scripts-code')
    <script>
    var user = {!! $user !!};
    var chAdminUsers = {!! json_encode($chAdminUsers) !!};
    var followUsers = {!! json_encode($followUsers) !!};
    var userPhotoDefault = "/img/profile/default.png";
    var totalNewMessages = {!! $totalNewMessages !!};
    var myChannelCnt = {!! $myChannelCnt !!};
    var mySubscribedChannelCnt = {!! $mySubscribedChannelCnt !!};
    var myRegisteredTutorialCnt = {!! $myRegisteredTutorialCnt !!};
    var myInterestTutorialCnt = {!! $myInterestTutorialCnt !!};

    window.onload = function () {

    }
    </script>


    <script src="{{ asset('js/pages/user/mywave.js') }}"></script>
@endsection
