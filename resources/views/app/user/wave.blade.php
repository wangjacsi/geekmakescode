@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{ asset("plugins/devicon/devicon.min.css") }}">
    <link rel="stylesheet" href="{{ asset("plugins/cropperjs/cropper.min.css") }}">

    <style>
        .social-btn-group {
            margin-bottom:10px;
        }
        .w-author .js-message-popup {
            margin-top: 20px;
        }
        .members-list{
            margin:0;
        }
        .block-rounded-shadow > div:not(:first-child) {
            margin-top:80px;
        }

        .dropdown li {
          border-bottom: 1px solid rgba(112, 128, 144, 0.1);
        }

        .dropdown li:last-child {
          border-bottom: none;
        }

         .dropdown li a {
          display: flex;
          width: 100%;
          align-items: center;
          font-size: 1.5em;
          line-height: 40px;
        }

         .dropdown li a .fa {
             font-size: 1.5em;
             line-height: 40px;
          padding-right: 0.5em;
        }
    </style>

    <link rel="stylesheet" href="{{ asset("plugins/ion.rangeSlider/css/ion.rangeSlider.css") }}">
    <link rel="stylesheet" href="{{ asset("plugins/ion.rangeSlider/css/ion.rangeSlider.skinFlat.css") }}">
@endsection

@section('content')
<div class="content-wrapper">

	<!-- Stunning Header -->

	<div class="crumina-stunning-header stunning-header--bg-photo stunning-header--breadcrumbs-bottom-left stunning-header--content-padding100 custom-color c-white fill-white" style="background-image:url({{ $user->getHeadImage() }})">
		<div class="container">
			<div class="stunning-header-content">
				<h6 class="category-link c-yellow-light">Training</h6>
				<h4 class="stunning-header-title">@{{ user.status_message}}</h4>
				<div class="icon-text-item display-flex">
					<div class="text"><i class="far fa-calendar-alt"></i> @{{moment(user.created_at).format('MMMM Do YYYY')}}</div>
				</div>

				<div class="breadcrumbs-wrap inline-items">
					<a href="/mywave" class="btn btn--black btn--round breadcrumbs-custom">
						<svg class="utouch-icon utouch-icon-home-icon-silhouette breadcrumbs-custom"><use xlink:href="#utouch-icon-home-icon-silhouette"></use></svg>
					</a>

                    <ul v-if="owner" class="breadcrumbs breadcrumbs--rounded">
						<li class="breadcrumbs-item">
							<a href="/mywave" class="breadcrumbs-custom">마이웨이브</a>
							<svg class="utouch-icon utouch-icon-media-play-symbol breadcrumbs-custom"><use xlink:href="#utouch-icon-media-play-symbol"></use></svg>
						</li>
						<li class="breadcrumbs-item active">
							<span class="breadcrumbs-custom">개인 정보</span>
							<svg class="utouch-icon utouch-icon-media-play-symbol breadcrumbs-custom"><use xlink:href="#utouch-icon-media-play-symbol"></use></svg>
						</li>
					</ul>
                    <ul v-else class="breadcrumbs breadcrumbs--rounded">
						<li class="breadcrumbs-item">
							<a :href="'/wave/'+user.slug" class="breadcrumbs-custom">@{{user.name}}</a>
							<svg class="utouch-icon utouch-icon-media-play-symbol breadcrumbs-custom"><use xlink:href="#utouch-icon-media-play-symbol"></use></svg>
						</li>
						<li class="breadcrumbs-item active">
							<span class="breadcrumbs-custom">개인 정보</span>
							<svg class="utouch-icon utouch-icon-media-play-symbol breadcrumbs-custom"><use xlink:href="#utouch-icon-media-play-symbol"></use></svg>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="overlay-standard overlay--yellow"></div>
	</div>

	<!-- ... end Stunning Header -->


	<!-- Main content + sidebar-->

	<section class="medium-padding100">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">

                    <!-- 나의 채널 정보 -->
                    <div v-if="mainView == 'user-info'" id="user-info" class="block-rounded-shadow">
                        <div v-if="options.viewoptions.mychannels" class="" id="mychannels">
                            <h5 class="line-block">운영 웨이브 채널</h5>
                            <div class="case-item-wrap">
                				<div class="row channel-lists" id="items-grid">
                                    <div class="curriculum-event-wrap">
                                        <my-channels get-info-url="/getChannelsInfoByUserId/{{$user->slug}}?without=tutorial&page=" from-comp='mychannels' :view-options-prop="myChViewOptions" channel-logo-default="/img/channels/logo_image.png" channel-head-default="/img/channels/head_image.png"
                                        user-photo-default="/img/profile/default.png"></my-channels>
                                    </div>
                                </div>
                            </div>
                            <div class="or none"></div>
                        </div>

                        <div v-if="options.viewoptions.mytutorials" class="" id="mytutorials">
                            <h5 class="line-block">운영 강좌</h5>
                            <div class="case-item-wrap">
                				<div class="row tutorial-lists" id="items-grid">
                                    <div class="curriculum-event-wrap">
                                        <my-tutorials channel-logo-default="/img/channels/logo_image.png" thumbnail-default="/img/tutorials/thumbnail.jpg" :view-options-prop="myTutorialsOptions" comp-name='mytutorials' url-path=
                                        "/getMyTutorialsInfoByUserId/{{$user->slug}}" background-colors-prop="{{ json_encode($backgroundColors) }}" progress-status-prop="{{ json_encode($progressStatus) }}"></my-tutorials>
                                    </div>
                                </div>
                            </div>
                            <div class="or none"></div>
                        </div>

                        <div v-if="options.viewoptions.subscribedChannels" class="" id="subscribedChannels">
                            <h5 class="line-block">구독 웨이브 채널</h5>
                            <div class="case-item-wrap">
                				<div class="row channel-lists" id="items-grid">
                                    <div class="curriculum-event-wrap">
                                        <my-subscribed-channels get-info-url="/getSubscribedChannelsInfoByUserId/{{$user->slug}}?without=tutorial&page=" from-comp='mysubscribedchannels' :view-options-prop="mySubscribedChViewOptions" channel-logo-default="/img/channels/logo_image.png" channel-head-default="/img/channels/head_image.png"
                                        user-photo-default="/img/profile/default.png"></my-subscribed-channels>
                                    </div>
                                </div>
                            </div>
                            <div class="or none"></div>
                        </div>

                        <div v-if="options.viewoptions.registeredTutorials" class="" id="registeredTutorials">
                            <h5 class="line-block">수강 강좌</h5>
                            <div class="case-item-wrap">
                				<div class="row tutorial-lists" id="items-grid">
                                    <div class="curriculum-event-wrap">
                                        <my-tutorials channel-logo-default="/img/channels/logo_image.png"  thumbnail-default="/img/tutorials/thumbnail.jpg" :view-options-prop="myRegisteredTutorialsOptions" comp-name='registeredtutorials' url-path=
                                        "/getRegisteredTutorialsInfoByUserId/{{$user->slug}}" background-colors-prop="{{ json_encode($backgroundColors) }}" progress-status-prop="{{ json_encode($progressStatus) }}"></my-tutorials>
                                    </div>
                                </div>
                            </div>
                            <div class="or none"></div>
                        </div>

                        <div v-if="options.viewoptions.interestTutorials" class="" id="interestTutorials">
                            <h5 class="line-block">관심 강좌</h5>
                            <div class="case-item-wrap">
                				<div class="row tutorial-lists" id="items-grid">
                                    <div class="curriculum-event-wrap">
                                        <my-tutorials channel-logo-default="/img/channels/logo_image.png"  thumbnail-default="/img/tutorials/thumbnail.jpg" :view-options-prop="interestTutorialsOptions" comp-name='interesttutorials' url-path=
                                        "/getInterestTutorialsInfoByUserId/{{$user->slug}}" background-colors-prop="{{ json_encode($backgroundColors) }}" progress-status-prop="{{ json_encode($progressStatus) }}"></my-tutorials>
                                    </div>
                                </div>
                            </div>
                            <div class="or none"></div>
                        </div>

                        <div v-if="options.viewoptions.tech" class="row tech-lists" id="techlists">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h5 class="line-block">보유 기술</h5>
                                <tech :techs-prop="options.tech" url-path="/wave/{{$user->slug}}/tech" :view-options-prop="myTechOptions" :owner="owner"></tech>
                                <div class="tech-form-button">
                                    <a @click.prevent="detailTechView" id="detailTechViewButton" class=""><i class="fas fa-glasses"></i> 상세 보기</a>
                                </div>
                            </div>
                        </div>

                        <div v-if="!options.viewoptions.tech && !options.viewoptions.registeredTutorials && !options.viewoptions.subscribedChannels && !options.viewoptions.mytutorials && !options.viewoptions.mychannels" class="" style="min-height:150px;">
                            <h5 class="line-block" >정보가 비공개 설정되어 있습니다</h5>

                        </div>
					</div>

                    <div id="user-profile" v-if="mainView == 'user-profile' && owner" class="wave-user-profile block-rounded-shadow">
                        <div class="">
                            <ul class="tabs-with-line" role="tablist">
                                <li role="presentation" class="tab-control active">
    								<a href="#profile" role="tab" data-toggle="tab" class="control-item" aria-expanded="false">프로필 정보 변경</a>
    							</li>
                                <li role="presentation" class="tab-control">
    								<a href="#contacts" role="tab" data-toggle="tab" class="control-item" aria-expanded="false">연락처 & SNS</a>
    							</li>
    							<li role="presentation" class="tab-control">
    								<a href="#password" role="tab" data-toggle="tab" class="control-item" aria-expanded="true">비밀번호 변경</a>
    							</li>
    							<li role="presentation" class="tab-control">
    								<a href="#settings" role="tab" data-toggle="tab" class="control-item" aria-expanded="false">사용 설정</a>
    							</li>
    						</ul>
                            <div class="tab-content">
    							<div role="tabpanel" class="tab-pane fade in active" id="profile">
    								<profile :user-info="user" url-path="/wave/{{$user->slug}}/profile"></profile>
    							</div>
    							<div role="tabpanel" class="tab-pane fade" id="contacts">
    								<contact sns-brands-prop="{{ json_encode($snsBrands) }}" :user-info="user" url-path="/wave/{{$user->slug}}/sns"></contact>
    							</div>
    							<div role="tabpanel" class="tab-pane fade" id="password">
    								<password url-path="/wave/{{$user->slug}}/password"></password>
    							</div>
    							<div role="tabpanel" class="tab-pane fade" id="settings">
    								<settings url-path="/wave/{{$user->slug}}/settings" :settings-prop="options.viewoptions"></settings>
    							</div>
    						</div>

                        </div>
                    </div>
				</div>



                <!-- sidebar -->
				<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
					<aside aria-label="sidebar" class="sidebar sidebar-right">
						<aside class="widget w-author">
                            <a href="" v-if="owner" @click.prevent="profileEditFormView" id="profileEditFormViewButton" class="c-primary" style="float: right;"><i class="fas fa-user-edit"></i> 프로필 수정</a>

							<div class="testimonial-img-author">
								<img src="{{ $user->getPhoto() }}" alt="author">
							</div>

							<a href="/wave/{{$user->slug}}" class="h5 title" style="margin-bottom:0;">@{{ user.name }}</a>

                            <p>@{{user.job}}</p>

							<p v-html="sanitizeMarkedBody(user.aboutme)" v-highlightjs></p>

                            <div v-if="options.viewoptions.sns && snsLists.length > 0" class="social-btn-group">

                                <a v-for="snsList in snsLists" target="_blank" :href="snsList.link" class="social-icon" v-bind:style="{ color: snsList.color }" :title="snsList.name"><span :class="snsList.class"></span></a>
                            </div>

                            <div v-if="options.viewoptions.contacts" class="">
                                <div v-if="options.phone" class="contact-item display-flex">
    								<svg class="utouch-icon utouch-icon-telephone-keypad-with-ten-keys"><use xlink:href="#utouch-icon-telephone-keypad-with-ten-keys"></use></svg>
    								<span class="info">@{{ options.phone }}</span>
    							</div>

    							<div class="contact-item display-flex">
    								<svg class="utouch-icon utouch-icon-message-closed-envelope-1"><use xlink:href="#utouch-icon-message-closed-envelope-1"></use></svg>
    								<span class="info">{{ $user->email }}</span>
    							</div>

                                <div v-if="options.site" class="contact-item display-flex">
    								<i class="fas fa-archway utouch-icon"></i>
    								<span class="info"><a target="_blank" v-bind:href="options.site">@{{ options.site }}</a></span>
    							</div>
                            </div>

							<a v-if="!owner" @click.prevent="userMessagePopup" href="" class="btn btn-small full-width btn--green btn--with-shadow cd-nav-trigger ">
								메세지 보내기
							</a>
						</aside>

					</aside>
				</div>
			</div>
		</div>
	</section>

	<!-- ... end Main content + sidebar-->

</div>
@endsection

@section('popups')
    @component('components.alarm', ['title'=>'', 'content'=>''])
    @endcomponent

    <usermessagepopup to-user={{ $user->slug }} url-path='/userSendMessage/{{$user->slug}}' popup-id='user-message' v-bind:user-logedin={{Auth::check() ? 'true' : 'false'}}></usermessagepopup>

    @component('components.searchPopup')
    @endcomponent
@endsection

@section('svgs')
  @component('components.svgIcons')
  @endcomponent
@endsection

@section('scripts-files')
@endsection

@section('scripts-code')
    <script>
    var userLoginStatus = {{ Auth::check() ? 'true' : 'false' }};
    var userPhotoDefault = "/img/profile/default.png";
    var options = {!! $options !!};
    var owner = {!! $owner ? 'true' : 'false' !!};
    var user = {!! $user !!};

    window.onload = function () {
    }
    </script>

    <script src="{{ asset('plugins/cropperjs/cropper.min.js')}}"></script>

    <script src="{{ asset('plugins/ion.rangeSlider/js/ion.rangeSlider.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery.pwstrength.bootstrap/pwstrength-bootstrap.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery.pwstrength.bootstrap/zxcvbn.js') }}"></script>

    <script src="{{ asset('js/commonFcns.js') }}"></script>
    <script src="{{ asset('js/pages/user/wave.js') }}"></script>

@endsection

@section('scripts-code-after')
@endsection
