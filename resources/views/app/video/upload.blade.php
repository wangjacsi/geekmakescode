@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{ asset("plugins/cropperjs/cropper.min.css") }}">

    <!-- Froala Editor -->
    <link rel="stylesheet" href="{{ asset('plugins/codemirror.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/froala_editor_2.8.4/css/froala_editor.pkgd.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/froala_editor_2.8.4/css/froala_style.min.css') }}">

    <!-- Vue-multiselect -->
    <link rel="stylesheet" href="{{ asset('plugins/vue-multiselect/vue-multiselect.min.css') }}">
@endsection

@section('content')
<div class="crumina-stunning-header stunning-header--breadcrumbs-bottom-left stunning-bg-clouds stunning-header--content-inline " style="margin-bottom: 100px">
    <div class="container">
        <div class="stunning-header-content">
            <div class="inline-items">
                <h4 class="stunning-header-title">비디오 업로드</h4>
                 <!--<a href="15_pricing_tables.html" class="btn btn--green btn--with-shadow f-right">
                     Get trial version
                 </a>-->
            </div>
            <div class="breadcrumbs-wrap inline-items">
                <a href="#" class="btn btn--primary btn--round">
                    <svg class="utouch-icon utouch-icon-home-icon-silhouette"><use xlink:href="#utouch-icon-home-icon-silhouette"></use></svg>
                </a>

                <ul class="breadcrumbs breadcrumbs--rounded">
                    <li class="breadcrumbs-item">
                        <a href="08_events.html">Events</a>
                        <svg class="utouch-icon utouch-icon-media-play-symbol"><use xlink:href="#utouch-icon-media-play-symbol"></use></svg>
                    </li>
                    <li class="breadcrumbs-item active">
                        <span>Presentation of the Utouch app</span>
                        <svg class="utouch-icon utouch-icon-media-play-symbol"><use xlink:href="#utouch-icon-media-play-symbol"></use></svg>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="container">
  <div class="row" style="margin: 0 0 75px 0">
      <div class="col-lg-8" style="float: none; margin: 0 auto">
          <div class="crumina-module crumina-heading">
              <h5 class="heading-sup-title">비디오 강좌를 업로드 하세요. 구독자들이 당신의 강좌를 기다리고 있어요~!!</h5>
              <hr />

              <form class="channel-form">

                  <video-upload></video-upload>

              </form>

          </div>
      </div>
  </div>
</div>
@endsection

@section('popups')
    @component('components.alarm', ['title'=>'', 'content'=>''])
    @endcomponent

    @component('components.sendMessagePopup')
    @endcomponent

    @component('components.searchPopup')
    @endcomponent
@endsection

@section('svgs')
  @component('components.svgIcons')
  @endcomponent
@endsection


@section('scripts-code')
    <script>
        var channels = {!! $channels !!};
    </script>
    <script src="{{ asset('plugins/cropperjs/cropper.min.js')}}"></script>

    <!-- Froala Editor -->
    <!-- Include external JS libs. -->
    <script src="{{ asset('plugins/codemirror.min.js') }}"></script>
    <script src="{{ asset('plugins/xml.min.js') }}"></script>

    <!-- Include Editor JS files. -->
    <script src="{{ asset('plugins/froala_editor_2.8.4/js/froala_editor.pkgd.min.js') }}"></script>
    <script src="{{ asset('plugins/froala_editor_2.8.4/js/languages/ko.js') }}"></script>
  <script src="{{ asset('js/pages/video/upload.js') }}"></script>
@endsection
