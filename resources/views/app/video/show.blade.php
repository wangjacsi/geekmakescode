@extends('layouts.app')

@section('styles')
    <style>
        .crumina-stunning-header .stunning-header-content.embed-video{
            padding:0;
        }
        .header.slideUp #menu-hide{
            display:none !important;
        }
        #video-text-content{
            margin-top:50px;
        }
        .video-info {
            display: none;
        }
        .author-info {
            min-height: 150px;
        }
        .comments {
            margin-bottom:50px;
        }
        .modal-backdrop {
            background-color: rgba(0, 130, 215, 0.5);
        }
        .blog-details-author .img-author img{
            width:150px;
        }
        .w-tags .tags-list{
            margin-left: 0px;
            margin-bottom: 0px;
        }
        .w-tags a{
            padding: 10px 14px;
            text-transform: none;
            border: 1px solid #d6dfeb;
        }
        .w-tags .tags-list li{
            float: none;
            display: inline-block;
            vertical-align: middle;
            line-height: 1;
            margin-left:0;
            margin-right:5px;
        }


        .w-category .category-list a{
            font-size: 16px;
            padding: 8px 0 8px 20px;
        }
        .w-category .category-list a.active{
            font-weight: bold;
        }

        .w-category .category-list .playtime{
            color: #0083ff;
            font-size: 13px;
            float: right;
        }
        .w-category .category-list li a.curr-watch{
            opacity: 0.5;
        }
        .w-category .category-list li a.curr-watch:before{
            background-color: #01a2a0;
        }

        .swiper-container .product-item-content .title{
            margin-bottom: 20px;
        }
        .swiper-container .details div{
            display: inline-block;
        }
        .swiper-container .details div:not(:last-child){
            margin-right:10px;
        }

        .pagination-arrow .btn-content-subtitle{
            width: 150px;
            white-space: nowrap;
            overflow: hidden !important;
            text-overflow: ellipsis;
        }

    </style>
    <!-- Froala Editor -->
    <link rel="stylesheet" href="{{ asset('plugins/codemirror.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/froala_editor_2.8.4/css/froala_editor.pkgd.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/froala_editor_2.8.4/css/froala_style.min.css') }}">

    <!-- highlight -->
    <link rel="stylesheet" href="{{ asset('plugins/highlight.css') }}">
@endsection

@section('meta_tags')
    @php
    $meta = [
        'title' => $tutorial->title,
        //'description' => $tutorial->description,
        'image' => $tutorial->getThumbnail('thumbnail'),
        'tags' => ''
    ];
    @endphp
    @isset($tutorialTags)
        @php
        $meta['tags'] = implode(',', $tutorialTags);
        @endphp
    @endisset
@endsection

@section('content')
<div class="content-wrapper">
    <div class="video-info">
        <input id="videoUID" type="text" name="" value="{{$video->uid}}" readonly disabled>
    </div>

    <div class="crumina-stunning-header stunning-header--content-center stunning-header--breadcrumbs-bottom-center stunning-bg-3 stunning-header--bg-photo stunning-header--min560 align-center custom-color c-white fill-white">
		<div class="container">
			<div class="stunning-header-content embed-video">
                <div class="embed-fit-size" id="embed-fit-size">
            		<div class='embed-container'>
                        @php
                        if($video->vimeo_url != ''){
                            $temp = explode('/', $video->vimeo_url);
                            $vimeoID = end($temp);
                        }
                        @endphp
                        <iframe src="https://player.vimeo.com/video/{{$vimeoID}}" frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
                    </div>
                </div>
			</div>
		</div>
        <div class="overlay-standard overlay--dark"></div>
	</div>



    <div class="crumina-stunning-header crumina-stunning-header--small stunning-header--content-inline bg-black">
		<div class="container">
			<div class="stunning-header-content c-white custom-color">
				<div class="inline-items">
					<h4 class="stunning-header-title">{{ $tutorial->title }}</h4>

                    <div class="post-additional-info f-right">
                        @if($tutorial->votesAllowed())
                        <video-vote video-uid="{{ $video->uid }}"></video-vote>
                        @endif
                        <span class="post__author author vcard">
                            <a class="fn">{{ $video->viewCount() }} {{ str_plural('view', $video->viewCount() ) }}</a>
                        </span>
                        <span class="post__comments">
                            <a>@{{ commentsLen }} <span>@{{ commentsLen | pluralize('comment') }}</span></a>
                        </span>
                    </div>
				</div>
			</div>
		</div>
	</div>

    <!--
    <div class="container">
		<div class="row">
			<div class="breadcrumbs-wrap inline-items with-border">
				<a href="#" class="btn btn--transparent btn--round">
					<svg class="utouch-icon utouch-icon-home-icon-silhouette"><use xlink:href="#utouch-icon-home-icon-silhouette"></use></svg>
				</a>

				<ul class="breadcrumbs">
					<li class="breadcrumbs-item active">
						<a href="index.html">{{ $video->viewCount() }} {{ str_plural('view', $video->viewCount() ) }}</a>
						<svg class="utouch-icon utouch-icon-media-play-symbol"><use xlink:href="#utouch-icon-media-play-symbol"></use></svg>
					</li>
					<li class="breadcrumbs-item active">
						<span>News</span>
						<svg class="utouch-icon utouch-icon-media-play-symbol"><use xlink:href="#utouch-icon-media-play-symbol"></use></svg>
					</li>
				</ul>
			</div>
		</div>
	</div>-->

    @if(($video->visibility == 'private' || $tutorial->visibility == 'private') && $owned)
        <div class="container">
            <div class="alert alert-info alert-dismissable" style="text-align:center;">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button" style="right:0; color:#fff; opacity:1;">×</button>
                현재 비디오는 비공개 설정되어 있습니다. 오직 당신만 볼 수 있습니다.
            </div>
        </div>
    @endif

    <div class="container" id="video-text-content">
		<div class="row">

			<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
				<article class="hentry post post-standard has-post-thumbnail sticky post-standard-details">

                    <div class="post__content">
                        <div class="video-description" v-highlightjs>
                            {!! $video->description !!}
                        </div>


						<div class="post-details-shared">
                            @if(count($videoTags))
                            <div class="widget w-tags">
                                <ul class="tags-list">
                                    <li>Tags:</li>
                                    @foreach($videoTags as $tag)
                                        <li>
                                            <a>{{$tag}}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif

                            <div class="widget w-follow">
                                <ul class="socials socials-share socials--round">
                                    <li>Share:</li>
                                    <li>
                                        <a class="social__item ttt" style="background-color: #1da1f2;" data-sharer="twitter" data-title="{{$tutorial->title}}" data-hashtags="codewwwave,코드웨이브," data-url="https://codewwwave.test/tutorial/{{$tutorial->slug}}" data-via="@codewwwave">
                                            <span class="utouch-icon fab fa-twitter"></span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="social__item ttt" style="background-color: #3b5998;" data-sharer="facebook" data-url="https://codewwwave.test/tutorial/{{$tutorial->slug}}">
                                            <span class="utouch-icon fab fa-facebook-f"></span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="social__item ttt" style="background-color: #dd4b39;" data-sharer="googleplus" data-url="https://codewwwave.test/tutorial/{{$tutorial->slug}}">
                                            <span class="utouch-icon fab fa-google-plus-g"></span>
                                        </a>
                                    </li>
                                </ul>
                            </div>

						</div>
					</div>


				</article>

				<div class="blog-details-author">

					<div class="img-author">
						<img src="{{ $channel->getThumbnail('logo_image') }}" alt="author">
					</div>
					<div class="author-info">
						<div>
                            <a href="/channel/{{$channel->slug}}" class="h6 author-name" style="">{{ $channel->name }}</a>
                            <subscribe-button channel-slug="{{ $channel->slug }}"></subscribe-button>
                        </div>
                        <div v-if="settings.viewoptions.contacts && settings.phone" class="contact-item display-flex">
							<svg class="utouch-icon utouch-icon-telephone-keypad-with-ten-keys"><use xlink:href="#utouch-icon-telephone-keypad-with-ten-keys"></use></svg>
							<span class="info">@{{settings.phone}}</span>
						</div>

						<div v-if="settings.viewoptions.contacts && settings.email" class="contact-item display-flex">
							<svg class="utouch-icon utouch-icon-message-closed-envelope-1"><use xlink:href="#utouch-icon-message-closed-envelope-1"></use></svg>
							<span class="info">@{{settings.email}}</span>
						</div>

                        <div v-if="settings.viewoptions.contacts && settings.site" class="contact-item display-flex">
							<i class="fas fa-archway utouch-icon"></i>
							<span class="info"><a :href="settings.site" target="_blank">@{{settings.site}}</a></span>
						</div>

                        <ul v-if="settings.viewoptions.sns && snsLists.length > 0" class="social-btn-group socials">
                            <li v-for="snsList in snsLists" >
                                <a target="_blank" :href="snsList.link" class="social-icon" v-bind:style="{ color: snsList.color }" :title="snsList.name"><span :class="snsList.class"></span></a>
                            </li>
                        </ul>
					</div>

				</div>



				<div class="pagination-arrow">

					<a :href="prevVideo ? '/video/'+prevVideo.uid : '#'" class="btn-prev-wrap">
						<div class="btn-prev">
							<svg class="utouch-icon icon-hover utouch-icon-arrow-left-1"><use xlink:href="#utouch-icon-arrow-left-1"></use></svg>
							<svg class="utouch-icon utouch-icon-arrow-left1"><use xlink:href="#utouch-icon-arrow-left1"></use></svg>
						</div>
						<div class="btn-content">
							<div class="btn-content-title">이전 강좌</div>
							<p class="btn-content-subtitle">@{{prevVideo ? prevVideo.title : '처음 강좌입니다'}}</p>
						</div>
					</a>

					<a class="list-post" >
						<svg class="utouch-icon utouch-icon-menu-1"><use xlink:href="#utouch-icon-menu-1"></use></svg>
					</a>

					<a :href="nextVideo ? '/video/'+nextVideo.uid : '#'" class="btn-next-wrap">
						<div class="btn-content">
							<div class="btn-content-title">다음 강좌</div>
							<p class="btn-content-subtitle">@{{nextVideo ? nextVideo.title : '마지막 강좌입니다'}}</p>
						</div>
						<div class="btn-next">
							<svg class="utouch-icon icon-hover utouch-icon-arrow-right-1"><use xlink:href="#utouch-icon-arrow-right-1"></use></svg>
							<svg class="utouch-icon utouch-icon-arrow-right1"><use xlink:href="#utouch-icon-arrow-right1"></use></svg>
						</div>
					</a>

				</div>

                @if($tutorial->allow_comments)
                    <video-comments video-uid="{{ $video->uid }}" @setcommentscount="changeCommentCount"></video-comments>
                @endif

			</div>


			<!-- Sidebar-->

			<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
				<aside aria-label="sidebar" class="sidebar sidebar-right">

                    <aside class="widget w-contacts">
						<h5 class="widget-title">{{ $tutorial->title }}</h5>
						<div class="full-width" >
                            <a href="/tutorial/{{$tutorial->slug}}"><img src="{{ $tutorial->getThumbnail('thumbnail') }}" alt=""></a>
                        </div>
					</aside>


					<aside class="widget w-category">
						<h5 class="widget-title">커리큘럼</h5>
						<ul class="category-list" v-if="videos.length > 0">
							<li v-for="(videolist, index) in videos" :key="videolist.uid">
								<a :href="'/video/'+videolist.uid" :class="{'active' : video.id == videolist.id, 'curr-watch' :videolist.watch}">@{{videolist.title}}
									<span class="playtime"><i class="xi-time-o xi-x"></i> @{{transTimeToPlaytime(videolist.playtime)}}</span>
								</a>
							</li>
						</ul>
                        <p v-else>강좌 커리큘럼 리스트가 비어있습니다</p>
					</aside>

					<aside class="widget w-popular-products crumina-module crumina-module-slider">
						<h5 class="widget-title">인기 강좌</h5>
						<div class="swiper-container">
							<div class="swiper-wrapper">

                                <div v-for="(tutorial, index) in recommendTutorials" class="swiper-slide product-item">
									<div class="product-item-thumb">
										<div class="square-colored"></div>
										<img :src="tutorial.thumbnail ? tutorial.thumbnail : '/img/tutorials/thumbnail.jpg'" alt="image">
									</div>
									<div class="product-item-content">
										<h6 class="title"><a :href="'/tutorial/'+tutorial.slug">@{{ tutorial.title }}</a></h6>
									</div>

                                    <div class="product-item-content details">
                                        <div class="icon-text-item ">
                                            <div class="text" title="총 비디오 수">
                                                <i class="fas fa-play-circle"></i> @{{ tutorial.videos_by_visible.length }} 비디오
                                            </div>
                                            <div class="text" title="총 플레이 시간">
                                                <i class="fas fa-clock"></i> @{{ transVideoPlayTime(tutorial.videos_by_visible) }}
                                            </div>
                                            <div class="text" title="수강자 수">
                                                <i class="fas fa-swimmer"></i> @{{ tutorial.registrations_count }} 명
                                            </div>
                                        </div>
                                    </div>
								</div>
							</div>
							<!-- If we need pagination -->
							<div class="swiper-pagination"></div>

						</div>
					</aside>


                    @if(count($tutorialTags))
                    <aside class="widget w-tags">
                        <h5 class="widget-title">Tags</h5>
                        <ul class="tags-list">
                            @foreach($tutorialTags as $tag)
                                <li>
                                    <a>{{$tag}}</a>
                                </li>
                            @endforeach
                        </ul>
                    </aside>
                    @endif
				</aside>
			</div>

			<!-- End Sidebar-->

		</div>
	</div>
</div>
@endsection

@section('popups')
    @component('components.sendMessagePopup')
    @endcomponent

    @component('components.searchPopup')
    @endcomponent

    @component('components.confirm', ['title' => '평가 취소', 'content' => '"좋아요" 평가를 취소하시겠습니까?'])
    @endcomponent

    @component('components.confirm', ['id' => 2, 'title' => '댓글 삭제', 'content' => '댓글을 삭제하시겠습니까?'])
    @endcomponent
@endsection

@section('svgs')
  @component('components.svgIcons')
  @endcomponent
@endsection

@section('scripts-files')
    <script src="{{ asset('plugins/player.js')}}"></script>
    <script type="text/javascript" src="{{ asset('plugins/sharerjs/sharer.min.js')}}"></script>
@endsection

@section('scripts-code')
    <script>
    var videos = {!! $videos !!};
    var video = {!! $video !!};
    var recommendTutorials = {!! $recommendTutorials !!};
    var records = {!! $records !!};
    var channel = {!! $channel !!};

    window.onload = function () {

        $('.header-spacer').height(0);


        $('#site-header').append('<a id="menu-hide" title="메뉴 숨기기" class="btn btn--orange blinking"><i class="fas fa-expand-arrows-alt" style=""></i></a>');

        var didScroll; // 스크롤시에 사용자가 스크롤했다는 것을 알림
        $(window).scroll(function(event){ didScroll = true; }); // hasScrolled()를 실행하고 didScroll 상태를 재설정
        setInterval(function() { if (didScroll) { hasScrolled(); didScroll = false; } }, 250);
        function hasScrolled() {
            if( $('#site-header').hasClass('headroom--top') ){
                $('#menu-hide').css('display', 'block');
            } else {
                $('#menu-hide').css('display', 'none');
             }
        }

        $('#menu-hide').on('click', function(){
            $('#site-header').removeClass('headroom--top');
            $('#site-header').removeClass('slideDown');
            $('#site-header').addClass('headroom--not-top');
            $('#site-header').addClass('slideUp');
            //$( '#site-header' ).css('display' , 'none');
        });

    }
    </script>

    <script src="{{ asset('plugins/cropperjs/cropper.min.js')}}"></script>

    <!-- Froala Editor -->
    <!-- Include external JS libs. -->
    <script src="{{ asset('plugins/codemirror.min.js') }}"></script>
    <script src="{{ asset('plugins/xml.min.js') }}"></script>

    <!-- Include Editor JS files. -->
    <script src="{{ asset('plugins/froala_editor_2.8.4/js/froala_editor.pkgd.min.js') }}"></script>
    <script src="{{ asset('js/pages/video/viewcount.js') }}"></script>
@endsection
