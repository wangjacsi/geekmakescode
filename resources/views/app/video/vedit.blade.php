@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{ asset("plugins/cropperjs/cropper.min.css") }}">

    <!-- Froala Editor -->
    <link rel="stylesheet" href="{{ asset('plugins/codemirror.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/froala_editor_2.8.4/css/froala_editor.pkgd.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/froala_editor_2.8.4/css/froala_style.min.css') }}">

    <!-- Vue-multiselect -->
    <link rel="stylesheet" href="{{ asset('plugins/vue-multiselect/vue-multiselect.min.css') }}">

    <style>
        .thumbnail-list > a{
            margin-right:10px;
            margin-bottom:10px;
            display: inline-block;
            border:3px solid #fff;
            position: relative;
        }
        .thumbnail-list > a a{
            position: absolute;
            cursor: pointer;
            top: -3px;
            right: 5px;
            color: red;
            font-size: 20px;
        }
        .thumbnail-list > a.selected{
            -webkit-box-shadow: inset 0 3px 5px rgba(0,0,0,.125);
            box-shadow: inset 0 3px 5px rgba(0,0,0,.125);
            border:3px solid blue;
        }
        .thumbnail-list > a.select-now{
            border:3px solid red;
        }
    </style>
@endsection

@section('content')
<div class="crumina-stunning-header stunning-header--breadcrumbs-bottom-left stunning-bg-clouds stunning-header--content-inline " style="margin-bottom: 100px">
    <div class="container">
        <div class="stunning-header-content">
            <div class="inline-items">
                <h4 class="stunning-header-title">비디오 수정</h4>
            </div>
            <div class="breadcrumbs-wrap inline-items">
                <a href="#" class="btn btn--primary btn--round">
                    <svg class="utouch-icon utouch-icon-home-icon-silhouette"><use xlink:href="#utouch-icon-home-icon-silhouette"></use></svg>
                </a>

                <ul class="breadcrumbs breadcrumbs--rounded">
                    <li class="breadcrumbs-item">
                        <a href="08_events.html">Events</a>
                        <svg class="utouch-icon utouch-icon-media-play-symbol"><use xlink:href="#utouch-icon-media-play-symbol"></use></svg>
                    </li>
                    <li class="breadcrumbs-item active">
                        <span>Presentation of the Utouch app</span>
                        <svg class="utouch-icon utouch-icon-media-play-symbol"><use xlink:href="#utouch-icon-media-play-symbol"></use></svg>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="container">
  <div class="row" style="margin: 0">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <h5 class="heading-sup-title">{{ $video->title }}</h5>
          <hr />
      </div>

      <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 50px;">

          <div class="video-info block-rounded-shadow" style="padding-top:40px;">
              <div v-if="viewMain=='videoInfo'" class="">
                  <h5 class="line-block">비디오 정보 수정</h5>
                  <videoinfo :video-prop="video"></videoinfo>
              </div>

              <div v-if="viewMain=='videoFile'" class="">
                  <h5 class="line-block">비디오 파일</h5>
                  <p>비디오 변경 시 비디오 파일만 대체 됩니다. 상세 정보는 비디오 정보 수정에서 수정하시고 비디오 이미지(썸네일)는 비디오 이미지 변경을 이용해 주세요. (오른쪽 메뉴를 이용해주세요)</p>
                  <videoreplace :video-prop="video"></videoreplace>
              </div>

              <div v-if="viewMain=='videoThumbnail'" class="">
                  <h5 class="line-block">비디오 이미지</h5>
                  <videothumbnail :video-prop="video"></videothumbnail>
              </div>

          </div>
      </div>

      <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
          <aside aria-label="sidebar" class="sidebar sidebar-right">
              <aside class="widget w-category" style="padding-top:40px;">
                  <h5 class="widget-title">마이 웨이브</h5>
                  <ul class="category-list">
                      <li>
                          <a @click.prevent="changeMainView('videoInfo')" href="" v-bind:class="{active : viewMain=='videoInfo' }">비디오 정보
                          </a>
                      </li>
                      <li>
                          <a @click.prevent="changeMainView('videoFile')" href="" v-bind:class="{active : viewMain=='videoFile'}">비디오 파일
                          </a>
                      </li>
                      <li>
                          <a @click.prevent="changeMainView('videoThumbnail')" href="" v-bind:class="{active : viewMain=='videoThumbnail'}">비디오 이미지
                          </a>
                      </li>
                  </ul>
              </aside>
          </aside>

      </div>
  </div>
</div>

<confirm modal-id='deleteVideoThumb' :content-prop="cdeleteVideoThumbContent" event-emit-name='deleteThumbnail' event-on-name='deleteThumbnailConfirm'></confirm>

@endsection

@section('popups')
    @component('components.alarm', ['title'=>'', 'content'=>''])
    @endcomponent

    @component('components.sendMessagePopup')
    @endcomponent

    @component('components.searchPopup')
    @endcomponent
@endsection

@section('svgs')
  @component('components.svgIcons')
  @endcomponent
@endsection


@section('scripts-code')
    <script>
        var video = {!! $video !!};
        var channels = {!! $channels !!};
        var tags = {!! json_encode($tags) !!};
    </script>
    <script src="{{ asset('plugins/cropperjs/cropper.min.js')}}"></script>

    <!-- Froala Editor -->
    <!-- Include external JS libs. -->
    <script src="{{ asset('plugins/codemirror.min.js') }}"></script>
    <script src="{{ asset('plugins/xml.min.js') }}"></script>

    <!-- Include Editor JS files. -->
    <script src="{{ asset('plugins/froala_editor_2.8.4/js/froala_editor.pkgd.min.js') }}"></script>
    <script src="{{ asset('plugins/froala_editor_2.8.4/js/languages/ko.js') }}"></script>

    <script src="{{ asset('plugins/player.js')}}"></script>
    <script src="{{ asset('js/pages/video/vedit.js') }}"></script>
@endsection
