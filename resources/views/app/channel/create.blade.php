@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{ asset("plugins/cropperjs/cropper.min.css") }}">

    <!-- Froala Editor -->
    <link rel="stylesheet" href="{{ asset('plugins/codemirror.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/froala_editor_2.8.4/css/froala_editor.pkgd.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/froala_editor_2.8.4/css/froala_style.min.css') }}">
@endsection

@section('content')
<div class="crumina-stunning-header stunning-header--breadcrumbs-bottom-left stunning-bg-clouds stunning-header--content-inline " style="margin-bottom: 100px">
    <div class="container">
        <div class="stunning-header-content">
            <div class="inline-items">
                <h4 class="stunning-header-title">채널 만들기</h4>
                 <!--<a href="15_pricing_tables.html" class="btn btn--green btn--with-shadow f-right">
                     Get trial version
                 </a>-->
            </div>
            <div class="breadcrumbs-wrap inline-items">
                <a href="#" class="btn btn--primary btn--round">
                    <svg class="utouch-icon utouch-icon-home-icon-silhouette"><use xlink:href="#utouch-icon-home-icon-silhouette"></use></svg>
                </a>

                <ul class="breadcrumbs breadcrumbs--rounded">
                    <li class="breadcrumbs-item">
                        <a href="08_events.html">Events</a>
                        <svg class="utouch-icon utouch-icon-media-play-symbol"><use xlink:href="#utouch-icon-media-play-symbol"></use></svg>
                    </li>
                    <li class="breadcrumbs-item active">
                        <span>Presentation of the Utouch app</span>
                        <svg class="utouch-icon utouch-icon-media-play-symbol"><use xlink:href="#utouch-icon-media-play-symbol"></use></svg>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="container">
  <div class="row" style="margin: 0 0 75px 0">
      <div class="col-lg-8" style="float: none; margin: 0 auto">
          <div class="crumina-module crumina-heading">
              <h5 class="heading-sup-title">나만의 채널을 만들어 보세요. 여러 개발자들과 소통하는 통로가 될 거에요~!!</h5>
              <hr />

              <form class="channel-form" v-bind:class="{'sk-loading': status=='uploading'}" >
                  <div class="sk-spinner sk-spinner-double-bounce">
                    <div class="sk-double-bounce1"></div>
                    <div class="sk-double-bounce2"></div>
                  </div>

                  <div class="form-group">
                    <label for="name">채널 이름</label>
                    <input type="text" class="" id="name" placeholder="채널 이름"
                    v-model="form.name" >
                    <input type="text" style="display: none;" hidden />
                  </div>
                  <div class="form-group">
                      <label for="headImage">채널 상단 이미지</label>
                      <div class="img-container">
                          <img src="/img/stunning-bg4.jpg" id="headCrop" class="crop-image">
                      </div>
                      <div class="btn-group image-button-group">
                          <a @click.prevent="imageCancel(0)" class="btn btn-small btn--orange-light" style="margin-bottom: 10px; ">이미지 취소
                          </a>
                          <label title="Upload image file" for="headImage" class="btn btn-small btn--primary">
                              <input type="file" accept="image/*" name="headImage" id="headImage" class="hide input-image-file">
                              이미지 선택
                          </label>
                      </div>
                      <p class="help-block">옵션사항입니다. 미지정 시 기본 이미지가 보여집니다.</p>
                  </div>

                  <div class="form-group">
                      <label for="logoImage">채널 로고</label>
                      <div class="img-container">
                          <img src="/img/teammember6.jpg" id="logoCrop" class="crop-image">
                      </div>
                      <div class="btn-group image-button-group">
                          <a @click.prevent="imageCancel(1)" class="btn btn-small btn--orange-light" style="margin-bottom: 10px; ">이미지 취소
                          </a>
                          <label title="Upload image file" for="logoImage" class="btn btn-small btn--primary">
                              <input type="file" accept="image/*" name="logoImage" id="logoImage" class="hide input-image-file">
                              이미지 선택
                          </label>
                      </div>
                      <p class="help-block">옵션사항입니다. 미지정 시 기본 이미지가 보여집니다.</p>
                  </div>
                  <div class="form-group">
                      <label for="description">채널 소개</label>
                      <froala :tag="'textarea'" :config="froalaConfig" v-model="form.description"></froala>
                  </div>

                  <a @click.prevent="upload" class="btn btn-border btn--with-shadow c-orange-light full-width" style="margin-bottom: 10px; ">
                      생성하기
                  </a>


                  <div v-if="this.errorMessage != ''" class="summit-message" style="margin-top:50px;">
                      <p class="summit-error" v-html="this.errorMessage"></p>
                  </div>

              </form>

          </div>
      </div>
  </div>
</div>



@endsection

@section('popups')
    @component('components.alarm', ['title'=>'', 'content'=>''])
    @endcomponent
    
    @component('components.sendMessagePopup')
    @endcomponent

    @component('components.searchPopup')
    @endcomponent
@endsection

@section('svgs')
  @component('components.svgIcons')
  @endcomponent
@endsection


@section('scripts-code')
    <script src="{{ asset('plugins/cropperjs/cropper.min.js')}}"></script>

    <!-- Froala Editor -->
    <!-- Include external JS libs. -->
    <script src="{{ asset('plugins/codemirror.min.js') }}"></script>
    <script src="{{ asset('plugins/xml.min.js') }}"></script>

    <!-- Include Editor JS files. -->
    <script src="{{ asset('plugins/froala_editor_2.8.4/js/froala_editor.pkgd.min.js') }}"></script>
    <script src="{{ asset('plugins/froala_editor_2.8.4/js/languages/ko.js') }}"></script>

  <script src="{{ asset('js/pages/channel/create.js') }}"></script>
@endsection
