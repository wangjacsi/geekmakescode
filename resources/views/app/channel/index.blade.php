@extends('layouts.app')

@section('styles')
    <style>
        #items-grid .tutorial-item{
            margin-bottom:50px;
        }
        .cat-list-bg-style .cat-list__item{
            background-color: #EAEAEA;
        }
    </style>
@endsection

@section('content')
<div class="content-wrapper">
    <section class="medium-padding120">
		<div class="container">
            <div class="row mb60">
                <div class="crumina-module crumina-module-img-bottom">
                    <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
                        <div class="crumina-module crumina-heading">
                            <h6 class="heading-sup-title">채널리스트</h6>
                            <h2 class="heading-title">원하는 기술 스택의 채널을 살펴보세요 ~</h2>
                            <div class="h6 heading-text">기술 강좌를 운영하는 채널들입니다. 마음에 드는 강좌가 있으시다면 해당 채널도 응원해주세요. 더 좋은 강좌가 나올 수 있도록 응원과 긍정의 피드백을 전달해 주세요. 
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-lg-offset-1 col-md-5 col-sm-4 col-sm-offset-0 col-xs-12">
                    <a href="{{ route('channel.create') }}" class="btn btn--green btn--with-shadow full-width mb30">
                            나의 채널 만들기
                        </a>
                    </div>
                </div>
            </div>

            <div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="case-item-wrap">
                        <div class="row align-center">
                            <div class="col-md-12">
                                <ul class="cat-list-bg-style sorting-menu">
            						<li class="cat-list__item" :class="{'active' : orderType == 'name'}"><a href="" @click.prevent="orderBy('name')" class="">채널명</a></li>
            						<li class="cat-list__item" :class="{'active' : orderType == 'subscription'}"><a href="" @click.prevent="orderBy('subscription')" class="">구독자</a></li>
            						<li class="cat-list__item" :class="{'active' : orderType == 'review'}"><a href="" @click.prevent="orderBy('review')" class="">리 뷰</a></li>
            						<li class="cat-list__item" :class="{'active' : orderType == 'registration'}"><a href="" @click.prevent="orderBy('registration')" class="">수강자</a></li>
            					</ul>
                            </div>
                        </div>
        				<div class="row channel-lists" id="items-grid">
                            <div class="curriculum-event-wrap">

                                <channellist from-comp='getchannellistinfo' :view-options-prop="chViewOptions" channel-logo-default="/img/channels/logo_image.png" channel-head-default="/img/channels/head_image.png"
                                user-photo-default="/img/profile/default.png"></channellist>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</div>



@endsection

@section('popups')
    @component('components.sendMessagePopup')
    @endcomponent

    @component('components.searchPopup')
    @endcomponent
@endsection

@section('svgs')
  @component('components.svgIcons')
  @endcomponent
@endsection

@section('scripts-files')
@endsection

@section('scripts-code')
    <script>

    window.onload = function () {
    }
    </script>

    <script src="{{ asset('js/pages/channel/index.js') }}"></script>
@endsection
