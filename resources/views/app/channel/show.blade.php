@extends('layouts.app')


@section('styles')
    <link rel="stylesheet" href="{{ asset("plugins/devicon/devicon.min.css") }}">
    <style>
        .channel-info .channel-count-info span{
            margin-right:10px;
        }
    </style>

@endsection


@section('content')
  <div class="content-wrapper">

  	<!-- Stunning Header -->
  	<div class="crumina-stunning-header stunning-header--content-left-bottom stunning-header--bg-photo stunning-header--min620 custom-color c-white" style="margin-bottom: 100px; background-image:url({{ $channel->getThumbnail('head_image') }});">
  		<div class="container">
  			<div class="stunning-header-content">
  				<!--<h6 class="category-link c-breez">Channel</h6>-->
  				<h2 class="h1 stunning-header-title">{{ $channel->name }}</h2>
  			</div>
  		</div>
        <div class="overlay-standard overlay--dark"></div>
  	</div>

  	<!-- ... end Stunning Header -->


  	<!-- Conference Block -->

  	<section>
  		<div class="container">
  			<div class="row">
  				<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
  					<div class="conference-details">
  						<ul class="conference-details-control tabs-with-line" role="tablist">

  							<li role="presentation" class="tab-control active">
  								<a href="#course-list" role="tab" data-toggle="tab" class="control-item">강좌리스트</a>
  							</li>

  							<li role="presentation" class="tab-control">
  								<a href="#channel-desc" role="tab" data-toggle="tab" class="control-item">채널소개</a>
  							</li>

  							<li role="presentation" class="tab-control">
  								<a href="#author" role="tab" data-toggle="tab" class="control-item">채널관리자</a>
  							</li>

  							<li role="presentation" class="tab-control">
  								<a href="#contact" role="tab" data-toggle="tab" class="control-item">연락하기</a>
  							</li>

  						</ul>

  						<div class="tab-content">
  							<div role="tabpanel" class="tab-pane fade in active" id="course-list">
                                <h3>운영 강좌</h3>
                                <div class="row tutorial-lists" id="items-grid">
                                    <div class="curriculum-event-wrap">
                                        <tutorials channel-logo-default="/img/channels/logo_image.png" thumbnail-default="/img/tutorials/default.jpg" :view-options-prop="tutorialsOptions" comp-name='tutorials' url-path='/getTutorialsInfoByChannel/{{ $channel->slug }}' background-colors-prop="{{ json_encode($backgroundColors) }}" progress-status-prop="{{ json_encode($progressStatus) }}"></tutorials>
                                    </div>
                                </div>
  							</div>
  							<div role="tabpanel" class="tab-pane fade" id="channel-desc">
  								<h3>{{ $channel->name }} 채널</h3>
  								{!! $channel->description !!}
  							</div>
  							<div role="tabpanel" class="tab-pane fade" id="author">
  								<h3>채널 관리자</h3>
                                <userlist user-photo-default="/img/profile/default.png" from-comp='userlist' url-path='/getChannelAdminUsers/{{ $channel->slug }}'></userlist>
  							</div>
  							<div role="tabpanel" class="tab-pane fade" id="contact">
  								<div class="crumina-module crumina-heading">
  									<h2 class="heading-title">메세지 보내기</h2>
  								</div>

  								<form class="contact-form items-with-border" method="post">
  									<div class="row">

  										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <input v-model="title" name="title" placeholder="제 목" type="text">
  										</div>

  										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
  											<textarea @keyup="textLimit" v-model="content" name="message" class="" required="" placeholder="내용을 작성해주세요" style="height: 160px; width:100%; max-width:100%; margin-bottom:0;"></textarea>
                                            <span style="float:right;">@{{ messageLen }} / @{{ maxLen }}</span>
  										</div>


                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">마크다운(Markdown <i class="fab fa-markdown"></i>) 지원합니다. # ~ ######: 헤더 h1 ~ h6, >: 인용 Blockquotes, ~~~ 코드블록 ~~~, ` 인라인 코드 블럭 `, * 기울여쓰기(italic) *, ** 굵게쓰기(bold) **, --- 수평선, [Google](http://www.google.co.kr “구글”) : 인라인 링크, &lt;http://google.com/&gt;: URl 링크, 1. list item : 리스트, * list item : 리스트, ![alt text](image_URL): 링크 이미지
      									</div>

  										<div style="margin-top:20px;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
  											<a @click.prevent="sendMessage" href="" class="btn btn--primary btn--with-shadow">
  												메세지 전송
  											</a>
  										</div>

                                        <div v-if="errorMessage != ''" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 summit-message" style="margin-top:50px;">
                                            <p class="summit-error" v-html="errorMessage"></p>
                                        </div>



  									</div>
  								</form>

  							</div>
  						</div>
  					</div>
  				</div>
  				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
  					<div class="counting-down channel-info">
  						<div class="author-block inline-items counting-header" style="vertical-align:middle;">
                            <div class="author-avatar" style="width:80px; height:80px;">
                                <img src="{{ $channel->getThumbnail('logo_image') }}" alt="author">
                            </div>
                            <div class="author-info">
                                <h6 class="title">{{ $channel->name}}</h6>
                            </div>
  						</div>

                        <div class="counting-date" v-if="(snsLists.length > 0 && settings.viewoptions.sns) || (settings.viewoptions.contacts && (settings.phone || settings.email || settings.site) )">
  							<!--<div class="icon-text-item display-flex">
  								<div class="text"><i class="far fa-calendar-alt"></i> 채널오픈일: @{{ channelCreated() }}</div>
                            </div>-->
                            <div>
                                <div v-if="snsLists.length > 0" class="social-btn-group">
                                    <a v-for="snsList in snsLists" target="_blank" :href="snsList.link" class="social-icon" v-bind:style="{ color: snsList.color }" :title="snsList.name"><span :class="snsList.class"></span></a>
                                </div>

                                <div v-if="settings.viewoptions.contacts" class="" style="margin-top:10px">
                                    <div v-if="settings.phone" class="contact-item display-flex">
        								<svg class="utouch-icon utouch-icon-telephone-keypad-with-ten-keys"><use xlink:href="#utouch-icon-telephone-keypad-with-ten-keys"></use></svg>
        								<span class="info">@{{ settings.phone }}</span>
        							</div>

        							<div v-if="settings.email" class="contact-item display-flex">
        								<svg class="utouch-icon utouch-icon-message-closed-envelope-1"><use xlink:href="#utouch-icon-message-closed-envelope-1"></use></svg>
        								<span class="info">@{{ settings.email }}</span>
        							</div>

                                    <div v-if="settings.site" class="contact-item display-flex">
        								<i class="fas fa-archway utouch-icon"></i>
        								<span class="info"><a target="_blank" v-bind:href="settings.site">@{{ settings.site }}</a></span>
        							</div>
                                </div>
  							</div>
  						</div>
  						<div class="counting-footer">
  							<div class="author-block inline-items channel-count-info" style="margin-bottom:20px;">
  								<span>총강좌수: {{$totalTutorials}}</span>
                                <span>총수강자수: {{$totalRegisterdTutorials}}</span>
                                <span>구독자수: @{{subscriptionCount}}</span>
  							</div>

  							<a v-if="!owner" @click.prevent="subscribeHandle" href="" class="btn btn-small full-width btn--with-shadow" v-bind:class="{'btn-border c-green': isSubscribed==true, 'btn--green' : isSubscribed==false}">
  								@{{ isSubscribed ? '구독취소' : '구독하기' }}
  							</a>
                            <a v-else :href="'/channel/'+channel.slug+
                            '/edit'" class="btn btn-small btn--grey full-width btn--with-shadow">
  								수정하기
  							</a>
  						</div>
  					</div>

                    <div class="counting-down" style="margin:30px 0;">
                        <aside v-if="owner" class="widget w-contacts">
                            <h5 class="widget-title">강좌 만들기</h5>

                            <p class="contacts-text" style="margin-bottom:0">새로운 강좌를 만들어보세요</p>

                            <div class="" style="margin-top:20px">
                                <img src="/img/tutorials/default.jpg" alt="image">
                                <a href="/tutorial/create" class="btn btn-small full-width btn--primary btn--with-shadow">
                                    강좌 만들기
                                </a>
                            </div>
                        </aside>
                    </div>
  				</div>
  			</div>
  		</div>
  	</section>
  	<!-- ... end Conference Block -->

  </div>
@endsection


@section('popups')
    @component('components.alarm', ['title'=>'', 'content'=>''])
    @endcomponent
    
    @component('components.sendMessagePopup')
    @endcomponent

    @component('components.searchPopup')
    @endcomponent
@endsection

@section('svgs')
  @component('components.svgIcons')
  @endcomponent
@endsection


@section('scripts-code')
<script type="text/javascript" src="{{ asset('js/jquery.countdown.min.js')}}"></script>

<script>
var userLoginStatus = {{ Auth::check() ? 'true' : 'false' }};
var owner = {!! $owner ? 'true' : 'false' !!};
var isSubscribed = {!! $isSubscribed ? 'true' : 'false' !!};
var channel = {!! $channel !!};
var subscriptionCount = {!! $subscriptionCount !!};

$(document).ready( function() {
});
</script>

<script src="{{ asset('js/pages/channel/show.js') }}"></script>
@endsection
