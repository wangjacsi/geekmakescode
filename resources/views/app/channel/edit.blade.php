@extends('layouts.app')


@section('styles')
    <link rel="stylesheet" href="{{ asset("plugins/devicon/devicon.min.css") }}">
    <link rel="stylesheet" href="{{ asset("plugins/cropperjs/cropper.min.css") }}">

    <link rel="stylesheet" href="{{ asset('plugins/codemirror.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/froala_editor_2.8.4/css/froala_editor.pkgd.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/froala_editor_2.8.4/css/froala_style.min.css') }}">
    <style>
        .channel-info .channel-count-info span{
            margin-right:10px;
        }

        .dropdown li {
          border-bottom: 1px solid rgba(112, 128, 144, 0.1);
        }

        .dropdown li:last-child {
          border-bottom: none;
        }

         .dropdown li a {
          display: flex;
          width: 100%;
          align-items: center;
          font-size: 1.5em;
          line-height: 40px;
        }

         .dropdown li a .fa {
             font-size: 1.5em;
             line-height: 40px;
          padding-right: 0.5em;
        }

        .selected img {
          width: auto;
          max-height: 23px;
          margin-right: 0.5rem;
        }

        .v-select .dropdown-menu li .d-center{
            font-size: 15px;
        }

        .v-select .dropdown-menu img{
            width:35px !important;
        }

    </style>
@endsection


@section('content')
  <div class="content-wrapper">
  	<!-- Stunning Header -->
  	<div class="crumina-stunning-header stunning-header--content-left-bottom stunning-header--bg-photo stunning-header--min620 custom-color c-white" style="margin-bottom: 100px; background-image:url({{ $channel->getThumbnail('head_image') }});">
  		<div class="container">
  			<div class="stunning-header-content">
  				<!--<h6 class="category-link c-breez">Channel</h6>-->
  				<h2 class="h1 stunning-header-title">{{ $channel->name }}</h2>
  			</div>
  		</div>
        <div class="overlay-standard overlay--dark"></div>
  	</div>

  	<!-- ... end Stunning Header -->


  	<!-- Conference Block -->
  	<section>
  		<div class="container">
  			<div class="row">
  				<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
  					<div class="conference-details">
  						<ul class="conference-details-control tabs-with-line" role="tablist">

  							<li role="presentation" class="tab-control active">
  								<a href="#tutorials" @click="viewChange('tutorials')" role="tab" data-toggle="tab" class="control-item">강좌 관리</a>
  							</li>

  							<li role="presentation" class="tab-control">
  								<a href="#information" @click="viewChange('information')" role="tab" data-toggle="tab" class="control-item">채널 정보</a>
  							</li>

  							<li role="presentation" class="tab-control">
  								<a href="#admins" @click="viewChange('admins')" role="tab" data-toggle="tab" class="control-item">채널 관리자</a>
  							</li>

  							<li role="presentation" class="tab-control">
  								<a href="#settings" @click="viewChange('settings')" role="tab" data-toggle="tab" class="control-item">채널 설정</a>
  							</li>

  						</ul>

  						<div class="tab-content">
  							<div role="tabpanel" class="tab-pane fade in active" id="tutorials">
                                <h3>강좌 리스트</h3>
                                <div class="row tutorial-lists" id="items-grid">
                                    <div class="curriculum-event-wrap">
                                        <tutorials channel-logo-default="/img/channels/logo_image.png" thumbnail-default="/img/tutorials/default.jpg" :view-options-prop="tutorialsOptions" comp-name='tutorials' url-path='/getTutorialsInfoByChannel/{{ $channel->slug }}' background-colors-prop="{{ json_encode($backgroundColors) }}" progress-status-prop="{{ json_encode($progressStatus) }}"></tutorials>
                                    </div>
                                </div>
  							</div>
  							<div role="tabpanel" class="tab-pane fade" id="information">
                                <h3>채널 기본 정보</h3>
                                <channelinfo v-if="view=='information'" :channel-prop="channel"></channelinfo>

                                <div class="or none"></div>
                                <h3 class="" style="margin-top:50px;">채널 연락처</h3>
                                <channelcontact :channel-prop="channel" sns-brands-prop="{{ json_encode($snsBrands) }}"></channelcontact>
  							</div>
  							<div role="tabpanel" class="tab-pane fade" id="admins">
  								<h3>채널 관리자</h3>
                                <p>슈퍼관리자는 채널 관리자를 변경(추가/해제)할 수 있습니다. 채널에는 반드시 최소 한명의 슈퍼관리자가 있어야 합니다.</p>
                                <userlist user-photo-default="/img/profile/default.png" from-comp='userlist' url-path='/getChannelAdminUsers/{{ $channel->slug }}' :channel-info-prop="channel" :view-options-prop="channelUsersOptions" :super-admin-prop="superAdmin" ></userlist>

                                <div class="or none" style="margin-bottom:50px;">
                                </div>
                                <h5>채널 관리자 추가</h5>
                                <p>관리자 요청 버튼을 누르면 요청 메세지가 해당 유저에게 전달됩니다. 해당 유저가 메세지함에서 확인 후 수락을 하면 관리자 등록이 완료됩니다.</p>
                                <searchlist :view-options-prop="searchListOptions" url-path='/channel/{{ $channel->slug }}/admin/request' :event-on="false"></searchlist>
  							</div>
  							<div role="tabpanel" class="tab-pane fade" id="settings">
  								<h3>채널 설정</h3>
                                <settings url-path='/channel/{{ $channel->slug }}/settings/update' :settings-prop="settings"></settings>
  							</div>
  						</div>
  					</div>
  				</div>
  				<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
  					<div class="counting-down channel-info">
  						<div class="author-block inline-items counting-header" style="vertical-align:middle;">
                            <div class="author-avatar" style="width:80px; height:80px;">
                                <img src="{{ $channel->getThumbnail('logo_image') }}" alt="author">
                            </div>
                            <div class="author-info">
                                <h6 class="title">{{ $channel->name}}</h6>
                            </div>
  						</div>

  						<div class="counting-date" v-if="(snsLists.length > 0 && settings.viewoptions.sns) || (settings.viewoptions.contacts && (settings.phone || settings.email || settings.site) )">
  							<!--<div class="icon-text-item display-flex">
  								<div class="text"><i class="far fa-calendar-alt"></i> 채널오픈일: @{{ channelCreated() }}</div>
                            </div>-->
                            <div>
                                <div v-if="snsLists.length > 0 && settings.viewoptions.sns" class="social-btn-group">
                                    <a v-for="snsList in snsLists" target="_blank" :href="snsList.link" class="social-icon" v-bind:style="{ color: snsList.color }" :title="snsList.name"><span :class="snsList.class"></span></a>
                                </div>

                                <div v-if="settings.viewoptions.contacts" class="" style="margin-top:10px">
                                    <div v-if="settings.phone" class="contact-item display-flex">
        								<svg class="utouch-icon utouch-icon-telephone-keypad-with-ten-keys"><use xlink:href="#utouch-icon-telephone-keypad-with-ten-keys"></use></svg>
        								<span class="info">@{{ settings.phone }}</span>
        							</div>

        							<div v-if="settings.email" class="contact-item display-flex">
        								<svg class="utouch-icon utouch-icon-message-closed-envelope-1"><use xlink:href="#utouch-icon-message-closed-envelope-1"></use></svg>
        								<span class="info">@{{ settings.email }}</span>
        							</div>

                                    <div v-if="settings.site" class="contact-item display-flex">
        								<i class="fas fa-archway utouch-icon"></i>
        								<span class="info"><a target="_blank" v-bind:href="settings.site">@{{ settings.site }}</a></span>
        							</div>
                                </div>
  							</div>
  						</div>
  						<div class="counting-footer">
  							<div class="author-block inline-items channel-count-info" style="margin-bottom:20px;">
  								<span>총강좌수: {{$totalTutorials}}</span>
                                <span>총수강자수: {{$totalRegisterdTutorials}}</span>
                                <span>구독자수: @{{subscriptionCount}}</span>
  							</div>
  						</div>
  					</div>

                    <div class="counting-down" style="margin:30px 0;">
                        <aside class="widget w-contacts">
                            <h5 class="widget-title">강좌 만들기</h5>

                            <p class="contacts-text" style="margin-bottom:0">새로운 강좌를 만들어보세요</p>

                            <div class="" style="margin-top:20px">
                                <img src="/img/tutorials/default.jpg" alt="image">
                                <a href="/tutorial/create" class="btn btn-small full-width btn--primary btn--with-shadow">
                                    강좌 만들기
                                </a>
                            </div>
                        </aside>
                    </div>
  				</div>
  			</div>
  		</div>
  	</section>
  	<!-- ... end Conference Block -->
  </div>

  <confirm modal-id='deleteChannelAdminConfirm' :content-prop="confirmModalContent" event-emit-name='deleteChannelAdmin' event-on-name='deleteChannelAdminConfirm'></confirm>

@endsection


@section('popups')
    @component('components.alarm', ['title'=>'', 'content'=>''])
    @endcomponent

    @component('components.sendMessagePopup')
    @endcomponent

    @component('components.searchPopup')
    @endcomponent
@endsection

@section('svgs')
  @component('components.svgIcons')
  @endcomponent
@endsection


@section('scripts-code')
<script type="text/javascript" src="{{ asset('js/jquery.countdown.min.js')}}"></script>
<script src="{{ asset('plugins/cropperjs/cropper.min.js')}}"></script>

<!-- Froala Editor -->
<!-- Include external JS libs. -->
<script src="{{ asset('plugins/codemirror.min.js') }}"></script>
<script src="{{ asset('plugins/xml.min.js') }}"></script>

<!-- Include Editor JS files. -->
<script src="{{ asset('plugins/froala_editor_2.8.4/js/froala_editor.pkgd.min.js') }}"></script>
<script src="{{ asset('plugins/froala_editor_2.8.4/js/languages/ko.js') }}"></script>

<script>

var userInfo = {!! $user !!};
var superAdmin = {!! $super ? 'true' : 'false' !!};
var channelInfo = {!! $channel !!};
var subscriptionCount = {!! $subscriptionCount !!};

$(document).ready( function() {
});
</script>

<script src="{{ asset('js/pages/channel/edit.js') }}"></script>
@endsection
