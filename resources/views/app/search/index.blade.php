@extends('layouts.app')

@section('styles')
    <style>
    .cat-list-bg-style .cat-list__item{
        background-color: #EAEAEA;
    }
    </style>
@endsection

@section('content')
<div class="content-wrapper">

    <div class="crumina-stunning-header stunning-header--breadcrumbs-bottom-left stunning-header--content-inline stunning-bg-clouds">
		<div class="container">
			<div class="stunning-header-content">
				<div class="inline-items">
					<h4 class="stunning-header-title">다양한 강좌를 검색해보세요</h4>
				</div>
				<div class="breadcrumbs-wrap inline-items">
					<a href="#" class="btn btn--primary btn--round breadcrumbs-custom">
                        <i class="xi-search xi-x"></i>
						<!--<svg class="utouch-icon utouch-icon-home-icon-silhouette breadcrumbs-custom"><use xlink:href="#utouch-icon-home-icon-silhouette"></use></svg>-->
					</a>

					<ul class="breadcrumbs breadcrumbs--rounded">
						<li class="breadcrumbs-item">
							<a href="08_events.html" class="breadcrumbs-custom">검색어</a>
							<svg class="utouch-icon utouch-icon-media-play-symbol breadcrumbs-custom"><use xlink:href="#utouch-icon-media-play-symbol"></use></svg>
						</li>
						<li class="breadcrumbs-item active">
							<span class="breadcrumbs-custom">{{ $searchWord }}</span>
							<svg class="utouch-icon utouch-icon-media-play-symbol breadcrumbs-custom"><use xlink:href="#utouch-icon-media-play-symbol"></use></svg>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>


    <!-- 검색 결과 : 강좌 리스트 -->
    <section class="" style="margin-top:50px; margin-bottom:150px;">
		<div class="container">
            <div class="row align-center">
                <div class="col-md-12">
                    <ul class="cat-list-bg-style sorting-menu">
						<li class="cat-list__item" :class="{'active' : orderType == 'recent'}"><a href="" @click.prevent="orderBy('recent')" class="">최신날짜</a></li>
						<li class="cat-list__item" :class="{'active' : orderType == 'votes'}"><a href="" @click.prevent="orderBy('votes')" class="">인 기</a></li>
						<li class="cat-list__item" :class="{'active' : orderType == 'review'}"><a href="" @click.prevent="orderBy('review')" class="">리 뷰</a></li>
						<li class="cat-list__item" :class="{'active' : orderType == 'registration'}"><a href="" @click.prevent="orderBy('registration')" class="">수강자</a></li>
					</ul>
                </div>
            </div>
            <div class="row">
                <tutoriallist channel-logo-default="/img/channels/logo_image.png"  thumbnail-default="/img/tutorials/thumbnail.jpg" :view-options-prop="tutorialsOptions" comp-name='searchtutorials' :url-path="getSearchTutorialUrl" background-colors-prop="{{ json_encode($backgroundColors) }}" progress-status-prop="{{ json_encode($progressStatus) }}"></tutoriallist>
            </div>
		</div>
	</section>

</div>



@endsection

@section('popups')
    @component('components.sendMessagePopup')
    @endcomponent

    @component('components.searchPopup')
    @endcomponent
@endsection

@section('svgs')
  @component('components.svgIcons')
  @endcomponent
@endsection

@section('scripts-files')
@endsection

@section('scripts-code')
    <script>
    var searchWord = '{{ $searchWord }}';
    window.onload = function () {

    }
    </script>

    <script src="{{ asset('js/pages/search/search.js') }}"></script>
@endsection
