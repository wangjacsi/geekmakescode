<footer class="footer" id="site-footer">
		<div class="header-lines-decoration">
			<span class="bg-secondary-color"></span>
			<span class="bg-blue"></span>
			<span class="bg-blue-light"></span>
			<span class="bg-orange-light"></span>
			<span class="bg-red"></span>
			<span class="bg-green"></span>
			<span class="bg-secondary-color"></span>
		</div>

		<div class="container">
			<div class="row">
				<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
					<div class="widget w-info">
						<div class="site-logo">
							<a href="/" class=""><img src="/img/logo/logoSB_w.png" alt="codewwwave"></a>

							<p>Claritas est etiam processus dynamicus, qui sequitur mutationem
								consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram,
								anteposuerit litterarum formas humanitatis per. Investigationes demonstraverunt lectores
								legere me lius quod ii legunt saepius.
							</p>
						</div>


					</div>

				</div>

				<div class="col-lg-2 col-lg-offset-1 col-md-3 col-sm-12 col-sm-offset-0 col-xs-12">
					<div class="widget w-list">

						<h5 class="widget-title">Userful Links</h5>
						<ul class="list list--primary">
							<li>
								<a href="/">메인 페이지</a>
								<svg class="utouch-icon utouch-icon-arrow-right"><use xlink:href="#utouch-icon-arrow-right"></use></svg>
							</li>
							<li>
								<a href="/tutorials">강좌 리스트</a>
								<svg class="utouch-icon utouch-icon-arrow-right"><use xlink:href="#utouch-icon-arrow-right"></use></svg>
							</li>
							<li>
								<a href="/channels">채널 소개</a>
								<svg class="utouch-icon utouch-icon-arrow-right"><use xlink:href="#utouch-icon-arrow-right"></use></svg>
							</li>
							<li>
								<a href="#">최신 소식</a>
								<svg class="utouch-icon utouch-icon-arrow-right"><use xlink:href="#utouch-icon-arrow-right"></use></svg>
							</li>
							<li>
								@if(Auth::check())
								<a href="/mywave">마이웨이브</a>
								@else
								<a href="/login">로그인</a>
								@endif
								<svg class="utouch-icon utouch-icon-arrow-right"><use xlink:href="#utouch-icon-arrow-right"></use></svg>
							</li>
							<li>
								<a href="#">CODEWWWAVE</a>
								<svg class="utouch-icon utouch-icon-arrow-right"><use xlink:href="#utouch-icon-arrow-right"></use></svg>
							</li>

						</ul>
					</div>
				</div>

				<div class="col-lg-3 col-lg-offset-1 col-md-4 col-sm-12 col-sm-offset-0 col-xs-12">
					<div class="widget w-contacts">

						<h5 class="widget-title">Contact with us</h5>

						<div class="contact-item display-flex">
							<svg class="utouch-icon utouch-icon-message-closed-envelope-1"><use xlink:href="#utouch-icon-message-closed-envelope-1"></use></svg>
							<span class="info">{{ env('MAIL_SERVICE') }}</span>
						</div>

						<a href="#" class="btn btn--green full-width btn--with-shadow service-message-popup cd-nav-trigger">
							메세지 보내기
						</a>
					</div>

					<div class="widget w-follow">
						<ul>
							<li>Follow Us:</li>

							<li>
								<a target="_blank" href="https://twitter.com/codewwwave" title="twitter" class="social-icon" style="color: #1da1f2;"><span class="fab fa-twitter-square"></span></a>
							</li>
							<li>
								<a target="_blank" href="https://www.facebook.com/Codewwwave-2170298399910863" title="facebook" class="social-icon" style="color: #3b5998;"><span class="fab fa-facebook-square"></span></a>
							</li>
							<li>
								<a target="_blank" href="https://www.instagram.com/codewwwave/" title="instagram" class="social-icon" style="color: #e1306c;"><span class="fab fa-instagram"></span></a>
							</li>
							<li>
								<a target="_blank" href="https://www.youtube.com/channel/UClzuFd0vGnRwWN9w2xtcPDA" title="youtube" class="social-icon" style="color: rgb(255, 0, 0);"><span class="fab fa-youtube"></span></a>
							</li>


						</ul>
					</div>
				</div>
			</div>

		</div>

	<div class="sub-footer">
		<a class="back-to-top" href="#">
			<svg class="utouch-icon utouch-icon-arrow-top"><use xlink:href="#utouch-icon-arrow-top"></use></svg>
		</a>
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <span>
                    Copyright © {{ now()->year }} <a href="/" class="sub-footer__link">{{env('APP_NAME')}}</a>
                </span>

				</div>
			</div>
		</div>
	</div>

</footer>
