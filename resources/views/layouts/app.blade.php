<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('layouts.partials._googleanalitics')
    
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @include('layouts.partials._favicons')

    @if(View::hasSection('meta_tags'))
        @isset($meta)
            <title>{{ config('app.name') }} {{ $meta['title'] }}</title>

            <!--<meta name='description' itemprop='description' content='' />-->
            <meta name='keywords' content='코드웨이브,codewwwave,{{$meta['tags']}}' />

            <!--<meta property="og:description" content="" />-->
            <meta property="og:title" content="{{$meta['title']}}" />
            <meta property="og:url" content="{{url()->current()}}" />
            <meta property="og:type" content="article" />
            <meta property="og:site_name" content="{{ config('app.name') }}" />
            <meta property="og:image" content="{{$meta['image']}}" />

            <meta name="twitter:title" content="{{$meta['title']}}" />
            <meta name="twitter:site" content="@codewwwave" />
        @endisset
    @else
        <title>{{ config('app.name') }} @yield('title')</title>
        <meta name='keywords' content='코드웨이브,codewwwave' />
        <meta name='description' itemprop='description' content='최신 웹/앱 기술을 배우고 소식을 나누는 커뮤니티 코드웨이브' />
        <meta property="og:title" content="{{ config('app.name') }} @yield('title')" />
        <meta property="og:url" content="{{url()->current()}}" />
        <meta property="og:image" content="{{ asset('img/logo.png') }}" />

        <meta name="twitter:title" content="{{ config('app.name') }} @yield('title')" />
        <meta name="twitter:site" content="@codewwwave" />
    @endif


    <!-- Scripts -->
    <script>
    window.codewwwave = {
        url: '{{ config('app.url') }}',
        user: {
            id:{{ Auth::check() ? Auth::user()->id : 'null' }},
            authenticated:{{ Auth::check() ? 1 : 0 }}
        }
    }
    </script>


    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:300,400,700,900">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Baloo+Paaji">
    <link rel="stylesheet" href="{{ asset("css/xeicon.min.css") }}">
    <link rel="stylesheet" href="{{ asset("css/fontawesome-all.css") }}">


    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap.min.css') }}">
    <!--<link href="{{ asset('css/bootstrap-4.x.css') }}" rel="stylesheet">-->
    <link rel="stylesheet" href="{{ asset("css/theme-styles.css") }}">
    <link rel="stylesheet" href="{{ asset("css/blocks.css") }}">
    <link rel="stylesheet" href="{{ asset("css/widgets.css") }}">
    <link rel="stylesheet" href="{{ asset("css/swiper.min.css") }}">
    <link rel="stylesheet" href="{{ asset("plugins/toastr-master/toastr.min.css") }}">

    @yield('styles')

</head>
<body>

    @if(!isset($preload))
        @include('components.preloader')
    @endif

    <div id="app">
        @include('layouts.header')

        @yield('content')

        @include('layouts.footer')

        @yield('popups')
        <sendmessagepopup url-path='/globalSendMessage' popup-id='service-message' v-bind:user-logedin={{Auth::check() ? 'true' : 'false'}}></sendmessagepopup>

    </div>

    @yield('svgs')



    <!-- Scripts -->
    @if(View::hasSection('scripts-files'))
        <script type="text/javascript" src="{{ asset('js/app.js')}}"></script>
        @yield('scripts-files')
    @else
        <script type="text/javascript" src="{{ asset('js/app.js')}}"></script>
    @endif

    <script type="text/javascript" src="{{ asset('plugins/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/crum-mega-menu.js')}}"></script>
    <script type="text/javascript" src="{{ asset('js/swiper.jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('js/theme-plugins.js')}}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.typeahead.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('js/velocity.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('plugins/toastr-master/toastr.min.js')}}"></script>


    @yield('scripts-code')

    <script type="text/javascript" src="{{ asset('js/main.js')}}"></script>

    @yield('scripts-code-after')

    <script>

    @include('layouts.partials._toastr')



    </script>

</body>
</html>
