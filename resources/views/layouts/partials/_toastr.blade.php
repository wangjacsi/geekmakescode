$(document).ready( function() {
    toastr.options = {
          closeButton: true,
          progressBar: true,
          showMethod: 'slideDown',
          timeOut: 10000
      };
    setTimeout(function() {
    @if(session('success'))
        toastr.success('{{ session('success') }}');
    @endif

    @if(session('warning'))
        toastr.warning('{{ session('warning') }}');
    @endif

    @if(session('info'))
        toastr.info('{{ session('info') }}');
    @endif

    @if(session('danger'))
        toastr.error('{{ session('danger') }}');
    @endif
    }, 2000);

});
