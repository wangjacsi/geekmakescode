<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
    <div class="curriculum-event c-{{$colorBackground}}" data-mh="curriculum">
        <div class="curriculum-event-thumb">
            <img src="{{ $tutorial->getThumbnail('thumbnail') }}" alt="image">
            <div class="category-link c-{{$colorText}}">Training</div>
            <div class="curriculum-event-content">
                <div class="author-block inline-items">
                    <div class="author-avatar">
                        <img src="{{ $tutorial->channel->getThumbnail('logo_image') }}" alt="author">
                    </div>
                    <div class="author-info">
                        <div class="author-prof">Channel</div>
                        <a href="/channel/{{ $tutorial->channel->slug }}" class="h6 author-name">{{ $tutorial->channel->name }}</a>
                    </div>
                </div>
            </div>
            <div class="overlay-standard overlay--{{$colorOverlay}}"></div>
        </div>
        <div class="curriculum-event-content">
            <div class="icon-text-item display-flex">
                <svg class="utouch-icon utouch-icon-calendar-2"><use xlink:href="#utouch-icon-calendar-2"></use></svg>
                <div class="text">May 17-19, California</div>
            </div>
            <a href="09_events_details_conference_workshops.html" class="h5 title">{{ $tutorial->title }}</a>
        </div>
    </div>
</div>
