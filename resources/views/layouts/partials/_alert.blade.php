@if(session('success'))
<div class="container">
    <div class="alert alert-success alert-dismissable" style="text-align:center;">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button" style="right:0; color:#fff; opacity:1;">×</button>
        {{ session('success') }}
    </div>
</div>
@endif


@if(session('warning'))
<div class="container">
    <div class="alert alert-warning alert-dismissable" style="text-align:center;">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button" style="right:0; color:#fff; opacity:1;">×</button>
        {{ session('warning') }}
    </div>
</div>
@endif

@if(session('info'))
<div class="container">
    <div class="alert alert-info alert-dismissable" style="text-align:center;">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button" style="right:0; color:#fff; opacity:1;">×</button>
        {{ session('info') }}
    </div>
</div>
@endif

@if(session('danger'))
<div class="container">
    <div class="alert alert-danger alert-dismissable" style="text-align:center;">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button" style="right:0; color:#fff; opacity:1;">×</button>
        {{ session('danger') }}
    </div>
</div>
@endif
