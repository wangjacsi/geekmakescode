<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12" data-mh="product-item">
    <div class="product-item">
        <div class="product-item-thumb">
            <div class="square-colored bg-product-blue"></div>
            <img src="{{ $video->getThumbnail() }}" alt="product">
        </div>
        <div class="product-item-content">
            <h6 class="title">{{ $video->title }}</h6>
            <a href="/video/{{ $video->uid }}" class="more-arrow">
                <span>강의 보기</span>
                <div class="btn-next">
                    <svg class="utouch-icon icon-hover utouch-icon-arrow-right-1"><use xlink:href="#utouch-icon-arrow-right-1"></use></svg>
                    <svg class="utouch-icon utouch-icon-arrow-right1"><use xlink:href="#utouch-icon-arrow-right1"></use></svg>
                </div>
            </a>
        </div>
    </div>
</div>
