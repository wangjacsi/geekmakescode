<div class="col-lg-{{ isset($col) ? $col : 4 }} col-md-12 col-sm-12 col-xs-12">
    <div class="curriculum-event c-secondary" data-mh="curriculum">
        <div class="curriculum-event-thumb">
            <img src="{{$channel->getThumbnail('head_image')}}" alt="image">
            <div class="category-link color-icon">Lesson</div>
            <div class="curriculum-event-content">
                <div class="row">
                    <div class="col-md-12">
                        <a href="09_events_details_conference_workshops.html" class="h5 title">{{$channel->name}}</a>
                    </div>
                    <div class="col-md-12">
                        <div class="row channel-info">
                            <div class="col-md-6">
                                <div class="author-block inline-items">
                                    <div class="author-avatar">
                                        <img src="{{$channel->getThumbnail('logo_image', 's')}}" alt="author">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <p class="text color-icon">{{ $channel->tutorials->count() }} 강좌 수</p>
                                <p class="text color-icon">{{ $channel->subscriptionCount() }} 구독자 수</p>
                                <p class="text color-icon">{{ $channel->totalRegisterdTutorials() }} 수강자 수</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 channel-admins">
                        <div class="author-prof">Speaker</div>
                        <div class="d-flex--content-inline">
                            <ul class="friends-harmonic">
                                @foreach ($channel->users as $user)
                                    <li>
        								<a title="{{$user->name}}">
        									<img src="{{ $user->getPhoto() }}" alt="image">
        								</a>
        							</li>
                                @endforeach
    						</ul>
                        </div>
                    </div>
                </div>
            </div>
            @if( !isset($own) || (isset($own) && $own == false))
            <a href="#" class="subscription-btn btn btn--primary btn--with-shadow btn-small">
                구 독
            </a>
            @endif
            <div class="overlay-standard overlay--dark"></div>
        </div>
    </div>
</div>
