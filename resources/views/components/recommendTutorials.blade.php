<div class="swiper-container top-navigation" data-show-items="3">
    <div class="swiper-wrapper">
        <div v-for="(tutorial, index) in recommendTutorials" class="swiper-slide">
            <div class="curriculum-event"  v-bind:class="backgroundColors[index]['background']">
                <div class="curriculum-event-thumb">
                    <img :src="tutorial.thumbnail ? tutorial.thumbnail : '/img/tutorials/thumbnail.jpg'" alt="image">
                    <div class="category-link" v-bind:class="backgroundColors[index]['text']">@{{ progressStatus[tutorial.progress_status] }}</div>
                    <div class="curriculum-event-content">
                        <div class="author-block inline-items">
                            <div class="author-avatar">
                                <img :src="tutorial.channel.logo_image ? tutorial.channel.logo_image : '/img/channels/logo_image.png'" alt="author">
                            </div>
                            <div class="author-info">
                                <div class="author-prof">Channel</div>
                                <a v-bind:href="'/channel/' + tutorial.channel.slug" class="h6 author-name">@{{tutorial.channel.name}}</a>
                            </div>
                        </div>
                    </div>
                    <div class="overlay-standard" v-bind:class="backgroundColors[index]['overlay']"></div>
                </div>
                <div class="curriculum-event-content tutorial-info">
                    <div class="icon-text-item display-flex">
                        <div class="text" title="수정일"><i class="fas fa-calendar-alt"></i> @{{ moment(tutorial.updated_at.date).format("MMM Do YYYY") }}</div>
                    </div>
                    <div class="icon-text-item display-flex video-info">
                        <div class="text" title="총 비디오 수">
                            <i class="fas fa-play-circle"></i> @{{ tutorial.videos_by_visible.length }} 비디오
                        </div>
                        <div class="text" title="총 플레이 시간">
                            <i class="fas fa-clock"></i> @{{ transVideoPlayTime(tutorial.videos_by_visible) }}
                        </div>
                    </div>
                    <div class="icon-text-item display-flex video-info">
                        <div class="text" title="수강자 수">
                            <i class="fas fa-swimmer"></i> @{{ tutorial.registrations_count }} 명
                        </div>
                    </div>
                    <a href="09_events_details_conference_workshops.html" class="h5 title">@{{ tutorial.title }}</a>
                </div>
            </div>
        </div>
    </div>
    <div class="btn-slider-wrap navigation-top-right">

        <div class="btn-prev">
            <svg class="utouch-icon icon-hover utouch-icon-arrow-left-1"><use xlink:href="#utouch-icon-arrow-left-1"></use></svg>
            <svg class="utouch-icon utouch-icon-arrow-left1"><use xlink:href="#utouch-icon-arrow-left1"></use></svg>
        </div>

        <div class="btn-next">
            <svg class="utouch-icon icon-hover utouch-icon-arrow-right-1"><use xlink:href="#utouch-icon-arrow-right-1"></use></svg>
            <svg class="utouch-icon utouch-icon-arrow-right1"><use xlink:href="#utouch-icon-arrow-right1"></use></svg>
        </div>

    </div>
</div>
