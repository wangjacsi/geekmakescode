@extends('layouts.app')

@section('title')
    Unauthorized Access
@endsection

@section('styles')

@endsection

@section('content')
    <section class="page404">
    	<div class="site-logo logo--center">
    		<a href="/" class="full-block"></a>
    		<img src="/img/logo/logoSW_w.png" alt="codewwwave">
    	</div>
    	<div class="col-8 bg-secondary-color"></div>
    	<div class="col-8 bg-primary-color"></div>
    	<div class="col-8 bg-blue-light">
    		<h2 class="error">Error</h2>
    	</div>
    	<div class="col-8 bg-orange-light">
    		<h2 class="number">4</h2>
    	</div>
    	<div class="col-8 bg-red">
    		<h2 class="number">0</h2>
    	</div>
    	<div class="col-8 bg-green">
    		<h2 class="number">3</h2>
    	</div>
    	<div class="col-8 bg-secondary-color"></div>
    	<div class="col-8 bg-primary-color"></div>

    	<div class="page404-content">
    		<h2 class="title">Sorry! Unauthorized Access ;(</h2>
    		<h5 class="sub-title">요청하신 페이지에 대한 접근 권한이 없습니다. 메인 페이지로 돌아가기 원하시면 <a href="/">Homepage</a> 클릭해주세요.</h5>
    	</div>


    </section>
@endsection

@section('popups')
    @component('components.sendMessagePopup')
    @endcomponent

    @component('components.searchPopup')
    @endcomponent
@endsection

@section('svgs')
  @component('components.svgIcons')
  @endcomponent
@endsection

@section('scripts-files')
@endsection

@section('scripts-code')
@endsection
