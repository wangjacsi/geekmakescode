Vue.component('videoinfo', require('../../components/video/VideoInfo.vue'));
Vue.component('videoreplace', require('../../components/video/VideoReplace.vue'));
Vue.component('videothumbnail', require('../../components/video/VideoThumbnail.vue'));
Vue.component('confirm', require('../../components/common/Confirm.vue'));
Vue.component('sendmessagepopup', require('../../components/common/sendMessagePopup.vue'));

import eventHub from '../../event.js'
//import vSelect from 'vue-select'
//Vue.component('v-select', vSelect)

const app = new Vue({
    el: '#app',
    data: {
        video:video,
        viewMain:'videoInfo',
        cdeleteVideoThumbContent:{
            title:'비디오 이미지 삭제',
            body:'비디오 이미지를 삭제하기 원하시면 아래 확인 버튼을 클릭해주세요',
            cancel:'취소',
            submit:'확인'
        },
    },
    mounted(){
        let self = this

    },
    methods: {
        changeMainView(view){
            this.viewMain = view
        }
    }
});
