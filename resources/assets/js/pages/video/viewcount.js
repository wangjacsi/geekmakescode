Vue.component('video-vote', require('../../components/VideoVote.vue'));
Vue.component('video-comments', require('../../components/VideoComments.vue'));
Vue.component('subscribe-button', require('../../components/SubscribeButton.vue'));
Vue.component('sendmessagepopup', require('../../components/common/sendMessagePopup.vue'));
//import eventHub from '../../event.js'

const app = new Vue({
    el: '#app',
    data: {
        duration:'',
        player:'',
        currentTime:'',
        viewTimer:'',
        viewTimerPause:true,
        videoUID:'',
        runningPercent:0,
        totalViewTime:0,
        countOn:false,
        commentsLen:0,
        totalFreqChkTime:0,
        videos:videos,
        video:video,
        recommendTutorials:recommendTutorials,
        records:records,
        prevVideo:null,
        nextVideo:null,
        settings:JSON.parse(channel.settings),
        snsLists:[],
    },
    mounted() {
        this.videoUID = $('#videoUID').val()
        // Select with the DOM API
        let self = this
        var iframe = document.querySelector('iframe')
        this.player = new Vimeo.Player(iframe)

        // 전체 길이 저장
        this.player.getDuration().then(function(duration) {
            console.log(duration);
            self.duration = duration
            // duration = the duration of the video in seconds
        }).catch(function(error) {
            // an error occurred
        });

        let settings = JSON.parse(channel.settings)

        _.forEach(settings.sns, function(value, key) {
            self.snsLists.push(value)
        });


        _.forEach(this.videos, function(value, key) {
            Vue.set(value, 'watch', self.checkRecord(value.id, value.playtime))
        });


        // 만약 시청하다 만 부분이 있을 시
        // this.player.setCurrentTime(30.456)
        this.videoPlayerSetTime()

        this.bothSideVideos()

        this.viewTimer = setInterval(() => {
            if(!self.viewTimerPause){
                if(!self.countOn){
                    self.totalViewTime++
                }

                if(!self.countOn && self.hasHitQuotaView()){
                   self.countOn = true
                   self.createView()
                }

               self.totalFreqChkTime++
               if(self.totalFreqChkTime == 60000){
                   self.totalFreqChkTime = 0
               }

               if(self.totalFreqChkTime > 60){ // 60 초
                   self.updateVideoCurrWatch(self.currentTime)
               }
           }
        }, 1000)


        // 플레이 시 플레이 시간 합계
        this.player.on('play', function(data) {
            console.log('play')
            console.log(data)
            self.getPlayerCurrentTime()

            self.viewTimerPause = false
        });

        // 일시 정지 시 타이머 정지
        this.player.on('pause', function(data){
            console.log('pause')
            console.log(data)
            self.viewTimerPause = true

            if(self.totalFreqChkTime > 10){ // 10 초
                self.updateVideoCurrWatch(data.seconds)
            }
        })

        // 종료 시 타이머 정지
        this.player.on('ended', function(data){
            console.log('end')
            console.log(data)
            self.viewTimerPause = true

            self.watchEndOfVideo()
        })
    },
    methods: {
        bothSideVideos(){
            let check = _.findIndex(this.videos, ['id', this.video.id])
            if(check > -1){
                if(check > 0){
                    this.prevVideo = this.videos[check - 1]
                }
                if(check != this.videos.length - 1){
                    this.nextVideo = this.videos[check + 1]
                }
            }
        },
        checkRecord(id, playtime){
            let check = _.findIndex(this.records, ['video_id', id])

            if(check > -1){
                return this.records[check]['watch']
            } else {
                return 0
            }
        },
        videoPlayerSetTime(){
            let check = _.findIndex(this.records, ['video_id', this.video.id])
            if(check > -1){
                this.player.setCurrentTime(this.records[check]['progress'])
            }
        },
        transVideoPlayTime(videos){
            let tseconds = _.sumBy(videos, 'playtime')
            let timeString = ''
            let hours = Math.floor(tseconds / 3600)
            let minutes =  Math.floor( (tseconds - (hours * 3600)) / 60 )
            //let seconds = tseconds % 60
            if(hours > 0 ){ // hour
                timeString += hours + ' 시간 '
            }
            if(minutes > 0){
                timeString += minutes + ' 분'
            }
            if(timeString == ''){
                return 0
            }
            return timeString
        },
        transTimeToPlaytime(playtime){
            return Math.round( playtime / 60 ) + ' min'
        },
        updateVideoCurrWatch(currentTime){
            this.totalFreqChkTime = 0
            axios.post('/video/'+ this.videoUID + '/watchCurrentTime', {time:currentTime})
        },
        watchEndOfVideo(){
            axios.post('/video/'+ this.videoUID + '/watch')
        },
        changeCommentCount(len){
            console.log(len)
            this.commentsLen = len
        },
        confirm(){
            console.log('emit here')
            this.$emit('delete-vote')
        },
        confirm2(){
            console.log('emit here 2')
            //eventHub.$emit('delete-comment')
            this.$emit('delete-comment')
        },
        createView(){
            axios.post('/video/'+ this.videoUID + '/viewcount')
            .then(response => {
                console.log(response)
            })
            .catch(e => {
                console.log(e)
            })
        },
        getPlayerCurrentTime(){
            let self = this
            this.player.getCurrentTime().then(function(seconds) {
                self.currentTime = seconds
            }).catch(function(error) {
                // an error occurred
            });
        },
        hasHitQuotaView(){
            if(!this.duration){
                return false
            }

            if(this.totalViewTime > Math.round(10 * this.duration / 1000) )
                return true
            else
                return false

        }

    }


});
