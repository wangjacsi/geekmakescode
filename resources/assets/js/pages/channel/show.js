Vue.component('tutorials', require('../../components/mywave/MyTutorials.vue'));
Vue.component('userlist', require('../../components/channel/UsersList.vue'));
Vue.component('sendmessagepopup', require('../../components/common/sendMessagePopup.vue'));

const app = new Vue({
    el: '#app',
    data: {
        owner:owner,
        isSubscribed:isSubscribed,
        tutorialsOptions:{
            viewType:'block',
            editBtn:false,
            deleteBtn:false,
            noDataCommentOn:true,
            noDataComment:'운영중인 강좌가 없습니다'
        },
        channel: channel,
        title:null,
        content:'',
        errorMessage:'',
        messageLen:null,
        maxLen: 2000,
        subscriptionCount:subscriptionCount,
        snsLists:[],
        settings:JSON.parse(channel.settings),
        userLoginStatus:userLoginStatus,
    },
    mounted(){
        let self = this

        this.settings = JSON.parse(channel.settings)
        _.forEach(this.settings.sns, function(value, key) {
            self.snsLists.unshift(value)
        })
    },
    methods: {
        setAlramShow(title, content){
            $('#alarm #title').text(title)
            $('#alarm #content').html(content)
            $('#alarm').modal('show')
            setTimeout(function(){ $('#alarm').modal('hide') }, 3000)
        },
        async sendMessage(){
            if(!this.userLoginStatus){
                this.setAlramShow('채널 메세지 전송', '채널에 메세지를 전송하기 위해서는 로그인이 필요합니다')
                return false
            }
            this.errorMessage = ''

            this.title = _.trim(this.title)
            if(this.title == '' ){
                this.errorMessage = '메세지 제목을 작성해주세요'
                return false
            }

            this.content = _.trim(this.content)
            if(this.content == '' ){
                this.errorMessage = '메세지 내용을 작성해주세요'
                return false
            }

            this.errorMessage = '메세지 전송 중 ...'
            let response = await axios.post('/sendMessage', {title: this.title, content:this.content, channelID: this.channel.id})

            this.initForm()
        },
        initForm(){
            this.title = null
            this.content = ''
            this.messageLen = null
            this.errorMessage = '메세지 전송 완료'
            let self = this
            setTimeout(function(){ self.errorMessage = '' }, 3000)
        },
        textLimit(){
            if(this.content.length > this.maxLen)
                this.content = this.content.substring(0, this.maxLen)

            this.messageLen = this.content.length
        },
        channelCreated(){
            return moment(this.channel.created_at).format("MMM Do YYYY")
        },
        subscribeHandle(){
            if(this.isSubscribed){
                this.unSubscribe()
            } else {
                this.subscribe()
            }
        },
        async subscribe(){
            await axios.post('/subscription/'+ this.channel.slug)
            this.isSubscribed = true
            this.subscriptionCount++
        },
        async unSubscribe(){
            await axios.delete('/subscription/'+ this.channel.slug)
            this.isSubscribed = false
            this.subscriptionCount--
        }
    }
});
