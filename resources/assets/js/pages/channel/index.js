Vue.component('channellist', require('../../components/mywave/MyChannels.vue'));
Vue.component('sendmessagepopup', require('../../components/common/sendMessagePopup.vue'));

import eventHub from '../../event.js'

const app = new Vue({
    el: '#app',
    data: {
        chViewOptions:{
            editBtn:false,
            deleteBtn:false,
            subscribeBtn:false,
            viewType:'block',
            noDataCommentOn:true,
            noDataComment:'채널 리스트가 없습니다',
            gridClass:'col-lg-4 col-md-6',
        },
        orderType:'name',
    },
    computed: {

    },
    updated: function() {
        let self = this

        this.$nextTick(function () {

        })
    },
    mounted() {

    },
    methods: {
        orderBy(type){
            if(this.orderType != type){
                this.orderType = type
                eventHub.$emit('channelOrderByChange', this.orderType)
            }
        }
    }
});
