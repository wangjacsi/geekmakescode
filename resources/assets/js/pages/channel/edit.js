Vue.component('tutorials', require('../../components/mywave/MyTutorials.vue'));
Vue.component('userlist', require('../../components/channel/UsersList.vue'));
Vue.component('channelinfo', require('../../components/channel/ChannelInfo.vue'));
Vue.component('channelcontact', require('../../components/channel/ChannelContact.vue'));
Vue.component('searchlist', require('../../components/channel/SearchList.vue'));
Vue.component('confirm', require('../../components/common/Confirm.vue'));
Vue.component('settings', require('../../components/channel/Settings.vue'));
Vue.component('sendmessagepopup', require('../../components/common/sendMessagePopup.vue'));

import eventHub from '../../event.js'
import vSelect from 'vue-select'
Vue.component('v-select', vSelect)

const app = new Vue({
    el: '#app',
    data: {
        user:userInfo,
        channel: channelInfo,
        settings:JSON.parse(channelInfo.settings),
        subscriptionCount:subscriptionCount,
        tutorialsOptions:{
            viewType:'block',
            editBtn:true,
            deleteBtn:true,
            noDataCommentOn:true,
            noDataComment:'운영중인 강좌가 없습니다'
        },
        channelUsersOptions:{
            channelAdminAdd:false,
            channelAdminDelete:true,
            channelAdminLevel:true,
            deleteBtn:false,
            noDataCommentOn:false,
            noDataComment:''
        },
        searchListOptions:{
            requestBtn: true,
            requestBtnText: '관리자 등록 요청하기'
        },
        confirmModalContent:{
            title:'채널 관리자 해제',
            body:'채널 관리자 해제를 원하시면 아래 확인 버튼을 클릭하세요',
            cancel:'취소',
            submit:'확인'
        },
        view:'tutorials',
        snsLists:[],
        superAdmin:superAdmin
    },
    mounted(){
        let self = this

        _.forEach(this.settings.sns, function(value, key) {
            self.snsLists.unshift(value)
        });

        eventHub.$on('channelInfoUpdate', this.channelInfoUpdate)

        eventHub.$on('channelContactUpdate', this.channelContactUpdate)

        eventHub.$on('snsUpdate', this.snsUpdate)

        eventHub.$on('channelSettingsUpdate', this.settingsUpdate)
    },
    methods: {
        channelCreated(){
            return moment(this.channel.created_at).format("MMM Do YYYY")
        },
        viewChange(view){
            this.view = view
        },
        channelInfoUpdate(channel){
            this.channel = channel
        },
        channelContactUpdate(data){
            this.settings.phone = data.phone
            this.settings.site = data.site
            this.settings.email = data.email
        },
        snsUpdate(snsLists){
            this.snsLists = snsLists
        },
        settingsUpdate(settings){
            console.log(settings)
            this.settings.viewoptions = settings
        },
    }
});
