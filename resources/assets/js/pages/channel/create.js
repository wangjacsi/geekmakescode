var URL;// = window.URL || window.webkitURL;
let images;// = document.querySelectorAll('.crop-image');
let croppers = [];
let inputImage = [];
var defaultImage = ['/img/stunning-bg4.jpg', '/img/teammember6.jpg'];
let headImage;
let logoImage;

var cropperImages = ['headImage', 'logoImage']
var options = { headImage: {
  aspectRatio: 3.08,
  viewMode: 2,
  highlight:true,
  modal:false,
  autoCropArea: 1,
  minCanvasWidth:100,
  minCanvasHeight:100,
  minCropBoxWidth:100,
  minCropBoxHeight:100,
  ready: function (e) {
      console.log('0:' + e.type);
      getHeadImageCrop()
  },
  cropstart: function (e) {console.log(e.type, e.detail.action);},
  cropmove: function (e) {console.log(e.type, e.detail.action);},
  cropend: function (e) {
      console.log(e.type, e.detail.action);
      console.log('0 crop end')
      getHeadImageCrop()
  },
  crop: function (e) {var data = e.detail;console.log(e.type);},
  zoom: function (e) {console.log(e.type, e.detail.ratio);}
},
logoImage: {
  aspectRatio: 1.00,
  viewMode: 2,
  highlight:true,
  modal:false,
  autoCropArea: 1,
  minCanvasWidth:30,
  minCanvasHeight:30,
  minCropBoxWidth:30,
  minCropBoxHeight:30,
  ready: function (e) {
      console.log('1:' + e.type);
      getLogoImageCrop()
  },
  cropstart: function (e) {console.log(e.type, e.detail.action);},
  cropmove: function (e) {console.log(e.type, e.detail.action);},
  cropend: function (e) {
      console.log(e.type, e.detail.action);
      console.log('1 crop end')
      getLogoImageCrop()
  },
  crop: function (e) {var data = e.detail;console.log(e.type);},
  zoom: function (e) {console.log(e.type, e.detail.ratio);}
}};

function getHeadImageCrop(){
    croppers[0].getCroppedCanvas({
        minWidth: 256,
        minHeight: 256,
        maxWidth: 2000,
        maxHeight: 1500,
        imageSmoothingQuality: 'high'
    }).toBlob(function (blob) {
        headImage = blob
        console.log(headImage)
    })
}

function getLogoImageCrop(){
    croppers[1].getCroppedCanvas({
        minWidth: 30,
        minHeight: 30,
        maxWidth: 1000,
        maxHeight: 1000,
        imageSmoothingQuality: 'high'
    }).toBlob(function (blob) {
        logoImage = blob
        console.log(logoImage)
    })
}


window.onload = function () {
    URL = window.URL || window.webkitURL;

    images = document.querySelectorAll('.crop-image');
    //croppers = [];
    inputImage = document.querySelectorAll('.input-image-file')

    var length = images.length;
    for (let i = 0; i < length; i++) {
      croppers.push(new Cropper(images[i], options[cropperImages[i]]));

      inputImage[i].onchange = function () {
        var files = this.files;
        var file;

        if (croppers[i] && files && files.length) {
          file = files[0];

          if (/^image\/\w+/.test(file.type)) {

            images[i].src = URL.createObjectURL(file);
            croppers[i].destroy();
            croppers[i] = new Cropper(images[i], options[cropperImages[i]]);
            //inputImage[i].value = null;
          } else {
            window.alert('Please choose an image file.');
          }
        }
      };

      croppers[i].getCroppedCanvas();
    }

};

Vue.component('sendmessagepopup', require('../../components/common/sendMessagePopup.vue'));
import VueFroala from 'vue-froala-wysiwyg'
Vue.use(VueFroala)

const app = new Vue({
    el: '#app',
    data: {
        form:{
            name:'',
            description:'',
            headImage:'',
            logoImage:''
        },
        status:null,
        tempID:null,
        errorMessage:'',
        froalaConfig: {
            imageDefaultWidth:0,
            requestHeaders: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            language: 'ko',
            imageUploadParam: 'imageFile',
            imageUploadURL: '/editor/uploadToTemp',
            imageUploadMethod: 'POST',
            imageMaxSize: 2 * 1024 * 1024,
            imageUploadParams: {id: Math.random().toString(36).substr(2, 9), type: 'channels', width:800, mode:'resize', limitWidth:1920, limitHeight:1000, fileSize:2},
            imageAllowedTypes: ['jpeg', 'jpg', 'png', 'gif', 'svg'],
            heightMin: 500,
            heightMax: 1000,
            fontFamily: {
              'Arial,Helvetica,sans-serif': 'Arial',
              'Georgia,serif': 'Georgia',
              'Impact,Charcoal,sans-serif': 'Impact',
              'Tahoma,Geneva,sans-serif': 'Tahoma',
              "'Times New Roman',Times,serif": 'Times New Roman',
              'Verdana,Geneva,sans-serif': 'Verdana',
              'Helvetica,sans-serif':'Helvetica',
              "'Helvetica Neue',sans-serif":'Helvetica Neue',
              "'Roboto', sans-serif":'Roboto',
              "'Nunito', sans-serif":  'Nunito',
              "'Nunito Sans', sans-serif":'Nunito Sans',
              "'Montserrat', sans-serif": 'Montserrat',
              "'Oswald', sans-serif":'Oswald',
              "'Raleway', sans-serif":'Raleway',
              "'Nanum Gothic', sans-serif":'나눔 고딕 Nanum Gothic',
              "'Nanum Myeongjo', serif":'나눔 명조 Nanum Myeongjo',
              "'Nanum Pen Script', cursive":'나눔 펜 스크립트 Nanum Pen Script',
              "'Nanum Gothic Coding', monospace":'나눔 고딕 코딩 Nanum Gothic Coding',
              "'Nanum Brush Script', cursive":'나눔 브러쉬 스크립트 Nanum Brush Script',
              "'Song Myung', serif":'송명 Song Myung',
              "'Poor Story', cursive":'푸어 스토리 Poor Story',
              "'Gugi', cursive":'구기 Gugi',
              "'Gothic A1', sans-serif":'고딕 A1 Gothic A1',
              "'Black Han Sans', sans-serif":'블랙 한글 Sans Black Han Sans',
              "'Do Hyeon', sans-serif":'도현 Do Hyeon',
              "'Hi Melody', cursive":'하이 멜로디 Hi Melody',
              "'Sunflower', sans-serif":'썬프라워 Sunflower',
              "'Jua', sans-serif":'주아 Jua',
              "'Gamja Flower', cursive":'감자 플라워 Gamja Flower',
              "'East Sea Dokdo', cursive":'동해 독도 East Sea Dokdo',
              "'Gaegu', cursive":'개구 Gaegu',
              "'Stylish', sans-serif":'스타일리쉬 Stylish',
              "'Black And White Picture', sans-serif":'블랙 앤 화이트 픽쳐 Black And White Picture',
              "'Kirang Haerang', cursive":'키랑 해랑 Kirang Haerang',
              "'Cute Font', cursive":'큐트 폰트 Cute Font',
              "'Yeon Sung', cursive":'연성 Yeon Sung',
              "'Dokdo', cursive":'독도 Dokdo'
            },
            //fontFamilySelection: true,
            toolbarButtons: ['fullscreen', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '|', 'fontFamily', 'fontSize', 'color', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', '|', 'insertLink', 'insertImage', 'insertVideo', 'insertTable', '|', 'emoticons', 'specialCharacters', 'insertHR', 'selectAll', 'clearFormatting', '|', 'print', 'help', 'html', '|', 'undo', 'redo'],
            events : {
                'froalaEditor.image.error': function (e, editor, error, response){
                    console.log(error)
                    console.log(response)
                    $('#alarm #title').text('이미지 업로드 에러')
                    $('#alarm #content').html('이미지 업로드에 실패하였습니다')
                    // Bad link.
                    if (error.code == 1) {
                        $('#alarm #content').html('유효한 링크가 아닙니다')
                    }

                    // No link in upload response.
                    else if (error.code == 2 || error.code == 3 || error.code == 4 || error.code == 7) {
                        $('#alarm #content').html('업로드 가능한 본문 이미지는<br>사이즈 2Mbytes 이하<br>폭 1920픽셀 이하<br>높이 1000픽셀 이하입니다')
                        if(response){
                            let re = JSON.parse(response)
                            if('message' in re){
                                let message = ''
                                _.forEach(re.message, function(value, key) {
                                      message += value
                                      message += '<br>'
                                 });
                                 $('#alarm #content').html(message)
                            }
                        }
                    }

                    // Image too text-large.
                    else if (error.code == 5) {
                        $('#alarm #content').html('업로드 가능한 본문 이미지의 사이즈는 2Mbytes 이하입니다')
                    }

                    // Invalid image type.
                    else if (error.code == 6) {
                        $('#alarm #content').html('업로드 가능한 이미지 타입은 jpeg, jpg, png, gif, svg 입니다')
                    }
                    $('#alarm').modal('show')
                },
                'froalaEditor.image.uploaded' : function (e, editor, response) {
                    // Image was uploaded to the server.
                    console.log('froalaEditor.image.uploaded')
                },
                'froalaEditor.image.inserted' : function (e, editor, $img, response) {
                  // Image was inserted in the editor.
                  console.log('froalaEditor.image.inserted')
                },
                'froalaEditor.image.replaced' : function (e, editor, $img, response) {
                  // Image was replaced in the editor.
                  console.log('froalaEditor.image.replaced')
                },
                'froalaEditor.image.removed' : async function (e, editor, $img) {
                    console.log('froalaEditor.image.removed')
                    let response = await axios.post('/editor/deleteImage', {src: $img.attr('src'), type:'local'})
                }
            }
        }
    },
    computed: {
      // a computed getter
    },
    updated() {
        let self = this
        this.$nextTick(function () {

        })
    },
    mounted() {
        this.tempID = this.froalaConfig.imageUploadParams.id

        // 업로딩 중에 페이지 이동을 하려고 할때 ...  경고 문구 발생 시키기
        window.onbeforeunload = () => {
            if(this.status == 'uploading') {
                return '채널 생성중입니다. 정말로 다른 페이지로 이동하시겠습니까?'
            }
        }
    },
    methods: {
        imageCancel (num) {
            this.resetInputImage(num)
        },
        resetInputImage(num){
          images[num].src = defaultImage[num];
          croppers[num].destroy();
          croppers[num] = new Cropper(images[num], options[cropperImages[num]]);
          inputImage[num].value = null;
        },
        formValid(){
            var errorChk = true
            this.errorMessage = ''

            this.form.name = _.trim(this.form.name)
            if(this.form.name == ''){
                errorChk = false
                this.errorMessage = '채널 제목을 작성해주세요'
                return false
            }

            if(this.form.description == ''){
                errorChk = false
                this.errorMessage = '채널 소개글을 작성해주세요'
                return false
            }

            // image set
            if(inputImage[0].value){ //head image
                this.form.headImage = headImage
            } else {
                this.form.headImage = ''
            }

            if(inputImage[1].value){ //logo image
                this.form.logoImage = logoImage
            } else {
                this.form.logoImage = ''
            }

            this.errorMessage = '채널 생성 중 ...'
            return errorChk
        },
        upload(){
            this.errorMessage = ''
            this.status = null

            if(this.formValid()){
                this.status = 'uploading'
                let data = new FormData();

                data.append('name', this.form.name)
                data.append('tempID', this.tempID)
                data.append('description', this.form.description)
                if(this.form.headImage)
                    data.append('headImage', this.form.headImage)
                if(this.form.logoImage)
                    data.append('logoImage', this.form.logoImage)

                axios.post(`/channel/store`, data,
                    {
                        headers: {
                            'Content-Type': 'multipart/form-data'
                        }
                    }
                )
                .then(response => {
                    console.log(response)
                    this.status = null
                    if(response.data.result == 'success'){
                        this.errorMessage = '채널 생성이 완료되었습니다'
                        window.location.href = "/channel/"+response.data.slug
                    } else if(response.data.result == 'error') {
                        this.errorMessage = '채널 생성에 실패하였습니다. 다시 시도해 주세요.'
                        if('errors' in response.data){
                          this.errorMessage = ''
                          let message = ''
                          _.forEach(response.data.errors, function(value, key) {
                                message += value
                                message += '<br>'
                           });
                           this.errorMessage = message
                        }
                    }
                })
                .catch(e => {
                  //alert(222);
                    this.errorMessage = '채널 생성에 실패하였습니다. 다시 시도해 주세요.'
                    console.log(e)
                })
            }
        }

    }


});
