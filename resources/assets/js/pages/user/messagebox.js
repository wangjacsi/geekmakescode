Vue.component('sendmessagepopup', require('../../components/common/sendMessagePopup.vue'));

import eventHub from '../../event.js'
import Pagination from '../../components/Pagination'
import marked from 'marked'

const app = new Vue({
    el: '#app',
    data: {
        user:user,
        messages:[],
        meta:null,
        pagination: {
                'current_page':null,
                'first':null,
                'from':null,
                'last':null,
                'last_page':null,
                'next':null,
                'path':null,
                'per_page':null,
                'prev':null,
                'to':null,
                'total':null
            },
        loadFlag:false,
        activeId:null,
        showTitle:null,
        showContent:null,
        subject:'',
        message:'',
        errorMessage:null,
        urlPath:null,
        myChannelCnt:myChannelCnt,
        myTotalMessagesCnt:myTotalMessagesCnt,
        myNewMessagesCnt:myNewMessagesCnt,
        myNewChMessageCnt:myNewChMessageCnt,
        myChTotalMessagesCnt:myChTotalMessagesCnt,
        currentBox:'user',
        sendChannel:null
    },
    components: {
        Pagination
    },
    mounted(){
        let self = this

        this.getUserMessages(1)

        this.updateNotificationCheckTime()

        eventHub.$on('mymessages-pagination', this.pageChange)
    },
    methods: {
        messageBoxChange(box){
            this.currentBox = box

            this.initForm()
            this.showTitle = null
            this.showContent = null
            this.getUserMessages(1)
            this.updateNotificationCheckTime()
        },
        async unreadMessage(){
            let response = await axios.post('/setUnReadMarkMessge/'+this.activeId)

            let self= this
            let message = _.find(this.messages, {id:self.activeId})
            message.read = false
        },
        async deleteMessage(){
            let response = await axios.delete('/message/'+this.activeId)
            let self = this
            let tempIndex = _.findIndex(this.messages, {id:self.activeId})
            if(tempIndex > -1){
                this.messages.splice(tempIndex, 1)
                if(this.messages.length > 0){
                    this.pageChange(this.pagination.current_page)
                } else {
                    if(this.pagination.current_page > 1){
                        this.pageChange(this.pagination.current_page - 1)
                    } else {
                        this.pageChange(1)
                    }
                }
            }
        },
        initForm(){
            this.subject = ''
            this.message = ''
        },
        validate(){
            this.errorMessage = ''
            let error = false

            if(this.subject == ''){
                this.errorMessage = '제목을 작성해주세요'
                return false
            }
            if(this.message == ''){
                this.errorMessage = '메세지를 작성해주세요'
                return false
            }
            return true
        },
        async sendMessage(){
            let validte = this.validate()

            if(!validte){
                return false
            }

            let sendData = {subject:this.subject, message:this.message}
            if(this.currentBox == 'channel'){
                sendData.channel_id = this.sendChannel
            }

            let response = await axios.post(this.urlPath, sendData)

            let self = this
            this.errorMessage = '메세지 전송이 완료되었습니다'
            this.initForm()
            setTimeout(function(){
                self.errorMessage = ''
            }, 3000)
        },
        changeMessage(id){
            this.activeId = id

            this.setMessageView(id)
        },
        sanitizeMarkedBody(body){
            return marked(body, { sanitize : true })
        },
        pageChange(page, path){
            this.initForm()
            this.showTitle = null
            this.showContent = null
            this.getUserMessages(page)
        },
        paginationSet(){
            Vue.set(this.pagination, 'current_page', this.meta.current_page)
            Vue.set(this.pagination, 'first', this.meta.first_page_url)
            Vue.set(this.pagination, 'from', this.meta.from)
            Vue.set(this.pagination, 'last', this.meta.last_page_url)
            Vue.set(this.pagination, 'last_page', this.meta.last_page)
            Vue.set(this.pagination, 'next', this.meta.next_page_url)
            Vue.set(this.pagination, 'path', this.meta.path)
            Vue.set(this.pagination, 'per_page', this.meta.per_page)
            Vue.set(this.pagination, 'prev', this.meta.prev_page_url)
            Vue.set(this.pagination, 'to', this.meta.to)
            Vue.set(this.pagination, 'total', this.meta.total)

            this.loadFlag = true
        },
        async getUserMessages(page){
            let response
            if(this.currentBox == 'user')
                response = await axios.post('/getUserMessages?page='+page)
            else
                response = await axios.post('/getUserChannelMessages?page='+page)

            this.messages = response.data.data
            this.meta = _.omit(response.data, ['data'])
            this.paginationSet()
        },
        openFirstMessage(){
            //activeId
            this.activeId = this.messages[0].id

            this.setMessageView(this.activeId)
        },
        setMessageView(id){
            let message = _.find(this.messages, {id:id})
            this.showTitle = message.title
            this.showContent = this.sanitizeMarkedBody(message.message)

            this.setUrlPath(message.user.slug)
            this.setSendMessageFormTitle(message.title)
            this.setReadMarkMessge(id)

            if(this.currentBox == 'channel'){
                this.sendChannel = message.rchannel_id
            }
        },
        async setReadMarkMessge(id){
            let response = await axios.post('/setReadMarkMessge/'+id)

            let message = _.find(this.messages, {id:id})
            message.read = true
        },
        setUrlPath(userSlug){
            this.urlPath = '/userSendMessage/'+userSlug
        },
        setSendMessageFormTitle(title){
            this.subject = 'Re: '+title
        },
        updateNotificationCheckTime(){
            let sendData
            if(this.currentBox == 'user'){
                sendData = {type: 'myMessage'}
            } else {
                sendData = {type: 'myChmessages'}
            }
            let response = axios.post('/notifCheckUpdate/', sendData)
        },
    }
});
