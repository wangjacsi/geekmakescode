Vue.component('my-channels', require('../../components/mywave/MyChannels.vue'));
Vue.component('my-tutorials', require('../../components/mywave/MyTutorials.vue'));
Vue.component('my-subscribed-channels', require('../../components/mywave/MyChannels.vue'));
Vue.component('my-notices', require('../../components/mywave/MyNotices.vue'));
Vue.component('sendmessagepopup', require('../../components/common/sendMessagePopup.vue'));

import eventHub from '../../event.js'
import marked from'marked'

const app = new Vue({
    el: '#app',
    data: {
        user:user,
        snsLists:[],
        options:null,
        views: {
            myChannelsTotal:null,
            myRegisteredTutorialsTotal:null,
            mySubscribedChannelsTotal:null,
            myInterestTutorialsTotal:null,
            main:null
        },
        viewLoadCnt:0,
        myChViewOptions:{
            editBtn:true,
            deleteBtn:true,
            subscribeBtn:false,
            viewType:'block'
        },
        mySubscribedChViewOptions:{
            subscribeBtn:true,
            viewType:'block'
        },
        myTutorialsOptions:{
            editBtn:true,
            deleteBtn:true,
            viewType:'block'
        },
        myRegisteredTutorialsOptions:{
            viewType:'block'
        },
        myInterestTutorialsOptions:{
            viewType:'block'
        },
        chAdminUsers:[],
        userPhotoDefault:null,
        followUsers:[],
        totalNewMessages:null,
        setRefreshTimer:false,
        category:'',
    },
    mounted(){
        let self = this
        eventHub.$on('notification-refresh-done', this.refreshNoticesDone)

        this.totalNewMessages = totalNewMessages
        this.views.myChannelsTotal = myChannelCnt
        this.views.myRegisteredTutorialsTotal = myRegisteredTutorialCnt
        this.views.mySubscribedChannelsTotal = mySubscribedChannelCnt
        this.views.myInterestTutorialsTotal = myInterestTutorialCnt


        if(myChannelCnt > 0)
            this.views.main = 'myChannels'
        else
            this.views.main = 'myRegisteredTutorials'

        this.chAdminUsers = chAdminUsers
        this.userPhotoDefault = userPhotoDefault
        this.followUsers = followUsers

        this.options = JSON.parse(user.options)
        _.forEach(this.options.sns, function(value, key) {
            self.snsLists.unshift(value)
        })
    },
    methods: {
        costSelectChange(){
            //console.log(this.category)
            eventHub.$emit('interesttutorials-cateroy-change', this.category)
        },
        sanitizeMarkedBody(content){
            if(content)
                return marked(content, { sanitize : true })
            else {
                return ''
            }
        },
        listViewChange(type, comp){
            if(comp == 'mychannels'){
                this.myChViewOptions.viewType = type
            } else if(comp == 'mysubscribedchannels'){
                this.mySubscribedChViewOptions.viewType = type
            } else if(comp == 'mytutorials'){
                this.myTutorialsOptions.viewType = type
            } else if(comp == 'registeredtutorials'){
                this.myRegisteredTutorialsOptions.viewType = type
            }

            eventHub.$emit(comp+'-list-view', type)
        },
        refreshNoticesDone(type){
            if(this.setRefreshTimer)
                $('#'+type).html('<i class="xi-renew xi-x"></i>')
        },
        changeMainView(view){
            this.views.main = view

            $.fn.matchHeight._update()
        },
        refreshNotices(notice){
            this.setRefreshTimer = false
            $('#'+notice).html('<i class="xi-spinner-4 xi-x xi-spin"></i>')
            eventHub.$emit(notice+'-refresh', {})

            let self = this
            setTimeout(function(){
                self.setRefreshTimer = true
                self.refreshNoticesDone(notice)
            }, 2000, notice);
        },
        async getNotificationsByEl(key){
            let data = await axios.post('/getNotifByUserPart', {type: key})
        },
        async getNotificationsByAll(){
            let self = this
            let data = await axios.post('/getNotifByUserAll', this.views)
            _.forEach(data.data, function(value, key){
                self[key].data = value.data
                self[key].pagination.current_page = value.current_page
                self[key].pagination.first = value.first
                self[key].pagination.from = value.from
                self[key].pagination.last = value.last
                self[key].pagination.last_page = value.last_page
                self[key].pagination.next = value.next
                self[key].pagination.path = value.path
                self[key].pagination.per_page = value.per_page
                self[key].pagination.prev = value.prev
                self[key].pagination.to = value.to
                self[key].pagination.total = value.total
            })
            //console.log(data.data.myChNotices)
        }
    }
});
