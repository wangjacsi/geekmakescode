Vue.component('my-channels', require('../../components/mywave/MyChannels.vue'));
Vue.component('my-tutorials', require('../../components/mywave/MyTutorials.vue'));
Vue.component('my-subscribed-channels', require('../../components/mywave/MyChannels.vue'));
Vue.component('tech', require('../../components/mywave/Tech.vue'));
//Vue.component('project', require('../../components/mywave/Project.vue'));
Vue.component('profile', require('../../components/mywave/Profile.vue'));
Vue.component('contact', require('../../components/mywave/Contact.vue'));
Vue.component('password', require('../../components/mywave/Password.vue'));
Vue.component('settings', require('../../components/mywave/Settings.vue'));
Vue.component('sendmessagepopup', require('../../components/common/sendMessagePopup.vue'));
Vue.component('usermessagepopup', require('../../components/common/sendMessagePopup.vue'));

import eventHub from '../../event.js'
import vSelect from 'vue-select'
import marked from'marked'
Vue.component('v-select', vSelect)

const app = new Vue({
    el: '#app',
    data: {
        options:options,
        owner:owner,
        user:user,
        mainView:'user-info',
        myChViewOptions:{
            subscribeBtn:false,
            editBtn:owner,
            deleteBtn:owner,
            viewType:'list',
            noDataCommentOn:true,
            noDataComment:'운영중인 채널이 없습니다'
        },
        mySubscribedChViewOptions:{
            subscribeBtn:owner,
            viewType:'list',
            noDataCommentOn:true,
            noDataComment:'구독중인 채널이 없습니다'
        },
        myTutorialsOptions:{
            viewType:'list',
            editBtn:owner,
            deleteBtn:owner,
            noDataCommentOn:true,
            noDataComment:'운영중인 강좌가 없습니다'
        },
        myRegisteredTutorialsOptions:{
            viewType:'list',
            noDataCommentOn:true,
            noDataComment:'수강하는 강좌가 없습니다'
        },
        interestTutorialsOptions:{
            viewType:'list',
            noDataCommentOn:true,
            noDataComment:'관심 강좌가 없습니다'
        },
        myTechOptions:{
            editBtn:owner,
            deleteBtn:owner,
            noDataCommentOn:true,
            noDataComment:'보유 기술 리스트가 없습니다'
        },
        myProjectOptions:{
            editBtn:owner,
            deleteBtn:owner,
            noDataCommentOn:true,
            noDataComment:'프로젝트 리스트가 없습니다'
        },
        userPhotoDefault:null,
        phone:null,
        site:null,
        snsLists:[],
        userLoginStatus:userLoginStatus,
    },
    mounted(){
        let self = this
        this.userPhotoDefault = userPhotoDefault

        eventHub.$on('changeView', this.changeView)

        eventHub.$on('contactUpdate', this.contactUpdate)

        this.phone = this.options.phone
        _.forEach(this.options.sns, function(value, key) {
            self.snsLists.unshift(value)
        });

        eventHub.$on('snsUpdate', this.snsUpdate)

        eventHub.$on('userSettingsUpdate', this.settingsUpdate)

        eventHub.$on('profileUpdate', this.profileUpdate)
    },
    methods: {
        setAlramShow(title, content){
            $('#alarm #title').text(title)
            $('#alarm #content').html(content)
            $('#alarm').modal('show')
            setTimeout(function(){ $('#alarm').modal('hide') }, 3000)
        },
        userMessagePopup(){
            if(!this.userLoginStatus){
                this.setAlramShow('채널 메세지 전송', '채널에 메세지를 전송하기 위해서는 로그인이 필요합니다')
            } else {
                setTimeout(function() {  $('.user-message').addClass('open'); }, 300)
            }
        },
        sanitizeMarkedBody(content){
            if(content)
                return marked(content, { sanitize : true })
            else {
                return ''
            }
        },
        detailTechView(){
            eventHub.$emit('tech:changeView')
        },
        profileEditFormView() {
            this.mainView = 'user-profile'
        },
        changeView(){
            this.mainView = 'user-info'
        },
        contactUpdate(data){
            console.log(data)
            this.options.phone = data.phone
            this.options.site = data.site
        },
        snsUpdate(snsLists){
            this.snsLists = snsLists
        },
        settingsUpdate(settings){
            console.log(settings)
            this.options.viewoptions = settings
        },
        profileUpdate(user){
            this.user = user
            console.log(user)
        }
    }
});
