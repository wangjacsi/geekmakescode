Vue.component('tutoriallist', require('../../components/mywave/MyTutorials.vue'));
Vue.component('sendmessagepopup', require('../../components/common/sendMessagePopup.vue'));

import eventHub from '../../event.js'

const app = new Vue({
    el: '#app',
    data: {
        searchWord:searchWord,
        tutorialsOptions:{
            editBtn:false,
            deleteBtn:false,
            viewType:'block',
            noDataCommentOn:true,
            noDataComment:'검색 결과가 없습니다',
            gridClass:'col-lg-4 col-md-6',

        },
        getSearchTutorialUrl:'/getSearchTutorialsInfo/'+searchWord,
        orderType:'recent',
    },
    computed: {

    },
    updated: function() {
        let self = this

        this.$nextTick(function () {

        })
    },
    mounted() {

    },
    methods: {
        orderBy(type){
            if(this.orderType != type){
                this.orderType = type
                eventHub.$emit('orderByChange', this.orderType)
            }
        }
    }
});
