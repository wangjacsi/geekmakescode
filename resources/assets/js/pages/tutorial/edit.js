Vue.component('curriculumlist', require('../../components/tutorial/CurriculumList.vue'));
Vue.component('tutorialinfo', require('../../components/tutorial/TutorialInfo.vue'));
Vue.component('tutorialimage', require('../../components/tutorial/TutorialImage.vue'));
Vue.component('confirm', require('../../components/common/Confirm.vue'));
Vue.component('tutorialsetting', require('../../components/tutorial/TutorialSetting.vue'));
Vue.component('sendmessagepopup', require('../../components/common/sendMessagePopup.vue'));

import eventHub from '../../event.js'
import VueFroala from 'vue-froala-wysiwyg'
Vue.use(VueFroala)
import Multiselect from 'vue-multiselect'
Vue.component('multiselect', Multiselect)

const app = new Vue({
    el: '#app',
    data: {
        user:codewwwave.user,
        videos:videos,
        curriculumViewOptions:{
            deleteBtn:true,
            editBtn:true,
            setWatchBtn:false,
            noDataCommentOn:true,
            noDataComment:'강좌 리스트가 없습니다',
            status:true
        },
        deleteVideoContent:{
            title:'비디오 강좌 삭제',
            body:'비디오 강좌를 삭제하기 원하시면 아래 확인 버튼을 클릭해주세요',
            cancel:'취소',
            submit:'확인'
        },
        tutorial:tutorial,
        channel:channel,
    },
    computed: {
          // a computed getter
    },
    updated: function() {
        let self = this

        this.$nextTick(function () {

        })
    },
    mounted() {
        let self = this

        eventHub.$on('tutorialInfoUpdate', this.tutorialInfoUpdate)
    },
    methods: {
        tutorialInfoUpdate(tutorial, channel){
            console.log(tutorial)
            console.log(channel)
            this.tutorial = tutorial
            this.channel = channel

        }
    }
});
