Vue.component('tutoriallist', require('../../components/mywave/MyTutorials.vue'));
Vue.component('sendmessagepopup', require('../../components/common/sendMessagePopup.vue'));

import eventHub from '../../event.js'

const app = new Vue({
    el: '#app',
    data: {
        tutorialsOptions:{
            editBtn:false,
            deleteBtn:false,
            viewType:'block',
            noDataCommentOn:true,
            noDataComment:'강좌 리스트가 없습니다',
            gridClass:'col-lg-4 col-md-6',

        },
        getSearchTutorialUrl:'/getTutorialsInfo',
        orderType:'recent',
    },
    computed: {

    },
    updated: function() {
        let self = this

        this.$nextTick(function () {

        })
    },
    mounted() {

    },
    methods: {
        orderBy(type){
            if(this.orderType != type){
                this.orderType = type
                eventHub.$emit('orderByChange', this.orderType)
            }
        }
    }
});
