Vue.component('sendmessagepopup', require('../../components/common/sendMessagePopup.vue'));
import VueFroala from 'vue-froala-wysiwyg'
Vue.use(VueFroala)

import Multiselect from 'vue-multiselect'
Vue.component('multiselect', Multiselect)


const app = new Vue({
    el: '#app',
    data: {
        form:{
            title:'',
            description:'',
            image1:'',
            channel:'',
            visibility:'',
            cost_type:'',
            allow_votes:1,
            allow_comments:1,
            allow_reviews:1,
            cost:null,
            skill:''
        },
        channels:channels,
        visibilities:visibilities,
        skills:skills,
        costTypes:costTypes,
        errorMessage:'',
        cost:{
            once:[5000, 10000, 20000, 25000, 30000, 40000, 50000, 60000, 70000, 80000, 90000, 100000, 120000, 150000, 200000],
            divide:[500, 1000, 2000, 5000, 10000, 20000]
        },
        costSelect:'self',
        costSelf:null,
        costSelectOptions:[],
        tempID:null,
        froalaConfig: {
            imageDefaultWidth:0,
            requestHeaders: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            language: 'ko',
            imageUploadParam: 'imageFile',
            imageUploadURL: '/editor/uploadToTemp',
            imageUploadMethod: 'POST',
            imageMaxSize: 2 * 1024 * 1024,
            imageUploadParams: {id: Math.random().toString(36).substr(2, 9), type: 'tutorials', width:800, mode:'resize', limitWidth:1920, limitHeight:1000, fileSize:2},
            imageAllowedTypes: ['jpeg', 'jpg', 'png', 'gif', 'svg'],
            heightMin: 500,
            heightMax: 1000,
            fontFamily: {
              'Arial,Helvetica,sans-serif': 'Arial',
              'Georgia,serif': 'Georgia',
              'Impact,Charcoal,sans-serif': 'Impact',
              'Tahoma,Geneva,sans-serif': 'Tahoma',
              "'Times New Roman',Times,serif": 'Times New Roman',
              'Verdana,Geneva,sans-serif': 'Verdana',
              'Helvetica,sans-serif':'Helvetica',
              "'Helvetica Neue',sans-serif":'Helvetica Neue',
              "'Roboto', sans-serif":'Roboto',
              "'Nunito', sans-serif":  'Nunito',
              "'Nunito Sans', sans-serif":'Nunito Sans',
              "'Montserrat', sans-serif": 'Montserrat',
              "'Oswald', sans-serif":'Oswald',
              "'Raleway', sans-serif":'Raleway',
              "'Nanum Gothic', sans-serif":'나눔 고딕 Nanum Gothic',
              "'Nanum Myeongjo', serif":'나눔 명조 Nanum Myeongjo',
              "'Nanum Pen Script', cursive":'나눔 펜 스크립트 Nanum Pen Script',
              "'Nanum Gothic Coding', monospace":'나눔 고딕 코딩 Nanum Gothic Coding',
              "'Nanum Brush Script', cursive":'나눔 브러쉬 스크립트 Nanum Brush Script',
              "'Song Myung', serif":'송명 Song Myung',
              "'Poor Story', cursive":'푸어 스토리 Poor Story',
              "'Gugi', cursive":'구기 Gugi',
              "'Gothic A1', sans-serif":'고딕 A1 Gothic A1',
              "'Black Han Sans', sans-serif":'블랙 한글 Sans Black Han Sans',
              "'Do Hyeon', sans-serif":'도현 Do Hyeon',
              "'Hi Melody', cursive":'하이 멜로디 Hi Melody',
              "'Sunflower', sans-serif":'썬프라워 Sunflower',
              "'Jua', sans-serif":'주아 Jua',
              "'Gamja Flower', cursive":'감자 플라워 Gamja Flower',
              "'East Sea Dokdo', cursive":'동해 독도 East Sea Dokdo',
              "'Gaegu', cursive":'개구 Gaegu',
              "'Stylish', sans-serif":'스타일리쉬 Stylish',
              "'Black And White Picture', sans-serif":'블랙 앤 화이트 픽쳐 Black And White Picture',
              "'Kirang Haerang', cursive":'키랑 해랑 Kirang Haerang',
              "'Cute Font', cursive":'큐트 폰트 Cute Font',
              "'Yeon Sung', cursive":'연성 Yeon Sung',
              "'Dokdo', cursive":'독도 Dokdo'
            },
            //fontFamilySelection: true,
            toolbarButtons: ['fullscreen', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '|', 'fontFamily', 'fontSize', 'color', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', '|', 'insertLink', 'insertImage', 'insertVideo', 'insertTable', '|', 'emoticons', 'specialCharacters', 'insertHR', 'selectAll', 'clearFormatting', '|', 'print', 'help', 'html', '|', 'undo', 'redo'],
            events : {
                'froalaEditor.image.error': function (e, editor, error, response){
                    console.log(error)
                    console.log(response)
                    $('#alarm #title').text('이미지 업로드 에러')
                    $('#alarm #content').html('이미지 업로드에 실패하였습니다')
                    // Bad link.
                    if (error.code == 1) {
                        $('#alarm #content').html('유효한 링크가 아닙니다')
                    }

                    // No link in upload response.
                    else if (error.code == 2 || error.code == 3 || error.code == 4 || error.code == 7) {
                        $('#alarm #content').html('업로드 가능한 본문 이미지는<br>사이즈 2Mbytes 이하<br>폭 1920픽셀 이하<br>높이 1000픽셀 이하입니다')
                        if(response){
                            let re = JSON.parse(response)
                            if('message' in re){
                                let message = ''
                                _.forEach(re.message, function(value, key) {
                                      message += value
                                      message += '<br>'
                                 });
                                 $('#alarm #content').html(message)
                            }
                        }
                    }

                    // Image too text-large.
                    else if (error.code == 5) {
                        $('#alarm #content').html('업로드 가능한 본문 이미지의 사이즈는 2Mbytes 이하입니다')
                    }

                    // Invalid image type.
                    else if (error.code == 6) {
                        $('#alarm #content').html('업로드 가능한 이미지 타입은 jpeg, jpg, png, gif, svg 입니다')
                    }
                    $('#alarm').modal('show')
                },
                'froalaEditor.image.uploaded' : function (e, editor, response) {
                    // Image was uploaded to the server.
                    console.log('froalaEditor.image.uploaded')
                },
                'froalaEditor.image.inserted' : function (e, editor, $img, response) {
                  // Image was inserted in the editor.
                  console.log('froalaEditor.image.inserted')
                },
                'froalaEditor.image.replaced' : function (e, editor, $img, response) {
                  // Image was replaced in the editor.
                  console.log('froalaEditor.image.replaced')
                },
                'froalaEditor.image.removed' : async function (e, editor, $img) {
                    console.log('froalaEditor.image.removed')
                    let response = await axios.post('/editor/deleteImage', {src: $img.attr('src'), type:'local'})
                }
            }
        },
        thumbnailImage:null,
        headImage:null,
        images:[],
        croppers:[],
        inputImage:[],
        defaultImage:[],
        waveHeadImage:null,
        waveThumbImage: null,
        cropOptions: {},
        getOptions:{},
        cropperImages:['waveHeadImage', 'waveThumbImage'],
        headDefault: '/img/tutorials/head_image.jpg',
        thumbDefault: '/img/tutorials/thumbnail.jpg',
        cropperLoad:false,
        tags:[],
        tag:[],
        isLoading: false,
        maxTags:20,
    },
    computed: {
          // a computed getter
    },
    updated: function() {
        let self = this
        this.$nextTick(function () {

        })
    },
    mounted() {
        var self = this

        $('#cost_type').on('change', function(){
            self.costTypeChange()
        })

        // froala
        this.tempID = this.froalaConfig.imageUploadParams.id

        // Images get
        this.images = document.querySelectorAll('.crop-image')
        // get Imput images
        this.inputImage = document.querySelectorAll('.input-image-file')
        // default
        this.defaultImage.push(this.headDefault)
        this.defaultImage.push(this.thumbDefault)
        // set options
        this.getOptions = {
            waveHeadImage :{
                width: 1920,
                height: 639,
                minWidth: 150,
                minHeight: 150,
                maxWidth: 2000,
                maxHeight: 1000,
                imageSmoothingEnabled: false,
                imageSmoothingQuality: 'high'
            },
            waveThumbImage: {
                width: 360,
                height: 220,
                minWidth: 30,
                minHeight: 30,
                maxWidth: 500,
                maxHeight: 500,
                imageSmoothingEnabled: false,
                imageSmoothingQuality: 'high'
            }
        }
        this.cropOptions = { waveHeadImage: {
              aspectRatio: 3,
              viewMode: 2,
              highlight:true,
              modal:false,
              autoCropArea: 1,
              minCanvasWidth:100,
              minCanvasHeight:100,
              minCropBoxWidth:100,
              minCropBoxHeight:100,
              ready: function (e) {
                  self.getImageCrop(0, self.getOptions['waveHeadImage'])
              },
              cropstart: function (e) {},
              cropmove: function (e) {},
              cropend: function (e) {
                  self.getImageCrop(0, self.getOptions['waveHeadImage'])
              },
              crop: function (e) {},
              zoom: function (e) {}
          },
          waveThumbImage: {
              aspectRatio: 1.64,
              viewMode: 2,
              highlight:true,
              modal:false,
              autoCropArea: 1,
              minCanvasWidth:30,
              minCanvasHeight:30,
              minCropBoxWidth:30,
              minCropBoxHeight:30,
              ready: function (e) {
                  self.getImageCrop(1, self.getOptions['waveThumbImage'])
              },
              cropstart: function (e) {},
              cropmove: function (e) {},
              cropend: function (e) {
                  self.getImageCrop(1, self.getOptions['waveThumbImage'])
              },
              crop: function (e) {},
              zoom: function (e) {}
          }
        }

        this.runCropInit()
    },
    methods: {
        addTag (newTag) {
            newTag = _.trim(newTag)
            newTag = newTag.replace(/[&\/\\#,+()$~^%\['\]":;*?!&<=>{|}]/g,'')
            //console.log(newTag)

            this.tag.push(newTag)
            this.tags.push(newTag)
        },
        asyncFind (query) {
            let self =this
              this.isLoading = true
              axios.get('/search/tags?q=' +escape(query))
                .then(response => {
                    self.tags = []
                    _.forEach(response.data, function(value, key) {
                        self.tags.push(value.name)
                    })
                    this.isLoading = false
                    //console.log(response)
                })
                .catch(e => {
                    console.log(e)
                })
        },
        clearAll () {
            this.tag = []
        },
        limitText (count) {
          return `외 ${count} 태그들이 선택됨`
        },
        costTypeChange(){
            this.form.cost_type = $('#cost_type').val()
            this.costChangeToSel()
        },
        costSelectChange(){
            //alert($('#costSelect').val())
            this.costSelect = $('#costSelect').val()
        },
        costChangeToSel(){
            this.costSelect = ''
            if(this.form.cost_type == 'free'){
                this.costSelectOptions = []
                this.costSelf = 0
            }
            else {
                this.costSelf = 1000
                this.costSelectOptions = this.cost[this.form.cost_type]
                $('#costSelect').val('')
            }
        },
        costChangeToSelf(){
            this.costSelect = 'self'
        },
        imageCancel (num) {
            this.resetInputImage(num)
        },
        resetInputImage(num){
          images[num].src = defaultImage[num];
          croppers[num].destroy();
          croppers[num] = new Cropper(images[num], options[cropperImages[num]]);
          inputImage[num].value = null;
        },
        formValid(){
            var errorChk = true
            this.errorMessage = ''

            this.form.title = _.trim(this.form.title)
            if(this.form.title == ''){
                errorChk = false
                this.errorMessage = '강좌 제목을 작성해주세요'
                return false
            }

            this.form.channel = $("#channel").val()
            if(this.form.channel == ''){
                errorChk = false
                this.errorMessage = '강좌가 속할 채널을 선택해주세요'
                return false
            }

            this.form.skill = $("#skill").val()
            if(this.form.skill == ''){
                errorChk = false
                this.errorMessage = '강좌 레벨을 선택해주세요'
                return false
            }

            if(this.form.description == ''){
                errorChk = false
                this.errorMessage = '강좌 소개글을 작성해주세요'
                return false
            }

            this.form.visibility = $("#visibility").val()
            if(this.form.visibility == ''){
                errorChk = false
                this.errorMessage = '강좌 공개를 선택해주세요'
                return false
            }

            //가격 정책 완료 후 변경 필요
            this.form.cost_type = 'free'//$("#cost_type").val()
            if(this.form.cost_type == ''){
                errorChk = false
                this.errorMessage = '강좌 가격 정책을 선택해주세요'
                return false
            }

            if(this.form.cost_type == 'free'){
                this.form.cost = 0
            } else {
                if(this.costSelect == 'self'){
                    this.form.cost = this.costSelf
                } else {
                    this.form.cost = this.costSelect
                }
                if(this.form.cost == ''){
                    errorChk = false
                    this.errorMessage = '강좌 가격을 선택해주세요'
                    return false
                }
            }
            this.form.allow_reviews = $("#allow_reviews").val()
            if(this.form.allow_reviews == ''){
                errorChk = false
                this.errorMessage = '강좌 리뷰 기능 사용 유무를 선택해주세요'
                return false
            }

            this.form.allow_votes = $("#allow_votes").val()
            if(this.form.allow_votes == ''){
                errorChk = false
                this.errorMessage = '강좌 좋아요 기능 사용 유무를 선택해주세요'
                return false
            }

            this.form.allow_comments = $("#allow_comments").val()
            if(this.form.allow_comments == ''){
                errorChk = false
                this.errorMessage = '강좌 댓글 기능 사용 유무를 선택해주세요'
                return false
            }

            /*// image set
            if(inputImage[0].value){ //head image
                if(image1.size / (1024 * 1024) > 0.5) { // 2M 보다 큰 경우
                    errorChk = false
                    this.errorMessage = '이미지 사이즈가 2MB를 초과하였습니다'
                    return false
                }
                this.form.image1 = image1
            } else {
                this.form.image1 = ''
            }*/

            this.errorMessage = '강좌 생성 중 ...'
            return errorChk
        },
        upload(){

          this.errorMessage = ''

          if(this.formValid()){
              let data = new FormData();
              //data = this.form

              data.append('title', this.form.title)
              data.append('channel_id', this.form.channel)
              data.append('description', this.form.description)
              data.append('visibility', this.form.visibility)
              data.append('skill', this.form.skill)
              data.append('cost_type', this.form.cost_type)
              data.append('cost', this.form.cost)
              data.append('allow_reviews', this.form.allow_reviews)
              data.append('allow_votes', this.form.allow_votes)
              data.append('allow_comments', this.form.allow_comments)
              data.append('tempID', this.tempID)
              if(this.tag.length > 0)
                data.append('tag', this.tag)

              // Image
              if(this.inputImage[0].value && this.waveHeadImage != null){ //head image
                  data.append('head_image', this.waveHeadImage)
              }

              if(this.inputImage[1].value && this.waveThumbImage != null){ //logo image
                  data.append('thumbnail', this.waveThumbImage)
              }


              axios.post(`/tutorial/store`, data,
              {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            })
            .then(response => {
                console.log(response)
                if(response.data.result == 'success'){
                    this.errorMessage = '강좌 생성이 완료되었습니다'
                    //window.location.href = "/channel/"+response.data.slug
                } else if(response.data.result == 'error') {
                    this.errorMessage = '강좌 생성에 실패하였습니다. 다시 시도해 주세요.'
                    if('errors' in response.data){
                      this.errorMessage = ''
                      let message = ''
                      _.forEach(response.data.errors, function(value, key) {
                            message += value
                            message += '<br>'
                       });
                       this.errorMessage = message
                    }
                }
            })
            .catch(e => {
              //alert(222);
                this.errorMessage = '강좌 생성에 실패하였습니다. 다시 시도해 주세요.'
                console.log(e)
            })
          }
      },
      runCropInit(){
          let self = this
          // cropper set
          let length = this.images.length

          for (let i = 0; i < length; i++) {

              if(!this.cropperLoad){
                  this.croppers[i] = new Cropper(this.images[i], this.cropOptions[this.cropperImages[i]]);
              }

              this.inputImage[i].onchange = function () {
                let files = this.files;
                let file;
                let URL = window.URL || window.webkitURL;

                if (self.croppers[i] && files && files.length) {
                      file = files[0];

                      if (/^image\/\w+/.test(file.type)) {
                          console.log(file.size)
                          console.log(file.size / (1024 * 1024))
                        self.images[i].src = URL.createObjectURL(file);
                        self.croppers[i].destroy();
                        self.croppers[i] = new Cropper(self.images[i], self.cropOptions[self.cropperImages[i]]);
                        //self.croppers[i].getCroppedCanvas(self.getOptions[self.cropperImages[i]]);
                      } else {
                        window.alert('이미지 파일을 선택해 주세요.');
                      }
                }
            }
            //this.croppers[i].getCroppedCanvas(this.getOptions[this.cropperImages[i]]);
          }
          this.cropperLoad = true
      },
      getImageCrop(index, option){
          let self = this
          this.croppers[index].getCroppedCanvas(option).toBlob(function (blob) {
              if(index == 0)
                  self.waveHeadImage = blob
              else
                  self.waveThumbImage = blob
          })
      },
      imageCancel (num) {
          this.resetInputImage(num)
      },
      resetInputImage(num){
          this.images[num].src = this.defaultImage[num];
          this.croppers[num].destroy();
          this.croppers[num] = new Cropper(this.images[num], this.cropOptions[this.cropperImages[num]]);
          this.inputImage[num].value = null;
          if(num == 0)
              this.waveHeadImage = null
          else
              this.waveThumbImage = null
      },

    }
});
