Vue.component('curriculumlist', require('../../components/tutorial/CurriculumList.vue'));
Vue.component('userlist', require('../../components/channel/UsersList.vue'));
Vue.component('interest', require('../../components/tutorial/InterestTutorialModal.vue'));
Vue.component('reviews', require('../../components/tutorial/ReviewList.vue'));
Vue.component('reviewform', require('../../components/tutorial/ReviewForm.vue'));
Vue.component('confirm', require('../../components/common/Confirm.vue'));
Vue.component('sendmessagepopup', require('../../components/common/sendMessagePopup.vue'));

import vSelect from 'vue-select'
Vue.component('v-select', vSelect)
import eventHub from '../../event.js'

const app = new Vue({
    el: '#app',
    data: {
        user:codewwwave.user,
        owner:owner,
        tutorial:tutorial,
        reviewAvg:(reviewAvg).toFixed(2),
        reviewGroup:[0,0,0,0,0],
        upVotesTotal:upVotesTotal,
        totalReviews:0,
        voted:voted,
        reviewed:reviewed,
        isRegistered:isRegistered,
        myReview:myReview,
        interested:interested,
        userInterestCategories: userInterestCategories,
        videos:videos,
        recommendTutorials: recommendTutorials,
        backgroundColors: backgroundColors,
        progressStatus:progressStatus,
        curriculumViewOptions:{
            deleteBtn:false,
            editBtn:false,
            noDataCommentOn:true,
            noDataComment:'강좌 리스트가 없습니다'
        },
        userListViewOptions:{
            channelAdminAdd:false,
            channelAdminDelete:false,
            channelAdminLevel:false,
            deleteBtn:false,
            noDataCommentOn:false,
            noDataComment:'',
            type:'block'
        },
        deleteMyReviewContent:{
            title:'나의 리뷰 삭제',
            body:'나의 리뷰를 삭제하기 원하시면 아래 확인 버튼을 클릭해주세요',
            cancel:'취소',
            submit:'확인'
        },
    },
    computed: {
          // a computed getter
    },
    updated: function() {
        let self = this
        let $progress_bar = $('.skills-item')

        this.$nextTick(function () {
            $progress_bar.each(function () {
                jQuery(this).waypoint(function () {
                    $(this.element).find('.count-animate').countTo();
                    $(this.element).find('.skills-item-meter-active').fadeTo(300, 1).addClass('skills-animate');
                    this.destroy();
                }, {offset: '90%'});
            });
        })
    },
    mounted() {
        //console.log(status)
        let self = this
        _.forEach(reviewGroup, function(value, key) {
            self.totalReviews += value.cnt
            if(value.review < 3){
                Vue.set(self.reviewGroup, '0', self.reviewGroup[0] + value.cnt)
            } else if(value.review < 5) {
                Vue.set(self.reviewGroup, '1', self.reviewGroup[1] + value.cnt)
            } else if(value.review < 7) {
                Vue.set(self.reviewGroup, '2', self.reviewGroup[2] + value.cnt)
            } else if(value.review < 9) {
                Vue.set(self.reviewGroup, '3', self.reviewGroup[3] + value.cnt)
            } else {
                Vue.set(self.reviewGroup, '4', self.reviewGroup[4] + value.cnt)
            }
        });

        eventHub.$on('interestTutorial', this.interestTutorial)

        eventHub.$on('deleteMyReview', this.deleteMyReview)

        eventHub.$on('createReviewDone', this.createReviewDone)
    },
    methods: {
        transVideoPlayTime(videos){
            let tseconds = _.sumBy(videos, 'playtime')
            let timeString = ''
            let hours = Math.floor(tseconds / 3600)
            let minutes =  Math.floor( (tseconds - (hours * 3600)) / 60 )
            //let seconds = tseconds % 60
            if(hours > 0 ){ // hour
                timeString += hours + ' 시간 '
            }
            if(minutes > 0){
                timeString += minutes + ' 분'
            }
            if(timeString == ''){
                return 0
            }
            return timeString
        },
        setAlramShow(title, content){
            $('#alarm #title').text(title)
            $('#alarm #content').html(content)
            $('#alarm').modal('show')
            setTimeout(function(){ $('#alarm').modal('hide') }, 3000)
        },
        editMyReview(data){
            eventHub.$emit('editReviewToForm', this.myReview)
        },
        async deleteMyReview(data){
            $('#deleteMyReview').modal('hide')

            let response = await axios.delete('/review/'+ data.data.id + '/deleteReview')

            eventHub.$emit('deleteReviewDone', {'id':data.data.id} )

            this.myReview = null
            this.reviewed = false
            this.totalReviews = this.totalReviews - 1

            this.setCountReviewGroup(data.data.review, 'minus')

            this.getAverageStarRate()
        },
        setCountReviewGroup(review, type){
            if(type == 'minus'){
                if(review < 3){
                    Vue.set(this.reviewGroup, '0', this.reviewGroup[0] - 1)
                } else if(review < 5) {
                    Vue.set(this.reviewGroup, '1', this.reviewGroup[1] - 1)
                } else if(review < 7) {
                    Vue.set(this.reviewGroup, '2', this.reviewGroup[2] - 1)
                } else if(review < 9) {
                    Vue.set(this.reviewGroup, '3', this.reviewGroup[3] - 1)
                } else {
                    Vue.set(this.reviewGroup, '4', this.reviewGroup[4] - 1)
                }
            } else if(type == 'plus'){
                if(review < 3){
                    Vue.set(this.reviewGroup, '0', this.reviewGroup[0] + 1)
                } else if(review < 5) {
                    Vue.set(this.reviewGroup, '1', this.reviewGroup[1] + 1)
                } else if(review < 7) {
                    Vue.set(this.reviewGroup, '2', this.reviewGroup[2] + 1)
                } else if(review < 9) {
                    Vue.set(this.reviewGroup, '3', this.reviewGroup[3] + 1)
                } else {
                    Vue.set(this.reviewGroup, '4', this.reviewGroup[4] + 1)
                }
            }

        },
        async getAverageStarRate(){
            let response = await axios.post('/tutorial/'+ this.tutorial.slug + '/reviewavg')

            this.reviewAvg = response.data.toFixed(2)
        },
        deleteMyReviewConfirm(){
            $('#deleteMyReview').modal('show')
            eventHub.$emit('deleteMyReviewConfirm', {'id':this.myReview.id, 'review':this.myReview.review} )
        },
        voteToTutorial(vote){
            if(this.owner || !this.tutorial.allow_votes){
                this.setAlramShow('강좌 좋아요 실패', '해당 강좌의 운영진은 강좌 좋아요를 하실 수 없습니다')
                return false
            }

            if(!this.user.authenticated){
                this.setAlramShow('강좌 좋아요 실패', '강좌 좋아요를 하기 원하시면 로그인을 먼저해주세요')
                return false
            }

            axios.post('/tutorial/'+ this.tutorial.slug + '/vote', {vote:vote})
            this.voted = vote
            this.upVotesTotal = vote ? this.upVotesTotal +  1 : this.upVotesTotal - 1
        },
        interestModal(interest){
            if(this.owner){
                this.setAlramShow('관심 강좌 등록 실패', '해당 강좌의 운영진은 관심 강좌 등록을 하실 수 없습니다')
                return false
            }

            if(!this.user.authenticated){
                this.setAlramShow('관심 강좌 등록 실패', '관심 강좌 등록을 원하시면 로그인을 먼저해주세요')
                return false
            }

            $('#interestTutorialModal').modal('show')
            eventHub.$emit('interestTutorialModalOn', {'id':this.tutorial.slug, 'interest':interest} )
        },
        interestTutorial(data){
            console.log(data)
            $('#interestTutorialModal').modal('hide')
            axios.post('/tutorial/'+ data.data.id + '/interest', {interest:data.data.interest, category:data.category, type:data.type})

            this.interested = data.type
        },
        starRate(review){
            let i = 0
            let html = ''
            let scoreLeft = review//this.reviewAvg
            for(i=0; i<5; i++){
                if(scoreLeft >= 1){
                    html+= '<li><a><i class="utouch-icon fas fa-star"></i></a></li>'
                } else if(scoreLeft >= 0.2) {
                    html+= '<li><a><i class="utouch-icon fas fa-star-half-alt"></i></a></li>'
                } else {
                    html+= '<li><a><i class="utouch-icon far fa-star"></i></a></li>'
                }
                scoreLeft -= 1
            }
            return html
        },
        createReviewDone(data){
            if(this.myReview && (data.review != this.myReview.review)){ // edit
                this.setCountReviewGroup(this.myReview.review, 'minus')
                this.setCountReviewGroup(data.review, 'plus')
                this.getAverageStarRate()
            } else if(!this.myReview){ // new
                this.setCountReviewGroup(data.review, 'plus')
                this.getAverageStarRate()
                this.totalReviews = this.totalReviews + 1
            }
            this.reviewed = true
            this.myReview = data
        },
        async registTutorial(){
            let self = this

            if(this.owner){
                this.setAlramShow('강좌 수강 등록 실패', '해당 강좌의 운영진은 강좌 등록을 하실 수 없습니다')
                return false
            }

            if(!this.user.authenticated){
                this.setAlramShow('강좌 수강 등록 실패', '강좌 수강을 원하시면 로그인을 먼저해주세요')
                return false
            }

            if(this.isRegistered){
                this.setAlramShow('강좌 수강 등록 실패', '이미 수강중인 강좌입니다')
                return false
            }

            if(this.tutorial.progress_status == 'prepare' ){
                this.setAlramShow('강좌 수강 등록 실패', '강좌가 준비중입니다. 강좌 오픈까지 기다려 주세요.')
                return false
            }

            if(this.tutorial.progress_status == 'stop' ){
                this.setAlramShow('강좌 수강 등록 실패', '강좌 진행이 중지되었습니다. 채널 관리자에게 문의하세요.')
                return false
            }

            if(this.tutorial.visibility == 'private'){
                this.setAlramShow('강좌 수강 등록 실패', '비공개 강좌입니다. 채널 관리자에게 문의하세요.')
                return false
            }

            let error = false
            let response = await axios.post('/tutorial/'+ this.tutorial.slug + '/regist').catch(e => {
                self.setAlramShow('강좌 수강 등록 실패', '수강 등록에 실패하였습니다. 관리자에게 문의해주세요.')
                error = true
            })
            if(error){
                return false
            }

            self.setAlramShow('강좌 수강 등록 완료', '수강 등록을 완료하였습니다.')

            eventHub.$emit('registeredTutorial', true)

            this.isRegistered = true
        }
    }
});
