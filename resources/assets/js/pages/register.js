//require('../bootstrap');
//window.Vue = require('vue');
Vue.component('sendmessagepopup', require('../components/common/sendMessagePopup.vue'));

const app = new Vue({
    el: '#app',
    data: {
        form:{
            name:'',
            email:'',
            password:'',
            password_confirmation:''
        },
        errorMessage:''
    },
    computed: {
      // a computed getter
      reversedMessage: function () {
        console.log('Test')
      }
    },
    methods: {
        formValid(){
            var errorChk = true
            this.errorMessage = ''
            if(this.form.name == ''){
                this.errorMessage += '사용자 이름을 기입해주세요<br>'
                errorChk = false
            }
            else if(this.form.name.length < 2 || this.form.name.length > 20){
                this.errorMessage += '사용자 이름은 2자 이상 20자 이하로 입력해주세요<br>'
                errorChk = false
            }
            if(this.form.email == ''){
                this.errorMessage += '이메일을 기입해주세요<br>'
                errorChk = false
            }
            if(this.form.password.length < 6){
                this.errorMessage += '최소 6자 이상의 비밀번호를 기입해주세요<br>'
                errorChk = false
            }
            else if(this.form.password_confirmation != this.form.password){
                this.errorMessage += '비밀번호가 일치하지 않습니다<br>'
                errorChk = false
            }

            let check = checkPassword(this.form.password, ['numbers', 'characters'], 6, 20)
            if(!check) {
                this.errorMessage = '비밀번호는 최소 6자이상 20자 이하로 숫자와 문자가 혼합되어야 합니다'
                errorChk = false
            }

            if(errorChk != true){
                return false
            }

            this.errorMessage = '가입 처리중 ...'
            return true
        },
        register(){
          this.errorMessage = ''

          if(this.formValid()){
            axios.post(`/register`, this.form)
            .then(response => {
                console.log(response)
                if(response.data.result == 'success'){
                    this.errorMessage = '가입이 완료되었습니다'
                    window.location.href = "/registered"
                } else if(response.data.result == 'error') {
                    this.errorMessage = '가입에 실패하였습니다. 다시 시도해 주세요.'
                    if('errors' in response.data){
                      //let temp = response.data.errors
                      //console.log(temp)
                      this.errorMessage = ''
                      let message = ''
                      _.forEach(response.data.errors, function(value, key) {
                            message += value
                            message += '<br>'
                            //return message
                       });
                       //console.log(message)
                       this.errorMessage = message
                    }
                }
                //console.log(response)
            })
            .catch(e => {
              //alert(222);
                this.errorMessage = '가입 실패. 다시 시도해 주세요.'
                console.log(e)
            })
          }
        }

    }


});
