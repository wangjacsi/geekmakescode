//require('../bootstrap');
//window.Vue = require('vue');
Vue.component('sendmessagepopup', require('../components/common/sendMessagePopup.vue'));

let run_test = 'tewadskdla'
console.log(run_test)

const app = new Vue({
    el: '#app',
    data: {
        loginForm:{
            email:'',
            password:'',
            remember:false
        },
        loginError:''
    },
    computed: {
      // a computed getter
      reversedMessage: function () {
        // `this` points to the vm instance
        return this.message.split('').reverse().join('')
      }
    },
    methods: {
        login(){
          this.loginError = ''
          if(this.loginForm.email == ''){
              this.loginError = '아이디를 입력해주세요'
              return false
          }
          if(this.loginForm.password == ''){
              this.loginError = '비밀번호를 입력해주세요'
              return false
          }
          axios.post(`/login`, this.loginForm)
          .then(response => {
              if(response.data.result == 'success'){
                  window.location.href = "/"
              } else if(response.data.result == 'error') {
                  this.loginError = '로그인에 실패하였습니다. 다시 시도해 주세요.'
                  if('errors' in response.data){

                      if('email' in response.data.errors){
                          this.loginError = response.data.errors.email[0]
                       }

                  }
              }
              console.log(response)
          })
          .catch(e => {
              this.loginError = '로그인에 실패하였습니다. 다시 시도해 주세요.'
              console.log(e)
          })
        }

    }


});
