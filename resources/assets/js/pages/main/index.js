Vue.component('sendmessagepopup', require('../../components/common/sendMessagePopup.vue'));

//import eventHub from '../../event.js'

const app = new Vue({
    el: '#app',
    data: {
        subscribeEmail:null,
        subscribeFormStatus:null,
        userLoginStatus:userLoginStatus,
        techBrands:[
            '<i class="devicon-amazonwebservices-plain-wordmark"></i>',
            '<i class="devicon-babel-plain"></i>',
            '<i class="devicon-bitbucket-plain-wordmark"></i>',
            '<i class="devicon-bootstrap-plain-wordmark"></i>',
            '<i class="devicon-css3-plain-wordmark"></i>',
            '<i class="devicon-django-plain"></i>',
            '<i class="devicon-docker-plain-wordmark"></i>',
            '<i class="devicon-git-plain-wordmark"></i>',
            '<i class="devicon-github-plain-wordmark"></i>',
            '<i class="devicon-html5-plain-wordmark"></i>',
            '<i class="devicon-javascript-plain"></i>',
            '<i class="devicon-laravel-plain-wordmark"></i>',
            '<i class="devicon-less-plain-wordmark"></i>',
            '<i class="devicon-jquery-plain-wordmark"></i>',
            '<i class="devicon-nginx-original"></i>',
            '<i class="devicon-nodejs-plain-wordmark"></i>',
            '<i class="devicon-python-plain-wordmark"></i>',
            '<i class="devicon-php-plain"></i>',
            '<i class="devicon-postgresql-plain-wordmark"></i>',
            '<i class="devicon-mysql-plain-wordmark"></i>',
            '<i class="devicon-react-original-wordmark"></i>',
            '<i class="devicon-sass-original"></i>',
            '<i class="devicon-sourcetree-plain-wordmark"></i>',
            '<i class="devicon-vuejs-plain-wordmark"></i>',
            '<i class="devicon-mongodb-plain-wordmark"></i>',
            '<i class="devicon-backbonejs-plain-wordmark"></i>'
        ],
        showTechBrands:[],
    },
    computed: {

    },
    updated: function() {
        let self = this
        this.$nextTick(function () {
        })
    },
    mounted() {
        this.selectTechBrands()
    },
    methods: {
        selectTechBrands(){
            let self = this
            let tempBrands = _.shuffle(self.techBrands)
            this.showTechBrands = _.take(tempBrands, 6)
        },
        async emailSubscribe(){
            let self = this
            let error = false
            if(this.subscribeEmail == ''){
                return false
            }

            this.subscribeFormStatus = 'uploading'

            let response = await axios.post('/emailSubscribe', {email:this.subscribeEmail}).catch(e => {

                self.setAlramShow('구독 신청 실패', '구독 신청에 실패하였습니다. 관리자에게 문의하세요.')

                error = true
            })

            if(response && 'result' in response.data){
                let message = ''
                if('errors' in response.data){
                  _.forEach(response.data.errors, function(value, key) {
                        message += value
                        message += '<br>'
                   });
               }
               self.setAlramShow('구독 신청 실패', '구독 신청에 실패하였습니다. <br />'+message)
               error = true
            }

            if(error){
                this.subscribeFormStatus = null
                return false
            }

            this.subscribeFormStatus = null
            this.subscribeEmail = null
            self.setAlramShow('구독 신청 완료', '구독 신청이 완료되었습니다. 새롭고 다양한 소식들을 즐겨주세요.')
        },
        setAlramShow(title, content){
            $('#alarm #title').text(title)
            $('#alarm #content').html(content)
            $('#alarm').modal('show')
            setTimeout(function(){ $('#alarm').modal('hide') }, 3000)
        },
    }
});
