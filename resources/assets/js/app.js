import highlight from './directives/highlight'

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import Vue2Filters from 'vue2-filters'
window.Vue.use(Vue2Filters)

//let sanitizeHtml
window.Vue.prototype.$sanitize = require('sanitize-html');

// moment
import moment from 'moment'
Vue.prototype.moment = moment
moment.locale('ko');
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

//Vue.component('video-upload', require('./components/VideoUpload.vue'));

/**
*   directives
*/
Vue.directive('highlightjs', highlight)

/*const app = new Vue({
    el: '#app'
});*/
