<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\Video;
use Vimeo\Laravel\Facades\Vimeo;

class VimeoVideoTranscodingCheck implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $video;
    public $tries = 20;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Video $video)
    {
        $this->video = $video;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        //$newVideo = Video::find($this->video->id);
        $newVideo = $this->video;//Video::where('id', $this->video->id)->first();

        if($newVideo && $newVideo->vimeo_url){
            $videoInfo = Vimeo::request($newVideo->vimeo_url, [], 'GET');

            if($videoInfo && $videoInfo['body']['status'] == 'available'){
                $newVideo->status = $videoInfo['body']['status'];
                $newVideo->playtime = $videoInfo['body']['duration'];
                $newVideo->link = $videoInfo['body']['link'];
                $newVideo->thumbnail = $videoInfo['body']['pictures']['sizes'][2]['link'];
                $newVideo->thumbnail_m = $videoInfo['body']['pictures']['sizes'][1]['link'];
                $newVideo->thumbnail_s = $videoInfo['body']['pictures']['sizes'][0]['link'];
                $newVideo->thumbnail_url = $videoInfo['body']['pictures']['uri'];
                $newVideo->enable = 1;
                $newVideo->save();
            } else {
                if($videoInfo){
                    $this->video->status = $videoInfo['body']['status'];
                    $this->video->save();
                }
                $this->release(120);//600);
            }
        } else {
            $this->release(120);
        }
    }


}
