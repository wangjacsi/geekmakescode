<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Storage;

class ImageUploadToS3ByFolder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 5;

    public $folder;
    public $destFolder;
    public $deleteMode;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($folder, $destFolder, $deleteMode = true)
    {
        $this->folder = $folder;
        $this->destFolder = $destFolder;
        $this->deleteMode = $deleteMode;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $files = Storage::files($this->folder);
        foreach ($files as $key => $value) {
            $faileArray = explode('/', $value );
            $filename = end($faileArray);
            if( Storage::disk('s3images')->put($this->destFolder . '/' . $filename, fopen(storage_path().'/'.$value, 'r+') ) ){
                if($this->deleteMode){
                    Storage::disk('local')->delete($value);
                }
            }
        }

        if($this->deleteMode){
            $files = Storage::files($this->folder);
            if(count($files) == 0){
                Storage::deleteDirectory($this->folder);
            }
        }
    }
}
