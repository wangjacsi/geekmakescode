<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Storage;

class ImageUploadToS3 implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 5;
    
    public $fileFullPath;
    public $destFullPath;
    public $modelObj;
    public $column;
    public $deleteFile;
    public $deleteFolder;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($fileFullPath, $destFullPath, $modelObj, $column, $deleteFile = '', $deleteFolder = '')
    {
        $this->fileFullPath = $fileFullPath;
        $this->destFullPath =$destFullPath;
        $this->modelObj = $modelObj;
        $this->column = $column;
        $this->deleteFile = $deleteFile;
        $this->deleteFolder = $deleteFolder;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // upload to s3
        if( Storage::disk('s3images')->put($this->destFullPath, fopen($this->fileFullPath, 'r+') ) ){
            if($this->column != ''){
                $this->modelObj->{$this->column} = $this->destFullPath;//Storage::disk('s3images')->url($this->destFullPath);
                $this->modelObj->save();
            }

            // delete local store
            if($this->deleteFile != ''){
                $exists = Storage::disk('local')->exists($this->deleteFile);
                if($exists){
                    Storage::disk('local')->delete($this->deleteFile);
                }
                //File::delete($this->deleteFile);
            }

            if($this->deleteFolder != ''){
                $files = Storage::files($this->deleteFolder);
                if(count($files) == 0){
                    //File::deleteDirectory($this->deleteFolder);
                    Storage::deleteDirectory($this->deleteFolder);
                }
            }
        }
    }

}
