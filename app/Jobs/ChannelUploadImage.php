<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\Channel;
use Storage;
use File;
use Image;

class ChannelUploadImage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 5;
    public $channel;
    public $fileId;
    public $type;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Channel $channel, $fileId, $type)
    {
        $this->channel = $channel;
        $this->fileId = $fileId;
        $this->type = $type;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // get the image
        $path = storage_path() . '/uploads/channels/' . $this->fileId;
        $fileName = $this->fileId;
        $tempArray = explode('.', $fileName );
        $ext = strtolower( end($tempArray));
        $firstName = str_replace_last('.' . $ext, '', $fileName);
        $fileNameS = $firstName . '_s.' . $ext;

        // resize
        if($this->type == 'channel-head'){
            $image = Image::make($path);
            $image->encode($ext)->fit(1920,622, function($c) {
                $c->upsize();
            })->save();
            $image->destroy();
        } else if($this->type == 'channel-logo') {
            $image = Image::make($path);
            $image->encode($ext)->fit(200,200, function($c) {
                $c->upsize();
            })->save();
            $image->destroy();
            $img_s = Image::make($path);
            $img_s->fit(60,60)->encode($ext);
            $img_s->destroy();
        }


        // upload to s3
        if( Storage::disk('s3images')->put('channels/' .$this->channel->id . '/' .$fileName, fopen($path, 'r+') ) ){
            if($this->type == 'channel-head'){
                $this->channel->head_image = Storage::disk('s3images')->url('channels/' . $this->channel->id . '/' . $fileName);
            } else if($this->type == 'channel-logo') {
                $this->channel->logo_image = Storage::disk('s3images')->url('channels/' . $this->channel->id . '/' . $fileName);
                Storage::disk('s3images')->put('channels/' .$this->channel->id . '/' . $fileNameS, (string) $img_s );
            }
            // delete local store
            File::delete($path);
            $this->channel->save();
        }

    }
}
