<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\Tutorial;
use Storage;
use File;
use Image;


class TutorialUploadImage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 5;

    public $tutorial;
    public $fileId;
    public $type;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Tutorial $tutorial, $fileId, $type)
    {
        $this->tutorial = $tutorial;
        $this->fileId = $fileId;
        $this->type = $type;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // get the image
        $path = storage_path() . '/uploads/tutorials/' . $this->fileId;
        $fileName = $this->fileId;
        $tempArray = explode('.', $fileName );
        $ext = strtolower( end($tempArray));
        $firstName = str_replace_last('.' . $ext, '', $fileName);

        // resize
        $image = Image::make($path);
        if($this->type == 'thumbnail'){
            $image->encode($ext)->fit(360,220, function($c) {
                $c->upsize();
            })->save();
        } else if($this->type == 'head_image') {
            $image->encode($ext)->fit(1920,639, function($c) {
                $c->upsize();
            })->save();
        }
        $image->destroy();

        // upload to s3
        if( Storage::disk('s3images')->put('tutorials/' .$this->tutorial->id . '/' .$fileName, fopen($path, 'r+') ) ){
            if($this->type == 'thumbnail'){
                $this->tutorial->thumbnail = Storage::disk('s3images')->url('tutorials/' . $this->tutorial->id . '/' . $fileName);
            } else if($this->type == 'head_image') {
                $this->tutorial->head_image = Storage::disk('s3images')->url('tutorials/' . $this->tutorial->id . '/' . $fileName);
            }

            // delete local store
            File::delete($path);
            $this->tutorial->save();
        }
    }
}
