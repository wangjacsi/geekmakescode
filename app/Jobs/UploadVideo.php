<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use File;
use Storage;
use App\Models\Video;
use Vimeo\Laravel\Facades\Vimeo;
use App\Models\Notification;

class UploadVideo implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 500;
    public $tries = 3;
    public $video;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Video $video)
    {
        $this->video = $video;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $file = storage_path() . '/uploads/videos/' . $this->video->id . '/' . $this->video->video_filename;

        // Vimeo upload
        $data = Vimeo::connection('main')->upload(storage_path() . '/uploads/videos/'. $this->video->id . '/' .$this->video->video_filename,
                ['name' => $this->video->title, 'description' => $this->video->description]);

        if($data && $data != ''){
            $this->video->vimeo_url = $data;
            $this->video->status = 'transcode_starting';
            $this->video->save();


            // add a video to channel
            //$channel = $this->video->channel()->first();
            //Vimeo::request($channel->vimeo_url.$this->video->vimeo_url, [], 'PUT');


            // Add a video to album
            $tutorial = $this->video->tutorial()->first();
            Vimeo::request($tutorial->vimeo_url.$this->video->vimeo_url, [], 'PUT');


            File::delete($file);
            File::deleteDirectory(storage_path() . '/uploads/videos/' . $this->video->id);
        }

        // this is for S3 solution
        /*if(Storage::disk('s3drop')->put($this->video->id . '/' .$this->video->video_filename, fopen($file, 'r+'))) {
            $this->video->video_filename = Storage::disk('s3drop')->url($this->video->id . '/' .$this->video->video_filename);
            $this->video->enable = true;
            $this->video->save();
            File::delete($file);
            File::deleteDirectory(storage_path() . '/uploads/videos/' . $this->video->id);
        }*/
    }

    public function failed(Exception $exception)
    {
        // Send user notification of failure, etc...
        $notification = $this->video->notifications()->create([
            'content' => '['. $this->video->title. '] 비디오 업로딩 에러가 발생하였습니다. 다시 비디오를 업로드해주세요.',
            'type' => 'vupload'
        ]);

        $this->video->status = 'upload_error';
        $this->video->enable = false;
        $this->video->save();

        $file = storage_path() . '/uploads/videos/' . $this->video->id . '/' . $this->video->video_filename;
        File::delete($file);
        File::deleteDirectory(storage_path() . '/uploads/videos/' . $this->video->id);
    }
}
