<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Channel;
use Illuminate\Auth\Access\HandlesAuthorization;

class ChannelPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function superadmin(User $user, Channel $channel){
        $channelJson = json_decode($channel->toJson());
        $settings = json_decode($channelJson->settings, true);
        if( isset( $settings['admins']['super'] ) ){
            return in_array($user->id, $settings['admins']['super']);
        } else{
            return false;
        }
    }

    public function edit(User $user, Channel $channel){
        return $channel->isAdmin($user->id);
    }


    public function update(User $user, Channel $channel){
        return $channel->isAdmin($user->id);
    }


    public function subscribe(User $user, Channel $channel){
        if( $user->isSubscribedTo($channel) ){
            return false;
        }
        return !$user->ownsChannel($channel);
    }


    public function unsubscribe(User $user, Channel $channel){
        return $user->isSubscribedTo($channel);
    }
}
