<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Video;
use Illuminate\Auth\Access\HandlesAuthorization;

class VideoPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function show(User $user, Video $video){
        return $video->canBeAccessed($user);
    }

    public function edit(User $user, Video $video){
        if( count( $video->channel->users()->where('users.id', $user->id)->get() ) > 0)
            return true;
        else
            return false;
    }

    public function update(User $user, Video $video){
        if( count( $video->channel->users()->where('users.id', $user->id)->get() ) > 0)
            return true;
        else
            return false;
    }

    public function delete(User $user, Video $video){
        if( count( $video->channel->users()->where('users.id', $user->id)->get() ) > 0)
            return true;
        else
            return false;
    }

    public function vote(User $user, Video $video){
        if(!$video->isRegisteredVideo($user)){
            return false;
        }
        return $video->tutorial->votesAllowed();
        //return $video->votesAllowed();
    }

    public function comment(User $user, Video $video){

        if(!$video->isRegisteredVideo($user)){
            if( count( $video->channel->users()->where('users.id', $user->id)->get() ) > 0)
                return true;
            else
                return false;
        }

        return $video->tutorial->commentsAllowed();

    }
}
