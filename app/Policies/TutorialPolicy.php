<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Tutorial;
use Illuminate\Auth\Access\HandlesAuthorization;

class TutorialPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function edit(User $user, Tutorial $tutorial){
        return $user->ownsTutorial($tutorial);
    }

    public function regist(User $user, Tutorial $tutorial){
        return $user->isRegisteredTo($tutorial) ? false : true;
    }

    public function review(User $user, Tutorial $tutorial){
        return $user->isRegisteredTo($tutorial);
    }

    public function vote(User $user, Tutorial $tutorial){
        if($tutorial->isOwner($user)){
            return false;
        }

        return $tutorial->votesAllowed();
    }

    public function interest(User $user, Tutorial $tutorial){
        /*if($tutorial->isOwner($user)){
            return false;
        }*/

        return true;
    }

}
