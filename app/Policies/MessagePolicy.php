<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Message;
use Illuminate\Auth\Access\HandlesAuthorization;

class MessagePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function read(User $user, Message $message){
        if($message->messagable_type == 'App\Models\Channel'){
            $channels = $user->channelIds();
            return in_array($message->messagable_id, $channels);
        } else {
            return $message->messagable_id == $user->id;
        }

    }
}
