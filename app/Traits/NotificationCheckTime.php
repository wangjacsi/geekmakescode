<?php
namespace App\Traits;
use Illuminate\Database\Eloquent\Builder;

trait NotificationCheckTime
{
    /*public function scopeNotificationCheckTime(Builder $builder, $column, $checkTime){
        return $builder->where($column, '>' , $checkTime);
    }*/

    public function scopeNotificationCheckTime( $query, $column, $checkTime){
        return $query->where($column, '>' , $checkTime);
    }
}
