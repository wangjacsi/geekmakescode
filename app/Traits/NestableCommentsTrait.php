<?php
use Illuminate\Pagination\LengthAwarePaginator;

trait NestableCommentsTrait
{
    public function nestedComments($page = 1, $perPage = 10){
        $comments = $this->comments();

        $grouped = $comments->get()->groupBy('reply_id');
        $root = $grouped->get(null)->forPage($page, $perPage);

        $ids = $this->buildIdNest($root, $grouped);

        $grouped = $comments->whereIn('id', $ids)->with(['user', 'user.channels'])->get()->groupBy('reply_id');

        $root = $grouped->get(null);

        return $this->buildNest($root, $grouped);
    }

    protected function buildIdNest($root, $grouped, &$ids = []){
        foreach ($root as $comment) {
            $ids[] = $comment->id;

            if($replies = $grouped->get($comment->id)){
                $this->buildIdNest($replies, $grouped, $ids);
            }
        }

        return $ids;
    }


    protected function buildNest($comments, $groupedComments){
        return $comments->each(function ($comment) use ($groupedComments) {
            if( $replies = $groupedComments->get($comment->id)){
                $comment->children = $replies;
                $this->buildNest($comment->children, $groupedComments);
            }
        });
    }
}



/*

// Pagination
use Illuminate\Pagination\LengthAwarePaginator;

$page = $request->get('page', 1);
$perPage = 20;

$article = Article::find(1);

$comments = $article->nestedComments($page, $perPage);

$comments = new LengthAwarePaginator(
    $comments,
    count($article->comments->where('reply_id', null)),
    $perPage,
    $page,
    ['path' => $request->url(), 'query'=>$request->query()]
);





*/
