<?php
namespace App\Traits;


trait NotificationAdminCheck
{
    public function scopeAdminCheck($query){
        return $query->whereNotIn('type', ['vote', 'review', 'evaluation', 'registration', 'message', 'subscription']);
    }
}
