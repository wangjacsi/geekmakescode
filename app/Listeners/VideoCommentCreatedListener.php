<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\VideoCommentCreated;
use App\Mail\VideoCommentCreatedMail;
use Mail;

class VideoCommentCreatedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(VideoCommentCreated $event)
    {
        $event->video->tutorial->notifications()->create([
            'content' => '비디오 강좌 "'.str_limit($event->video->title, 225, ' ... ') . '" 새로운 댓글이 있습니다',
            'type' => 'comment',
            'user_id' => $event->user->id,
            'option' => $event->video->tutorial_id
        ]);

        // 채널 관리자 메일 보내기
        // Send Email
        $admins = $event->video->channel->users()->get()->pluck('email')->toArray();
        Mail::to($admins)->send(new VideoCommentCreatedMail($event->video, $event->user, $event->comment));
    }
}
