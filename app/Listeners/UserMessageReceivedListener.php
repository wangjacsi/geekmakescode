<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\UserMessageReceived;
use App\Mail\UserMessageReceivedMail;
use Mail;

class UserMessageReceivedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(UserMessageReceived $event)
    {
        
        Mail::to($event->receiver)->send(new UserMessageReceivedMail($event->sender, $event->receiver, $event->message, $event->title));
    }
}
