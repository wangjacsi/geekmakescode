<?php

namespace App\Listeners;

use App\Events\TutorialLaunched;
use App\Mail\TutorialLaunchedMail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;
use App\Models\Subscription;
use App\Models\Notification;

class TutorialLaunchedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(TutorialLaunched $event)
    {
        // notification
        $notice = Notification::where('notificationable_id', $event->channel->id)
        ->where('notificationable_type', 'App\Models\Channel')
        ->where('option', $event->tutorial->id)
        ->where('type', 'tcreate')->first();

        if(!$notice){
            $event->channel->notifications()->create([
                'content' => '새로운 강좌 <span style="font-weight:bold; color:green;"><a href="/tutorial/'.$event->tutorial->slug.'">'. $event->tutorial->title .'</a></span>를 오픈하였습니다.',
                'type' => 'tcreate',
                'option' => $event->tutorial->id
            ]);

            // 구독자 mail 전송하기
            // Send Email
            $subscriptions = Subscription::where('channel_id', $event->channel->id)
            ->leftJoin('users', 'users.id', '=', 'user_id')
            ->select('users.*')->get()->pluck('email')->toArray();

            $when = now()->addMinutes(30);

            Mail::to($subscriptions)->later($when, new TutorialLaunchedMail($event->tutorial, $event->channel));
        }


    }
}
