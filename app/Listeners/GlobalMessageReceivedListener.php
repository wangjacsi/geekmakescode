<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\GlobalMessageReceived;
use App\Mail\GlobalMessageReceivedMail;
use Mail;

class GlobalMessageReceivedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(GlobalMessageReceived $event)
    {
        // 서비스 관리자 메일 보내기
        // Send Email
        Mail::to(env('MAIL_SERVICE'))->send(new GlobalMessageReceivedMail($event->sendData));
    }
}
