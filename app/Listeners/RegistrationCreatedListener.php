<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\RegistrationCreated;
use App\Mail\RegistrationCreatedMail;
use Mail;
use App\Models\Notification;

class RegistrationCreatedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(RegistrationCreated $event)
    {
        $notice = Notification::where('notificationable_id', $event->tutorial->id)
        ->where('notificationable_type', 'App\Models\Tutorial')
        ->where('user_id', $event->user->id)
        ->where('type', 'registration')->first();

        if(!$notice){
            $event->tutorial->notifications()->create([
                'content' => '새로운 강좌 수강',
                'type' => 'registration',
                'user_id' => $event->user->id
            ]);

            // 채널 관리자 메일 보내기
            // Send Email
            $admins = $event->tutorial->channel->users()->get()->pluck('email')->toArray();
            Mail::to($admins)->send(new RegistrationCreatedMail($event->tutorial, $event->user));
        }
    }
}
