<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\ChannelReceivedMessage;
use App\Mail\ChannelReceivedMessageMail;
use Mail;

class ChannelReceivedMessageListener
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(ChannelReceivedMessage $event)
    {
        $event->channel->notifications()->create([
            'content' => '새로운 메세지 도착:<br />'.str_limit($event->message->title, 230, ' ... '),
            'type' => 'message',
            'user_id' => $event->user->id
        ]);

        // 채널 관리자 메일 보내기
        // Send Email
        $admins = $event->channel->users()->get()->pluck('email')->toArray();
        Mail::to($admins)->send(new ChannelReceivedMessageMail($event->channel, $event->user, $event->message));
    }
}
