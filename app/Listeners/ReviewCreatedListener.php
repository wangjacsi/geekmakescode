<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\ReviewCreated;
use App\Mail\ReviewCreatedMail;
use Mail;

class ReviewCreatedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(ReviewCreated $event)
    {
        // notification
        $event->tutorial->notifications()->create([
            'content' => '새로운 강좌 평가: '. round(($event->review->review / 2), 1) . '<br /> ' . str_limit($event->review->content , 225),
            'type' => 'review',
            'user_id' => $event->user->id
        ]);

        // 채널 관리자 메일 보내기
        // Send Email
        $admins = $event->tutorial->channel->users()->get()->pluck('email')->toArray();
        Mail::to($admins)->send(new ReviewCreatedMail($event->tutorial, $event->user, $event->review));
    }
}
