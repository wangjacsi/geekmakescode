<?php

namespace App\Listeners;

use App\Events\SocialLinked;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Mail\SocialLinkedEmail;
use Mail;

class SendSocialLinkedEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SocialLinked  $event
     * @return void
     */
    public function handle(SocialLinked $event)
    {
        $user = $event->userSocial->user()->first();
        $service = config("services.{$event->userSocial->service}.name");
        // Send Email
        Mail::to($user->email)->send(new SocialLinkedEmail($user, $service));

    }
}
