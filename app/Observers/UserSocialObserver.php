<?php

namespace App\Observers;


use App\Models\UserSocial;
use App\Events\SocialLinked;

class UserSocialObserver
{
    public function created(UserSocial $userSocial){
        event(new SocialLinked($userSocial));
    }
}
