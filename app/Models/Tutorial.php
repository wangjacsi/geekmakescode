<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Scout\Searchable;
use App\Traits\Orderable;

class Tutorial extends Model
{
    use SoftDeletes;
    //use Searchable;
    use Orderable;

    public function getRouteKeyName(){
        return 'slug';
    }

    protected $fillable = [
        'title',
        'slug',
        'channel_id',
        'description',
        'visibility',
        'cost_type',
        'cost',
        'allow_votes',
        'allow_comments',
        'progress_status',
        'thumbnail',
        'vimeo_url',
        'settings',
        'head_image',
        'skill',
        'allow_reviews',
    ];

    public function toSearchableArray()
    {
        $array = $this->toArray();

        $related = $this->channel->only('name', 'slug', 'logo_image');

        return array_merge($array, $related);
    }

    public function channel(){
        return $this->belongsTo(Channel::class);
    }

    public function videosByVisible(){
        return $this->hasMany(Video::class)->visible()->orderBy('order')->orderBy('title')->orderBy('created_at');
    }

    public function videos(){
        return $this->hasMany(Video::class)->orderBy('order')->orderBy('title')->orderBy('created_at');
    }

    public function videosCount(){
        return $this->videos->count();
    }

    public function videosCountByVisible(){
        return $this->videos()->visible()->count();
    }

    public function totalPlayTime(){
        return $this->videos->sum('playtime');
    }

    public function totalPlayTimeByVisible(){
        return $this->videos()->visible()->sum('playtime');
    }


    public function getThumbnail($image){

        if($this->{$image} == ''){
            return '/img/tutorials/' . $image . '.jpg';
        } else {
            return $this->{$image};
        }
    }


    public function registrations(){
        return $this->hasMany(Registration::class);
    }


    public function registrationsCount(){
        return $this->registrations->count();
    }

    public function totalVideoViews(){
        return $this->hasManyThrough(VideoView::class, Video::class)->count();
    }

    // Tags
    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }

    // Vote
    public function votesAllowed(){
        return (bool) $this->allow_votes;
    }

    public function commentsAllowed(){
        return (bool) $this->allow_comments;
    }

    public function votes(){
        return $this->morphMany(Vote::class, 'voteable');
    }

    public function upVotes(){
        return $this->votes->where('type', 'up');
    }

    public function downVotes(){
        return $this->votes->where('type', 'down');
    }

    public function voteFromUser(User $user){
        return $this->votes->where('user_id', $user->id);
    }

    public function isOwner(User $user){
        return $this->channel->users->where('id', '=', $user->id)->first() ? true : false;
    }

    // notifications
    public function notifications()
    {
        return $this->morphMany(Notification::class, 'notificationable');
    }

    // records
    public function records(){
        return $this->hasManyThrough(Record::class, Video::class);
    }

    public function recordsWithUserId($userId){
        return $this->records()->where('user_id', '=', $userId)->get();
    }

    // review
    public function reviewsAllowed(){
        return (bool) $this->allow_reviews;
    }

    public function reviews(){
        return $this->hasMany(Review::class);
    }

    public function reviewedByUser($userId){
        return $this->reviewsWithUserId($userId) ? true : false;
    }

    public function reviewsWithUserId($userId){
        return $this->reviews()->where('user_id', '=', $userId)->first();
    }

    public function reviewAvg(){
        return $this->reviews()->avg('review');
    }

    // interest
    public function interests(){
        return $this->hasMany(Interest::class);
    }

    public function interestedByUser($userId){
        return $this->interestsWithUserId($userId) ? true : false;
    }

    public function interestsWithUserId($userId){
        return $this->interests()->where('user_id', '=', $userId)->first();
    }

    // Scope
    public function scopeProgressStatus($query){
        return $query->where('progress_status', 'complete');
    }

    public function scopePublic($query){
        return $query->where('visibility', 'public');
    }

    public function scopeVisible($query){
        return $query->progressStatus()->public();
    }
}
