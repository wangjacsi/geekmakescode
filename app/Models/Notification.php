<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Orderable;
use App\Traits\NotificationAdminCheck;

class Notification extends Model
{
    use Orderable;
    use NotificationAdminCheck;

    protected $fillable = [
        'notificationable_id',
        'notificationable_type',
        'content',
        'user_id',
        'type',
        'option',
    ];

    public function notificationable()
    {
        return $this->morphTo();
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function tutorial(){
        return $this->belongsTo(Tutorial::class, 'notificationable_id');
    }

    public function channel(){
        return $this->belongsTo(Channel::class, 'notificationable_id');
    }

    public function video(){
        return $this->belongsTo(Video::class, 'notificationable_id');
    }
}
