<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
//use App\Events\SocialLinked;

class UserSocial extends Model
{
    protected $table = 'users_social';

    protected $fillable = ['social_id', 'service'];

    /*protected $dispatchesEvents = [
        'created' => SocialLinked::class,
    ];*/

    public function user(){
        return $this->belongsTo(User::class);
    }
}
