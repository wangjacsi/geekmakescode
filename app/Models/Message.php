<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\NotificationCheckTime;
use App\Traits\Orderable;

class Message extends Model
{
    //use SoftDeletes;
    use NotificationCheckTime;
    use Orderable;

    protected $fillable = [
        'user_id',
        'channel_id',
        'messagable_id',
        'messagable_type',
        'message',
        'reply_id',
        'read',
    ];

    public function messagable()
    {
        return $this->morphTo();
    }



    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function channel()
    {
        return $this->belongsTo(Channel::class);
    }
}
