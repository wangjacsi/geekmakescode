<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Scout\Searchable;
use App\Traits\Orderable;

class Video extends Model
{
    //use SoftDeletes;
    //use Searchable;
    use Orderable;

    public function getRouteKeyName(){
        return 'uid';
    }

    protected $fillable = [
        'uid',
        'title',
        'order',
        'description',
        'video_filename',
        'visibility',
        'allow_votes',
        'allow_comments',
        'thumbnail',
        'thumbnail_m',
        'thumbnail_s',
        'tutorial_id',
        'channel_id',
        'enable',
        'vimeo_url',
        'settings',
        'playtime',
        'status',
        'link',
        'thumbnail_url',
        'filesize',
        'Preview',
    ];

    public function toSearchableArray()
    {
        $array = $this->toArray();

        $array['channel_slug'] = $this->channel->slug;
        //$array['channel_id'] = $this->channel->id;
        $array['channel_name'] = $this->channel->name;
        $array['channel_logo_image'] = $this->channel->logo_image;
        $array['tutorial_slug'] = $this->tutorial->slug;
        $array['tutorial_title'] = $this->tutorial->title;
        //$array['tutorial_id'] = $this->tutorial->id;
        $array['tutorial_thumbnail'] = $this->tutorial->thumbnail;
        $array['visible'] = $this->isProcessed() && $this->isPublic();

        return $array;
    }

    public function channel(){
        return $this->belongsTo(Channel::class);
    }

    public function tutorial(){
        return $this->belongsTo(Tutorial::class);
    }

    public function scopeLatestFirstByUser($query){
        return $query->orderBy('videos.created_at', 'desc');
    }

    public function ownedByUser($user = null){
        if(!$user)
            return false;

        if(count( $this->channel->users()->where('users.id', $user->id)->get() ) > 0 )
            return true;
        else
            return false;
    }

    public function getThumbnail(){
        if($this->thumbnail == ''){
            return env('AWS_URL') . 'videos/default.png';
        } else {
            return $this->thumbnail;
        }
    }

    public function votesAllowed(){
        return (bool) $this->allow_votes;
    }

    public function commentsAllowed(){
        return (bool) $this->allow_comments;
    }

    public function isProcessed(){ // deleted
        return $this->enable;//$this->processed;
    }

    public function isPrivate(){
        return $this->visibility === 'private';
    }

    public function isPublic(){
        return $this->visibility === 'public';
    }

    public function canBeAccessed($user = null){
        if(!$this->enable){
            return false;
        }

        if($this->ownedByUser($user)){
            return true;
        }

        if($this->visibility == 'private' || $this->tutorial->visibility == 'private'){
            return false;
        }

        if($this->preview){
            return true;
        }

        return true;
    }

    public function isRegisteredVideo($user){
        return (bool) Registration::where('tutorial_id', $this->tutorial_id)->where('user_id', $user->id)->count();
    }

    public function views(){
        return $this->hasMany(VideoView::class);
    }

    public function viewCount(){
        return $this->views->count();
    }

    public function votes(){
        return $this->morphMany(Vote::class, 'voteable');
    }

    public function upVotes(){
        return $this->votes->where('type', 'up');
    }

    public function downVotes(){
        return $this->votes->where('type', 'down');
    }

    public function voteFromUser(User $user){
        return $this->votes->where('user_id', $user->id);
    }

    public function comments(){
        return $this->morphMany(Comment::class, 'commentable')->whereNull('reply_id');
    }

    // Tags
    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }

    // notifications
    public function notifications()
    {
        return $this->morphMany(Notification::class, 'notificationable');
    }

    // records
    public function records(){
        return $this->hasMany(Record::class);
    }

    // Scope
    public function scopeProcessed($query){ // deleted processed
        return $query->where('enable', true);//where('processed', true);
    }

    public function scopePublic($query){
        return $query->where('visibility', 'public');
    }

    public function scopeVisible($query){
        return $query->processed()->public();
    }
}
