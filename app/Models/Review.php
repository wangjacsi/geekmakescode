<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Orderable;

class Review extends Model
{
    use Orderable;
    
    protected $fillable = [
        'user_id',
        'tutorial_id',
        'review',
        'content',
    ];


    public function user(){
        return $this->belongsTo(User::class);
    }

    public function tutorial(){
        return $this->belongTo(Tutorial::class);
    }

}
