<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Scout\Searchable;
use App\Traits\Orderable;
use App\Traits\NotificationCheckTime;
use Auth;

class Channel extends Model
{
    use SoftDeletes;
    //use Searchable;
    use Orderable;
    use NotificationCheckTime;

    public function getRouteKeyName(){
        return 'slug';
    }

    protected $fillable = [
        'name',
        'slug',
        'description',
        'head_image',
        'logo_image',
        'creator',
        'vimeo_url',
        'settings',
    ];

    public function isAdmin($userID){
        return $this->users->where('id', '=', $userID)->count() > 0 ? true : false;
    }

    public function users(){
        return $this->belongsToMany(User::class, 'channel_user', 'channel_id', 'user_id');//belongsTo(User::class);
    }

    public function tutorials(){
        return $this->hasMany(Tutorial::class);
    }

    public function totalTutorials(){
        return $this->tutorials->count();
    }

    public function videos(){
        return $this->hasMany(Video::class);
    }

    public function totalVideos(){
        return $this->videos->count();
    }

    public function getThumbnail($image, $type = ''){
        $ext = '';
        if($type == 's'){
            $ext = '_s';
        }

        if($this->{$image} == ''){
            return '/img/channels/' . $image . '.png';
        } else {
            if($ext != ''){
                $tempArray = explode('.', $this->{$image} );
                $type = end($tempArray);
                $firstName = str_replace_last('.' . $type, '', $this->{$image});
                return $firstName . $ext. '.' . $type;
            } else {
                return $this->{$image};
            }
        }
    }

    public function subscriptions(){
        return $this->hasMany(Subscription::class);
    }

    public function isSubscribed($userId = null){
        if(!$userId){
            if(Auth::user())
                $userId = Auth::user()->id;
            else
                return false;
        }
        return $this->subscriptions->where('user_id', '=', $userId)->count() ? true : false;

    }

    public function subscriptionCount(){
        return $this->subscriptions->count();
    }

    public function totalVideoViews(){
        return $this->hasManyThrough(VideoView::class, Video::class)->count();
    }

    public function totalRegisterdTutorials(){
        return $this->hasManyThrough(Registration::class, Tutorial::class)->count();
    }

    // Tags
    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }

    // message
    public function messages()
    {
        return $this->morphMany(Message::class, 'messagable');
    }

    public function sendMessages(){
        return $this->hasMany(Message::class);
    }

    public function messagesByChIds($chids){
        return Message::where('messagable_type', 'App\Models\Channel')
            ->whereIn('messagable_id', $chids);
    }


    // notifications
    public function notifications()
    {
        return $this->morphMany(Notification::class, 'notificationable');
    }
}
