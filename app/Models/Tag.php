<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;

class Tag extends Model
{
    //use SoftDeletes;

    protected $fillable = [
        'name',
    ];

    public function channels()
    {
        return $this->morphedByMany(Channel::class, 'taggable');
    }

    public function tutorials()
    {
        return $this->morphedByMany(Tutorial::class, 'taggable');
    }

    public function videos()
    {
        return $this->morphedByMany(Video::class, 'taggable');
    }


}
