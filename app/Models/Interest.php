<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Interest extends Model
{
    protected $fillable = [
        'user_id',
        'tutorial_id',
        'category',
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function tutorial(){
        return $this->belongTo(Tutorial::class);
    }
}
