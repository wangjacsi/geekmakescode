<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $fillable = [
        'channel_id',
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }


    // notifications
    public function notifications()
    {
        return $this->morphMany(Notification::class, 'notificationable');
    }
}
