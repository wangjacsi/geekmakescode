<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Registration extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'tutorial_id',
        'user_id',
    ];


    public function user(){
        return $this->belongsTo(User::class);
    }

    public function tutorial(){
        return $this->belongsTo(Tutorial::class);
    }


    // notifications
    public function notifications()
    {
        return $this->morphMany(Notification::class, 'notificationable');
    }
}
