<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Record extends Model
{
    protected $fillable = [
        'user_id',
        'video_id',
        'progress',
        'watch',
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function userWithVideoID($videoId){
        return $this->user()->where('video_id', '=', $videoId)->get();
    }

    public function video(){
        return $this->belongsTo(Video::class);
    }

}
