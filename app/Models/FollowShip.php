<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FollowShip extends Model
{
    protected $fillable = [
        'user_id',
        'follow_id',
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function followUser(){
        return $this->belongsTo(User::class, 'follow_id');
    }
}
