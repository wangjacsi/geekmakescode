<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Builder;
use App\Notifications\ResetPasswordNotification;
use App\Traits\NotificationCheckTime;
use Illuminate\Support\Carbon;
use App\Traits\Orderable;

class User extends Authenticatable
{
    use Notifiable;
    use NotificationCheckTime;
    use Orderable;

    public function getRouteKeyName(){
        return 'slug';
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
        'active', 'activation_token', 'photo',
        'slug', 'options','aboutme', 'status_message',
        'head_image', 'job'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // Notify
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token, $this->name));
    }

    // Social
    public function social(){
        return $this->hasMany(UserSocial::class);
    }


    public function hasSocialLinked($service){
        return (bool) $this->social->where('service', $service)->count();
    }


    // Profile
    public function getPhoto(){
        return $this->photo ? $this->photo : '/img/profile/default.png';
    }

    // Profile
    public function getHeadImage(){
        return $this->head_image ? $this->head_image : '/img/profile/wave-top-image.jpg';
    }

    // Video
    // 유저 채널에 속한 비디오 모두
    public function videos($ids = ''){
        if($ids == '')
            $ids = $this->channelIds();
        return Video::whereIn('channel_id', $ids);
    }

    public function videolIds()
    {
        return $this->videos()->pluck('id')->toArray();
    }

    // comment
    public function comments(){
        return $this->hasMany(Comment::class);
    }


    // Channel
    public function channels(){
      return $this->belongsToMany(Channel::class, 'channel_user', 'user_id', 'channel_id');//chnnel_user
    }

    public function channelCount(){
        return $this->channels->count();
    }

    public function channelIds()
    {
        return $this->channels->pluck('id')->toArray();
    }

    // Subscription Channel
    public function subscriptions(){
        return $this->hasMany(Subscription::class);
    }

    public function subscribedChannels(){
        return $this->belongsToMany(Channel::class, 'subscriptions');
    }

    public function subscribedChannelsCnt(){
        return $this->subscribedChannels->count();
    }

    public function subscribedChannelsIds(){
        return $this->subscribedChannels->pluck('id')->toArray();
    }

    public function isSubscribedTo(Channel $channel){
        return (bool) $this->subscriptions->where('channel_id', $channel->id)->count();
    }

    public function ownsChannel(Channel $channel){
        return (bool) $this->channels->where('id', $channel->id)->count();
    }

    // 채널 아이디에 따른 유저들 조회
    public static function userIdsByChIds($ids){
        return User::whereHas('channels', function($query) use ($ids) {
            $query->whereIn('channels.id', $ids); // But this does
        });
    }

    public static function userIdsByChIdsNotIncludeMe($ids, $me){
        return User::whereHas('channels', function($query) use ($ids) {
            $query->whereIn('channels.id', $ids); // But this does
        })->where('id', '<>', $me);
    }




    // Registrations Tutorial
    public function registrations(){
        return $this->hasMany(Registration::class);
    }

    public function registeredTutorials(){
        return $this->belongsToMany(Tutorial::class, 'registrations');
    }

    public function registeredTutorialsCnt(){
        return $this->registeredTutorials->count();
    }

    public function registeredTutorialIds(){
        return $this->registeredTutorials()->pluck('tutorial_id')->toArray();
    }

    public function isRegisteredTo(Tutorial $tutorial){
        return (bool) $this->registrations->where('tutorial_id', $tutorial->id)->count();
    }

    public function ownsTutorial(Tutorial $tutorial){
        return (bool) $this->channels->where('id', $tutorial->channel_id)->count();
    }

    public function tutorials($ids = ''){
        if($ids == '')
            $ids = $this->channelIds();
        return Tutorial::whereIn('channel_id', $ids);
    }

    public function tutorialsIds($ids = ''){
        if($ids == '')
            $ids = $this->channelIds();
        return Tutorial::whereIn('channel_id', $ids)->pluck('id')->toArray();
    }


    // Message
    public function messages()
    {
        return $this->morphMany(Message::class, 'messagable');
    }

    public function messagesCount(){
        return $this->messages->count();
    }

    public function messagesNewCount(){
        return $this->newNotifyMessage->count();
    }

    public function sendMessages(){
        return $this->hasMany(Message::class);
    }

    public function messagesByUserChIds($ids = ''){
        if($ids == '')
            $ids = $this->channelIds();
        return Message::where('messagable_type', 'App\Models\Channel')
            ->whereIn('messagable_id', $ids);
    }

    public function messagesCountByUserChIds($ids = ''){
        return $this->messagesByUserChIds($ids)->count();
    }

    public function messagesNewCountByUserChIds($ids = ''){
        return $this->newNotifyMessageByUserChannel($ids)->count();
    }

    // Follow Ship
    public function followships(){
        return $this->hasMany(FollowShip::class);
    }

    // records
    public function records(){
        return $this->hasMany(Record::class);
    }
    public function recordsWithTutorialId($tutorialId){
        return $this->records()->where('tutorial_id', '=', $tutorialId)->get();
    }

    // review
    public function reviews(){
        return $this->hasMany(Review::class);
    }

    public function reviewsWithTutorialId($tutorialId){
        return $this->reviews()->where('tutorial_id', '=', $tutorialId)->get();
    }

    // interest
    public function interests(){
        return $this->hasMany(Interest::class);
    }

    public function interestTutorials(){
        return $this->belongsToMany(Tutorial::class, 'interests')->where('visibility', '=', 'public');
    }


    public function interestTutorialsCnt(){
        return $this->interestTutorials->count();
    }

    public function interestTutorialIds(){
        return $this->interestTutorials()->pluck('tutorial_id')->toArray();
    }

    public function isInterestTo(Tutorial $tutorial){
        return (bool) $this->interests->where('tutorial_id', $tutorial->id)->count();
    }









    // Notification Check
    public function notificationChecks(){
        return $this->hasMany(NotificationCheck::class);
    }

    // 새로온 메세지 알림
    // 사용법: $message = $user->newNotifyMessage()->get();
    public function newNotifyMessage(){
        return $this->messages()->notificationCheckTime('created_at', $this->getCheckTime('message'));
    }

    // 내 채널에 온 메세지 알림
    // 사용법: $messages4 = $user->newNotifyMessageByUserChannel()->get();
    public function newNotifyMessageByUserChannel($ids = ''){
        return $this->messagesByUserChIds($ids)->notificationCheckTime('created_at', $this->getCheckTime('message-channel'));
    }


    // 내 채널 관련 알림
    public function newNotifyMyChannels($ids = ''){
        if($ids == '')
            $ids = $this->channelIds();
        return Notification::where('notificationable_type', 'App\Models\Channel')
            ->whereIn('notificationable_id', $ids)
            ->where('created_at', '>' ,$this->getCheckTime('mychannel-notice'));
    }

    // 구독 채널 관련 알림
    public function newNotifySubsChannels($ids = ''){
        if($ids == '')
            $ids = $this->subscribedChannelsIds();
        return Notification::where('notificationable_type', 'App\Models\Channel')
            ->whereIn('notificationable_id', $ids)
            ->where('created_at', '>' ,$this->getCheckTime('subschannel-notice'));
    }

    // 수강 강좌 관련 알림
    public function newNotifyRegiTutorials($ids = ''){
        if($ids == '')
            $ids = $this->registeredTutorialIds();
        return Notification::where('notificationable_type', 'App\Models\Tutorial')
            ->whereIn('notificationable_id', $ids)
            ->where('created_at', '>' ,$this->getCheckTime('regitutorial-notice'));
    }

    // 내 강좌 관련 알림
    public function newNotifyTutorials($ids = ''){ // 튜토리얼 아이디 입력
        if($ids == '')
            $ids = $this->tutorialsIds();
        return Notification::where('notificationable_type', 'App\Models\Tutorial')
            ->whereIn('notificationable_id', $ids)
            ->where('created_at', '>' ,$this->getCheckTime('mytutorial-notice'));
    }

    // 내 비디오 관련 알림
    public function newNotifyVideos($ids = ''){
        if($ids == '')
            $ids = $this->videolIds();
        return Notification::where('notificationable_type', 'App\Models\Video')
            ->whereIn('notificationable_id', $ids)
            ->where('created_at', '>' ,$this->getCheckTime('myvideo-notice'));
    }



    public function getCheckTime($type){
        $time = $this->notificationChecks()->where('type', '=', $type)->first();
        if(!$time){
            $notif = NotificationCheck::updateOrCreate([
                'user_id' => $this->id,
                'type' => $type
            ], [
                'checked_at' => Carbon::now()
            ]);
            return $notif->checked_at;
        }
        return $time->checked_at;
    }



    // Scope
    public function scopeByActivationColumns(Builder $builder, $email, $token){
        return $builder->where('email', $email)
                    ->whereNotNull('activation_token')
                    ->where('activation_token', $token);
    }


    public function scopeByEmail(Builder $builder, $email){
        return $builder->where('email', $email);
    }

}
