<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NotificationCheck extends Model
{
    protected $table = 'notificationchecks';

    protected $fillable = [
        'user_id',
        'type',
        'checked_at',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
