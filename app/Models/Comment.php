<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Orderable;
use Illuminate\Support\Carbon;

class Comment extends Model
{
    use SoftDeletes;
    use Orderable;

    protected $fillable = [
        'user_id',
        'reply_id',
        'body',
    ];

    protected $dates = [
        'edited_at',
    ];

    public static function boot(){
        parent::boot();

        static::updating( function ($comment){
            $comment->edited_at = Carbon::now();
        });
    }

    public function commentable(){
        return $this->morphTo();
    }

    public function replies(){
        return $this->hasMany(Comment::class, 'reply_id', 'id')
            ->orderBy('created_at', 'asc');
    }

    public function user(){
        return $this->belongsTo(User::class);
    }


    // notifications
    public function notifications()
    {
        return $this->morphMany(Notification::class, 'notificationable');
    }
}
