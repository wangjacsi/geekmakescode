<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\User;


class UserTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(User $user)
    {
        return [
            'name' => $user->name,
            'photo' => $user->getPhoto(),
        ];
    }
}
