<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Tutorial;

class TutorialTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Tutorial $tutorial)
    {
        return [
            'id' => $tutorial->id,
        ];
    }
}
