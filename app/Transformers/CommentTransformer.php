<?php

namespace App\Transformers;

use App\Models\Comment;
use League\Fractal\TransformerAbstract;
use Illuminate\Support\Carbon;

class CommentTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'channels',
        'replies',
        'user',
    ];

    public function transform(Comment $comment){

        Carbon::setLocale('ko');
        $now = Carbon::now();//'UTC');

        return [
            'id' => $comment->id,
            'user_id' => $comment->user_id,
            'body' => $comment->body,
            'created_at' => $comment->created_at->toDateTimeString(),
            'created_at_human' => Carbon::parse($now)->diffForHumans($comment->created_at, true). ' 전',
        ];
    }

    public function includeUser(Comment $comment){
        //dd($comment->user);
        return $this->item($comment->user, new UserTransformer);
    }

    public function includeChannels(Comment $comment){
        return $this->collection($comment->user->channels, new ChannelTransformer);
    }

    public function includeReplies(Comment $comment){
        return $this->collection($comment->replies, new CommentTransformer);
    }
}
