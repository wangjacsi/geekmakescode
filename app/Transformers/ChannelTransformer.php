<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Channel;

class ChannelTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Channel $channel) //$channels
    {
        return [
            'id' => $channel->id,
            'name' => $channel->name,
            'slug' => $channel->slug,
            'logo_image' => $channel->getThumbnail('logo_image', 's'),
        ];
    }
}
