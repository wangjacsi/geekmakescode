<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ResetPasswordNotification extends Notification
{
    use Queueable;
    public $token;
    public $name;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
     public function __construct($token, $name)
     {
         $this->token = $token;
         $this->name = $name;
     }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        /*return (new MailMessage)
            ->subject('codewwwave 비밀번호 재설정')
            ->line('사용자 비밀번호 재설정을 위한 이메일입니다. 아래 버튼을 클릭하세요.')
            ->action('비밀번호 재설정', url(config('app.url').route('password.reset', $this->token, false)))
            ->line('비밀번호 재설정을 원하지 않을 시 필요한 작업은 없습니다.');
*/
        return (new MailMessage)->subject(env('APP_NAME') . ' 비밀번호 재설정')->view(
            'emails.authentication.passwordReset', ['token' => $this->token, 'name' => $this->name]
        );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
