<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\UserSocial;
use App\Observers\UserSocialObserver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        UserSocial::observe(UserSocialObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
