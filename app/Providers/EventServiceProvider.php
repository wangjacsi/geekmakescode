<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\UserActivationEmail' => [
            'App\Listeners\SendActivationEmail',
        ],
        'App\Events\SocialLinked' => [
            'App\Listeners\SendSocialLinkedEmail',
        ],
        'App\Events\TutorialLaunched' => [
            'App\Listeners\TutorialLaunchedListener',
        ],
        'App\Events\ChannelReceivedMessage' => [
            'App\Listeners\ChannelReceivedMessageListener',
        ],
        'App\Events\VideoCommentCreated' => [
            'App\Listeners\VideoCommentCreatedListener',
        ],
        'App\Events\ReviewCreated' => [
            'App\Listeners\ReviewCreatedListener',
        ],
        'App\Events\RegistrationCreated' => [
            'App\Listeners\RegistrationCreatedListener',
        ],
        'App\Events\GlobalMessageReceived' => [
            'App\Listeners\GlobalMessageReceivedListener',
        ],
        'App\Events\UserMessageReceived' => [
            'App\Listeners\UserMessageReceivedListener',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
