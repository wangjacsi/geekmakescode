<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Video;
use App\Models\User;
use App\Models\Comment;

class VideoCommentCreatedMail extends Mailable
{
    use Queueable, SerializesModels;

    public $video;
    public $user;
    public $comment;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Video $video, User $user, Comment $comment)
    {
        $this->video = $video;
        $this->user = $user;
        $this->comment = $comment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('[비디오 강좌 ' . $this->video->title . '] 새로운 댓글 알림')->view('emails.tutorial.videoComment');
    }
}
