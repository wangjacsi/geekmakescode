<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Tutorial;
use App\Models\Channel;

class TutorialLaunchedMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;
    public $tutorial;
    public $channel;
    public $tries = 3;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Tutorial $tutorial, Channel $channel)
    {
        $this->tutorial = $tutorial;
        $this->channel = $channel;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->tutorial->title . ' 강좌 오픈')->view('emails.channel.tutorialLaunch');
    }
}
