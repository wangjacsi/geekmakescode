<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\User;

class UserMessageReceivedMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $tries = 2;
    public $sender;
    public $receiver;
    public $messageM;
    public $title;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $sender, User $receiver, $messageM, $title)
    {
        $this->sender = $sender;
        $this->receiver = $receiver;
        $this->messageM = $messageM;
        $this->title = $title;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('[새로운 메세지] ' . $this->title)->view('emails.user.receivedMessage');
    }
}
