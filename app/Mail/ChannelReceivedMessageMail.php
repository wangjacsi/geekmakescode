<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\User;
use App\Models\Channel;
use App\Models\Message as MessageApp;

class ChannelReceivedMessageMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;
    public $tries = 2;
    public $channel;
    public $user;
    public $messageM;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Channel $channel, User $user, MessageApp $message)
    {
        $this->channel = $channel;
        $this->user = $user;
        $this->messageM = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('[채널 ' . $this->channel->name . ' 새로운 메세지] ' . $this->messageM->title)->view('emails.channel.receivedMessage');
    }
}
