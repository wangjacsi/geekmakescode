<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Tutorial;
use App\Models\User;

class RegistrationCreatedMail extends Mailable
{
    use Queueable, SerializesModels;

    public $tutorial;
    public $user;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Tutorial $tutorial, User $user)
    {
        $this->tutorial = $tutorial;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('[강좌 ' . $this->tutorial->title . '] 새로운 수강 등록 알림')->view('emails.tutorial.registration');
    }
}
