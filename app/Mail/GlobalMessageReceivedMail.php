<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class GlobalMessageReceivedMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;
    public $tries = 2;
    public $sendData;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($sendData)
    {
        $this->sendData = $sendData;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('[새로운 메세지] ' . $this->sendData['subject'])->view('emails.service.receivedMessage');
    }
}
