<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\User;
//use App\Http\Helper;

class SocialLinkedEmail extends Mailable
{
    use Queueable, SerializesModels;
    public $user;
    public $service;
    //public $snsBrands;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, $service)
    {
        $this->user = $user;
        $this->service = $service;
        //$this->snsBrands = Helper::$snsBrands;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject(env('APP_NAME'). ' ' .$this->service.' 로그인 알림')
                    ->view('emails.authentication.socialLinked');
                    //->markdown('emails.socialLinked');
                    //->with(['service' => $this->service]);
    }
}
