<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Tutorial;
use App\Models\User;
use App\Models\Review;

class ReviewCreatedMail extends Mailable
{
    use Queueable, SerializesModels;
    public $tutorial;
    public $user;
    public $review;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Tutorial $tutorial, User $user, Review $review)
    {
        $this->tutorial = $tutorial;
        $this->user = $user;
        $this->review = $review;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('[강좌 ' . $this->tutorial->title . ' 새로운 평가] ')->view('emails.tutorial.review');
    }
}
