<?php
namespace App\Http;

use Storage;

class Helper {

    public static function transTutorialStatusToKorean($status){
        $statusArray = [//'prepare' => '강좌 준비중',
                        'publishing' => '강좌 연재 진행중',
                        'pause' => '강좌 연재 임시 연기',
                        'stop' => '강좌 진행 중단',
                        'complete' => '강좌 연재 완료'];
        return $statusArray[$status];
    }

    public static function getProgressStatusKorean(){
        $statusArray = [//'prepare' => '강좌 준비중',
                        'publishing' => '강좌 연재 진행중',
                        'pause' => '강좌 연재 임시 연기',
                        'stop' => '강좌 진행 중단',
                        'complete' => '강좌 연재 완료'];
        return $statusArray;
    }

    public static function transTimeToPlaytime($duration, $type='hours'){ // input minutes
        if($type == 'hours'){
            return round( $duration / 3600 , 1) . ' Hours';
        } else if($type == 'hours-minutes') {
            $hours = floor( $duration / 3600 );
            $minutes = floor(($duration - ($hours * 3600)) / 60);
            $hours = $hours ? ($hours . ' ' . str_plural('Hour', $hours) . ' ') : '';
            $minutes = $minutes ? ($minutes . ' ' . str_plural('Minute', $minutes)) : '';
            return $hours . $minutes;
        }
    }


    public static function filesCopy($sourceFolder, $destFolder){
        $files = Storage::files($sourceFolder);
        foreach ($files as $key => $value) {
            $faileArray = explode('/', $value );
            $filename = end($faileArray);
            Storage::disk('local')->copy($value, $destFolder.'/'.$filename);
        }
        return;
    }

    public static function filesMove($sourceFolder, $destFolder){
        $files = Storage::files($sourceFolder);
        foreach ($files as $key => $value) {
            $faileArray = explode('/', $value );
            $filename = end($faileArray);
            Storage::disk('local')->move($value, $destFolder.'/'.$filename);
        }
        return;
    }

    public static function deleteFolder($folder){
        Storage::deleteDirectory($folder);
        return;
    }

    public static function getSkills(){
        return ['beginner', 'intermediate', 'expert', 'all levels'];
    }

    public static function getVisibility(){
        return ['public','private'];//['public','unlisted','private'];
    }

    public static function getCostType(){
        return ['free','once','divide'];
    }

    public static function generateUUID(){
        return md5(uniqid(rand(), true));
    }

    // 준비, 연재중, 연재대기, 연재중단, 연재완료
    public static function getProgressStatus(){
        return ['prepare','publishing','pause','stop','complete'];
    }

    public static function makeImageFilename($imageFile){
        $mime = explode('/', $imageFile->getClientMimeType() );
        $ext = strtolower( end($mime));
        $fileId = self::generateUUID(). '.' .$ext;//uniqid(true). '.' .$ext;
        return $fileId;
    }

    public static function makeImageFilenameWithExt($imageFile){
        $mime = explode('/', $imageFile->getClientMimeType() );
        $ext = strtolower( end($mime));
        $fileId = self::generateUUID(). '.' .$ext;//uniqid(true). '.' .$ext;
        return array($fileId, $ext);
    }

    public static function makeImageFilenameS($fileName){
        $tempArray = explode('.', $fileName );
        $ext = strtolower( end($tempArray));
        $firstName = str_replace_last('.' . $ext, '', $fileName);
        $fileNameS = $firstName . '_s.' . $ext;
        return $fileNameS;
    }

    public static $backgroundColors = [ ['background' => 'peach',  'text' => 'white', 'overlay' => 'yellow'],
    ['background' => 'green',  'text' => 'lime-light', 'overlay' => 'green'],
    ['background' => 'orange-light',  'text' => 'white', 'overlay' => 'yellow'],
    ['background' => 'gray',  'text' => 'white', 'overlay' => 'blue'],
    ['background' => 'yellow',  'text' => 'yellow-light', 'overlay' => 'yellow'],
    ['background' => 'secondary',  'text' => 'gray', 'overlay' => 'dark'],
    ['background' => 'lime',  'text' => 'yellow', 'overlay' => 'peach'],
    ['background' => 'primary',  'text' => 'primary', 'overlay' => 'blue'],
    ['background' => 'orange',  'text' => 'white', 'overlay' => 'peach'],
    ['background' => 'red',  'text' => 'peach', 'overlay' => 'red'],
    ['background' => 'breez',  'text' => 'gray', 'overlay' => 'blue'],
    ['background' => 'lime-light',  'text' => 'yellow', 'overlay' => 'green'],
    ['background' => 'black',  'text' => 'peach', 'overlay' => 'gray'],
    ];

    public static $techBrands = [
        'algolia' => ['name' => 'algolia', 'html' => '<i class="fab fa-algolia"></i>', 'class' => 'fab fa-algolia'],
        'android' => ['name' => 'android', 'html' => '<i class="fab fa-android"></i>', 'class' => 'fab fa-android'],
        'angular' => ['name' => 'angular', 'html' => '<i class="fab fa-angular"></i>', 'class' => 'fab fa-angular'],
        'apple-ios' => ['name' => 'apple ios', 'html' => '<i class="fab fa-app-store-ios"></i>', 'class' => 'fab fa-app-store-ios'],
        'aws' => ['name' => 'aws', 'html' => '<i class="fab fa-aws"></i>', 'class' => 'fab fa-aws'],
        'bitbucket' => ['name' => 'bitbucket', 'html' => '<i class="fab fa-bitbucket"></i>', 'class' => 'fab fa-bitbucket'],
        'bitcoin' => ['name' => 'bitcoin', 'html' => '<i class="fab fa-bitcoin"></i>', 'class' => 'fab fa-bitcoin'],
        'css3' => ['name' => 'css3', 'html' => '<i class="fab fa-css3-alt"></i>', 'class' => 'fab fa-css3-alt'],
        'chrome' => ['name' => 'chrome', 'html' => '<i class="fab fa-chrome"></i>', 'class' => 'fab fa-chrome'],
        'deploydog' => ['name' => 'deploydog', 'html' => '<i class="fab fa-deploydog"></i>', 'class' => 'fab fa-deploydog'],
        'digital-ocean' => ['name' => 'digital ocean', 'html' => '<i class="fab fa-digital-ocean"></i>', 'class' => 'fab fa-digital-ocean'],
        'docker' => ['name' => 'docker', 'html' => '<i class="fab fa-docker"></i>', 'class' => 'fab fa-docker'],
        'drupal' => ['name' => 'drupal', 'html' => '<i class="fab fa-drupal"></i>', 'class' => 'fab fa-drupal'],
        'edge' => ['name' => 'edge', 'html' => '<i class="fab fa-edge"></i>', 'class' => 'fab fa-edge'],
        'ember' => ['name' => 'ember', 'html' => '<i class="fab fa-ember"></i>', 'class' => 'fab fa-ember'],
        'erlang' => ['name' => 'erlang', 'html' => '<i class="fab fa-erlang"></i>', 'class' => 'fab fa-erlang'],
        'ethereum' => ['name' => 'ethereum', 'html' => '<i class="fab fa-ethereum"></i>', 'class' => 'fab fa-ethereum'],
        'firefox' => ['name' => 'firefox', 'html' => '<i class="fab fa-firefox"></i>', 'class' => 'fab fa-firefox'],
        'git' => ['name' => 'git', 'html' => '<i class="fab fa-git-square"></i>', 'class' => 'fab fa-git-square'],
        'grunt' => ['name' => 'grunt', 'html' => '<i class="fab fa-grunt"></i>', 'class' => 'fab fa-grunt'],
        'gulp' => ['name' => 'gulp', 'html' => '<i class="fab fa-gulp"></i>', 'class' => 'fab fa-gulp'],
        'html5' => ['name' => 'html5', 'html' => '<i class="fab fa-html5"></i>', 'class' => 'fab fa-html5'],
        'internet-explorer' => ['name' => 'internet explorer', 'html' => '<i class="fab fa-internet-explorer"></i>', 'class' => 'fab fa-internet-explorer'],
        'java' => ['name' => 'java', 'html' => '<i class="fab fa-java"></i>', 'class' => 'fab fa-java'],
        'jenkins' => ['name' => 'jenkins', 'html' => '<i class="fab fa-jenkins"></i>', 'class' => 'fab fa-jenkins'],
        'joomla' => ['name' => 'joomla', 'html' => '<i class="fab fa-joomla"></i>', 'class' => 'fab fa-joomla'],
        'javascript' => ['name' => 'javascript', 'html' => '<i class="fab fa-js-square"></i>', 'class' => 'fab fa-js-square'],
        'laravel' => ['name' => 'laravel', 'html' => '<i class="fab fa-laravel"></i>', 'class' => 'fab fa-laravel'],
        'less' => ['name' => 'less', 'html' => '<i class="fab fa-less"></i>', 'class' => 'fab fa-less'],
        'linode' => ['name' => 'linode', 'html' => '<i class="fab fa-linode"></i>', 'class' => 'fab fa-linode'],
        'linux' => ['name' => 'linux', 'html' => '<i class="fab fa-linux"></i>', 'class' => 'fab fa-linux'],
        'magento' => ['name' => 'magento', 'html' => '<i class="fab fa-magento"></i>', 'class' => 'fab fa-magento'],
        'mailchimp' => ['name' => 'mailchimp', 'html' => '<i class="fab fa-mailchimp"></i>', 'class' => 'fab fa-mailchimp'],
        'markdown' => ['name' => 'markdown', 'html' => '<i class="fab fa-markdown"></i>', 'class' => 'fab fa-markdown'],
        //'microsoft' => ['name' => 'microsoft', 'html' => '<i class="fab fa-microsoft"></i>', 'class' => 'fab fa-microsoft'],
        'megaport' => ['name' => 'megaport', 'html' => '<i class="fab fa-megaport"></i>', 'class' => 'fab fa-megaport'],
        'node-js' => ['name' => 'node js', 'html' => '<i class="fab fa-node-js"></i>', 'class' => 'fab fa-node-js'],
        'node' => ['name' => 'node', 'html' => '<i class="fab fa-node"></i>', 'class' => 'fab fa-node'],
        'npm' => ['name' => 'npm', 'html' => '<i class="fab fa-npm"></i>', 'class' => 'fab fa-npm'],
        'opera' => ['name' => 'opera', 'html' => '<i class="fab fa-opera"></i>', 'class' => 'fab fa-opera'],
        'pagelines' => ['name' => 'pagelines', 'html' => '<i class="fab fa-pagelines"></i>', 'class' => 'fab fa-pagelines'],
        'php' => ['name' => 'php', 'html' => '<i class="fab fa-php"></i>', 'class' => 'fab fa-php'],
        'python' => ['name' => 'python', 'html' => '<i class="fab fa-python"></i>', 'class' => 'fab fa-python'],
        'react' => ['name' => 'react', 'html' => '<i class="fab fa-react"></i>', 'class' => 'fab fa-react'],
        'safari' => ['name' => 'safari', 'html' => '<i class="fab fa-safari"></i>', 'class' => 'fab fa-safari'],
        'sass' => ['name' => 'sass', 'html' => '<i class="fab fa-sass"></i>', 'class' => 'fab fa-sass'],
        'trello' => ['name' => 'trello', 'html' => '<i class="fab fa-trello"></i>', 'class' => 'fab fa-trello'],
        'vuejs' => ['name' => 'vuejs', 'html' => '<i class="fab fa-vuejs"></i>', 'class' => 'fab fa-vuejs'],
        'windows' => ['name' => 'windows', 'html' => '<i class="fab fa-windows"></i>', 'class' => 'fab fa-windows'],
        'wordpress' => ['name' => 'wordpress', 'html' => '<i class="fab fa-wordpress"></i>', 'class' => 'fab fa-wordpress'],
    ];

    public static $paymentBrands = [
        'apple-pay' => ['name' => 'apple pay', 'html' => '<i class="fab fa-cc-apple-pay"></i>', 'class' => 'fab fa-cc-apple-pay'],
        'amazon-pay' => ['name' => 'amazon pay', 'html' => '<i class="fab fa-cc-amazon-pay"></i>', 'class' => 'fab fa-cc-amazon-pay'],
        'amex' => ['name' => 'amex', 'html' => '<i class="fab fa-cc-amex"></i>', 'class' => 'fab fa-cc-amex'],
        'discover' => ['name' => 'discover', 'html' => '<i class="fab fa-cc-discover"></i>', 'class' => 'fab fa-cc-discover'],
        'jcb' => ['name' => 'jcb', 'html' => '<i class="fab fa-cc-jcb"></i>', 'class' => 'fab fa-cc-jcb'],
        'mastercard' => ['name' => 'mastercard', 'html' => '<i class="fab fa-cc-mastercard"></i>', 'class' => 'fab fa-cc-mastercard'],
        'paypal' => ['name' => 'paypal', 'html' => '<i class="fab fa-cc-paypal"></i>', 'class' => 'fab fa-cc-paypal'],
        'stripe' => ['name' => 'stripe', 'html' => '<i class="fab fa-cc-stripe"></i>', 'class' => 'fab fa-cc-stripe'],
        'visa' => ['name' => 'visa', 'html' => '<i class="fab fa-cc-visa"></i>', 'class' => 'fab fa-cc-visa'],
        'stripe' => ['name' => 'stripe', 'html' => '<i class="fab fa-cc-stripe"></i>', 'class' => 'fab fa-cc-stripe'],
    ];

    public static $snsBrands = [
        'behance' => ['name' => 'behance', 'html' => '<i class="fab fa-behance-square"></i>', 'class' => 'fab fa-behance-square', 'color'=>'#1769ff'],
        'bitbucket' => ['name' => 'bitbucket', 'html' => '<i class="fab fa-bitbucket"></i>', 'class' => 'fab fa-bitbucket', 'color'=>'#205081'],
        'blogger' => ['name' => 'blogger', 'html' => '<i class="fab fa-blogger"></i>', 'class' => 'fab fa-blogger', 'color'=>'#f57d00'],
        'digg' => ['name' => 'digg', 'html' => '<i class="fab fa-digg"></i>', 'class' => 'fab fa-digg', 'color'=>'#000000'],
        'dribbble' => ['name' => 'dribbble', 'html' => '<i class="fab fa-dribbble-square"></i>', 'class' => 'fab fa-dribbble-square', 'color'=>'#ea4c89'],
        //'dropbox' => ['name' => 'dropbox', 'html' => '<i class="fab fa-dropbox"></i>', 'class' => 'fab fa-dropbox'],
        'facebook' => ['name' => 'facebook', 'html' => '<i class="fab fa-facebook"></i>', 'class' => 'fab fa-facebook', 'color'=>'#3b5998'],
        'flickr' => ['name' => 'flickr', 'html' => '<i class="fab fa-flickr"></i>', 'class' => 'fab fa-flickr', 'color'=>'#ff0084'],
        'github' => ['name' => 'github', 'html' => '<i class="fab fa-github"></i>', 'class' => 'fab fa-github', 'color'=>'#333'],
        'google' => ['name' => 'google', 'html' => '<i class="fab fa-google"></i>', 'class' => 'fab fa-google', 'color'=>'#dd4b39'],
        'google-plus' => ['name' => 'google plus', 'html' => '<i class="fab fa-google-plus"></i>', 'class' => 'fab fa-google-plus', 'color'=>'#dd4b39'],
        'instagram' => ['name' => 'instagram', 'html' => '<i class="fab fa-instagram"></i>', 'class' => 'fab fa-instagram', 'color'=>'#e1306c'],
        'line' => ['name' => 'line', 'html' => '<i class="fab fa-line"></i>', 'class' => 'fab fa-line', 'color'=>'#00c300'],
        'linkedin' => ['name' => 'linkedin', 'html' => '<i class="fab fa-linkedin"></i>', 'class' => 'fab fa-linkedin', 'color'=>'#0077b5'],
        'meetup' => ['name' => 'meetup', 'html' => '<i class="fab fa-meetup"></i>', 'class' => 'fab fa-meetup', 'color'=>'#e0393e'],
        'pinterest' => ['name' => 'pinterest', 'html' => '<i class="fab fa-pinterest"></i>', 'class' => 'fab fa-pinterest', 'color'=>'#bd081c'],
        'reddit' => ['name' => 'reddit', 'html' => '<i class="fab fa-reddit"></i>', 'class' => 'fab fa-reddit', 'color'=>'#ff4500'],
        'skype' => ['name' => 'skype', 'html' => '<i class="fab fa-skype"></i>', 'class' => 'fab fa-skype', 'color'=>'#00aff0'],
        'slack' => ['name' => 'slack', 'html' => '<i class="fab fa-slack"></i>', 'class' => 'fab fa-slack', 'color'=>'#6ecadc'],
        'snapchat' => ['name' => 'snapchat', 'html' => '<i class="fab fa-snapchat"></i>', 'class' => 'fab fa-snapchat', 'color'=>'#fffc00'],
        'soundcloud' => ['name' => 'soundcloud', 'html' => '<i class="fab fa-soundcloud"></i>', 'class' => 'fab fa-soundcloud', 'color'=>'#ff8800'],
        'telegram' => ['name' => 'telegram', 'html' => '<i class="fab fa-telegram"></i>', 'class' => 'fab fa-telegram', 'color'=>'#0088cc'],
        'weibo' => ['name' => 'weibo', 'html' => '<i class="fab fa-weibo"></i>', 'class' => 'fab fa-weibo', 'color'=>'#fa2f2f'],
        'trello' => ['name' => 'trello', 'html' => '<i class="fab fa-trello"></i>', 'class' => 'fab fa-trello', 'color'=>'#0079bf'],
        'tumblr' => ['name' => 'tumblr', 'html' => '<i class="fab fa-tumblr-square"></i>', 'class' => 'fab fa-tumblr-square', 'color'=>'#35465c'],
        'twitter' => ['name' => 'twitter', 'html' => '<i class="fab fa-twitter-square"></i>', 'class' => 'fab fa-twitter-square', 'color'=>'#1da1f2'],
        'vimeo' => ['name' => 'vimeo', 'html' => '<i class="fab fa-vimeo-square"></i>', 'class' => 'fab fa-vimeo-square', 'color'=>'#1ab7ea'],
        'vine' => ['name' => 'vine', 'html' => '<i class="fab fa-vine"></i>', 'class' => 'fab fa-vine', 'color'=>'#00b488'],
        'whatsapp' => ['name' => 'whatsapp', 'html' => '<i class="fab fa-whatsapp-square"></i>', 'class' => 'fab fa-whatsapp-square', 'color'=>'#25d366'],
        'youtube' => ['name' => 'youtube', 'html' => '<i class="fab fa-youtube"></i>', 'class' => 'fab fa-youtube', 'color'=>'#ff0000'],

    ];

}
