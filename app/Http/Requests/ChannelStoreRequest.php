<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChannelStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255|unique:channels,name',
            'description' => 'required',
        ];
    }


    public function messages(){
        return [
            'name.required' => '채널 이름을 작성해주세요',
            'name.max' => '채널 이름은 최대 255자 이하입니다',
            'name.unique' => '이미 사용중인 채널 이름입니다',
            'description.required'  => '채널 소개글을 작성해 주세요',
        ];
    }

}
