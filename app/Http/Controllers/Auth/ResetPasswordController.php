<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    protected function rules()
    {
        return [
            'token' => 'required',
            'email' => 'required|email|exists:users,email',
            'password' => 'required|confirmed|min:6|max:20',
        ];
    }

    /**
     * Get the password reset validation error messages.
     *
     * @return array
     */
    protected function validationErrorMessages()
    {
        $messages = [            
            'token.required' => '유효하지 않은 데이터 오류입니다.',
            'email.required' => '이메일을 입력해주세요',
            'email.email' => '유효한 이메일주소가 아닙니다. 다시 확인 하시기 바랍니다.',
            'email.exists' => '등록되어 있는 이메일주소가 아닙니다.',
            'password.min' => '비밀번호는 최소 6자 이상입니다',
            'password.max' => '비밀번호는 최대 20자 이하입니다',
            'confirmed' => '비밀번호가 동일하지 않습니다',
        ];
        return $messages;
    }

    public function showResetForm(Request $request, $token = null)
    {
        return view('auth.passwords.reset')->with(
            ['token' => $token, 'email' => $request->email , 'preload' => false]
        );
    }


    protected function resetPassword($user, $password)
    {
        $user->password = Hash::make($password);

        $user->setRememberToken(Str::random(60));

        $user->save();

        //event(new PasswordReset($user));
        if($user->active) {
            $this->guard()->login($user);
        }

    }


    protected function sendResetResponse($response)
    {
        return redirect($this->redirectPath())
                            ->with('success', '비밀번호가 변경되었습니다.');
    }




    protected function sendResetFailedResponse(Request $request, $response)
    {
        $message = '';
        if($response == Password::INVALID_TOKEN){
            $message = '유효하지 않은 데이터 오류입니다.';
        } else {
            $message = trans($response);
        }
        return redirect()->back()
                    ->withInput($request->only('email'))
                    ->withErrors(['email' => $message]);
    }


}
