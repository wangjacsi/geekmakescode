<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Socialite;
use App\Models\User;
use Auth;


class SocialLoginController extends Controller
{
    public function __construct()
    {
        $this->middleware(['social', 'guest']);
    }

    public function redirect(Request $request, $service){
        return Socialite::driver($service)->redirect();
    }

    public function callback(Request $request, $service){
        $serviceUser = Socialite::driver($service)->user();

        $user = $this->getExistingUser($serviceUser, $service);

        if(!$user){
            $sameNameUser = User::where('name', $serviceUser->getName())->first();
            if($sameNameUser){
                $name = $serviceUser->getName() . '-' . uniqid(true);//uniqid(rand(), true);
            } else {
                $name = $serviceUser->getName();
            }
            $user = User::create([
                'name' => $name,
                'email' => $serviceUser->getEmail(),
                'photo' => $serviceUser->getAvatar(),
                'slug' => str_slug($name, '-'),
                'active' => true,
            ]);

            $user->options = collect(['viewoptions' =>
                                        ['mychannels' => true,
                                        'mytutorials' => true,
                                        'subscribedChannels' => true,
                                        'registeredTutorials' => true,
                                        'interestTutorials' => true,
                                        'chAdmins' => true,
                                        'follows' => true,
                                        'sns' => true,
                                        'tech' => true,
                                        'project' => true,
                                        'careers' => true,
                                        'contacts' => true,
                                        'messageForm' => true],
                                    'sns' => new \ArrayObject(),
                                    'tech' => new \ArrayObject(),
                                    'projects' => new \ArrayObject(),
                                    'careers' => new \ArrayObject(),
                                    'email' => '',
                                    'phone' => '',
                                    'site' => ''
                                    ])->toJson();
            $user->save();
        } else { // Update info by SNS
            $user->photo = $serviceUser->getAvatar();
            $user->last_login = now();
            $user->active = true;
            $user->save();
        }

        if($this->needsToCreateSocial($user, $service)) {
            $user->social()->create([
                'social_id' => $serviceUser->getId(),
                'service' => $service,
            ]);

            // Login and no remember
            Auth::login($user, false);

            return redirect()->intended()->withSuccess(env('APP_NAME'). ' 가입을 환영합니다');
        }

        // Login and no remember
        Auth::login($user, false);

        return redirect()->intended();


    }

    protected function needsToCreateSocial(User $user, $service){
        return !$user->hasSocialLinked($service);
    }

    /**
     * 해당 이메일의 사용자가 유저 테이블에 있는지 ? 또는
     * SNS 유저의 유저 아이디와 같은 유저가 소셜 테이블에 있는지 ?
     */
    protected function getExistingUser($serviceUser, $service){
        return User::where('email', $serviceUser->getEmail())
                ->orWhereHas('social', function($q) use($serviceUser, $service){
                    $q->where('social_id', $serviceUser->getId())
                        ->where('service', $service);
                })->first();
    }
}
