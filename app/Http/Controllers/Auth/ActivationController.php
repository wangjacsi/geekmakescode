<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Auth;

class ActivationController extends Controller
{
    public function activate(Request $request){

        $user = User::byActivationColumns($request->email, $request->token)
                    ->first();

        if(!$user){
            return redirect()->route('auth.activate.resend')->withWarning('유효한 데이터가 아닙니다. 다시 시도해 주세요.');
        }

        $user->update([
            'active' => true,
            'activation_token' => null
        ]);

        Auth::loginUsingId($user->id);

        return redirect()->route('index')->withSuccess('이메일 확인이 완료되었습니다. codewwwave와 함께해요.');
    }
}
