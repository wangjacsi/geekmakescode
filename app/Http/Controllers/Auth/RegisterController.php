<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
//use Illuminate\Auth\Events\Registered;
use App\Events\UserActivationEmail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/login';
    public $validator;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    protected function redirectTo()
    {
        return '/login';
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'slug' => str_slug($data['name'], '-'),//, 'ko'),
            'password' => Hash::make($data['password']),
            'active' => false,
            'activation_token' => str_random(255),
        ]);
    }

    protected function validator(array $data)
    {
        $messages = [
            'name.min' => '아이디는 최소 2자 이상입니다',
            'name.max' => '아이디는 최대 20자 이하입니다',
            'name.unique' => '이미 사용중인 아이디입니다',
            'email.unique'  => '이미 사용중인 이메일입니다',
            'password.min' => '비밀번호는 최소 6자 이상입니다',
            'password.max' => '비밀번호는 최대 20자 이하입니다',
            'confirmed' => '비밀번호가 일치하지 않습니다',
        ];

        $this->validator =  Validator::make($data, [
            'name' => 'required|string|min:2|max:20|unique:users',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|max:20|confirmed',
        ], $messages);
    }

    public function register(Request $request)
    {
        //$this->validator($request->all())->validate();
        $this->validator($request->all());

        if ($this->validator->fails()) {
            return response()->json([
                'result' => 'error',
                'message' => '유효하지 않은 데이터입니다',
                'errors' => $this->validator->errors(),
            ]);
        }

        //event(new Registered($user = $this->create($request->all())));

        $user = $this->create($request->all());
        $user->options = collect(['viewoptions' =>
                                    ['mychannels' => true,
                                    'mytutorials' => true,
                                    'subscribedChannels' => true,
                                    'registeredTutorials' => true,
                                    'interestTutorials' => true,
                                    'chAdmins' => true,
                                    'follows' => true,
                                    'sns' => true,
                                    'tech' => true,
                                    'project' => true,
                                    'careers' => true,
                                    'contacts' => true,
                                    'messageForm' => true],
                                'sns' => new \ArrayObject(),
                                'tech' => new \ArrayObject(),
                                'projects' => new \ArrayObject(),
                                'careers' => new \ArrayObject(),
                                'email' => '',
                                'phone' => '',
                                'site' => ''
                                ])->toJson();
        $user->save();

        event(new UserActivationEmail($user));

        return response()->json([
            'result' => 'success',
            'message' => '가입 성공'
        ]);
    }


    protected function registered(Request $request)
    {
        //$this->guard()->logout();
        return redirect($this->redirectPath())
            ->withSuccess('가입이 완료되었습니다. 서비스 이용을 위해 이메일을 확인 후 활성화 작업을 완료해 주세요.');
    }
}
