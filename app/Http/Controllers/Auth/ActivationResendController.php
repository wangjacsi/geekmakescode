<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Events\UserActivationEmail;

class ActivationResendController extends Controller
{
    public function showResendForm(){
        return view('auth.activate.resend')->with('preload', false);
    }

    public function resend(Request $request){
        $this->validateResendRequest($request);

        $user = User::byEmail($request->email)->first();

        if($user->active){
            return redirect()->route('login')
                ->withSuccess('이미 등록완료된 메일입니다. 로그인 후 서비스 이용해주세요.');
        }

        event(new UserActivationEmail($user));

        return redirect()->route('login')
            ->withSuccess('메일이 전송되었습니다. 메일함을 확인해주세요.');
    }

    protected function validateResendRequest(Request $request){
        $this->validate($request, [
            'email' => 'required|email|exists:users,email'
        ], [
            'email.exists' => '등록된 메일주소가 아닙니다.'
        ]);
    }
}
