<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\EmailSubscription;
use Illuminate\Support\Facades\Validator;

class EmailSubscriptionController extends Controller
{
    public function store(Request $request){
        if($request->has('email')){
            $validator =  Validator::make($request->all(), [
                'email' => 'required|email'
            ], ['email.required' => '이메일 주소를 작성해주세요',
                'email.email' => '유효한 이메일 형식이 아닙니다',
            ]);

            if ($validator->fails()) {
                return response()->json([
                    'result' => 'error',
                    'message' => '유효하지 않은 데이터입니다',
                    'errors' => $validator->errors(),
                ], 200);
            }

            EmailSubscription::updateOrCreate(
                ['email' => $request->email],
                ['enable' => true]
            );
            return response()->json(null, 200);
        } else {
            return response()->json(null, 400);
        }
    }
}
