<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use App\Models\Channel;
use App\Models\Tutorial;
use App\Models\Video;
use App\Models\User;
use App\Models\Tag;
use App\Http\Resources\UserResource;
use App\Http\Helper;

class SearchController extends Controller
{
    public function searchUsers(Request $request){
        if(! $request->q ){
            return;
        }

        $users = User::where('name', 'LIKE', '%'.$request->q.'%')
                ->orWhere('email', 'LIKE', '%'.$request->q.'%')->take(20)
                ->get();

        return UserResource::collection(
            $users
        );
    }

    public function searchTags(Request $request){
        if(! $request->q){
            return;
        }

        $tags = Tag::where('name', 'LIKE', '%'.$request->q.'%')->take(20)->get();

        return response()->json($tags, 200);
    }

    public function index(Request $request){
        if(! $request->q ){
            return back();
        }

        return view('app.search.index', [
            'searchWord' => $request->q,
            'backgroundColors' => Helper::$backgroundColors,
            'progressStatus' => Helper::getProgressStatusKorean(),
        ]);
    }

}
