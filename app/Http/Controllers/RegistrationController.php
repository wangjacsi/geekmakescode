<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Tutorial;
use App\Models\Registration;
use App\Events\RegistrationCreated;

class RegistrationController extends Controller
{
    public function regist(Request $request, Tutorial $tutorial){
        //$this->authorize('regist', $tutorial);

        $tutorial->registrations()->create([
            'user_id' => $request->user()->id
        ]);

        // notification event
        event(new RegistrationCreated($tutorial, $request->user()));


        return response()->json(null, 200);
    }
}
