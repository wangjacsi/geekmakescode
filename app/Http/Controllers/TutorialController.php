<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Helper;
use App\Models\Channel;
use Illuminate\Support\Facades\Validator;
use App\Models\Tutorial;
use App\Jobs\TutorialUploadImage;
use Vimeo\Laravel\Facades\Vimeo;
use App\Http\Resources\TutorialResource;
use App\Http\Resources\TutorialRecommendResource;
use App\Models\User;
use App\Jobs\ImageUploadToS3ByFolder;
use App\Models\Vote;
use App\Models\Interest;
use App\Models\Tag;
use Illuminate\Support\Facades\DB;
use App\Jobs\ImageDeleteInS3;
use App\Events\TutorialLaunched;

//use App\Models\Notification;

class TutorialController extends Controller
{
    public $numItemsPerPage = 8;


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('app.tutorial.index', [
            'backgroundColors' => Helper::$backgroundColors,
            'progressStatus' => Helper::getProgressStatusKorean(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        // User's Channel get
        $channels = Auth::user()->channels()->get();

        // get Information
        $visibility = Helper::getVisibility();
        $costType = Helper::getCostType();
        $skills = Helper::getSkills();

        return view('app.tutorial.create', [
                    'channels' => $channels,
                    'visibility' =>$visibility,
                    'costType' =>$costType,
                    'skills' =>$skills
                    ]);
    }


    protected function validator(array $data)
    {
        $messages = [
            'title.required' => '강좌 제목을 작성해주세요',
            'title.max' => '강좌 제목은 최대 255자 이하입니다',
            'channel_id.required' => '채널을 선택해주세요',
            'visibility.required'  => '강좌 옵션을 선택해주세요',
            'cost_type.required'  => '강좌 가격 정책을 선택해주세요',
            'cost.required'  => '강좌 가격을 작성해주세요',
            //'allow_votes.required'  => '강좌 평가 유무를 선택해주세요',
            //'allow_comments.required'  => '강좌 댓글 유무를 선택해주세요',
        ];

        $this->validator =  Validator::make($data, [
            'title' => 'required|max:255',
            'channel_id' => 'required',
            'visibility' => 'required',
            'cost_type' => 'required',
            'cost' => 'required',
            //'allow_votes' => 'required',
            //'allow_comments' => 'required',
        ], $messages);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // 권한 체크 : policy 를 사용할 수 없음.
        $channel = Channel::where('id', $request->channel_id)->first();
        //dd($channel);
        if( $channel ){
            if( ! count( $channel->users()->where('users.id', Auth::user()->id)->get() ) > 0 ){
                return response()->json([
                    'result' => 'error',
                    'message' => '선택하신 채널에 대한 권한이 없습니다. 다시 한번 확인해주세요.'
                ]);
            }
        } else {
            return response()->json([
                'result' => 'error',
                'message' => '선택하신 채널이 확인되지 않습니다. 다시 한번 확인해주세요.'
            ]);
        }

        // normal function here
        // validate
        $this->validator($request->all());

        if ($this->validator->fails()) {
            return response()->json([
                'result' => 'error',
                'message' => '유효하지 않은 데이터입니다',
                'errors' => $this->validator->errors(),
            ]);
        }

        $tutorial = Tutorial::create([
            'title' => $request->title,
            'slug' => str_slug($request->title),
            'channel_id' => $request->channel_id,
            'visibility' => $request->visibility,
            'description' => $request->description,
            'cost_type' => $request->cost_type,
            'cost' => $request->cost,
            'allow_votes' => $request->allow_votes,
            'allow_comments' => $request->allow_comments,
            'allow_reviews' =>$request->allow_reviews,
            'progress_status' => 'publishing',
            'skill' => $request->skill
        ]);

        if($tutorial){
            // image patch
            $newDescription = str_replace('/tutorials/'.$request->tempID, env('AWS_URL').'tutorials/'.$tutorial->id, $request->description);
            $tutorial->description = $newDescription;
            $tutorial->save();

            // tag
            if ($request->has('tag')) {
                $tagIds = $this->createTags($request->tag);
                if(count($tagIds) > 0){
                    $this->attachTags($tutorial, $tagIds);
                }
            }

            $this->dispatch(new ImageUploadToS3ByFolder('app/public/tutorials/'.$request->tempID, 'tutorials/'.$tutorial->id));

            // image request check
            if ($request->has('head_image')) {
                $headImage = $request->file('head_image');
                $filename = Helper::makeImageFilename($headImage);
                $headImage->move(storage_path(). '/uploads/tutorials/',  $filename);
                $this->dispatch(new TutorialUploadImage($tutorial, $filename, 'head_image'));
            }

            if ($request->has('thumbnail')) {
                $thumbnail = $request->file('thumbnail');
                $filename2 = Helper::makeImageFilename($thumbnail);
                $thumbnail->move(storage_path(). '/uploads/tutorials/',  $filename2);
                $this->dispatch(new TutorialUploadImage($tutorial, $filename2, 'thumbnail'));
            }


            // vimeo 앨범 및 프로젝트 만들기
            $data = Vimeo::connection('main')->request('/me/albums',//'/users/' . env('VIMEO_USER_ID') . '/albums',
                ['description' => $tutorial->description,
                'link' => '',
                'name' => $tutorial->title,
                'privacy' => 'embed_only', // embed_only, password
                //'password' => '', // 없어도 됨
                'sort' => 'added_first'],
                'POST');

            if($data['status'] == 201) {
                $tutorial->vimeo_url = $data['body']['uri'];
                $tutorial->save();
            }

            // 공개 시 구독자 메일 전송하기 및 알림 저장
            if($tutorial->visibility == 'public'){
                event(new TutorialLaunched($tutorial, $channel));
            }

            return response()->json([
                'result' => 'success',
                'message' => '강좌 생성 완료',
                'slug' => $tutorial->slug,
            ]);
        } else {
            return response()->json([
                'result' => 'error',
                'message' => '강좌 생성에 실패하였습니다'
            ]);
        }
    }

    public function createTags($data)
    {
        // make array
        if(!is_array($data)){
            $tags = explode(',', $data);
        } else {
            $tags = $data;
        }

        $tagIds = [];
        for ($i = 0; $i < count($tags); $i ++)
        {
            if (! $tag = Tag::firstOrCreate(['name' => $tags[$i]]))
                throw new FailedTagCreateException;
            $tagIds[] = $tag->id;
        }
        return $tagIds;
    }

    public function attachTags($tutorial, $tagIds){
        $tutorial->tags()->sync($tagIds);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Tutorial $tutorial)
    {
        // 체크 강좌가 오픈되는 강좌인지 오픈 될 수 없다면 관리자에게만 오픈되도록
        $channel = $tutorial->channel()->first();
        $admins = $channel->users()->get();
        $user = Auth::user();

        if($user){
            $userID = $user->id;
            $isRegistered = $user->isRegisteredTo($tutorial);
            $userInterestCategories = $user->interests()->get()->unique('category');
        } else {
            $userID = null;
            $isRegistered = false;
            $userInterestCategories = null;
        }

        $owner = $channel->users()->where('users.id', '=', $userID)->first() ? true : false;

        if($owner)
            $videos = $tutorial->videos()->get();
        else
            $videos = $tutorial->videosByVisible()->get();

        $totalVideos = $videos->count();
        $totalPlayTime = $videos->sum('playtime');
        $totalRegistrations = $tutorial->registrationsCount();

        $totalPlayTime = Helper::transTimeToPlaytime($totalPlayTime, 'hours-minutes');
        $records = $tutorial->recordsWithUserId($userID);

        // 리뷰 정보 담기 : 순서 중요 -> where 나의 리뷰 여부 검색시 결과가 나의 것만 보여줌
        $reviews = $tutorial->reviews();


        $reviewGroup = DB::table('reviews')
                ->select(DB::raw('COUNT(id) as cnt'), 'review')
                ->where('tutorial_id', $tutorial->id)
                ->groupBy('review')
                ->get();

        $reviewAvg = $reviews->get()->avg('review') / 2;
        $myReview = $reviews->where('user_id', '=', $userID)->first();
        $reviewed = $myReview ? true : false;

        $upVotes = $tutorial->upVotes(); //->count();
        $upVotesTotal = $upVotes->count();
        $voted = $upVotes->where('user_id', $userID)->first() ? true : false;

        $interested = $tutorial->interestedByUser($userID);


        // Tags
        $tagsCollect = $tutorial->tags()->get();//->toArray();
        $tags = $tagsCollect->map(function ($item, $key) {
            return $item['name'];
        })->toArray();

        // suggestions
        $suggestions = $this->getSuggestTutorials($tutorial->id);


        return view('app.tutorial.show', [
            'owner' => $owner,
            'isRegistered' => $isRegistered,
            'tutorial' => $tutorial,
            'channel' => $channel,
            'admins' => $admins,
            'videos' =>$videos,
            'totalVideos' =>$totalVideos,
            'totalPlayTime' =>$totalPlayTime,
            'totalRegistrations' => $totalRegistrations,
            'records' => $records,
            'reviewGroup' => $reviewGroup,
            'reviewed' => $reviewed,
            'myReview' => $myReview,
            'reviewAvg' => $reviewAvg,
            'upVotesTotal' => $upVotesTotal,
            'voted' => $voted,
            'interested' => $interested,
            'userInterestCategories' =>$userInterestCategories,
            'tags' => $tags,
            'suggestions' => $suggestions,
            'backgroundColors' => Helper::$backgroundColors,
            'progressStatus' => Helper::getProgressStatusKorean()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Tutorial $tutorial)
    {
        $this->authorize('edit', $tutorial);

        $channel = $tutorial->channel()->first();
        $videos = $tutorial->videos()->get();
        $channels = Auth::user()->channels()->get();
        $skills = Helper::getSkills();
        $visibility = Helper::getVisibility();
        $progressStatus = Helper::getProgressStatusKorean();

        // Tags
        $tagsCollect = $tutorial->tags()->get();//->toArray();
        $tags = $tagsCollect->map(function ($item, $key) {
            return $item['name'];
        })->toArray();

        return view('app.tutorial.edit', [
            'tutorial' => $tutorial,
            'channel' => $channel,
            'videos' =>$videos,
            'channels' => $channels,
            'skills' => $skills,
            'tags' => $tags,
            'visibility' =>$visibility,
            'progressStatus' => $progressStatus
        ]);
    }

    public function updateSetting(Request $request, Tutorial $tutorial){
        $this->authorize('edit', $tutorial);

        $tutorial->visibility = $request->visibility;
        $tutorial->progress_status = $request->progress_status;
        $tutorial->allow_reviews = $request->allow_reviews;
        $tutorial->allow_comments = $request->allow_comments;
        $tutorial->allow_votes = $request->allow_votes;
        $tutorial->save();

        // 공개 시 구독자 메일 전송하기 및 알림 저장
        if($tutorial->visibility == 'public'){
            event(new TutorialLaunched($tutorial, $tutorial->channel()->first() ));
        }


        return response()->json(null, 200);
    }

    public function updateContent(Request $request, Tutorial $tutorial){
        $this->authorize('edit', $tutorial);

        $validator =  Validator::make($request->all(), [
            'channel_id' => 'required',
            'skill' => 'required',
            'description' => 'required',
            'title' => 'required|min:2|max:255',
        ], ['title.required' => '강좌 제목을 작성해주세요',
            'title.min' => '강좌 제목은 최소 2자 이상입니다',
            'title.max' => '강좌 제목은 최대 255자 이하입니다',
            'channel_id.required' => '채널을 선택해주세요',
            'skill.required' => '강좌 레벨을 선택해주세요',
            'description.required' => '강좌 소개글을 작성해주세요'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'result' => 'error',
                'message' => '유효하지 않은 데이터입니다',
                'errors' => $validator->errors(),
            ]);
        }

        $tutorial->title = $request->title;
        $tutorial->slug = str_slug($request->title, '-');
        $tutorial->channel_id = $request->channel_id;
        $tutorial->skill = $request->skill;
        $tutorial->description = $request->description;
        $tutorial->save();

        // tag
        if ($request->has('tag')) {
            $tagIds = $this->createTags($request->tag);
            if(count($tagIds) > 0){
                $this->attachTags($tutorial, $tagIds);
            }
        }

        // vimeo update
        //PATCH https://api.vimeo.com/me/albums/{album_id}
        //PATCH https://api.vimeo.com/users/{user_id}/albums/{album_id}
        $data = Vimeo::connection('main')->request($tutorial->vimeo_url,
            ['description' => $tutorial->description,
            'name' => $tutorial->title],
            'PATCH');

        return response()->json([
            'result' => 'success',
            'message' => '채널 수정 완료',
            'tutorial' => $tutorial,
            'channel' => $tutorial->channel()->first(),
        ]);
    }

    public function updateImage(Request $request, Tutorial $tutorial){
        $this->authorize('edit', $tutorial);

        $originHeadFile = $tutorial->head_image;
        $originThumbnailFile = $tutorial->thumbnail;

        // image request check
        if ($request->has('head_image')) {
            $headImage = $request->file('head_image');
            $filename = Helper::makeImageFilename($headImage);
            $headImage->move(storage_path(). '/uploads/tutorials/',  $filename);
            $this->dispatch(new TutorialUploadImage($tutorial, $filename, 'head_image'));
            // delete S3
            if($originHeadFile != ''){
                $this->dispatch(new ImageDeleteInS3($originHeadFile));
            }
        }

        if ($request->has('thumbnail')) {
            $thumbImage = $request->file('thumbnail');
            $filename2 = Helper::makeImageFilename($thumbImage);
            $thumbImage->move(storage_path(). '/uploads/tutorials/',  $filename2);
            $this->dispatch(new TutorialUploadImage($tutorial, $filename2, 'thumbnail'));
            // delete S3
            if($originThumbnailFile != ''){
                $this->dispatch(new ImageDeleteInS3($originThumbnailFile));
            }
        }

        return response()->json(null, 200);
    }

    public function updateDescription(Request $request, Tutorial $tutorial){
        $this->authorize('edit', $tutorial);

        $tutorial->description = $request->body;
        $tutorial->save();
        return response()->json(null, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



    // API response
    // get suggested tutorials
    public function getSuggestTutorials($without, $type = 'local'){

        $tutorials = Tutorial::withCount([
            'reviews',
            'votes',
            'registrations',
            'interests',
        ])
        ->where('tutorials.id', '<>', $without)
        ->with(['channel', 'videosByVisible'])
        ->leftJoin('reviews', 'tutorials.id', '=', 'reviews.tutorial_id')
        ->addSelect(array(
    		DB::raw('AVG(review) as review')
    	))
        ->groupBy('tutorials.id')
        ->orderBy('review', 'DESC')
        ->orderBy('votes_count', 'DESC')
        ->orderBy('registrations_count', 'DESC')
        ->orderBy('interests_count', 'DESC')
        ->take(9)->get();

        if($type == 'local'){
            return $tutorials;
        }

        return response()->json($tutorials, 200);
    }

    // get tutorials by channel ID by post method
    public function getTutorialsByChannel(Request $request){
        $channel = Channel::where('id', $request->channel)->first();

        $tutorials = $channel->tutorials()->get();

        return response()->json([
            'result' => 'success',
            'tutorials' => $tutorials,
        ]);
    }

    public function vote(Request $request, Tutorial $tutorial){
        $this->authorize('vote', $tutorial);

        if($request->vote){ // create
            Vote::updateOrCreate(
                ['user_id' => $request->user()->id,
                'voteable_id' => $tutorial->id,
                'voteable_type' => 'App\Models\Tutorial'],
                ['type' => $request->vote ? 'up' : 'down']
            );
        } else { // remove
            Vote::where('user_id', $request->user()->id)
                ->where('voteable_id', $tutorial->id)
                ->where('voteable_type', 'App\Models\Tutorial')
                ->delete();
        }

        return response()->json(null, 200);
    }

    public function interest(Request $request, Tutorial $tutorial){
        $this->authorize('interest', $tutorial);

        if($request->type){ // create
            Interest::updateOrCreate(
                ['user_id' => $request->user()->id,
                'tutorial_id' => $tutorial->id],
                ['category' => $request->category]
            );
        } else { // remove
            Interest::where('user_id', $request->user()->id)
                ->where('tutorial_id', $tutorial->id)
                ->delete();
        }

        return response()->json(null, 200);
    }


    // Registed Tutorials Info by login user
    public function getTutorialsInfoByChannel(Request $request, Channel $channel){
         $tutorials = TutorialResource::collection(
            $channel->tutorials()->latestFirst()->paginate($this->numItemsPerPage)
        );
        $tutorials->additional(['channel' => $channel]);
        return $tutorials;
    }

    // Registed Tutorials Info by login user
    public function getRegisteredTutorialsInfoByLoginUser(Request $request){
        return $tutorials = TutorialResource::collection(
            $request->user()->registeredTutorials()->with(['channel'])->latestFirst()->paginate($this->numItemsPerPage)
        );
    }

    // Registed Tutorials Info by User id
    public function getRegisteredTutorialsInfoByUserId(Request $request, User $user){
        return $tutorials = TutorialResource::collection(
            $user->registeredTutorials()->with(['channel'])->latestFirst()->paginate($this->numItemsPerPage)
        );
    }

    // My tutorials
    public function getMyTutorialsInfoByLoginUser(Request $request){
        return $tutorials = TutorialResource::collection(
            $request->user()->tutorials()->with(['channel'])->latestFirst()->paginate($this->numItemsPerPage -1)
        );
    }

    // Registed Tutorials Info by User id
    public function getMyTutorialsInfoByUserId(Request $request, User $user){
        return $tutorials = TutorialResource::collection(
            $user->tutorials()->with(['channel'])->latestFirst()->paginate($this->numItemsPerPage -1)
        );
    }

    // interest Tutorials info by User id
    public function getInterestTutorialsInfoByUserId(Request $request, User $user){
        return $tutorials = TutorialResource::collection(
            $user->interestTutorials()->with(['channel'])->latestFirst()->paginate($this->numItemsPerPage)
        );
    }

    public function getInterestTutorialsInfoByLoginUser(Request $request){
        if($request->has('category') && $request->category != ''){
            return $tutorials = TutorialResource::collection(
                Tutorial::with(['channel'])
                    ->whereHas('interests', function ($query) use($request) {
                        $query->where('interests.user_id', $request->user()->id)
                        ->where('interests.category', $request->category);
                    })->where('visibility', '=', 'public')->latestFirst()->paginate($this->numItemsPerPage)
            );
        } else {
            return $tutorials = TutorialResource::collection(
                $request->user()->interestTutorials()->with(['channel'])->latestFirst()->paginate($this->numItemsPerPage)
            );
        }
    }

    // Search
    public function getSearchTutorialsInfo(Request $request){
        if(!$request->search){
            return false;
        }

        if(!$request->has('order') || $request->order == 'recent'){
            return $tutorials = TutorialResource::collection(
                Tutorial::where('title', 'LIKE', '%'.$request->search.'%')
                ->orWhereHas('tags', function ($query) use($request) {
                    $query->where('tags.name', 'like', '%'.$request->search.'%');
                })
                ->where('visibility', '=', 'public')->with('channel')->latestFirst()->paginate(15)
            );
        } else if($request->order == 'votes'){
            return $tutorials = TutorialResource::collection(
                Tutorial::withCount(['votes', 'interests'])
                ->where('title', 'LIKE', '%'.$request->search.'%')
                ->orWhereHas('tags', function ($query) use($request) {
                    $query->where('tags.name', 'like', '%'.$request->search.'%');
                })
                ->where('visibility', '=', 'public')
                ->with('channel')
                ->orderBy('votes_count', 'DESC')
                ->orderBy('interests_count', 'DESC')
                ->latestFirst()->paginate(15)
            );
        } else if($request->order == 'review'){
            return $tutorials = TutorialResource::collection(
                Tutorial::withCount(['reviews'])
                ->where('title', 'LIKE', '%'.$request->search.'%')
                ->orWhereHas('tags', function ($query) use($request) {
                    $query->where('tags.name', 'like', '%'.$request->search.'%');
                })
                ->where('visibility', '=', 'public')
                ->with('channel')
                ->leftJoin('reviews', 'tutorials.id', '=', 'reviews.tutorial_id')
                ->addSelect(array(
            		DB::raw('AVG(review) as review')
            	))
                ->groupBy('tutorials.id')
                ->orderBy('review', 'DESC')
                ->latestFirst()->paginate(15)
            );
        } else if($request->order == 'registration'){
            return $tutorials = TutorialResource::collection(
                Tutorial::withCount(['registrations'])
                ->where('title', 'LIKE', '%'.$request->search.'%')
                ->orWhereHas('tags', function ($query) use($request) {
                    $query->where('tags.name', 'like', '%'.$request->search.'%');
                })
                ->where('visibility', '=', 'public')
                ->with('channel')
                ->orderBy('registrations_count', 'DESC')
                ->latestFirst()->paginate(15)
            );
        }
    }

    public function getTutorialsInfo(Request $request){

        if(!$request->has('order') || $request->order == 'recent'){
            return $tutorials = TutorialResource::collection(
                Tutorial::where('visibility', '=', 'public')->with('channel')->latestFirst()->paginate(15)
            );
        } else if($request->order == 'votes'){
            return $tutorials = TutorialResource::collection(
                Tutorial::withCount(['votes', 'interests'])
                ->where('visibility', '=', 'public')
                ->with('channel')
                ->orderBy('votes_count', 'DESC')
                ->orderBy('interests_count', 'DESC')
                ->latestFirst()->paginate(15)
            );
        } else if($request->order == 'review'){
            return $tutorials = TutorialResource::collection(
                Tutorial::withCount(['reviews'])
                ->where('visibility', '=', 'public')
                ->with('channel')
                ->leftJoin('reviews', 'tutorials.id', '=', 'reviews.tutorial_id')
                ->addSelect(array(
            		DB::raw('AVG(review) as review')
            	))
                ->groupBy('tutorials.id')
                ->orderBy('review', 'DESC')
                ->latestFirst()->paginate(15)
            );
        } else if($request->order == 'registration'){
            return $tutorials = TutorialResource::collection(
                Tutorial::withCount(['registrations'])
                ->where('visibility', '=', 'public')
                ->with('channel')
                ->orderBy('registrations_count', 'DESC')
                ->latestFirst()->paginate(15)
            );
        }
    }




}
