<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Video;
use App\Models\Record;
use Illuminate\Support\Carbon;

class VideoViewController extends Controller
{
    const BUFFER = 24;

    public function watchCurrentTime(Request $request, Video $video){
        if(!$video->canBeAccessed($request->user()) ){
            return;
        }

        if($request->user()){
            Record::updateOrCreate(
                ['user_id' => $request->user()->id,
                'video_id' => $video->id],
                ['progress' => $request->time]
            );
        }

        return response()->json(null, 200);
    }


    public function watch(Request $request, Video $video){
        if(!$video->canBeAccessed($request->user()) ){
            return;
        }

        if($request->user()){
            $record = Record::updateOrCreate(
                ['user_id' => $request->user()->id,
                'video_id' => $video->id],
                ['watch' => $request->has('watch') ? $request->watch : true]
            );
        }

        return response()->json($record, 200);
    }

    public function create(Request $request, Video $video){

        if(!$video->canBeAccessed($request->user()) ){
            return;
        }

        if($request->user()){
            $lastUserView = $video->views()->latestByUser($request->user())->first();

            if($this->withinBuffer($lastUserView)){
                return;
            }
        }

        $lastIpView = $video->views()->latestByIp($request->ip())->first();

        if($this->withinBuffer($lastIpView)){
            return;
        }

        $video->views()->create([
            'user_id' => $request->user() ? $request->user()->id : null,
            'ip' => $request->ip(),
        ]);

        return response()->json(null, 200);
    }

    public function withinBuffer($lastUserView){
        return $lastUserView && $lastUserView->created_at->diffInSeconds(Carbon::now()) < self::BUFFER;
    }
}
