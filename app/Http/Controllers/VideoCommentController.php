<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Video;
//use App\Transformers\CommentTransformer;
use App\Http\Requests\CreateCommentRequest;
use App\Models\Comment;
use App\Http\Resources\CommentResource; //방법 2
use App\Events\VideoCommentCreated;

class VideoCommentController extends Controller
{
    public $numItemsPerPage = 10;

    public function index(Video $video){

        // 2
        return $comments = CommentResource::collection(
            $video->comments()->with(['replies', 'user', 'user.channels', 'replies.user.channels'])->latestFirst()->paginate($this->numItemsPerPage)
        );
    }

    public function update(Request $request, Video $video, Comment $comment){
        $this->authorize('update', $comment);

        $this->validate($request, [
            'body' => 'required|max:5500'
        ]);

        $comment->update([
            'body' => $request->body
        ]);

        return new CommentResource($comment);
    }


    public function create(CreateCommentRequest $request, Video $video){
        $this->authorize('comment', $video);

        $comment = $video->comments()->create([
            'body' => $request->body,
            'reply_id' => $request->get('reply_id', null),
            'user_id' => $request->user()->id,
        ]);

        // Notification & event
        event(new VideoCommentCreated($video, $request->user(), $comment));


        return new CommentResource($comment);
        /*
        return response()->json(
            fractal()->item($comment)
                ->parseIncludes(['channels', 'user'])
                ->transformWith(new CommentTransformer)
                ->toArray()
        );*/
    }


    public function delete(Video $video, Comment $comment){
        $this->authorize('delete', $comment);

        $comment->delete();

        return response()->json(null, 200);
    }
}
