<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Video;
use Illuminate\Support\Facades\Validator;
use Auth;
use App\Models\Tutorial;
use App\Jobs\UploadVideo;
use App\Jobs\ReplaceVideo;
use App\Jobs\VimeoVideoTranscodingCheck;
use Vimeo\Laravel\Facades\Vimeo;
use App\Http\Helper;
use App\Jobs\ImageUploadToS3ByFolder;
use Storage;
use App\Models\Tag;
use Illuminate\Support\Facades\DB;

class VideoController extends Controller
{
    public $validator;

    public function show(Video $video){
        $this->authorize('show', $video);

        $tutorial = $video->tutorial()->first();
        $channel = $tutorial->channel()->first();
        $user = Auth::user();
        $owned = $video->ownedByUser($user);

        // Tags
        $tagsCollect = $tutorial->tags()->get();//->toArray();
        $tutorialTags = $tagsCollect->map(function ($item, $key) {
            return $item['name'];
        })->toArray();

        $tagsCollect = $video->tags()->get();//->toArray();
        $videoTags = $tagsCollect->map(function ($item, $key) {
            return $item['name'];
        })->toArray();

        // videos
        $videos = $tutorial->videosByVisible()->get();

        // suggestions
        $recommendTutorials = $this->getSuggestTutorials($tutorial->id);

        // record
        $records = $tutorial->recordsWithUserId($user->id);

        return view('app.video.show', [
            'video' => $video,
            'videos' => $videos,
            'tutorial' => $tutorial,
            'channel' => $channel,
            'owned' => $owned,
            'tutorialTags' => $tutorialTags,
            'videoTags' => $videoTags,
            'recommendTutorials' => $recommendTutorials,
            'records' => $records,
        ]);
    }

    // get suggested tutorials
    public function getSuggestTutorials($without){

        $tutorials = Tutorial::withCount([
            'reviews',
            'votes',
            'registrations',
            'interests',
        ])
        ->where('tutorials.id', '<>', $without)
        ->with(['channel', 'videosByVisible'])
        ->leftJoin('reviews', 'tutorials.id', '=', 'reviews.tutorial_id')
        ->addSelect(array(
    		DB::raw('AVG(review) as review')
    	))
        ->groupBy('tutorials.id')
        ->orderBy('review', 'DESC')
        ->orderBy('votes_count', 'DESC')
        ->orderBy('registrations_count', 'DESC')
        ->orderBy('interests_count', 'DESC')
        ->take(9)->get();

        return $tutorials;
    }

    public function index(Request $request){
        $videos = $request->user()->videos()->latestFirstByUser()->get();

        //dd($videos);
        return view('app.video.index', ['videos' => $videos]);
    }

    public function upload(){

        // Show the user's channel
        $channels = Auth::user()->channels()->get();

        // Show the user's tutorial -> Automatic search

        // Video upload From view
        return view('app.video.upload', ['channels' => $channels]);
    }

    protected function validator(array $data)
    {
        $messages = [
            'video.required' => '비디오 파일을 선택해주세요',
            'title.required' => '비디오 타이틀을 작성해주세요',
            'title.max' => '비디오 타이틀은 최대 255자 이하입니다',
            'channel_id.required' => '채널을 선택해주세요',
            'tutorial_id.required' => '강좌를 선택해주세요',
            //'description.required' => '소개글을 작성해주세요',
            'visibility.required' => '비디오 설정을 선택해주세요',
            'preview.required' => '비디오 미리보기 설정을 선택해주세요',
            //'allow_votes.required' => '평가 사용 여부를 선택해주세요',
            //'allow_comments.required' => '댓글 사용 여부를 선택해주세요',
            //'order.required' => '비디오 순서를 선택해주세요',
        ];

        $this->validator =  Validator::make($data, [
            'video' => 'required',
            'title' => 'required|max:255',
            'channel_id' => 'required',
            'tutorial_id' => 'required',
            'visibility' => 'required',
            'preview' => 'required',
            //'allow_votes' => 'required',
            //'allow_comments' => 'required',
            //'order' => 'required',
        ], $messages);
    }

    public function store(Request $request){//VideoUpdateRequest
        // validate
        $this->validator($request->all());

        if ($this->validator->fails()) {
            return response()->json([
                'result' => 'error',
                'message' => '유효하지 않은 데이터입니다',
                'errors' => $this->validator->errors(),
            ]);
        }

        // check the tutorial and user
        $tutorial = Tutorial::where('id', $request->tutorial_id)->first();
        if(! $tutorial){
            return response()->json([
                'result' => 'error',
                'message' => '유효하지 않은 데이터입니다 [강좌 아이디 에러]',
            ]);
        }
        $channel = $tutorial->channel()->first();
        if(! $channel){
            return response()->json([
                'result' => 'error',
                'message' => '유효하지 않은 데이터입니다 [채널 아이디 에러]',
            ]);
        }
        $user = $channel->users()->where('users.id', Auth::user()->id)->first();
        if(! $channel){
            return response()->json([
                'result' => 'error',
                'message' => '접근 권한이 없습니다 [유저 아이디 에러]',
            ]);
        }

        // make random file name
        $videoInput = $request->file('video');
        $filenameArray = Helper::makeImageFilenameWithExt($videoInput);
        $filename = $filenameArray[0];
        $ext = $filenameArray[1];

        // update
        $video = Video::create([
            'title' => $request->title,
            'uid' => $filename,
            'channel_id' => $request->channel_id,
            'tutorial_id' => $request->tutorial_id,
            'description' =>$request->description,
            'visibility' =>$request->visibility,
            'preview' =>$request->preview,
            //'allow_votes' => $request->allow_votes,
            //'allow_comments' => $request->allow_comments,
            'video_filename' => $filename,
            'order' => $request->order,
            'filesize' => filesize($request->file('video'))
        ]);

        if($video) {
            // tag
            if ($request->has('tag')) {
                $tagIds = $this->createTags($request->tag);
                if(count($tagIds) > 0){
                    $this->attachTags($video, $tagIds);
                }
            }

            // image patch
           $newDescription = str_replace('/videos/'.$request->tempID, env('AWS_URL').'videos/'.$video->id, $request->description);
           $video->description = $newDescription;
           $video->status = 'uploading';
           $video->save();

           // 에디터 초기화 시 이미지 삭제를 피하기 위한 방안
           Helper::filesMove('app/public/videos/'.$request->tempID, 'app/public/videos/'.$video->id);
           Helper::deleteFolder('app/public/videos/'.$request->tempID);

           $this->dispatch(new ImageUploadToS3ByFolder('app/public/videos/'.$video->id, 'videos/'.$video->id));

            // Video Upload
            $request->file('video')->move( storage_path() . '/uploads/videos/'. $video->id . '/', $video->video_filename );

            // video upload job work
            UploadVideo::dispatch($video)->onQueue('videoupload');

            // video transcoding check job work
            VimeoVideoTranscodingCheck::dispatch($video)->delay(now()->addMinutes(3));

            // notification
            $this->videoNotification($video, $tutorial, 'vcreate');


            return response()->json([
                'result' => 'success',
                'message' => '비디오 업데이트 완료',
                'videoUID' => $filename,
            ]);
        }else {
            return response()->json([
                'result' => 'error',
                'message' => '비디오 업데이트에 실패하였습니다',
            ]);
        }
    }

    public function videoNotification($video, $tutorial, $type){
        if($video->visibility == 'public'){
            $notice = Notification::where('notificationable_id', $tutorial->id)
            ->where('notificationable_type', 'App\Models\Tutorial')
            ->where('option', $tutorial->id)
            ->where('type', $type)->first();

            if(!$notice){
                if($type == 'vcreate'){
                    $message = '새로운 비디오 "'.$video->title.'"가 추가되었습니다';
                } else if($type == 'vupload'){
                    $message = '"'.$video->title.'" 비디오가 변경되었습니다';
                } else if($type == 'update'){
                    $message = '"'.$video->title.'" 컨텐츠가 수정되었습니다';
                }
                $tutorial->notifications()->create([
                    'content' => $message,
                    'type' => $type,
                    'option' => $tutorial->id
                ]);
            }
        }
    }

    public function replace(Request $request, Video $video){
        $this->authorize('edit', $video);

        $video->filesize = filesize($request->file('video'));
        $video->enable = false;
        $video->status = 'uploading';
        $video->save();

        // Video Upload
        $request->file('video')->move( storage_path() . '/uploads/videos/'. $video->id . '/', $video->video_filename );

        // video upload job work
        ReplaceVideo::dispatch($video)->onQueue('videoupload');

        // video transcoding check job work
        VimeoVideoTranscodingCheck::dispatch($video)->delay(now()->addMinutes(3));

        // notification
        $this->videoNotification($video, $video->tutorial()->first(), 'vupload');

        return response()->json([
            'result' => 'success',
            'message' => '비디오 업데이트 완료',
            'video' => $video,
        ]);

    }

    // Show the Edit form
    public function edit(Video $video) {
        $this->authorize('edit', $video);

        // Tags
        $tagsCollect = $video->tags()->get();//->toArray();
        $tags = $tagsCollect->map(function ($item, $key) {
            return $item['name'];
        })->toArray();

        $channels = Auth::user()->channels()->get();

        return view('app.video.vedit', [
            'video' => $video,
            'channels' => $channels,
            'tags' => $tags
        ]);
    }

    public function addThumbnail(Request $request, Video $video){
        $this->authorize('edit', $video);

        //POSThttps://api.vimeo.com/videos/{video_id}/pictures
        $result = Vimeo::request($video->vimeo_url.'/pictures', ['active' => true, 'time' => $request->time], 'POST');

        $video->thumbnail = $result['body']['sizes'][2]['link'];
        $video->thumbnail_m = $result['body']['sizes'][1]['link'];
        $video->thumbnail_s = $result['body']['sizes'][0]['link'];
        $video->thumbnail_url = $result['body']['uri'];
        $video->save();

        return response()->json($result, 200);
    }


    public function setDefaultVideoTumbnail(Request $request, Video $video){
        $this->authorize('edit', $video);

        //PATCHhttps://api.vimeo.com/videos/{video_id}/pictures/{picture_id}
        $result = Vimeo::request($request->uri, ['active' => true], 'PATCH');

        $newVideo = $this->updateDefaultThumbnailToVideosTable($video, $request->uri);

        return response()->json(['thumbnail' => $newVideo->thumbnail, 'thumbnail_url' => $newVideo->thumbnail_url], 200);
    }

    public function updateDefaultThumbnailToVideosTable($video, $uri){
        //$videoImage = Vimeo::request('/videos/288395630/pictures/723794218', [], 'GET');
        $videoImage = Vimeo::request($uri, [], 'GET');
        $video->thumbnail = $videoImage['body']['sizes'][2]['link'];
        $video->thumbnail_m = $videoImage['body']['sizes'][1]['link'];
        $video->thumbnail_s = $videoImage['body']['sizes'][0]['link'];
        $video->thumbnail_url = $videoImage['body']['uri'];
        $video->save();
        return $video;
    }

    public function delteVideoTumbnail(Request $request, Video $video){
        $this->authorize('edit', $video);

        //https://api.vimeo.com/videos/{video_id}/pictures/{picture_id}
        $result = Vimeo::request($request->uri, [], 'DELETE');
        return response()->json(null, 200);
    }

    public function getVideoTumbnails(Request $request){
        if($request->type == 'all'){
            // '/videos/288395630/pictures'
            $videoImages = Vimeo::request($request->thumbnailUrl, [], 'GET');
            return response()->json([
                'images' => $videoImages['body']['data']
            ]);
        } else if($request->type == 'one'){
            // 'videos/288395630/pictures/723794218'
            $videoImages = Vimeo::request($request->thumbnailUrl, [], 'GET');
            return response()->json([
                'images' => $videoImages['body']
            ]);
        }
    }

    public function contentUpdate(Request $request, Video $video){
        $this->authorize('edit', $video);

        $messages = [
            'title.required' => '비디오 타이틀을 작성해주세요',
            'title.max' => '비디오 타이틀은 최대 255자 이하입니다',
            'channel_id.required' => '채널을 선택해주세요',
            'tutorial_id.required' => '강좌를 선택해주세요',
            'visibility.required' => '비디오 설정을 선택해주세요',
            'preview.required' => '비디오 미리보기 설정을 선택해주세요',
        ];

        $validator =  Validator::make($request->all(), [
            'title' => 'required|max:255',
            'channel_id' => 'required',
            'tutorial_id' => 'required',
            'visibility' => 'required',
            'preview' => 'required',
        ], $messages);

        if ($validator->fails()) {
            return response()->json([
                'result' => 'error',
                'message' => '유효하지 않은 데이터입니다',
                'errors' => $validator->errors(),
            ]);
        }

        $video->title = $request->title;
        $video->description = $request->description;
        $video->channel_id = $request->channel_id;
        $video->tutorial_id = $request->tutorial_id;
        $video->visibility = $request->visibility;
        $video->preview = $request->preview;
        $video->order = $request->order;
        $video->save();

        // vimeo 정보 업데이트
        Vimeo::request($video->vimeo_url,
                ['name' => $video->title, 'description' => $video->description], 'PATCH');

        // Add a video to album
        $tutorial = $video->tutorial()->first();
        Vimeo::request($tutorial->vimeo_url.$video->vimeo_url, [], 'PUT');

        // notification
        $this->videoNotification($video, $tutorial, 'update');

        return response()->json([
            'result' => 'success',
            'message' => '비디오 정보 업데이트를 완료하였습니다'
        ]);

    }

    // editor save function
    public function updateDescription(Request $request, Video $video){
        $this->authorize('edit', $video);

        $video->description = $request->body;
        $video->save();
        return response()->json(null, 200);
    }

    public function delete(Request $request, Video $video){
        $this->authorize('delete', $video);

        $video->delete();

        // delete Vimeo
        // DELETE https://api.vimeo.com/videos/{video_id}
        Vimeo::request($video->vimeo_url, [], 'DELETE');

        // return message
        return response()->json(null, 200);
    }


    public function createTags($data)
    {
        // make array
        if(!is_array($data)){
            $tags = explode(',', $data);
        } else {
            $tags = $data;
        }

        $tagIds = [];
        for ($i = 0; $i < count($tags); $i ++)
        {
            if (! $tag = Tag::firstOrCreate(['name' => $tags[$i]]))
                throw new FailedTagCreateException;
            $tagIds[] = $tag->id;
        }
        return $tagIds;
    }

    public function attachTags($video, $tagIds){
        $video->tags()->sync($tagIds);
    }
}
