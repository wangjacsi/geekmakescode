<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\UserResource;
use App\Models\User;
use Auth;
use App\Http\Resources\ChannelResource;
use App\Http\Resources\TutorialResource;
use App\Http\Helper;
use App\Models\Channel;
use App\Models\Message;
use App\Models\Tutorial;
use Illuminate\Support\Facades\Validator;
use Image;
use Storage;
use App\Jobs\ImageUploadToS3;
use File;
use App\Jobs\ImageDeleteInS3;
use Illuminate\Support\Facades\Hash;

class UserHomeController extends Controller
{
    public function messagebox(Request $request){
        $user = $request->user();
        $myChannelCnt = $user->channelCount();
        if($myChannelCnt){
            $myChTotalMessagesCnt = $user->messagesCountByUserChIds();
            $myNewChMessageCnt = $user->messagesNewCountByUserChIds();
        } else {
            $myChTotalMessagesCnt = 0;
            $myNewChMessageCnt = 0;
        }

        $myTotalMessagesCnt = $user->messagesCount();
        $myNewMessagesCnt = $user->messagesNewCount();

        return view('app.user.messagebox', [
            'user' => $user,
            'myTotalMessagesCnt' => $myTotalMessagesCnt,
            'myNewMessagesCnt' => $myNewMessagesCnt,
            'myNewChMessageCnt' => $myNewChMessageCnt,
            'myChTotalMessagesCnt' => $myChTotalMessagesCnt,
            'myChannelCnt' => $myChannelCnt,
        ]);
    }

    public function index(Request $request){

        $user = $request->user();

        // 채널관리자
        $channelIds = $user->channelIds();
        $chAdminUsers = User::userIdsByChIdsNotIncludeMe($channelIds, $user->id)->get()->toArray();

        // 내가 퐐로우 하는 사람
        $followUsers = $user->followships()->with(['followUser', 'followUser.channels'])->get()->toArray();

        $myNewMessageCnt = $user->messagesNewCount();
        $myChNewMessageCnt = $user->messagesNewCountByUserChIds($channelIds);
        $totalNewMessages = $myNewMessageCnt + $myChNewMessageCnt;

        $myChannelCnt = $user->channelCount();
        $mySubscribedChannelCnt = $user->subscribedChannelsCnt();
        $myRegisteredTutorialCnt = $user->registeredTutorialsCnt();
        $myInterestTutorialCnt = $user->interestTutorialsCnt();

        if($myInterestTutorialCnt > 0){
            $userInterestCategories = $user->interests()->get()->unique('category');
        } else {
            $userInterestCategories = null;
        }


        return view('app.user.mywave', [
            'user' => $user,
            'backgroundColors' => Helper::$backgroundColors,
            'progressStatus' => Helper::getProgressStatusKorean(),
            'chAdminUsers' =>$chAdminUsers,
            'followUsers' =>$followUsers,
            'totalNewMessages' =>$totalNewMessages,
            'myChannelCnt' =>$myChannelCnt,
            'mySubscribedChannelCnt' => $mySubscribedChannelCnt,
            'myRegisteredTutorialCnt' => $myRegisteredTutorialCnt,
            'myInterestTutorialCnt' => $myInterestTutorialCnt,
            'myInterestCategories' => $userInterestCategories
        ]);
    }

    public function show(User $user, Request $request){
        if(Auth::check()){
            $owner = $user->id == $request->user()->id;
        } else {
            $owner = false;
        }
        $options =  ($user->options);

        return view('app.user.wave', [
            'user' => $user,
            'owner' => $owner,
            'options' => $options,
            'backgroundColors' => Helper::$backgroundColors,
            'progressStatus' => Helper::getProgressStatusKorean(),
            'techBrands' => Helper::$techBrands,
            'snsBrands' => Helper::$snsBrands,
        ]);
    }

    public function settingsUpdatge(Request $request, User $user){
        $this->authorize('edit', $user);
        $userJson = json_decode($user->toJson());
        $options = json_decode($userJson->options, true);

        foreach ($request->all() as $key => $value) {
            $options['viewoptions'][$key] = $value;
        }
        $user->options = json_encode($options);
        $user->save();
        return response()->json(['result' => 'success'], 200);
    }

    public function passwordUpdatge(Request $request, User $user){
        $this->authorize('edit', $user);
        $user->password = Hash::make($request->password);
        $user->save();
        return response()->json(['result' => 'success'], 200);
    }

    public function contact(Request $request, User $user){
        $this->authorize('edit', $user);

        $userJson = json_decode($user->toJson());
        $options = json_decode($userJson->options, true);

        $options['phone'] = $request->phone;
        $options['site'] = $request->site;
        $user->options = json_encode($options);
        $user->save();
        return response()->json(['phone' => $request->phone, 'site' => $request->site], 200);
    }

    public function profileEdit(Request $request, User $user){
        $this->authorize('edit', $user);

        // 에러 발생 시키기
        $validator =  Validator::make($request->all(), [
            'aboutme' => 'max:500',
            'name' => 'required|string|min:2|max:20|unique:users,name,'.$user->id
        ], ['aboutme.max' => '자기소개글은 500자 이내로 작성해주세요',
            'name.min' => '아이디는 최소 2자 이상입니다',
            'name.max' => '아이디는 최대 20자 이하입니다',
            'name.required' => '아이디를 작성해주세요',
            'name.unique'=>'이미 사용중인 아이디입니다'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'result' => 'error',
                'message' => '유효하지 않은 데이터입니다',
                'errors' => $validator->errors(),
            ]);
        }

        $user->name = $request->name;
        $user->slug = str_slug($request->name, '-');
        $user->status_message = $request->status_message;
        $user->aboutme = $request->aboutme;
        $user->job = $request->job;

        // image request check
        if ($request->has('head_image')) {
            $headImage = $request->file('head_image');
            $filenameArray = Helper::makeImageFilenameWithExt($headImage);
            $filename = $filenameArray[0];
            $ext = $filenameArray[1];
            $headImage->move(storage_path(). '/app/public/profile/'.$user->id.'/',  $filename);
            $originFile = $user->head_image;

            Image::make(storage_path(). '/app/public/profile/'.$user->id.'/' .$filename)->fit(1920,358, function($c) {
                $c->upsize();
            })->save()->destroy();

            $user->head_image = '/profile/'.$user->id.'/' .$filename;

            $this->dispatch(new ImageUploadToS3(storage_path(). '/app/public/profile/'.$user->id.'/'. $filename,
                'profile/'.$user->id.'/'.$filename,
                $user,
                'head_image',
                'app/public/profile/'.$user->id.'/'.$filename,
                'app/public/profile/'.$user->id
            ));

            // delete S3
            if($originFile != ''){
                $this->dispatch(new ImageDeleteInS3($originFile));
            }
        }

        if ($request->has('photo')) {
            $photoImage = $request->file('photo');
            $filenameArray = Helper::makeImageFilenameWithExt($photoImage);
            $filename = $filenameArray[0];
            $ext = $filenameArray[1];
            $filenameS = Helper::makeImageFilenameS($filename);
            $photoImage->move(storage_path(). '/app/public/profile/'.$user->id.'/',  $filename);
            $originFile = $user->photo;

            Image::make(storage_path(). '/app/public/profile/'.$user->id.'/'. $filename)->fit(120,120, function($c) {
                $c->upsize();
            })->save()->destroy();

            Image::make(storage_path(). '/app/public/profile/'.$user->id.'/'. $filename)->fit(60,60, function($c) {
                $c->upsize();
            })->save(storage_path(). '/app/public/profile/'.$user->id.'/'. $filenameS)->destroy();

            $user->photo = '/profile/'.$user->id.'/' .$filename;


            $this->dispatch(new ImageUploadToS3(storage_path(). '/app/public/profile/'.$user->id.'/'.$filename,
                'profile/'.$user->id.'/'.$filename,
                $user,
                'photo',
                'app/public/profile/'.$user->id.'/'.$filename,
                'app/public/profile/'.$user->id
            ));

            $this->dispatch(new ImageUploadToS3(storage_path(). '/app/public/profile/'.$user->id.'/'.$filenameS,
                'profile/'.$user->id.'/'.$filenameS,
                $user,
                '',
                'app/public/profile/'.$user->id.'/'.$filenameS,
                'app/public/profile/'.$user->id
            ));

            // delete S3
            if($originFile != ''){
                $this->dispatch(new ImageDeleteInS3($originFile));
            }
        }

        $user->save();

        return response()->json([
            'result' => 'success',
            'user' => $user//->slug
        ], 200);
    }

    public function snsStore(Request $request, User $user){
        $userJson = json_decode($user->toJson());
        $options = json_decode($userJson->options, true);

        if( isset( $options['sns'][$request->name] ) ){
            return response()->json([
                'result' => 'error',
                'message' => '이미 해당 SNS가 저장되어 있습니다'
            ]);
        } else {
            $options['sns'][$request->name] = ['name' => $request->name, 'class'=>$request->class, 'link' => $request->link, 'color' => $request->color];
        }
        $user->options = json_encode($options);
        $user->save();

        return response()->json(['name' => $request->name, 'class'=>$request->class, 'link' => $request->link, 'color' => $request->color], 200);
    }

    public function snsDelete(Request $request, User $user){
        $userJson = json_decode($user->toJson());
        $options = json_decode($userJson->options, true);

        if( isset( $options['sns'][$request->name] ) ){
            unset($options['sns'][$request->name]);
            $user->options = json_encode($options);
            $user->save();
            return response()->json(null, 200);
        } else {
            return response()->json([
                'result' => 'error',
                'message' => '삭제 실패'
            ]);
        }
    }

    public function snsEdit(Request $request, User $user){
        $userJson = json_decode($user->toJson());
        $options = json_decode($userJson->options, true);

        if( isset( $options['sns'][$request->name] ) ){
            $options['sns'][$request->name]['link'] = $request->link;
            $user->options = json_encode($options);
            $user->save();
            return response()->json(['name' => $request->name, 'link' => $request->link], 200);
        } else {
            return response()->json([
                'result' => 'error',
                'message' => '수정 실패'
            ]);
        }
    }



    public function techStore(Request $request, User $user){
        $userJson = json_decode($user->toJson());
        $options = json_decode($userJson->options, true);

        if( isset( $options['tech'][$request->name] ) ){
            return response()->json([
                'result' => 'error',
                'message' => '이미 해당 기술이 저장되어 있습니다'
            ]);
        } else {
            $options['tech'][$request->name] = ['name' => $request->name, 'class'=>$request->class, 'level' => $request->level];
        }
        $user->options = json_encode($options);
        $user->save();

        return response()->json(['name' => $request->name, 'class'=>$request->class, 'level' => $request->level], 200);
    }

    public function techDelete(Request $request, User $user){
        $userJson = json_decode($user->toJson());
        $options = json_decode($userJson->options, true);

        if( isset( $options['tech'][$request->name] ) ){
            unset($options['tech'][$request->name]);
            $user->options = json_encode($options);
            $user->save();
            return response()->json(null, 200);
        } else {
            return response()->json([
                'result' => 'error',
                'message' => '삭제 실패'
            ]);
        }
    }

    public function techEdit(Request $request, User $user){
        $userJson = json_decode($user->toJson());
        $options = json_decode($userJson->options, true);

        if( isset( $options['tech'][$request->name] ) ){
            $options['tech'][$request->name]['level'] = $request->level;
            $user->options = json_encode($options);
            $user->save();
            return response()->json(['name' => $request->name, 'level' => $request->level], 200);
        } else {
            return response()->json([
                'result' => 'error',
                'message' => '수정 실패'
            ]);
        }
    }
}
