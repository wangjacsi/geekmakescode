<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Channel;
use App\Jobs\ChannelUploadImage;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\ChannelStoreRequest;
use App\Http\Requests\ChannelUpdteRequest;
use App\Http\Resources\ChannelAllInfoResource;
use App\Http\Resources\UserResource;
use App\Models\User;
use Auth;
use Vimeo\Laravel\Facades\Vimeo;
use App\Jobs\ImageUploadToS3ByFolder;
use Storage;
use App\Http\Helper;
use App\Http\Resources\TutorialResource;
use App\Jobs\ImageDeleteInS3;
use App\Models\Message;
use App\Models\Tutorial;
use Illuminate\Support\Facades\DB;

class ChannelController extends Controller
{
    public $numItemsPerPage = 8;

    // Show the create form
    public function create(){
        return view('app.channel.create');
    }

    public function settingsUpdate(Request $request, Channel $channel){
        $this->authorize('edit', $channel);

        $channelJson = json_decode($channel->toJson());
        $settings = json_decode($channelJson->settings, true);

        foreach ($request->all() as $key => $value) {
            $settings['viewoptions'][$key] = $value;
        }
        $channel->settings = json_encode($settings);
        $channel->save();
        return response()->json(['result' => 'success'], 200);
    }

    public function adminAccept(Request $request){
        if(! $request->user || ! $request->channel){
            return;
        }
        $channel = Channel::find($request->channel);
        $user = User::find($request->user);
        if(! $user || !$channel){
            return;
        }

        // message check
        $check = $user->messages->where('title', '=', '채널 관리자 등록 요청')->where('channel_id', '=', $channel->id)->first();
        $isAdmin = $channel->isAdmin($user->id);
        if($check && !$isAdmin){
            $channel->users()->attach($user->id);

            // notification
            $channel->notifications()->create([
                'content' => '채널 관리자가 추가되었습니다',
                'type' => 'adminadd',
                'user_id' => $user->id
            ]);

            return redirect()->back()->with('success', $channel->name . ' 채널 관리자 등록이 완료되었습니다');
        } else {
            return false;
        }
    }

    public function adminRequest(Request $request, Channel $channel){
        $this->authorize('superadmin', $channel);

        $user = User::find($request->id);
        $isAdmin = $channel->isAdmin($request->id);
        if($isAdmin){
            return response()->json(['result' => 'error', 'message' => '관리자 등록된 유저입니다'], 200);
        } else {

            // 메세지 생성
            $message =  new Message;
            $message->user_id = Auth::user()->id;
            $message->channel_id = $channel->id;
            $message->message = '##### "'.$channel->name.'" 채널관리자 요청입니다

아래 링크를 클릭하시면 관리자 등록이 완료됩니다

[관리자등록 완료]('.url('/channel/admin/accept?channel='.$channel->id.'&user='.$user->id).')';
            $message->read = false;
            $message->title = '채널 관리자 등록 요청';
            $message = $user->messages()->save($message);

            //$channel->users()->attach($request->id);
            return response()->json(['result' => 'success', 'message' => '채널 관리자 등록 요청 전송'], 200);
        }
    }

    public function adminDelete(Request $request, Channel $channel){
        $this->authorize('superadmin', $channel);

        $user = User::find($request->userid);

        if( $channel->users()->count() < 2){
            return response()->json(['result' => 'error', 'message' => '채널의 관리자는 최소 최소 한명이상이어야 합니다'], 200);
        }

        $user->channels()->detach($channel->id);

        return response()->json(['result' => 'success', 'userID' => $request->userid], 200);
    }

    public function superAdminStore(Request $request, Channel $channel){
        $this->authorize('superadmin', $channel);

        $channelJson = json_decode($channel->toJson());
        $settings = json_decode($channelJson->settings, true);

        if( isset( $settings['admins']['super'] )){
            $key = array_search($request->userid, $settings['admins']['super']);
            if (false == $key) {
                array_push($settings['admins']['super'], $request->userid);
                $channel->settings = json_encode($settings);
                $channel->save();
            }
            return response()->json(['result' => 'success', 'userID' => $request->userid], 200);
        } else {
            return response()->json(['result' => 'error', 'message' => '시스템에 에러가 발생하였습니다. 관리자에게 문의하세요.'], 200);
        }
    }

    public function superAdminDelete(Request $request, Channel $channel){
        $this->authorize('superadmin', $channel);

        $channelJson = json_decode($channel->toJson());
        $settings = json_decode($channelJson->settings, true);

        if( isset( $settings['admins']['super'] ) &&  count($settings['admins']['super']) > 1){
            $key = array_search($request->userid, $settings['admins']['super']);
            if (false !== $key) {
                unset($settings['admins']['super'][$key]);
            }
            $channel->settings = json_encode($settings);
            $channel->save();
            return response()->json(['result' => 'success', 'userID' => $request->userid], 200);
        } else {
            return response()->json(['result' => 'error', 'message' => '채널의 슈퍼관리자는 최소 한명이상이어야 합니다'], 200);
        }
    }

    public function snsDelete(Request $request, Channel $channel){
        $channelJson = json_decode($channel->toJson());
        $settings = json_decode($channelJson->settings, true);

        if( isset( $settings['sns'][$request->name] ) ){
            unset($settings['sns'][$request->name]);
            $channel->settings = json_encode($settings);
            $channel->save();
            return response()->json(null, 200);
        } else {
            return response()->json([
                'result' => 'error',
                'message' => '삭제 실패'
            ]);
        }
    }

    public function snsEdit(Request $request, Channel $channel){
        $channelJson = json_decode($channel->toJson());
        $settings = json_decode($channelJson->settings, true);

        if( isset( $settings['sns'][$request->name] ) ){
            $settings['sns'][$request->name]['link'] = $request->link;
            $channel->settings = json_encode($settings);
            $channel->save();
            return response()->json(['name' => $request->name, 'link' => $request->link], 200);
        } else {
            return response()->json([
                'result' => 'error',
                'message' => '수정 실패'
            ]);
        }
    }


    public function snsStore(Request $request, Channel $channel){
        $channelJson = json_decode($channel->toJson());
        $settings = json_decode($channelJson->settings, true);

        if( isset( $settings['sns'][$request->name] ) ){
            return response()->json([
                'result' => 'error',
                'message' => '이미 해당 SNS가 저장되어 있습니다'
            ]);
        } else {
            $settings['sns'][$request->name] = ['name' => $request->name, 'class'=>$request->class, 'link' => $request->link, 'color' => $request->color];
        }
        $channel->settings = json_encode($settings);
        $channel->save();

        return response()->json(['name' => $request->name, 'class'=>$request->class, 'link' => $request->link, 'color' => $request->color], 200);
    }

    public function contact(Request $request, Channel $channel){
        $this->authorize('edit', $channel);

        $channelJson = json_decode($channel->toJson());
        $settings = json_decode($channelJson->settings, true);

        $settings['phone'] = $request->phone;
        $settings['site'] = $request->site;
        $settings['email'] = $request->email;
        $channel->settings = json_encode($settings);
        $channel->save();
        return response()->json(['phone' => $request->phone, 'site' => $request->site, 'email' => $request->email], 200);
    }

    public function show(Channel $channel){
        // check the owner
        if(Auth::user()){
            $owner = $channel->isAdmin(Auth::user()->id);
        } else {
            $owner = false;
        }


        return view('app.channel.show',
            ['owner' => $owner,
            'channel' => $channel,
            'totalTutorials' => $channel->totalTutorials(),
            'subscriptionCount' => $channel->subscriptionCount(),
            'totalRegisterdTutorials' => $channel->totalRegisterdTutorials(),
            'isSubscribed' => $channel->isSubscribed(),
            'progressStatus' => Helper::getProgressStatusKorean(),
            'backgroundColors' => Helper::$backgroundColors
        ]);
    }

    public function updateProfile(Request $request, Channel $channel){
        $this->authorize('edit', $channel);

        // 에러 발생 시키기
        $validator =  Validator::make($request->all(), [
            'status_message' => 'max:255',
            'name' => 'required|min:2|max:20|unique:channels,name,'.$channel->id
        ], ['status_message.max' => '채널상단 메세지는 255자 이내로 작성해주세요',
            'name.required' => '채널 이름을 작성해주세요',
            'name.min' => '채널 이름은 최소 2자 이상입니다',
            'name.max' => '채널 이름은 최대 255자 이하입니다',
            'name.unique' => '이미 사용중인 채널 이름입니다',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'result' => 'error',
                'message' => '유효하지 않은 데이터입니다',
                'errors' => $validator->errors(),
            ]);
        }

        $channel->name = $request->name;
        $channel->slug = str_slug($request->name, '-');
        $channel->status_message = $request->status_message;
        $channel->description = $request->description;
        $channel->save();
        $originHeadFile = $channel->head_image;
        $originLogoFile = $channel->logo_image;

        // image request check
        if ($request->has('head_image')) {
            $headImage = $request->file('head_image');
            $filename = $this->makeImageFilename($headImage);
            $headImage->move(storage_path(). '/uploads/channels/',  $filename);
            $this->dispatch(new ChannelUploadImage($channel, $filename, 'channel-head'));
            // delete S3
            if($originHeadFile != ''){
                $this->dispatch(new ImageDeleteInS3($originHeadFile));
            }
        }

        if ($request->has('logo_image')) {
            $logoImage = $request->file('logo_image');
            $filename2 = $this->makeImageFilename($logoImage);
            $logoImage->move(storage_path(). '/uploads/channels/',  $filename2);
            $this->dispatch(new ChannelUploadImage($channel, $filename2, 'channel-logo'));
            // delete S3
            if($originLogoFile != ''){
                $this->dispatch(new ImageDeleteInS3($originLogoFile));
            }
        }

        // vimeo update
        $data = Vimeo::connection('main')->request($channel->vimeo_url,
            ['description' => $channel->description,
            'name' => $channel->name],
            'PATCH');

        return response()->json([
            'result' => 'success',
            'message' => '채널 수정 완료',
            'channel' => $channel,
        ]);
    }

    protected function validator(array $data)
    {
        $messages = [
            'name.required' => '채널 이름을 작성해주세요',
            'name.min' => '채널 이름은 최소 2자 이상입니다',
            'name.max' => '채널 이름은 최대 255자 이하입니다',
            'name.unique' => '이미 사용중인 채널 이름입니다',
            'description.required'  => '채널 소개글을 작성해 주세요',
        ];

        $this->validator =  Validator::make($data, [
            'name' => 'required|min:2|max:255|unique:channels,name',
            'description' => 'required',
        ], $messages);
    }

    public function store(Request $request){
        // validate
        $this->validator($request->all());

        if ($this->validator->fails()) {
            return response()->json([
                'result' => 'error',
                'message' => '유효하지 않은 데이터입니다',
                'errors' => $this->validator->errors(),
            ]);
        }

        // store information
        $channel =  Channel::create([
            'name' => $request->name,
            'slug' => str_slug($request->name, '-'),//, 'ko'),
            'description' => $request->description,
            'creator' => Auth::user()->id,
            'settings' => collect(['viewoptions' =>
                                        ['sns' => true,
                                        'tech' => true,
                                        'contacts' => true,
                                        'messageForm' => true],
                                    'admins' => [
                                        'super' => [Auth::user()->id] // 모든 것을 관리 가능
                                        //'tutorial' => [],
                                        //'message' => []
                                    ],
                                    'sns' => new \ArrayObject(),
                                    'tech' => new \ArrayObject(),
                                    'email' => '',
                                    'phone' => '',
                                    'site' => ''
                                    ])->toJson()
        ]);

        // channel users 테이블
        Auth::user()->channels()->save($channel);

        // image request check
        if ($request->has('headImage')) {
            $headImage = $request->file('headImage');
            $filename = $this->makeImageFilename($headImage);
            $headImage->move(storage_path(). '/uploads/channels/',  $filename);
            $this->dispatch(new ChannelUploadImage($channel, $filename, 'channel-head'));
        }

        if ($request->has('logoImage')) {
            $logoImage = $request->file('logoImage');
            $filename2 = $this->makeImageFilename($logoImage);
            $logoImage->move(storage_path(). '/uploads/channels/',  $filename2);
            $this->dispatch(new ChannelUploadImage($channel, $filename2, 'channel-logo'));
        }

        if($channel){
            // image path change
            $newDescription = str_replace('/channels/'.$request->tempID, env('AWS_URL').'channels/'.$channel->id, $request->description);
            $channel->description = $newDescription;
            $channel->save();

            $this->dispatch(new ImageUploadToS3ByFolder('app/public/channels/'.$request->tempID, 'channels/'.$channel->id));

            // vimeo channel create
            //https://api.vimeo.com/channels
            $data = Vimeo::connection('main')->request('/channels',
                ['description' => $channel->description,
                'link' => '',
                'name' => $channel->name,
                'privacy' => 'moderators'],
                'POST');

            if($data['status'] == 200) {
                $channel->vimeo_url = $data['body']['uri'];
                $channel->save();
            }


            return response()->json([
                'result' => 'success',
                'message' => '채널 생성 완료',
                'slug' => $channel->slug,
            ]);
        } else {
            return response()->json([
                'result' => 'error',
                'message' => '채널 생성 실패하였습니다'
            ]);
        }
    }

    public function makeImageFilename($imageFile){
        $mime = explode('/', $imageFile->getClientMimeType() );
        $ext = strtolower( end($mime));
        $fileId = uniqid(true). '.' .$ext;
        return $fileId;
    }

    public function index(){

        return view('app.channel.index');
    }

    public function edit(Request $request, Channel $channel){
        // authorize check
        $this->authorize('edit', $channel);

        $channelJson = json_decode($channel->toJson());
        $settings = json_decode($channelJson->settings, true);

        $super = false;
        if( isset( $settings['admins']['super'] ) ){
            $super = in_array($request->user()->id, $settings['admins']['super']);
        }

        return view('app.channel.edit', [
            'user' => $request->user(),
            'super' => $super,
            'channel' => $channel,
            'totalTutorials' => $channel->totalTutorials(),
            'subscriptionCount' => $channel->subscriptionCount(),
            'totalRegisterdTutorials' => $channel->totalRegisterdTutorials(),
            'backgroundColors' => Helper::$backgroundColors,
            'progressStatus' => Helper::getProgressStatusKorean(),
            'snsBrands' => Helper::$snsBrands
        ]);
    }

    public function updateDescription(Request $request, Channel $channel){
        $this->authorize('edit', $channel);

        $channel->description = $request->body;
        $channel->save();
        return response()->json(null, 200);
    }


    // API response
    // Get channel admin users info by channel
    public function getChannelAdminUsers(Request $request, Channel $channel){
        $with = ['channels'];
        if($request->without){
            if (in_array($request->without, $with))
            {
                unset($with[array_search($request->without,$with)]);
            }
        }
        return $users = UserResource::collection(
            $channel->users()->with($with)->latestFirst()->paginate($this->numItemsPerPage)
        );
    }

    // Channel Info By Login User
    public function getChannelsInfoByLoginUser(Request $request){
        $with = ['users', 'tutorials'];
        if($request->without){
            if (in_array($request->without, $with))
            {
                unset($with[array_search($request->without,$with)]);
            }
        }
        return $channels = ChannelAllInfoResource::collection(
            $request->user()->channels()->with($with)->latestFirst()->paginate($this->numItemsPerPage-1)
        );
    }

    // Channel Info By User Id
    public function getChannelsInfoByUserId(Request $request, User $user){
        $with = ['users', 'tutorials'];
        if($request->without){
            if (in_array($request->without, $with))
            {
                unset($with[array_search($request->without,$with)]);
            }
        }
        return $channels = ChannelAllInfoResource::collection(
            $user->channels()->with($with)->latestFirst()->paginate($this->numItemsPerPage-1)
        );
    }

    // Subscribed Channels Info By Login User
    public function getSubscribedChannelsInfoByLoginUser(Request $request){
        $with = ['users', 'tutorials'];
        if($request->without){
            if (in_array($request->without, $with))
            {
                unset($with[array_search($request->without,$with)]);
            }
        }
        return $subscribedChannels = ChannelAllInfoResource::collection(
            $request->user()->subscribedChannels()->with($with)->latestFirst()->paginate($this->numItemsPerPage)
        );
    }

    // Subscribed Channels Info By user id
    public function getSubscribedChannelsInfoByUserId(Request $request, User $user){
        $with = ['users', 'tutorials'];
        if($request->without){
            if (in_array($request->without, $with))
            {
                unset($with[array_search($request->without,$with)]);
            }
        }
        return $subscribedChannels = ChannelAllInfoResource::collection(
            $user->subscribedChannels()->with($with)->latestFirst()->paginate($this->numItemsPerPage)
        );
    }

    public function getChannelsInfo(Request $request){
        $subTutorials = DB::table('tutorials')
                    ->select('channel_id','channel_id as channels.id', 'tutorials.id', DB::raw('count(registrations.id) as registrations_count'))
                    ->leftJoin('registrations', 'registrations.tutorial_id', '=', 'tutorials.id')
                    ->leftJoin('reviews', 'tutorials.id', '=', 'reviews.tutorial_id')
                    ->addSelect(array(
                        DB::raw('AVG(review) as review')
                    ))
                    ->where('visibility', '=', 'public')
                    ->groupBy('tutorials.id');


        if(!$request->has('order') || $request->order == 'name'){

            $channels = Channel::with('users')->leftJoin('subscriptions', 'channels.id', '=', 'subscriptions.channel_id')
                    ->leftJoinSub($subTutorials, 'sub_tutorials', function($join) {
                        $join->on('channels.id', '=', 'sub_tutorials.channel_id');
                    })
                    ->select('channels.*', DB::raw('AVG(review) as review'), DB::raw('sum(registrations_count) as totalRegisterdTutorials'), DB::raw('count(subscriptions.id) as totalSubscriptions'),
                    DB::raw('count(sub_tutorials.channel_id) as totalTutorials'))
                    ->groupBy('channels.id')
                    ->orderBy('channels.name', 'ASC')
                    ->paginate(15);
        } else if($request->order == 'subscription'){

            $channels = Channel::with('users')->leftJoin('subscriptions', 'channels.id', '=', 'subscriptions.channel_id')
                    ->leftJoinSub($subTutorials, 'sub_tutorials', function($join) {
                        $join->on('channels.id', '=', 'sub_tutorials.channel_id');
                    })
                    ->select('channels.*', DB::raw('AVG(review) as review'), DB::raw('sum(registrations_count) as totalRegisterdTutorials'), DB::raw('count(subscriptions.id) as totalSubscriptions'),
                    DB::raw('count(sub_tutorials.channel_id) as totalTutorials'))
                    ->groupBy('channels.id')
                    ->orderBy('totalSubscriptions', 'DESC')
                    ->paginate(15);
        } else if($request->order == 'review'){

            $channels = Channel::with('users')->leftJoin('subscriptions', 'channels.id', '=', 'subscriptions.channel_id')
                    ->leftJoinSub($subTutorials, 'sub_tutorials', function($join) {
                        $join->on('channels.id', '=', 'sub_tutorials.channel_id');
                    })
                    ->select('channels.*', DB::raw('AVG(review) as review'), DB::raw('sum(registrations_count) as totalRegisterdTutorials'), DB::raw('count(subscriptions.id) as totalSubscriptions'),
                    DB::raw('count(sub_tutorials.channel_id) as totalTutorials'))
                    ->groupBy('channels.id')
                    ->orderBy('review', 'DESC')
                    ->paginate(15);
        } else if($request->order == 'registration'){
            $channels = Channel::with('users')->leftJoin('subscriptions', 'channels.id', '=', 'subscriptions.channel_id')
                    ->leftJoinSub($subTutorials, 'sub_tutorials', function($join) {
                        $join->on('channels.id', '=', 'sub_tutorials.channel_id');
                    })
                    ->select('channels.*', DB::raw('AVG(review) as review'), DB::raw('sum(registrations_count) as totalRegisterdTutorials'), DB::raw('count(subscriptions.id) as totalSubscriptions'),
                    DB::raw('count(sub_tutorials.channel_id) as totalTutorials'))
                    ->groupBy('channels.id')
                    ->orderBy('totalRegisterdTutorials', 'DESC')
                    ->paginate(15);
        }


        return response()->json($channels, 200);

    }
}
