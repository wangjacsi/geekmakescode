<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\NotificationCheck;
use Illuminate\Support\Carbon;

class NotificationController extends Controller
{
    public $numItemsPerPage = 10;

    public function notifCheckUpdate(Request $request){
        if(!$request->type)
            return;

        if($request->type == 'myMessage' ){
            NotificationCheck::updateOrCreate(
                ['user_id' => $request->user()->id,
                'type' => 'message'],
                ['checked_at'=> Carbon::now()]
            );
        } else if($request->type == 'myChmessages'){
            NotificationCheck::updateOrCreate(
                ['user_id' => $request->user()->id,
                'type' => 'message-channel'],
                ['checked_at'=> Carbon::now()]
            );
        } else if($request->type == 'myChNotices'){
            NotificationCheck::updateOrCreate(
                ['user_id' => $request->user()->id,
                'type' => 'mychannel-notice'],
                ['checked_at'=> Carbon::now()]
            );
        } else if($request->type == 'myTutoNotices'){
            NotificationCheck::updateOrCreate(
                ['user_id' => $request->user()->id,
                'type' => 'mytutorial-notice'],
                ['checked_at'=> Carbon::now()]
            );
        } else if($request->type == 'regiTutoNotices'){
            NotificationCheck::updateOrCreate(
                ['user_id' => $request->user()->id,
                'type' => 'regitutorial-notice'],
                ['checked_at'=> Carbon::now()]
            );
        } else if($request->type == 'subsChNotices'){
            NotificationCheck::updateOrCreate(
                ['user_id' => $request->user()->id,
                'type' => 'subschannel-notice'],
                ['checked_at'=> Carbon::now()]
            );
        }
        return response()->json(null, 200);
    }

    public function getNotifByUserPart(Request $request){
        if(!$request->type && !$request->page)
            return;

        $user = $request->user();
        if($request->type == 'myMessage' ){
            // 개인 메세지
            $myMessage = $user->newNotifyMessage()->with(['channel', 'user'])->latestFirst()->paginate($this->numItemsPerPage);
            $returnData = $myMessage;
        } else if($request->type == 'myChmessages'){
            // 운영 채널 메세지
            $myChmessages = $user->newNotifyMessageByUserChannel($channelIds)->with(['channel', 'user'])->latestFirst()->paginate($this->numItemsPerPage);
            $returnData = $myChmessages;
        } else if($request->type == 'myChNotices' ){
            // ChIds
            $channelIds = $user->channelIds();
            // 운영 채널 알림
            $myChNotices = $user->newNotifyMyChannels($channelIds)->with(['user', 'channel'])->latestFirst()->paginate($this->numItemsPerPage);
            $returnData = $myChNotices;
        } else if($request->type == 'myTutoNotices' ){
            // 운영 중인 강좌
            $myTutoNotices = $user->newNotifyTutorials()->with(['user', 'tutorial'])->latestFirst()->paginate($this->numItemsPerPage);
            $returnData = $myTutoNotices;
        } else if($request->type == 'regiTutoNotices'){
            $ids = $user->registeredTutorialIds();
            // 수강 중인 강좌 알림
            $regiTutoNotices = $user->newNotifyRegiTutorials($ids)->with(['user', 'tutorial'])->adminCheck()->latestFirst()->paginate($this->numItemsPerPage);
            $returnData = $regiTutoNotices;
        } else if($request->type == 'subsChNotices'){
            $ids = $user->subscribedChannelsIds();
            // 구독 채널 알림
            $subsChNotices = $user->newNotifySubsChannels($ids)->with(['user', 'channel'])->adminCheck()->latestFirst()->paginate($this->numItemsPerPage);
            $returnData = $subsChNotices;
        }
        return response()->json($returnData, 200);
    }

    public function getNotifByUserAll(Request $request){
        $user = $request->user();
        $returnData = array();
        // 개인 메세지
        $myMessage = $user->newNotifyMessage()->with(['channel', 'user'])->latestFirst()->paginate($this->numItemsPerPage);
        $returnData['myMessage'] = $myMessage;
        if($request->myChannels ){
            // ChIds
            $channelIds = $user->channelIds();
            // 운영 채널 메세지
            $myChmessages = $user->newNotifyMessageByUserChannel($channelIds)->with('channel')->latestFirst()->paginate($this->numItemsPerPage);
            // 운영 채널 알림
            $myChNotices = $user->newNotifyMyChannels($channelIds)->with(['user', 'channel'])->latestFirst()->paginate($this->numItemsPerPage);
            // 운영 중인 강좌
            $myTutoNotices = $user->newNotifyTutorials()->with(['user', 'tutorial'])->latestFirst()->paginate($this->numItemsPerPage);
            // 강좌 비디오 알림
            //$myVideoNotices = $user->newNotifyVideos($channelIds)->paginate($this->numItemsPerPage);
            $returnData['myChmessages'] = $myChmessages;
            $returnData['myChNotices'] = $myChNotices;
            $returnData['myTutoNotices'] = $myTutoNotices;
        }
        if($request->myRegisteredTutorials ){
            $ids = $user->registeredTutorialIds();
            // 수강 중인 강좌 알림
            $regiTutoNotices = $user->newNotifyRegiTutorials($ids)->with(['user', 'tutorial'])->adminCheck()->latestFirst()->paginate($this->numItemsPerPage);
            $returnData['regiTutoNotices'] = $regiTutoNotices;
        }
        if($request->mySubscribedChannels ){
            $ids = $user->subscribedChannelsIds();
            // 구독 채널 알림
            $subsChNotices = $user->newNotifySubsChannels($ids)->with(['user', 'channel'])->adminCheck()->latestFirst()->paginate($this->numItemsPerPage);
            $returnData['subsChNotices'] = $subsChNotices;
        }

        return response()->json($returnData, 200);

    }

}
