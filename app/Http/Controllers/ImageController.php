<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Image;
use Storage;
use App\Jobs\ImageUploadToS3;
use App\Models\User;
use App\Http\Helper;
use File;
use App\Jobs\ImageDeleteInS3;

class ImageController extends Controller
{
    public function uploadToTemp(Request $request){
        if ($request->has('imageFile')) {
            $image = $request->file('imageFile');
            $filenameArray = Helper::makeImageFilenameWithExt($image);
            $filename = $filenameArray[0];
            $ext = $filenameArray[1];

            list($width, $height) = getimagesize($image);
            $errorMessage = [];
            if($request->has('limitWidth') ){
                if($width > $request->limitWidth){
                    $errorMessage['width'] = '이미지 폭이 '.$request->limitWidth.'픽셀을 초과하였습니다';
                }
            }

            if( $request->has('limitHeight')){
                if($height > $request->limitHeight){
                    $errorMessage['height'] = '이미지 높이가 '.$request->limitHeight.'픽셀을 초과하였습니다';
                }
            }

            if( $request->has('fileSize')){
                $filesize = filesize($image);
                $filesize = round($filesize / 1024 / 1024, 1); //mega bytes
                if($filesize > $request->fileSize){
                    $errorMessage['size'] = '파일 사이즈가 '.$request->fileSize.'Mbytes를 초과하였습니다';
                }
            }

            if(count($errorMessage) > 0){
                return response()->json(['message' => $errorMessage], 500);
            }


            $image->move(storage_path(). '/app/public/'.$request->type.'/'.$request->id.'/',  $filename);
            $fullFilePathName = storage_path(). '/app/public/'.$request->type.'/'.$request->id.'/' .$filename;
            $urlPath = '/'.$request->type.'/'.$request->id.'/' .$filename;

            if($request->mode == 'fit'){
                if($request->has('height')){
                    $height = $request->height;
                } else {
                    $height = $request->width;// 1:1 ratio
                }
                $processImage = Image::make($fullFilePathName);
                $processImage->fit($request->width, $height, function($c) {
                    $c->upsize();
                })->save();
                $processImage->destroy();
            } else if($request->mode == 'resize'){
                if($request->has('height')){
                    $height = $request->height;
                } else {
                    $height = null;
                }
                if($request->has('width')){
                    $width = $request->width;
                } else {
                    $width = null;
                }

                $processImage = Image::make($fullFilePathName);
                $processImage->resize($width, $height, function($c) {
                    $c->aspectRatio();
                    $c->upsize();
                })->save();
                $processImage->destroy();

            }

            return response()->json(['link' =>  $urlPath], 200);
        } else {
            return response()->json(null, 200);
        }
    }

    public function uploadToS3(Request $request){
        if ($request->has('imageFile')) {
            $image = $request->file('imageFile');
            $filenameArray = Helper::makeImageFilenameWithExt($image);
            $filename = $filenameArray[0];
            $ext = $filenameArray[1];

            list($width, $height) = getimagesize($image);
            $errorMessage = [];
            if($request->has('limitWidth') ){
                if($width > $request->limitWidth){
                    $errorMessage['width'] = '이미지 폭이 '.$request->limitWidth.'픽셀을 초과하였습니다';
                }
            }

            if( $request->has('limitHeight')){
                if($height > $request->limitHeight){
                    $errorMessage['height'] = '이미지 높이가 '.$request->limitHeight.'픽셀을 초과하였습니다';
                }
            }

            if( $request->has('fileSize')){
                $filesize = filesize($image);
                $filesize = round($filesize / 1024 / 1024, 1); //mega bytes
                if($filesize > $request->fileSize){
                    $errorMessage['size'] = '파일 사이즈가 '.$request->fileSize.'Mbytes를 초과하였습니다';
                }
            }

            if(count($errorMessage) > 0){
                return response()->json(['message' => $errorMessage], 500);
            }

            $image->move(storage_path(). '/app/public/'.$request->type.'/'.$request->id.'/',  $filename);
            $fullFilePathName = storage_path(). '/app/public/'.$request->type.'/'.$request->id.'/' .$filename;
            $S3UrlPath = $request->type.'/'.$request->id.'/' .$filename;

            if($request->mode == 'fit'){
                if($request->has('height')){
                    $height = $request->height;
                } else {
                    $height = $request->width;// 1:1 ratio
                }
                $processImage = Image::make($fullFilePathName);
                $processImage->fit($request->width, $height, function($c) {
                    $c->upsize();
                })->save();
                $processImage->destroy();
            } else if($request->mode == 'resize'){
                if($request->has('height')){
                    $height = $request->height;
                } else {
                    $height = null;
                }
                if($request->has('width')){
                    $width = $request->width;
                } else {
                    $width = null;
                }
                $processImage = Image::make($fullFilePathName);
                $processImage->resize($width, $height, function($c) {
                    $c->aspectRatio();
                    $c->upsize();
                })->save();
                $processImage->destroy();
            }

            // upload to s3
            if( Storage::disk('s3images')->put($S3UrlPath, fopen($fullFilePathName, 'r+') ) ){
                return response()->json(['link' =>  env('AWS_URL'). $S3UrlPath], 200);
            } else {
                return response()->json(null, 422);
            }
        } else {
            return response()->json(null, 422);
        }
    }

    public function deleteImage(Request $request){
        $type = '';
        if($request->has('src')){
            $src = $this->replaceS3URL($request->src);
            if($request->has('type')){
                $type = $request->type;
            }
            if($type == ''){
                $this->deleteImageOnS3($src);
                $this->deleteImageOnLocal($src);
            } else {
                if($type == 'local'){
                    $this->deleteImageOnLocal($src);
                } else if($type == 's3'){
                    $this->deleteImageOnS3($src);
                }
            }
            return response()->json(null, 200);
        } else {
            return response()->json(null, 422);
        }
    }

    public function replaceS3URL($filename){
        $replaced = str_replace(env('AWS_URL'), "", $filename);
        return $replaced;
    }

    public function deleteImageOnS3($fileName){
        $exists = Storage::disk('s3images')->exists($fileName);
        if($exists){
            Storage::disk('s3images')->delete($fileName);
        }
    }

    public function deleteImageOnLocal($fileName){
        $exists = Storage::disk('local')->exists('app/public' . $fileName);
        if($exists){
            Storage::disk('local')->delete('app/public' .$fileName);
        }
    }
}
