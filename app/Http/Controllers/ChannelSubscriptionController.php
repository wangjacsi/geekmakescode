<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Channel;

class ChannelSubscriptionController extends Controller
{
    public function show(Request $request, Channel $channel){

        $response = [
            'count' => $channel->subscriptionCount(),
            'user_subscribed' => false,
            'can_subscribe' => false,
        ];

        if($request->user()){ // login 유저의 경우
            $response = array_merge($response, [
                'user_subscribed' => $request->user()->isSubscribedTo($channel),
                'can_subscribe' => !$request->user()->ownsChannel($channel),
            ]);
        }

        return response()->json([
            'data' => $response
        ], 200);
    }

    public function create(Request $request, Channel $channel){
        $this->authorize('subscribe', $channel);

        $request->user()->subscriptions()->create([
            'channel_id' => $channel->id
        ]);

        // notification
        $channel->notifications()->create([
            'content' => '새로운 구독 신청이 있습니다',
            'type' => 'subscription',
            'user_id' => $request->user()->id,
        ]);

        return response()->json(null, 200);
    }

    public function delete(Request $request, Channel $channel){
        $this->authorize('unsubscribe', $channel);

        $request->user()->subscriptions()->where('channel_id', $channel->id)->delete();

        return response()->json(null, 200);
    }
}
