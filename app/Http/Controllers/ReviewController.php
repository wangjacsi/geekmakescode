<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tutorial;
use App\Http\Resources\ReviewResource;
use App\Models\Review;
use App\Events\ReviewCreated;

class ReviewController extends Controller
{
    public $numItemsPerPage = 10;

    // get reviews by tutorial id
    public function getReviewsByTutorial(Request $request, Tutorial $tutorial){
        return $reviews = ReviewResource::collection(
            $tutorial->reviews()->with(['user'])->latestFirst()->paginate($this->numItemsPerPage)
            //$tutorial->reviews()->latestFirst()->paginate($this->numItemsPerPage)
        );
    }

    public function reviewAvg(Request $request, Tutorial $tutorial){
        $reviewAvg = $tutorial->reviews->avg('review');
        $reviewAvg = $reviewAvg / 2;

        return response()->json($reviewAvg, 200);
    }

    public function storeReview(Request $request, Tutorial $tutorial){
        $this->authorize('review', $tutorial);

        $review= Review::updateOrCreate(
            ['user_id' => $request->user()->id,
            'tutorial_id' => $tutorial->id],
            ['review' => $request->review,
            'content' => $request->content]
        );
        $review['user'] = $request->user();

        // notification event
        event(new ReviewCreated($tutorial, $request->user(), $review));

        return response()->json($review, 200);
    }

    public function deleteReview(Request $request, Review $review){
        $this->authorize('delete', $review);

        $review->delete();
        //Review::where('id', $request->id)->delete();

        return response()->json(null, 200);
    }
}
