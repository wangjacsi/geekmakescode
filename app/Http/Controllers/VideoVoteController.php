<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Video;
use App\Http\Requests\CreateVoteRequest;
use App\Models\Notification;

class VideoVoteController extends Controller
{

    public function create(Request $request, Video $video){
        $this->authorize('vote', $video);

        $vote = $video->voteFromUser($request->user())->first();

        if(!$vote){
            $video->votes()->create([
                'type' => $request->type,
                'user_id' => $request->user()->id,
            ]);
        } else {
            return response()->json(null, 200);
        }

        // notification
        $notice = Notification::where('notificationable_id', $video->tutorial->id)
        ->where('notificationable_type', 'App\Models\Tutorial')
        ->where('option', $video->id)
        ->where('user_id', $request->user()->id)
        ->where('type', 'vote')->first();

        if(!$notice){
            $video->tutorial->notifications()->create([
                'content' => '비디오 강좌 "'.str_limit($video->title, 225, ' ... ') . '" 좋아요 추가되었습니다',
                'type' => 'vote',
                'user_id' => $request->user()->id,
                'option' => $video->id
            ]);
        }

        return response()->json(null, 200);
    }

    public function remove(Request $request, Video $video){
        $this->authorize('vote', $video);

        $vote = $video->voteFromUser($request->user())->first();
        if($vote){
            $vote->delete();
        }

        return response()->json(null, 200);
    }

    public function show(Request $request, Video $video){
        $response = [
            'up' =>null,
            'down' => null,
            'can_vote' => $video->tutorial->votesAllowed() && !$video->ownedByUser($request->user()),
            'user_vote' => null,
        ];

        if( $video->tutorial->votesAllowed() ){
            $response['up'] = $video->upVotes()->count();
            $response['down'] = $video->downVotes()->count();
        }

        if( $request->user() ){
            $voteFromUser = $video->voteFromUser($request->user())->first();
            $response['user_vote'] = $voteFromUser ? $voteFromUser->type : null;
        }

        return response()->json([
            'data' => $response
        ], 200);
    }
}
