<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Channel;
use App\Models\User;
use App\Models\Message;
use Auth;
use App\Events\ChannelReceivedMessage;
use App\Events\GlobalMessageReceived;
use App\Events\UserMessageReceived;
use Illuminate\Support\Facades\DB;

class MessageController extends Controller
{
    public $perPage = 10;

    public function delete(Request $request, Message $message){
        $this->authorize('read', $message);
        $message->delete();

        return response()->json(null, 200);
    }

    public function setUnReadMarkMessge(Request $request, Message $message){
        $this->authorize('read', $message);
        $message->read = false;
        $message->save();

        return response()->json(null, 200);
    }

    public function setReadMarkMessge(Request $request, Message $message){
        $this->authorize('read', $message);
        $message->read = true;
        $message->save();

        return response()->json(null, 200);
    }


    public function getUserMessages(Request $request){
        $user = $request->user();
        if($user){
            $messages = Message::with(['user','channel', 'user.channels'])
            ->where('messagable_id', $user->id)
            ->where('messagable_type', 'App\Models\User')
            ->latestFirst()
            ->paginate($this->perPage);
            return response()->json($messages, 200);
        } else {
            return response()->json(null, 403);
        }
    }

    public function getUserChannelMessages(Request $request){
        $user = $request->user();
        if($user){
            $ids = $user->channelIds();
            $messages = Message::with(['user','channel', 'user.channels'])
            ->leftJoin('channels', 'messagable_id', '=', 'channels.id')
            ->whereIn('messagable_id', $ids)
            ->where('messagable_type', 'App\Models\Channel')
            ->select('messages.*')
            ->addSelect(array(
                'channels.id as rchannel_id',
                'channels.name as rchannel_name',
                'channels.slug as rchannel_slug',
                'channels.logo_image as rchannel_logo_image'
            ))
            ->latestFirst()
            ->paginate($this->perPage);
            return response()->json($messages, 200);
        } else {
            return response()->json(null, 403);
        }
    }


    // 채팅 형식으로 메세지 주고 받는 주체를 묶어서 보기
    public function getMessagesUserGroup(Request $request){
        $user = $request->user();
        if($user){
            $checkTime = $user->getCheckTime('message');
            $messages = Message::with('channel')
            ->leftJoin('users', 'user_id', '=', 'users.id')
            ->where('messagable_id', $user->id)
            ->where('messagable_type', 'App\Models\User')
            ->whereNull('channel_id')
            ->select('users.*')
            ->addSelect(array(
                DB::raw('count(*) as total'),
                DB::raw('count(messages.created_at > "'.$checkTime.'") as new')
                ))
            ->groupBy('user_id')
            ->latestFirst()
            ->paginate($this->perPage);

        } else {
            return response()->json(null, 403);
        }
    }

    public function globalSendMessage(Request $request){
        if(Auth::check()){
            $sendData = [
                'name' => Auth::user()->name,
                'email' => Auth::user()->email,
                'subject' => $request->subject,
                'message' => $request->message,
                'slug' => Auth::user()->slug,
                'logedin' => true,
            ];
        } else {
            $sendData = [
                'name' => $request->name,
                'email' => $request->email,
                'subject' => $request->subject,
                'message' => $request->message,
                'slug' => null,
                'logedin' => false,
            ];
        }

        // event & notification
        event(new GlobalMessageReceived($sendData));

        return response()->json(['result' => 'success'], 200);
    }

    public function userSendMessage(Request $request, User $user){
        $message =  new Message;
        $message->user_id = Auth::user()->id;
        $message->message = $request->message;
        $message->read = false;
        $message->title = str_limit($request->subject, 255);
        if($request->has('channel_id')){
            $message->channel_id = $request->channel_id;
        }

        $message = $user->messages()->save($message);
        // event & notification
        // sender, receiver, message, title
        event(new UserMessageReceived(Auth::user(), $user, $request->message, $request->subject));

        return response()->json(['result' => 'success'], 200);
    }


    public function sendMessage(Request $request){
        if($request->has('channelID')){
            $channel = Channel::find($request->channelID);
            $message =  new Message;
            $message->user_id = Auth::user()->id;
            $message->message = $request->content;
            $message->read = false;
            $message->title = $request->title;

            $message = $channel->messages()->save($message);

            // event & notification
            event(new ChannelReceivedMessage($channel, Auth::user(), $message ));

            return response()->json(['result' => 'success'], 200);
        }
    }
}
