<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\ChannelResource;
use Illuminate\Support\Carbon;

class TutorialResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        Carbon::setLocale('ko');
        $now = Carbon::now();
        $videos = $this->videos()->visible();

        return [
            'id' => $this->id,
            'title' => $this->title,
            'slug' => $this->slug,
            'channel_id' => $this->channel_id,
            'description' =>$this->description,
            'visibility' =>$this->visibility,
            'cost_type' => $this->cost_type,
            'cost' => $this->cost,
            'allow_votes' => $this->allow_votes,
            'allow_comments' => $this->allow_comments,
            'progress_status' =>$this->progress_status,
            'thumbnail' => $this->getThumbnail('thumbnail'),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_at_human' =>  Carbon::parse($now)->diffForHumans($this->created_at, true). ' 전',
            'updated_at_human' =>  Carbon::parse($now)->diffForHumans($this->updated_at, true). ' 전',
            'totalRegisterd' => $this->registrationsCount(),
            'totalVideoViews' => $this->totalVideoViews(),
            'totalVideos' => $videos->count(),
            'totalVideoPlayTime' => $videos->sum('playtime'),
            'channel' => new ChannelResource($this->whenLoaded('channel')),
        ];
    }
}
