<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'email' => $this->email,
            'options' => $this->options,
            'created_at' => $this->created_at,
            'last_login' => $this->last_login,
            'status_message' => $this->status_message,
            'job' => $this->job,
            'photo' => $this->getPhoto(),
            'channels' => ChannelResource::collection($this->whenLoaded('channels')),
            //'subscribedChannels' => ChannelResource::collection($this->whenLoaded('subscribedChannels')),
            //'registeredTutorials' => TutorialResource::collection($this->whenLoaded('registeredTutorials'))
        ];
    }
}
