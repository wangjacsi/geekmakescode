<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Carbon;

class CommentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        Carbon::setLocale('ko');
        $now = Carbon::now();

        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'body' => $this->body,
            'child' => !is_null($this->reply_id),
            'parent_id' => $this->reply_id,
            'owner' => optional($request->user())->id === $this->user_id,
            'edited' => optional($this->edited_at)->diffForHumans($this->created_at, true). ' 전',
            'user' => new UserResource($this->user),
            'replies' => CommentResource::collection($this->whenLoaded('replies')),
            'created_at' => Carbon::parse($now)->diffForHumans($this->created_at, true). ' 전',
        ];
    }
}
