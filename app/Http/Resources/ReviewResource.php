<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Carbon;

class ReviewResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        Carbon::setLocale('ko');
        $now = Carbon::now();

        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'review' => $this->review,
            'content' => $this->content,
            'created_at' => $this->created_at,
            'created_at_human' => Carbon::parse($now)->diffForHumans($this->created_at, true). ' 전',
            'user' => new UserResource($this->whenLoaded('user')),//$this->user),//$this->whenLoaded('replies')
        ];
    }
}
