<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\TutorialResource;
use App\Http\Resources\UserResource;
use Illuminate\Support\Carbon;

class ChannelResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        Carbon::setLocale('ko');
        $now = Carbon::now();

        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'head_image' => $this->getThumbnail('head_image'),
            'logo_image' => $this->getThumbnail('logo_image', 's'),
            'created_at' => $this->created_at,
            'created_at_human' => Carbon::parse($now)->diffForHumans($this->created_at, true). ' 전',
            'users' => UserResource::collection($this->whenLoaded('users')),
            'tutorials' => TutorialResource::collection($this->whenLoaded('tutorials')),
        ];
    }
}
