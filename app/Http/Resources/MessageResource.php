<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Carbon;

class MessageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        Carbon::setLocale('ko');
        $now = Carbon::now();

        return [
            'id' => $this->id,
            'fromable_id' => $this->fromable_id,
            'fromable_type' => $this->fromable_type,
            'toable_id' => $this->toable_id,
            'toable_type' => $this->toable_type,
            'message' => $this->message,
            'read' => $this->read,
            'reply_id' => $this->reply_id,
            'created_at' => $this->created_at,
            'created_at_human' => Carbon::parse($now)->diffForHumans($this->created_at, true). ' 전',
        ];
    }
}
