<?php

namespace App\Events;

//use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Models\Tutorial;
use App\Models\User;
use App\Models\Review;

class ReviewCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    
    public $tutorial;
    public $user;
    public $review;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Tutorial $tutorial, User $user, Review $review)
    {
        $this->tutorial = $tutorial;
        $this->user = $user;
        $this->review = $review;
    }


}
