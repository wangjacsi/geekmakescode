<?php

namespace App\Events;

//use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Models\User;
use App\Models\Tutorial;

class RegistrationCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $tutorial;
    public $user;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Tutorial $tutorial, User $user)
    {
        $this->tutorial = $tutorial;
        $this->user = $user;
    }


}
