<?php

namespace App\Events;

//use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Models\Video;
use App\Models\User;
use App\Models\Comment;

class VideoCommentCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $video;
    public $user;
    public $comment;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Video $video, User $user, Comment $comment)
    {
        $this->user = $user;
        $this->video = $video;
        $this->comment = $comment;
    }


}
