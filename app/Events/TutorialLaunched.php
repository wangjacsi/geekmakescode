<?php

namespace App\Events;

//use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Models\Tutorial;
use App\Models\Channel;

class TutorialLaunched
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $tutorial;
    public $channel;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Tutorial $tutorial, Channel $channel)
    {
        $this->tutorial = $tutorial;
        $this->channel = $channel;
    }

}
