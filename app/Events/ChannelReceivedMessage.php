<?php

namespace App\Events;

//use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Models\User;
use App\Models\Channel;
use App\Models\Message;

class ChannelReceivedMessage
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $channel;
    public $user;
    public $message;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Channel $channel, User $user, Message $message)
    {
        $this->channel = $channel;
        $this->user = $user;
        $this->message = $message;
    }


}
