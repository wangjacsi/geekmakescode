/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 521);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

module.exports = new Vue();

// export default new Vue()

/***/ }),

/***/ 10:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__event_js__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__event_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__event_js__);


function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
    data: function data() {
        return {
            name: '',
            email: '',
            subject: '',
            message: '',
            errorMessage: ''
        };
    },

    props: {
        popupId: {
            type: String
        },
        userLogedin: {
            type: Boolean,
            required: true
        },
        urlPath: {
            type: String,
            required: true
        },
        toUser: {
            type: String
        }
    },
    mounted: function mounted() {},

    methods: {
        sendMessage: function () {
            var _ref = _asyncToGenerator( /*#__PURE__*/__WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default.a.mark(function _callee() {
                var validte, sendData, response, self;
                return __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default.a.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                validte = this.validate();

                                if (validte) {
                                    _context.next = 3;
                                    break;
                                }

                                return _context.abrupt('return', false);

                            case 3:
                                sendData = void 0;

                                if (this.userLogedin) {
                                    sendData = { subject: this.subject, message: this.message };
                                } else {
                                    sendData = { name: this.name, email: this.email, subject: this.subject, message: this.message };
                                }

                                _context.next = 7;
                                return axios.post(this.urlPath, sendData);

                            case 7:
                                response = _context.sent;
                                self = this;

                                this.errorMessage = '메세지 전송이 완료되었습니다';
                                this.initForm();
                                setTimeout(function () {
                                    self.errorMessage = '';
                                    $('.' + self.popupId).removeClass('open');
                                }, 3000);

                            case 12:
                            case 'end':
                                return _context.stop();
                        }
                    }
                }, _callee, this);
            }));

            function sendMessage() {
                return _ref.apply(this, arguments);
            }

            return sendMessage;
        }(),
        initForm: function initForm() {
            this.name = '';
            this.email = '';
            this.subject = '';
            this.message = '';
        },
        validate: function validate() {
            this.errorMessage = '';
            var error = false;
            if (!this.userLogedin) {
                if (this.name == '') {
                    this.errorMessage = '이름을 작성해주세요';
                    return false;
                }

                if (this.email == '') {
                    this.errorMessage = '이메일을 작성해주세요';
                    return false;
                }
            }

            if (this.subject == '') {
                this.errorMessage = '제목을 작성해주세요';
                return false;
            }

            if (this.message == '') {
                this.errorMessage = '메세지를 작성해주세요';
                return false;
            }

            return true;
        }
    }
});

/***/ }),

/***/ 11:
/***/ (function(module, exports, __webpack_require__) {

/**
 * Copyright (c) 2014-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

// This method of obtaining a reference to the global object needs to be
// kept identical to the way it is obtained in runtime.js
var g = (function() { return this })() || Function("return this")();

// Use `getOwnPropertyNames` because not all browsers support calling
// `hasOwnProperty` on the global `self` object in a worker. See #183.
var hadRuntime = g.regeneratorRuntime &&
  Object.getOwnPropertyNames(g).indexOf("regeneratorRuntime") >= 0;

// Save the old regeneratorRuntime in case it needs to be restored later.
var oldRuntime = hadRuntime && g.regeneratorRuntime;

// Force reevalutation of runtime.js.
g.regeneratorRuntime = undefined;

module.exports = __webpack_require__(12);

if (hadRuntime) {
  // Restore the original runtime.
  g.regeneratorRuntime = oldRuntime;
} else {
  // Remove the global property added by runtime.js.
  try {
    delete g.regeneratorRuntime;
  } catch(e) {
    g.regeneratorRuntime = undefined;
  }
}


/***/ }),

/***/ 12:
/***/ (function(module, exports) {

/**
 * Copyright (c) 2014-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

!(function(global) {
  "use strict";

  var Op = Object.prototype;
  var hasOwn = Op.hasOwnProperty;
  var undefined; // More compressible than void 0.
  var $Symbol = typeof Symbol === "function" ? Symbol : {};
  var iteratorSymbol = $Symbol.iterator || "@@iterator";
  var asyncIteratorSymbol = $Symbol.asyncIterator || "@@asyncIterator";
  var toStringTagSymbol = $Symbol.toStringTag || "@@toStringTag";

  var inModule = typeof module === "object";
  var runtime = global.regeneratorRuntime;
  if (runtime) {
    if (inModule) {
      // If regeneratorRuntime is defined globally and we're in a module,
      // make the exports object identical to regeneratorRuntime.
      module.exports = runtime;
    }
    // Don't bother evaluating the rest of this file if the runtime was
    // already defined globally.
    return;
  }

  // Define the runtime globally (as expected by generated code) as either
  // module.exports (if we're in a module) or a new, empty object.
  runtime = global.regeneratorRuntime = inModule ? module.exports : {};

  function wrap(innerFn, outerFn, self, tryLocsList) {
    // If outerFn provided and outerFn.prototype is a Generator, then outerFn.prototype instanceof Generator.
    var protoGenerator = outerFn && outerFn.prototype instanceof Generator ? outerFn : Generator;
    var generator = Object.create(protoGenerator.prototype);
    var context = new Context(tryLocsList || []);

    // The ._invoke method unifies the implementations of the .next,
    // .throw, and .return methods.
    generator._invoke = makeInvokeMethod(innerFn, self, context);

    return generator;
  }
  runtime.wrap = wrap;

  // Try/catch helper to minimize deoptimizations. Returns a completion
  // record like context.tryEntries[i].completion. This interface could
  // have been (and was previously) designed to take a closure to be
  // invoked without arguments, but in all the cases we care about we
  // already have an existing method we want to call, so there's no need
  // to create a new function object. We can even get away with assuming
  // the method takes exactly one argument, since that happens to be true
  // in every case, so we don't have to touch the arguments object. The
  // only additional allocation required is the completion record, which
  // has a stable shape and so hopefully should be cheap to allocate.
  function tryCatch(fn, obj, arg) {
    try {
      return { type: "normal", arg: fn.call(obj, arg) };
    } catch (err) {
      return { type: "throw", arg: err };
    }
  }

  var GenStateSuspendedStart = "suspendedStart";
  var GenStateSuspendedYield = "suspendedYield";
  var GenStateExecuting = "executing";
  var GenStateCompleted = "completed";

  // Returning this object from the innerFn has the same effect as
  // breaking out of the dispatch switch statement.
  var ContinueSentinel = {};

  // Dummy constructor functions that we use as the .constructor and
  // .constructor.prototype properties for functions that return Generator
  // objects. For full spec compliance, you may wish to configure your
  // minifier not to mangle the names of these two functions.
  function Generator() {}
  function GeneratorFunction() {}
  function GeneratorFunctionPrototype() {}

  // This is a polyfill for %IteratorPrototype% for environments that
  // don't natively support it.
  var IteratorPrototype = {};
  IteratorPrototype[iteratorSymbol] = function () {
    return this;
  };

  var getProto = Object.getPrototypeOf;
  var NativeIteratorPrototype = getProto && getProto(getProto(values([])));
  if (NativeIteratorPrototype &&
      NativeIteratorPrototype !== Op &&
      hasOwn.call(NativeIteratorPrototype, iteratorSymbol)) {
    // This environment has a native %IteratorPrototype%; use it instead
    // of the polyfill.
    IteratorPrototype = NativeIteratorPrototype;
  }

  var Gp = GeneratorFunctionPrototype.prototype =
    Generator.prototype = Object.create(IteratorPrototype);
  GeneratorFunction.prototype = Gp.constructor = GeneratorFunctionPrototype;
  GeneratorFunctionPrototype.constructor = GeneratorFunction;
  GeneratorFunctionPrototype[toStringTagSymbol] =
    GeneratorFunction.displayName = "GeneratorFunction";

  // Helper for defining the .next, .throw, and .return methods of the
  // Iterator interface in terms of a single ._invoke method.
  function defineIteratorMethods(prototype) {
    ["next", "throw", "return"].forEach(function(method) {
      prototype[method] = function(arg) {
        return this._invoke(method, arg);
      };
    });
  }

  runtime.isGeneratorFunction = function(genFun) {
    var ctor = typeof genFun === "function" && genFun.constructor;
    return ctor
      ? ctor === GeneratorFunction ||
        // For the native GeneratorFunction constructor, the best we can
        // do is to check its .name property.
        (ctor.displayName || ctor.name) === "GeneratorFunction"
      : false;
  };

  runtime.mark = function(genFun) {
    if (Object.setPrototypeOf) {
      Object.setPrototypeOf(genFun, GeneratorFunctionPrototype);
    } else {
      genFun.__proto__ = GeneratorFunctionPrototype;
      if (!(toStringTagSymbol in genFun)) {
        genFun[toStringTagSymbol] = "GeneratorFunction";
      }
    }
    genFun.prototype = Object.create(Gp);
    return genFun;
  };

  // Within the body of any async function, `await x` is transformed to
  // `yield regeneratorRuntime.awrap(x)`, so that the runtime can test
  // `hasOwn.call(value, "__await")` to determine if the yielded value is
  // meant to be awaited.
  runtime.awrap = function(arg) {
    return { __await: arg };
  };

  function AsyncIterator(generator) {
    function invoke(method, arg, resolve, reject) {
      var record = tryCatch(generator[method], generator, arg);
      if (record.type === "throw") {
        reject(record.arg);
      } else {
        var result = record.arg;
        var value = result.value;
        if (value &&
            typeof value === "object" &&
            hasOwn.call(value, "__await")) {
          return Promise.resolve(value.__await).then(function(value) {
            invoke("next", value, resolve, reject);
          }, function(err) {
            invoke("throw", err, resolve, reject);
          });
        }

        return Promise.resolve(value).then(function(unwrapped) {
          // When a yielded Promise is resolved, its final value becomes
          // the .value of the Promise<{value,done}> result for the
          // current iteration. If the Promise is rejected, however, the
          // result for this iteration will be rejected with the same
          // reason. Note that rejections of yielded Promises are not
          // thrown back into the generator function, as is the case
          // when an awaited Promise is rejected. This difference in
          // behavior between yield and await is important, because it
          // allows the consumer to decide what to do with the yielded
          // rejection (swallow it and continue, manually .throw it back
          // into the generator, abandon iteration, whatever). With
          // await, by contrast, there is no opportunity to examine the
          // rejection reason outside the generator function, so the
          // only option is to throw it from the await expression, and
          // let the generator function handle the exception.
          result.value = unwrapped;
          resolve(result);
        }, reject);
      }
    }

    var previousPromise;

    function enqueue(method, arg) {
      function callInvokeWithMethodAndArg() {
        return new Promise(function(resolve, reject) {
          invoke(method, arg, resolve, reject);
        });
      }

      return previousPromise =
        // If enqueue has been called before, then we want to wait until
        // all previous Promises have been resolved before calling invoke,
        // so that results are always delivered in the correct order. If
        // enqueue has not been called before, then it is important to
        // call invoke immediately, without waiting on a callback to fire,
        // so that the async generator function has the opportunity to do
        // any necessary setup in a predictable way. This predictability
        // is why the Promise constructor synchronously invokes its
        // executor callback, and why async functions synchronously
        // execute code before the first await. Since we implement simple
        // async functions in terms of async generators, it is especially
        // important to get this right, even though it requires care.
        previousPromise ? previousPromise.then(
          callInvokeWithMethodAndArg,
          // Avoid propagating failures to Promises returned by later
          // invocations of the iterator.
          callInvokeWithMethodAndArg
        ) : callInvokeWithMethodAndArg();
    }

    // Define the unified helper method that is used to implement .next,
    // .throw, and .return (see defineIteratorMethods).
    this._invoke = enqueue;
  }

  defineIteratorMethods(AsyncIterator.prototype);
  AsyncIterator.prototype[asyncIteratorSymbol] = function () {
    return this;
  };
  runtime.AsyncIterator = AsyncIterator;

  // Note that simple async functions are implemented on top of
  // AsyncIterator objects; they just return a Promise for the value of
  // the final result produced by the iterator.
  runtime.async = function(innerFn, outerFn, self, tryLocsList) {
    var iter = new AsyncIterator(
      wrap(innerFn, outerFn, self, tryLocsList)
    );

    return runtime.isGeneratorFunction(outerFn)
      ? iter // If outerFn is a generator, return the full iterator.
      : iter.next().then(function(result) {
          return result.done ? result.value : iter.next();
        });
  };

  function makeInvokeMethod(innerFn, self, context) {
    var state = GenStateSuspendedStart;

    return function invoke(method, arg) {
      if (state === GenStateExecuting) {
        throw new Error("Generator is already running");
      }

      if (state === GenStateCompleted) {
        if (method === "throw") {
          throw arg;
        }

        // Be forgiving, per 25.3.3.3.3 of the spec:
        // https://people.mozilla.org/~jorendorff/es6-draft.html#sec-generatorresume
        return doneResult();
      }

      context.method = method;
      context.arg = arg;

      while (true) {
        var delegate = context.delegate;
        if (delegate) {
          var delegateResult = maybeInvokeDelegate(delegate, context);
          if (delegateResult) {
            if (delegateResult === ContinueSentinel) continue;
            return delegateResult;
          }
        }

        if (context.method === "next") {
          // Setting context._sent for legacy support of Babel's
          // function.sent implementation.
          context.sent = context._sent = context.arg;

        } else if (context.method === "throw") {
          if (state === GenStateSuspendedStart) {
            state = GenStateCompleted;
            throw context.arg;
          }

          context.dispatchException(context.arg);

        } else if (context.method === "return") {
          context.abrupt("return", context.arg);
        }

        state = GenStateExecuting;

        var record = tryCatch(innerFn, self, context);
        if (record.type === "normal") {
          // If an exception is thrown from innerFn, we leave state ===
          // GenStateExecuting and loop back for another invocation.
          state = context.done
            ? GenStateCompleted
            : GenStateSuspendedYield;

          if (record.arg === ContinueSentinel) {
            continue;
          }

          return {
            value: record.arg,
            done: context.done
          };

        } else if (record.type === "throw") {
          state = GenStateCompleted;
          // Dispatch the exception by looping back around to the
          // context.dispatchException(context.arg) call above.
          context.method = "throw";
          context.arg = record.arg;
        }
      }
    };
  }

  // Call delegate.iterator[context.method](context.arg) and handle the
  // result, either by returning a { value, done } result from the
  // delegate iterator, or by modifying context.method and context.arg,
  // setting context.delegate to null, and returning the ContinueSentinel.
  function maybeInvokeDelegate(delegate, context) {
    var method = delegate.iterator[context.method];
    if (method === undefined) {
      // A .throw or .return when the delegate iterator has no .throw
      // method always terminates the yield* loop.
      context.delegate = null;

      if (context.method === "throw") {
        if (delegate.iterator.return) {
          // If the delegate iterator has a return method, give it a
          // chance to clean up.
          context.method = "return";
          context.arg = undefined;
          maybeInvokeDelegate(delegate, context);

          if (context.method === "throw") {
            // If maybeInvokeDelegate(context) changed context.method from
            // "return" to "throw", let that override the TypeError below.
            return ContinueSentinel;
          }
        }

        context.method = "throw";
        context.arg = new TypeError(
          "The iterator does not provide a 'throw' method");
      }

      return ContinueSentinel;
    }

    var record = tryCatch(method, delegate.iterator, context.arg);

    if (record.type === "throw") {
      context.method = "throw";
      context.arg = record.arg;
      context.delegate = null;
      return ContinueSentinel;
    }

    var info = record.arg;

    if (! info) {
      context.method = "throw";
      context.arg = new TypeError("iterator result is not an object");
      context.delegate = null;
      return ContinueSentinel;
    }

    if (info.done) {
      // Assign the result of the finished delegate to the temporary
      // variable specified by delegate.resultName (see delegateYield).
      context[delegate.resultName] = info.value;

      // Resume execution at the desired location (see delegateYield).
      context.next = delegate.nextLoc;

      // If context.method was "throw" but the delegate handled the
      // exception, let the outer generator proceed normally. If
      // context.method was "next", forget context.arg since it has been
      // "consumed" by the delegate iterator. If context.method was
      // "return", allow the original .return call to continue in the
      // outer generator.
      if (context.method !== "return") {
        context.method = "next";
        context.arg = undefined;
      }

    } else {
      // Re-yield the result returned by the delegate method.
      return info;
    }

    // The delegate iterator is finished, so forget it and continue with
    // the outer generator.
    context.delegate = null;
    return ContinueSentinel;
  }

  // Define Generator.prototype.{next,throw,return} in terms of the
  // unified ._invoke helper method.
  defineIteratorMethods(Gp);

  Gp[toStringTagSymbol] = "Generator";

  // A Generator should always return itself as the iterator object when the
  // @@iterator function is called on it. Some browsers' implementations of the
  // iterator prototype chain incorrectly implement this, causing the Generator
  // object to not be returned from this call. This ensures that doesn't happen.
  // See https://github.com/facebook/regenerator/issues/274 for more details.
  Gp[iteratorSymbol] = function() {
    return this;
  };

  Gp.toString = function() {
    return "[object Generator]";
  };

  function pushTryEntry(locs) {
    var entry = { tryLoc: locs[0] };

    if (1 in locs) {
      entry.catchLoc = locs[1];
    }

    if (2 in locs) {
      entry.finallyLoc = locs[2];
      entry.afterLoc = locs[3];
    }

    this.tryEntries.push(entry);
  }

  function resetTryEntry(entry) {
    var record = entry.completion || {};
    record.type = "normal";
    delete record.arg;
    entry.completion = record;
  }

  function Context(tryLocsList) {
    // The root entry object (effectively a try statement without a catch
    // or a finally block) gives us a place to store values thrown from
    // locations where there is no enclosing try statement.
    this.tryEntries = [{ tryLoc: "root" }];
    tryLocsList.forEach(pushTryEntry, this);
    this.reset(true);
  }

  runtime.keys = function(object) {
    var keys = [];
    for (var key in object) {
      keys.push(key);
    }
    keys.reverse();

    // Rather than returning an object with a next method, we keep
    // things simple and return the next function itself.
    return function next() {
      while (keys.length) {
        var key = keys.pop();
        if (key in object) {
          next.value = key;
          next.done = false;
          return next;
        }
      }

      // To avoid creating an additional object, we just hang the .value
      // and .done properties off the next function object itself. This
      // also ensures that the minifier will not anonymize the function.
      next.done = true;
      return next;
    };
  };

  function values(iterable) {
    if (iterable) {
      var iteratorMethod = iterable[iteratorSymbol];
      if (iteratorMethod) {
        return iteratorMethod.call(iterable);
      }

      if (typeof iterable.next === "function") {
        return iterable;
      }

      if (!isNaN(iterable.length)) {
        var i = -1, next = function next() {
          while (++i < iterable.length) {
            if (hasOwn.call(iterable, i)) {
              next.value = iterable[i];
              next.done = false;
              return next;
            }
          }

          next.value = undefined;
          next.done = true;

          return next;
        };

        return next.next = next;
      }
    }

    // Return an iterator with no values.
    return { next: doneResult };
  }
  runtime.values = values;

  function doneResult() {
    return { value: undefined, done: true };
  }

  Context.prototype = {
    constructor: Context,

    reset: function(skipTempReset) {
      this.prev = 0;
      this.next = 0;
      // Resetting context._sent for legacy support of Babel's
      // function.sent implementation.
      this.sent = this._sent = undefined;
      this.done = false;
      this.delegate = null;

      this.method = "next";
      this.arg = undefined;

      this.tryEntries.forEach(resetTryEntry);

      if (!skipTempReset) {
        for (var name in this) {
          // Not sure about the optimal order of these conditions:
          if (name.charAt(0) === "t" &&
              hasOwn.call(this, name) &&
              !isNaN(+name.slice(1))) {
            this[name] = undefined;
          }
        }
      }
    },

    stop: function() {
      this.done = true;

      var rootEntry = this.tryEntries[0];
      var rootRecord = rootEntry.completion;
      if (rootRecord.type === "throw") {
        throw rootRecord.arg;
      }

      return this.rval;
    },

    dispatchException: function(exception) {
      if (this.done) {
        throw exception;
      }

      var context = this;
      function handle(loc, caught) {
        record.type = "throw";
        record.arg = exception;
        context.next = loc;

        if (caught) {
          // If the dispatched exception was caught by a catch block,
          // then let that catch block handle the exception normally.
          context.method = "next";
          context.arg = undefined;
        }

        return !! caught;
      }

      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        var record = entry.completion;

        if (entry.tryLoc === "root") {
          // Exception thrown outside of any try block that could handle
          // it, so set the completion value of the entire function to
          // throw the exception.
          return handle("end");
        }

        if (entry.tryLoc <= this.prev) {
          var hasCatch = hasOwn.call(entry, "catchLoc");
          var hasFinally = hasOwn.call(entry, "finallyLoc");

          if (hasCatch && hasFinally) {
            if (this.prev < entry.catchLoc) {
              return handle(entry.catchLoc, true);
            } else if (this.prev < entry.finallyLoc) {
              return handle(entry.finallyLoc);
            }

          } else if (hasCatch) {
            if (this.prev < entry.catchLoc) {
              return handle(entry.catchLoc, true);
            }

          } else if (hasFinally) {
            if (this.prev < entry.finallyLoc) {
              return handle(entry.finallyLoc);
            }

          } else {
            throw new Error("try statement without catch or finally");
          }
        }
      }
    },

    abrupt: function(type, arg) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.tryLoc <= this.prev &&
            hasOwn.call(entry, "finallyLoc") &&
            this.prev < entry.finallyLoc) {
          var finallyEntry = entry;
          break;
        }
      }

      if (finallyEntry &&
          (type === "break" ||
           type === "continue") &&
          finallyEntry.tryLoc <= arg &&
          arg <= finallyEntry.finallyLoc) {
        // Ignore the finally entry if control is not jumping to a
        // location outside the try/catch block.
        finallyEntry = null;
      }

      var record = finallyEntry ? finallyEntry.completion : {};
      record.type = type;
      record.arg = arg;

      if (finallyEntry) {
        this.method = "next";
        this.next = finallyEntry.finallyLoc;
        return ContinueSentinel;
      }

      return this.complete(record);
    },

    complete: function(record, afterLoc) {
      if (record.type === "throw") {
        throw record.arg;
      }

      if (record.type === "break" ||
          record.type === "continue") {
        this.next = record.arg;
      } else if (record.type === "return") {
        this.rval = this.arg = record.arg;
        this.method = "return";
        this.next = "end";
      } else if (record.type === "normal" && afterLoc) {
        this.next = afterLoc;
      }

      return ContinueSentinel;
    },

    finish: function(finallyLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.finallyLoc === finallyLoc) {
          this.complete(entry.completion, entry.afterLoc);
          resetTryEntry(entry);
          return ContinueSentinel;
        }
      }
    },

    "catch": function(tryLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.tryLoc === tryLoc) {
          var record = entry.completion;
          if (record.type === "throw") {
            var thrown = record.arg;
            resetTryEntry(entry);
          }
          return thrown;
        }
      }

      // The context.catch method must only be called with a location
      // argument that corresponds to a known catch block.
      throw new Error("illegal catch attempt");
    },

    delegateYield: function(iterable, resultName, nextLoc) {
      this.delegate = {
        iterator: values(iterable),
        resultName: resultName,
        nextLoc: nextLoc
      };

      if (this.method === "next") {
        // Deliberately forget the last sent value so that we don't
        // accidentally pass it on to the delegate.
        this.arg = undefined;
      }

      return ContinueSentinel;
    }
  };
})(
  // In sloppy mode, unbound `this` refers to the global object, fallback to
  // Function constructor if we're in global strict mode. That is sadly a form
  // of indirect eval which violates Content Security Policy.
  (function() { return this })() || Function("return this")()
);


/***/ }),

/***/ 13:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "window-popup", class: _vm.popupId }, [
    _c(
      "a",
      {
        staticClass: "popup-close js-popup-close cd-nav-trigger",
        attrs: { href: "#" }
      },
      [
        _c("svg", { staticClass: "utouch-icon utouch-icon-cancel-1" }, [
          _c("use", { attrs: { "xlink:href": "#utouch-icon-cancel-1" } })
        ])
      ]
    ),
    _vm._v(" "),
    _c("div", { staticClass: "send-message-popup" }, [
      _c("h5", [_vm._v("Send a Message")]),
      _vm._v(" "),
      _c("form", { staticClass: "contact-form" }, [
        !_vm.userLogedin
          ? _c("div", {}, [
              _c("div", { staticClass: "with-icon" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model.trim",
                      value: _vm.name,
                      expression: "name",
                      modifiers: { trim: true }
                    }
                  ],
                  attrs: {
                    name: "name",
                    placeholder: "Your Name",
                    type: "text",
                    required: "required"
                  },
                  domProps: { value: _vm.name },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.name = $event.target.value.trim()
                    },
                    blur: function($event) {
                      _vm.$forceUpdate()
                    }
                  }
                }),
                _vm._v(" "),
                _c("svg", { staticClass: "utouch-icon utouch-icon-user" }, [
                  _c("use", { attrs: { "xlink:href": "#utouch-icon-user" } })
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "with-icon" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model.trim",
                      value: _vm.email,
                      expression: "email",
                      modifiers: { trim: true }
                    }
                  ],
                  attrs: {
                    name: "email",
                    placeholder: "Email Adress",
                    type: "text",
                    required: "required"
                  },
                  domProps: { value: _vm.email },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.email = $event.target.value.trim()
                    },
                    blur: function($event) {
                      _vm.$forceUpdate()
                    }
                  }
                }),
                _vm._v(" "),
                _c(
                  "svg",
                  {
                    staticClass:
                      "utouch-icon utouch-icon-message-closed-envelope-1"
                  },
                  [
                    _c("use", {
                      attrs: {
                        "xlink:href": "#utouch-icon-message-closed-envelope-1"
                      }
                    })
                  ]
                )
              ])
            ])
          : _vm._e(),
        _vm._v(" "),
        _c("div", { staticClass: "with-icon" }, [
          _c("input", {
            directives: [
              {
                name: "model",
                rawName: "v-model.trim",
                value: _vm.subject,
                expression: "subject",
                modifiers: { trim: true }
              }
            ],
            staticClass: "with-icon",
            attrs: {
              name: "subject",
              placeholder: "Subject",
              type: "text",
              required: "required"
            },
            domProps: { value: _vm.subject },
            on: {
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.subject = $event.target.value.trim()
              },
              blur: function($event) {
                _vm.$forceUpdate()
              }
            }
          }),
          _vm._v(" "),
          _c("svg", { staticClass: "utouch-icon utouch-icon-icon-1" }, [
            _c("use", { attrs: { "xlink:href": "#utouch-icon-icon-1" } })
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "with-icon" }, [
          _c("textarea", {
            directives: [
              {
                name: "model",
                rawName: "v-model.trim",
                value: _vm.message,
                expression: "message",
                modifiers: { trim: true }
              }
            ],
            staticStyle: { height: "180px" },
            attrs: {
              name: "message",
              required: "",
              placeholder: "Your Message"
            },
            domProps: { value: _vm.message },
            on: {
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.message = $event.target.value.trim()
              },
              blur: function($event) {
                _vm.$forceUpdate()
              }
            }
          }),
          _vm._v(" "),
          _c("svg", { staticClass: "utouch-icon utouch-icon-edit" }, [
            _c("use", { attrs: { "xlink:href": "#utouch-icon-edit" } })
          ])
        ]),
        _vm._v(" "),
        _vm._m(0),
        _vm._v(" "),
        _c(
          "button",
          {
            staticClass: "btn btn--green btn--with-shadow full-width",
            on: {
              click: function($event) {
                $event.preventDefault()
                return _vm.sendMessage($event)
              }
            }
          },
          [_vm._v("\n\t\t\t\t메세지 전송\n\t\t\t")]
        ),
        _vm._v(" "),
        this.errorMessage != ""
          ? _c(
              "div",
              {
                staticClass: "summit-message",
                staticStyle: { "margin-top": "20px" }
              },
              [
                _c("p", {
                  staticClass: "summit-error",
                  domProps: { innerHTML: _vm._s(this.errorMessage) }
                })
              ]
            )
          : _vm._e()
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      {
        staticStyle: {
          "text-align": "initial",
          "font-size": "13px",
          "margin-bottom": "20px"
        }
      },
      [
        _vm._v("마크다운(Markdown "),
        _c("i", { staticClass: "fab fa-markdown" }),
        _vm._v(
          ") 지원합니다. # ~ ######: 헤더 h1 ~ h6, >: 인용 Blockquotes, ~~~ 코드블록 ~~~, ` 인라인 코드 블럭 `, * 기울여쓰기(italic) *, ** 굵게쓰기(bold) **, --- 수평선, [Google](http://www.google.co.kr “구글”) : 인라인 링크, <http://google.com/>: URl 링크, 1. list item : 리스트, * list item : 리스트, ![alt text](image_URL): 링크 이미지\n            "
        )
      ]
    )
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-c4573c2c", module.exports)
  }
}

/***/ }),

/***/ 14:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(16)
/* template */
var __vue_template__ = __webpack_require__(17)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/components/Pagination.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-3895afde", Component.options)
  } else {
    hotAPI.reload("data-v-3895afde", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 16:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__event_js__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__event_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__event_js__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
    props: {
        pagination: {
            type: Object,
            required: true
        },
        for: {
            type: String,
            default: 'default'
        }
    },
    data: function data() {
        return {};
    },
    mounted: function mounted() {},

    methods: {
        pageChange: function pageChange(page) {
            //console.log(page)
            if (page === parseInt(this.pagination.current_page) - 4 || page === parseInt(this.pagination.current_page) + 4 || page == this.pagination.current_page) {
                return;
            } else if (page == 'next') {
                console.log(this.for);
                __WEBPACK_IMPORTED_MODULE_0__event_js___default.a.$emit(this.for + '-pagination', this.pagination.current_page + 1, this.pagination.path);
            } else if (page == 'prev') {
                console.log(this.for);
                __WEBPACK_IMPORTED_MODULE_0__event_js___default.a.$emit(this.for + '-pagination', this.pagination.current_page - 1, this.pagination.path);
            } else {
                console.log(this.for);
                __WEBPACK_IMPORTED_MODULE_0__event_js___default.a.$emit(this.for + '-pagination', page, this.pagination.path);
            }
        }
    }
});

/***/ }),

/***/ 17:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.pagination.last_page > 1
    ? _c(
        "nav",
        { staticClass: "navigation", attrs: { role: "navigation" } },
        [
          _vm.pagination.current_page <= 1
            ? _c(
                "a",
                {
                  staticClass: "disabled page-numbers",
                  attrs: {
                    "aria-aria-disabled": "true",
                    "aria-label": "previous"
                  }
                },
                [_c("span", { staticClass: "xi-angle-left" })]
              )
            : _c(
                "a",
                {
                  staticClass: "page-numbers",
                  attrs: { rel: "prev", "aria-label": "previous" },
                  on: {
                    click: function($event) {
                      $event.preventDefault()
                      _vm.pageChange("prev")
                    }
                  }
                },
                [_c("span", { staticClass: "xi-angle-left" })]
              ),
          _vm._v(" "),
          _vm.pagination.current_page > 0
            ? _vm._l(parseInt(_vm.pagination.last_page), function(page) {
                return page === 1 ||
                  page === _vm.pagination.last_page ||
                  (page > parseInt(_vm.pagination.current_page) - 5 &&
                    page < parseInt(_vm.pagination.current_page) + 5)
                  ? _c(
                      "a",
                      {
                        key: page,
                        staticClass: "page-numbers",
                        class: {
                          "current disabled":
                            _vm.pagination.current_page === page,
                          disabled:
                            page ===
                              parseInt(_vm.pagination.current_page) - 4 ||
                            page === parseInt(_vm.pagination.current_page) + 4
                        },
                        on: {
                          click: function($event) {
                            $event.preventDefault()
                            _vm.pageChange(page)
                          }
                        }
                      },
                      [
                        page === parseInt(_vm.pagination.current_page) - 4 ||
                        page === parseInt(_vm.pagination.current_page) + 4
                          ? _c("span", [_vm._v("...")])
                          : _c("span", [_vm._v(_vm._s(page))])
                      ]
                    )
                  : _vm._e()
              })
            : _vm._e(),
          _vm._v(" "),
          _vm.pagination.current_page < _vm.pagination.last_page
            ? _c(
                "a",
                {
                  staticClass: "page-numbers",
                  attrs: { rel: "next", "aria-label": "next" },
                  on: {
                    click: function($event) {
                      $event.preventDefault()
                      _vm.pageChange("next")
                    }
                  }
                },
                [_c("span", { staticClass: "xi-angle-right" })]
              )
            : _c(
                "a",
                {
                  staticClass: "page-numbers disabled",
                  attrs: { "aria-disabled": "true", "aria-label": "next" }
                },
                [_c("span", { staticClass: "xi-angle-right" })]
              )
        ],
        2
      )
    : _vm._e()
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-3895afde", module.exports)
  }
}

/***/ }),

/***/ 2:
/***/ (function(module, exports) {

/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file.
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier /* server only */
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = injectStyles
  }

  if (hook) {
    var functional = options.functional
    var existing = functional
      ? options.render
      : options.beforeCreate

    if (!functional) {
      // inject component registration as beforeCreate hook
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    } else {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functioal component in vue file
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return existing(h, context)
      }
    }
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 3:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(11);


/***/ }),

/***/ 32:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(46)
/* template */
var __vue_template__ = __webpack_require__(47)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/components/mywave/MyChannels.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-7412bf5f", Component.options)
  } else {
    hotAPI.reload("data-v-7412bf5f", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 4:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(6)
}
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(10)
/* template */
var __vue_template__ = __webpack_require__(13)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/components/common/sendMessagePopup.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-c4573c2c", Component.options)
  } else {
    hotAPI.reload("data-v-c4573c2c", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 46:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__event_js__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__event_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__event_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Pagination__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Pagination___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__Pagination__);


function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
    data: function data() {
        return {
            channels: [],
            meta: null,
            links: null,
            pagination: {
                'current_page': null,
                'first': null,
                'from': null,
                'last': null,
                'last_page': null,
                'next': null,
                'path': null,
                'per_page': null,
                'prev': null,
                'to': null,
                'total': null
            },
            loadFlag: false,
            url: null,
            viewOptions: {
                subscribeBtn: false,
                editBtn: false,
                deleteBtn: false,
                viewType: 'block',
                noDataCommentOn: false,
                noDataComment: '',
                gridClass: 'col-lg-6 col-md-6'
            },
            orderType: null,
            pagenationType: null
        };
    },

    components: {
        Pagination: __WEBPACK_IMPORTED_MODULE_2__Pagination___default.a
    },
    props: {
        channelHeadDefault: {
            required: true,
            type: String
        },
        channelLogoDefault: {
            required: true,
            type: String
        },
        userPhotoDefault: {
            required: true,
            type: String
        },
        fromComp: {
            type: String,
            required: true
        },
        viewOptionsProp: {
            type: Object
        },
        getInfoUrl: {
            type: String
        }
    },
    updated: function updated() {
        var self = this;
        this.$nextTick(function () {
            console.log('this is updated channels');
            if (self.viewOptions.viewType == 'block') {
                $('.curriculum-event[data-mh="' + self.fromComp + '"]').matchHeight();
            }
        });
    },
    mounted: function mounted() {
        console.log('this is mounted channels');
        this.pagenationType = 1;

        if (this.getInfoUrl) {
            this.url = this.getInfoUrl;
        } else {
            if (this.fromComp == 'mychannels') {
                this.url = '/getChannelsInfoByLoginUser?without=tutorial&page=';
            } else if (this.fromComp == 'mysubscribedchannels') {
                this.url = '/getSubscribedChannelsInfoByLoginUser?without=tutorial&page=';
            } else if (this.fromComp == 'getchannellistinfo') {
                this.url = '/getChannelsInfo?page=';
                this.pagenationType = 2;
            }
        }

        var self = this;
        if (this.viewOptionsProp) {
            _.forEach(this.viewOptionsProp, function (value, key) {
                Vue.set(self.viewOptions, key, value);
            });
        }

        this.initLoadChannels(1);

        __WEBPACK_IMPORTED_MODULE_1__event_js___default.a.$on(this.fromComp + '-pagination', this.pageChange);

        __WEBPACK_IMPORTED_MODULE_1__event_js___default.a.$on(this.fromComp + '-list-view', this.listViewChange);

        __WEBPACK_IMPORTED_MODULE_1__event_js___default.a.$on('channelOrderByChange', this.orderByChange);
    },

    methods: {
        orderByChange: function orderByChange(type) {
            this.orderType = type;
            this.url = '/getChannelsInfo?order=' + type + '&page=';
            this.loadChannels(1);
        },
        listViewChange: function listViewChange(type) {
            this.viewOptions.viewType = type;
        },
        initLoadChannels: function () {
            var _ref = _asyncToGenerator( /*#__PURE__*/__WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default.a.mark(function _callee() {
                return __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default.a.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                _context.next = 2;
                                return this.loadChannels(1);

                            case 2:
                            case 'end':
                                return _context.stop();
                        }
                    }
                }, _callee, this);
            }));

            function initLoadChannels() {
                return _ref.apply(this, arguments);
            }

            return initLoadChannels;
        }(),
        loadChannels: function () {
            var _ref2 = _asyncToGenerator( /*#__PURE__*/__WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default.a.mark(function _callee2() {
                var page = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;
                var channels;
                return __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default.a.wrap(function _callee2$(_context2) {
                    while (1) {
                        switch (_context2.prev = _context2.next) {
                            case 0:
                                _context2.next = 2;
                                return axios.get(this.url + page);

                            case 2:
                                channels = _context2.sent;


                                console.log(channels);

                                this.channels = channels.data.data;
                                if (this.pagenationType == 1) {
                                    this.meta = channels.data.meta;
                                    this.links = channels.data.links;
                                } else {
                                    this.meta = _.omit(channels.data, ['data']);
                                }

                                this.paginationSet();

                            case 7:
                            case 'end':
                                return _context2.stop();
                        }
                    }
                }, _callee2, this);
            }));

            function loadChannels() {
                return _ref2.apply(this, arguments);
            }

            return loadChannels;
        }(),
        paginationSet: function paginationSet() {
            if (this.pagenationType == 1) {
                Vue.set(this.pagination, 'current_page', this.meta.current_page);
                Vue.set(this.pagination, 'first', this.links.first);
                Vue.set(this.pagination, 'from', this.meta.from);
                Vue.set(this.pagination, 'last', this.links.last);
                Vue.set(this.pagination, 'last_page', this.meta.last_page);
                Vue.set(this.pagination, 'next', this.links.next);
                Vue.set(this.pagination, 'path', this.meta.path);
                Vue.set(this.pagination, 'per_page', this.meta.per_page);
                Vue.set(this.pagination, 'prev', this.links.prev);
                Vue.set(this.pagination, 'to', this.meta.to);
                Vue.set(this.pagination, 'total', this.meta.total);
            } else {
                Vue.set(this.pagination, 'current_page', this.meta.current_page);
                Vue.set(this.pagination, 'first', this.meta.first_page_url);
                Vue.set(this.pagination, 'from', this.meta.from);
                Vue.set(this.pagination, 'last', this.meta.last_page_url);
                Vue.set(this.pagination, 'last_page', this.meta.last_page);
                Vue.set(this.pagination, 'next', this.meta.next_page_url);
                Vue.set(this.pagination, 'path', this.meta.path);
                Vue.set(this.pagination, 'per_page', this.meta.per_page);
                Vue.set(this.pagination, 'prev', this.meta.prev_page_url);
                Vue.set(this.pagination, 'to', this.meta.to);
                Vue.set(this.pagination, 'total', this.meta.total);
            }
            this.loadFlag = true;
        },
        checkOwn: function checkOwn(channel) {
            var user = channel.users.find(function (user) {
                return user.id === codewwwave.user.id;
            });
            if (user) return true;else return false;
        },
        pageChange: function pageChange(page, path) {
            this.loadChannels(page);
        }
    }
});

/***/ }),

/***/ 47:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _vm.viewOptions.viewType == "block"
        ? _c(
            "div",
            [
              _vm.viewOptions.noDataCommentOn && _vm.channels.length == 0
                ? _c(
                    "div",
                    { staticClass: "col-lg-12 col-md-12 col-sm-12 col-xs-12" },
                    [_c("p", [_vm._v(_vm._s(_vm.viewOptions.noDataComment))])]
                  )
                : _vm._e(),
              _vm._v(" "),
              _vm._l(_vm.channels, function(channel) {
                return _c(
                  "div",
                  {
                    key: channel.id,
                    staticClass: "col-sm-12 col-xs-12",
                    class: _vm.viewOptions.gridClass
                  },
                  [
                    _c(
                      "div",
                      {
                        staticClass: "curriculum-event c-secondary",
                        attrs: { "data-mh": _vm.fromComp }
                      },
                      [
                        _c("div", { staticClass: "curriculum-event-thumb" }, [
                          _c("img", {
                            attrs: {
                              src: channel.head_image
                                ? channel.head_image
                                : _vm.channelHeadDefault,
                              alt: "image"
                            }
                          }),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "curriculum-event-content" },
                            [
                              _c("div", { staticClass: "row" }, [
                                _c("div", { staticClass: "col-md-12" }, [
                                  _c(
                                    "a",
                                    {
                                      staticClass: "h5 title",
                                      attrs: {
                                        href: "/channel/" + channel.slug
                                      }
                                    },
                                    [_vm._v(_vm._s(channel.name))]
                                  )
                                ]),
                                _vm._v(" "),
                                _c("div", { staticClass: "col-md-12" }, [
                                  _c(
                                    "div",
                                    { staticClass: "row channel-info" },
                                    [
                                      _c("div", { staticClass: "col-md-6" }, [
                                        _c(
                                          "div",
                                          {
                                            staticClass:
                                              "author-block inline-items"
                                          },
                                          [
                                            _c(
                                              "div",
                                              { staticClass: "author-avatar" },
                                              [
                                                _c("img", {
                                                  attrs: {
                                                    src: channel.logo_image
                                                      ? channel.logo_image
                                                      : _vm.channelLogoDefault,
                                                    alt: "author"
                                                  }
                                                })
                                              ]
                                            )
                                          ]
                                        )
                                      ]),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "col-md-6" }, [
                                        _c(
                                          "p",
                                          { staticClass: "text color-icon" },
                                          [
                                            _vm._v(
                                              _vm._s(channel.totalTutorials) +
                                                " 강좌 수"
                                            )
                                          ]
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "p",
                                          { staticClass: "text color-icon" },
                                          [
                                            _vm._v(
                                              _vm._s(
                                                channel.totalSubscriptions
                                              ) + " 구독자 수"
                                            )
                                          ]
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "p",
                                          { staticClass: "text color-icon" },
                                          [
                                            _vm._v(
                                              _vm._s(
                                                channel.totalRegisterdTutorials
                                              ) + " 수강자 수"
                                            )
                                          ]
                                        )
                                      ])
                                    ]
                                  )
                                ]),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  { staticClass: "col-md-12 channel-admins" },
                                  [
                                    _c("div", { staticClass: "author-prof" }, [
                                      _vm._v("채널 운영자")
                                    ]),
                                    _vm._v(" "),
                                    _c(
                                      "div",
                                      { staticClass: "d-flex--content-inline" },
                                      [
                                        _c(
                                          "ul",
                                          { staticClass: "friends-harmonic" },
                                          _vm._l(channel.users, function(user) {
                                            return _c("li", { key: user.id }, [
                                              _c(
                                                "a",
                                                {
                                                  attrs: {
                                                    title: user.name,
                                                    href: "/wave/" + user.slug
                                                  }
                                                },
                                                [
                                                  _c("img", {
                                                    attrs: {
                                                      src: user.photo
                                                        ? user.photo
                                                        : _vm.userPhotoDefault,
                                                      alt: "image"
                                                    }
                                                  })
                                                ]
                                              )
                                            ])
                                          })
                                        )
                                      ]
                                    )
                                  ]
                                )
                              ])
                            ]
                          ),
                          _vm._v(" "),
                          _c("div", { staticClass: "action-button-group" }, [
                            !_vm.checkOwn(channel) &&
                            _vm.viewOptions.subscribeBtn
                              ? _c("div", {}, [
                                  _vm.fromComp == "mysubscribedchannels"
                                    ? _c(
                                        "a",
                                        { attrs: { title: "구독취소" } },
                                        [
                                          _c("i", {
                                            staticClass: "fas fa-minus-square"
                                          })
                                        ]
                                      )
                                    : _c("a", { attrs: { title: "구독" } }, [
                                        _c("i", {
                                          staticClass: "fas fa-plus-square"
                                        })
                                      ])
                                ])
                              : _vm._e(),
                            _vm._v(" "),
                            _vm.checkOwn(channel) && _vm.viewOptions.editBtn
                              ? _c(
                                  "a",
                                  {
                                    staticClass: "edit-button",
                                    attrs: { title: "수정" }
                                  },
                                  [
                                    _c("i", {
                                      staticClass: "fas fa-pen-square"
                                    })
                                  ]
                                )
                              : _vm._e(),
                            _vm._v(" "),
                            _vm.checkOwn(channel) && _vm.viewOptions.deleteBtn
                              ? _c(
                                  "a",
                                  {
                                    staticClass: "delete-button",
                                    attrs: { title: "삭제" }
                                  },
                                  [
                                    _c("i", {
                                      staticClass: "fas fa-minus-square"
                                    })
                                  ]
                                )
                              : _vm._e()
                          ]),
                          _vm._v(" "),
                          _c("div", {
                            staticClass: "overlay-standard overlay--dark"
                          })
                        ])
                      ]
                    )
                  ]
                )
              })
            ],
            2
          )
        : _c(
            "div",
            { staticClass: "col-lg-12 col-md-12 col-sm-12 col-xs-12" },
            [
              _vm.viewOptions.noDataCommentOn && _vm.channels.length == 0
                ? _c("p", [_vm._v(_vm._s(_vm.viewOptions.noDataComment))])
                : _vm._e(),
              _vm._v(" "),
              _c(
                "ul",
                { staticClass: "teammember-list" },
                _vm._l(_vm.channels, function(channel) {
                  return _c(
                    "li",
                    {
                      key: channel.id,
                      staticClass:
                        "crumina-module crumina-teammembers-item teammember-item--author-in-round"
                    },
                    [
                      _c("div", { staticClass: "teammembers-thumb" }, [
                        _c("img", {
                          attrs: {
                            src: channel.logo_image
                              ? channel.logo_image
                              : _vm.channelLogoDefault,
                            alt: "team member"
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "teammember-content" }, [
                        _c(
                          "a",
                          {
                            staticClass: "h5 teammembers-item-name",
                            attrs: { href: "/channel/" + channel.slug }
                          },
                          [_vm._v(_vm._s(channel.name))]
                        ),
                        _vm._v(" "),
                        _c("p", [
                          _vm._v(
                            _vm._s(channel.totalTutorials) +
                              " 강좌 수, " +
                              _vm._s(channel.totalSubscriptions) +
                              " 구독자 수, " +
                              _vm._s(channel.totalRegisterdTutorials) +
                              " 수강자 수\n                    "
                          )
                        ]),
                        _vm._v(" "),
                        _c(
                          "ul",
                          { staticClass: "friends-harmonic" },
                          _vm._l(channel.users, function(user) {
                            return _c("li", { key: user.id }, [
                              _c(
                                "a",
                                {
                                  attrs: {
                                    title: user.name,
                                    href: "/wave/" + user.slug
                                  }
                                },
                                [
                                  _c("img", {
                                    attrs: {
                                      src: user.photo
                                        ? user.photo
                                        : _vm.userPhotoDefault,
                                      alt: "image"
                                    }
                                  })
                                ]
                              )
                            ])
                          })
                        )
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "action-button-group c-gray" }, [
                        !_vm.checkOwn(channel) && _vm.viewOptions.subscribeBtn
                          ? _c("div", {}, [
                              _vm.fromComp == "mysubscribedchannels"
                                ? _c("a", { attrs: { title: "구독취소" } }, [
                                    _c("i", {
                                      staticClass: "fas fa-minus-square"
                                    })
                                  ])
                                : _c("a", { attrs: { title: "구독" } }, [
                                    _c("i", {
                                      staticClass: "fas fa-plus-square"
                                    })
                                  ])
                            ])
                          : _vm._e(),
                        _vm._v(" "),
                        _vm.checkOwn(channel) && _vm.viewOptions.editBtn
                          ? _c(
                              "a",
                              {
                                staticClass: "edit-button",
                                attrs: { title: "수정" }
                              },
                              [_c("i", { staticClass: "fas fa-pen-square" })]
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        _vm.checkOwn(channel) && _vm.viewOptions.deleteBtn
                          ? _c(
                              "a",
                              {
                                staticClass: "delete-button",
                                attrs: { title: "삭제" }
                              },
                              [_c("i", { staticClass: "fas fa-minus-square" })]
                            )
                          : _vm._e()
                      ])
                    ]
                  )
                })
              )
            ]
          ),
      _vm._v(" "),
      _vm.loadFlag &&
      "last_page" in _vm.pagination &&
      _vm.pagination.last_page > 1
        ? [
            _c(
              "div",
              { staticClass: "col-lg-12 col-md-12 col-sm-12 col-xs-12" },
              [
                _c("pagination", {
                  attrs: { for: _vm.fromComp, pagination: _vm.pagination }
                })
              ],
              1
            )
          ]
        : _vm._e()
    ],
    2
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-7412bf5f", module.exports)
  }
}

/***/ }),

/***/ 5:
/***/ (function(module, exports) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
module.exports = function(useSourceMap) {
	var list = [];

	// return the list of modules as css string
	list.toString = function toString() {
		return this.map(function (item) {
			var content = cssWithMappingToString(item, useSourceMap);
			if(item[2]) {
				return "@media " + item[2] + "{" + content + "}";
			} else {
				return content;
			}
		}).join("");
	};

	// import a list of modules into the list
	list.i = function(modules, mediaQuery) {
		if(typeof modules === "string")
			modules = [[null, modules, ""]];
		var alreadyImportedModules = {};
		for(var i = 0; i < this.length; i++) {
			var id = this[i][0];
			if(typeof id === "number")
				alreadyImportedModules[id] = true;
		}
		for(i = 0; i < modules.length; i++) {
			var item = modules[i];
			// skip already imported module
			// this implementation is not 100% perfect for weird media query combinations
			//  when a module is imported multiple times with different media queries.
			//  I hope this will never occur (Hey this way we have smaller bundles)
			if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
				if(mediaQuery && !item[2]) {
					item[2] = mediaQuery;
				} else if(mediaQuery) {
					item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
				}
				list.push(item);
			}
		}
	};
	return list;
};

function cssWithMappingToString(item, useSourceMap) {
	var content = item[1] || '';
	var cssMapping = item[3];
	if (!cssMapping) {
		return content;
	}

	if (useSourceMap && typeof btoa === 'function') {
		var sourceMapping = toComment(cssMapping);
		var sourceURLs = cssMapping.sources.map(function (source) {
			return '/*# sourceURL=' + cssMapping.sourceRoot + source + ' */'
		});

		return [content].concat(sourceURLs).concat([sourceMapping]).join('\n');
	}

	return [content].join('\n');
}

// Adapted from convert-source-map (MIT)
function toComment(sourceMap) {
	// eslint-disable-next-line no-undef
	var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));
	var data = 'sourceMappingURL=data:application/json;charset=utf-8;base64,' + base64;

	return '/*# ' + data + ' */';
}


/***/ }),

/***/ 521:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(522);


/***/ }),

/***/ 522:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__event_js__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__event_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__event_js__);
Vue.component('channellist', __webpack_require__(32));
Vue.component('sendmessagepopup', __webpack_require__(4));



var app = new Vue({
    el: '#app',
    data: {
        chViewOptions: {
            editBtn: false,
            deleteBtn: false,
            subscribeBtn: false,
            viewType: 'block',
            noDataCommentOn: true,
            noDataComment: '채널 리스트가 없습니다',
            gridClass: 'col-lg-4 col-md-6'
        },
        orderType: 'name'
    },
    computed: {},
    updated: function updated() {
        var self = this;

        this.$nextTick(function () {});
    },
    mounted: function mounted() {},

    methods: {
        orderBy: function orderBy(type) {
            if (this.orderType != type) {
                this.orderType = type;
                __WEBPACK_IMPORTED_MODULE_0__event_js___default.a.$emit('channelOrderByChange', this.orderType);
            }
        }
    }
});

/***/ }),

/***/ 6:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(7);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(8)("09ee0e62", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-c4573c2c\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./sendMessagePopup.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-c4573c2c\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./sendMessagePopup.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 7:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(5)(false);
// imports


// module
exports.push([module.i, "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n", ""]);

// exports


/***/ }),

/***/ 8:
/***/ (function(module, exports, __webpack_require__) {

/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
  Modified by Evan You @yyx990803
*/

var hasDocument = typeof document !== 'undefined'

if (typeof DEBUG !== 'undefined' && DEBUG) {
  if (!hasDocument) {
    throw new Error(
    'vue-style-loader cannot be used in a non-browser environment. ' +
    "Use { target: 'node' } in your Webpack config to indicate a server-rendering environment."
  ) }
}

var listToStyles = __webpack_require__(9)

/*
type StyleObject = {
  id: number;
  parts: Array<StyleObjectPart>
}

type StyleObjectPart = {
  css: string;
  media: string;
  sourceMap: ?string
}
*/

var stylesInDom = {/*
  [id: number]: {
    id: number,
    refs: number,
    parts: Array<(obj?: StyleObjectPart) => void>
  }
*/}

var head = hasDocument && (document.head || document.getElementsByTagName('head')[0])
var singletonElement = null
var singletonCounter = 0
var isProduction = false
var noop = function () {}
var options = null
var ssrIdKey = 'data-vue-ssr-id'

// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
// tags it will allow on a page
var isOldIE = typeof navigator !== 'undefined' && /msie [6-9]\b/.test(navigator.userAgent.toLowerCase())

module.exports = function (parentId, list, _isProduction, _options) {
  isProduction = _isProduction

  options = _options || {}

  var styles = listToStyles(parentId, list)
  addStylesToDom(styles)

  return function update (newList) {
    var mayRemove = []
    for (var i = 0; i < styles.length; i++) {
      var item = styles[i]
      var domStyle = stylesInDom[item.id]
      domStyle.refs--
      mayRemove.push(domStyle)
    }
    if (newList) {
      styles = listToStyles(parentId, newList)
      addStylesToDom(styles)
    } else {
      styles = []
    }
    for (var i = 0; i < mayRemove.length; i++) {
      var domStyle = mayRemove[i]
      if (domStyle.refs === 0) {
        for (var j = 0; j < domStyle.parts.length; j++) {
          domStyle.parts[j]()
        }
        delete stylesInDom[domStyle.id]
      }
    }
  }
}

function addStylesToDom (styles /* Array<StyleObject> */) {
  for (var i = 0; i < styles.length; i++) {
    var item = styles[i]
    var domStyle = stylesInDom[item.id]
    if (domStyle) {
      domStyle.refs++
      for (var j = 0; j < domStyle.parts.length; j++) {
        domStyle.parts[j](item.parts[j])
      }
      for (; j < item.parts.length; j++) {
        domStyle.parts.push(addStyle(item.parts[j]))
      }
      if (domStyle.parts.length > item.parts.length) {
        domStyle.parts.length = item.parts.length
      }
    } else {
      var parts = []
      for (var j = 0; j < item.parts.length; j++) {
        parts.push(addStyle(item.parts[j]))
      }
      stylesInDom[item.id] = { id: item.id, refs: 1, parts: parts }
    }
  }
}

function createStyleElement () {
  var styleElement = document.createElement('style')
  styleElement.type = 'text/css'
  head.appendChild(styleElement)
  return styleElement
}

function addStyle (obj /* StyleObjectPart */) {
  var update, remove
  var styleElement = document.querySelector('style[' + ssrIdKey + '~="' + obj.id + '"]')

  if (styleElement) {
    if (isProduction) {
      // has SSR styles and in production mode.
      // simply do nothing.
      return noop
    } else {
      // has SSR styles but in dev mode.
      // for some reason Chrome can't handle source map in server-rendered
      // style tags - source maps in <style> only works if the style tag is
      // created and inserted dynamically. So we remove the server rendered
      // styles and inject new ones.
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  if (isOldIE) {
    // use singleton mode for IE9.
    var styleIndex = singletonCounter++
    styleElement = singletonElement || (singletonElement = createStyleElement())
    update = applyToSingletonTag.bind(null, styleElement, styleIndex, false)
    remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true)
  } else {
    // use multi-style-tag mode in all other cases
    styleElement = createStyleElement()
    update = applyToTag.bind(null, styleElement)
    remove = function () {
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  update(obj)

  return function updateStyle (newObj /* StyleObjectPart */) {
    if (newObj) {
      if (newObj.css === obj.css &&
          newObj.media === obj.media &&
          newObj.sourceMap === obj.sourceMap) {
        return
      }
      update(obj = newObj)
    } else {
      remove()
    }
  }
}

var replaceText = (function () {
  var textStore = []

  return function (index, replacement) {
    textStore[index] = replacement
    return textStore.filter(Boolean).join('\n')
  }
})()

function applyToSingletonTag (styleElement, index, remove, obj) {
  var css = remove ? '' : obj.css

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = replaceText(index, css)
  } else {
    var cssNode = document.createTextNode(css)
    var childNodes = styleElement.childNodes
    if (childNodes[index]) styleElement.removeChild(childNodes[index])
    if (childNodes.length) {
      styleElement.insertBefore(cssNode, childNodes[index])
    } else {
      styleElement.appendChild(cssNode)
    }
  }
}

function applyToTag (styleElement, obj) {
  var css = obj.css
  var media = obj.media
  var sourceMap = obj.sourceMap

  if (media) {
    styleElement.setAttribute('media', media)
  }
  if (options.ssrId) {
    styleElement.setAttribute(ssrIdKey, obj.id)
  }

  if (sourceMap) {
    // https://developer.chrome.com/devtools/docs/javascript-debugging
    // this makes source maps inside style tags work properly in Chrome
    css += '\n/*# sourceURL=' + sourceMap.sources[0] + ' */'
    // http://stackoverflow.com/a/26603875
    css += '\n/*# sourceMappingURL=data:application/json;base64,' + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + ' */'
  }

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = css
  } else {
    while (styleElement.firstChild) {
      styleElement.removeChild(styleElement.firstChild)
    }
    styleElement.appendChild(document.createTextNode(css))
  }
}


/***/ }),

/***/ 9:
/***/ (function(module, exports) {

/**
 * Translates the list format produced by css-loader into something
 * easier to manipulate.
 */
module.exports = function listToStyles (parentId, list) {
  var styles = []
  var newStyles = {}
  for (var i = 0; i < list.length; i++) {
    var item = list[i]
    var id = item[0]
    var css = item[1]
    var media = item[2]
    var sourceMap = item[3]
    var part = {
      id: parentId + ':' + i,
      css: css,
      media: media,
      sourceMap: sourceMap
    }
    if (!newStyles[id]) {
      styles.push(newStyles[id] = { id: id, parts: [part] })
    } else {
      newStyles[id].parts.push(part)
    }
  }
  return styles
}


/***/ })

/******/ });