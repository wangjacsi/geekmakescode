/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 507);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

module.exports = new Vue();

// export default new Vue()

/***/ }),

/***/ 10:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__event_js__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__event_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__event_js__);


function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
    data: function data() {
        return {
            name: '',
            email: '',
            subject: '',
            message: '',
            errorMessage: ''
        };
    },

    props: {
        popupId: {
            type: String
        },
        userLogedin: {
            type: Boolean,
            required: true
        },
        urlPath: {
            type: String,
            required: true
        },
        toUser: {
            type: String
        }
    },
    mounted: function mounted() {},

    methods: {
        sendMessage: function () {
            var _ref = _asyncToGenerator( /*#__PURE__*/__WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default.a.mark(function _callee() {
                var validte, sendData, response, self;
                return __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default.a.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                validte = this.validate();

                                if (validte) {
                                    _context.next = 3;
                                    break;
                                }

                                return _context.abrupt('return', false);

                            case 3:
                                sendData = void 0;

                                if (this.userLogedin) {
                                    sendData = { subject: this.subject, message: this.message };
                                } else {
                                    sendData = { name: this.name, email: this.email, subject: this.subject, message: this.message };
                                }

                                _context.next = 7;
                                return axios.post(this.urlPath, sendData);

                            case 7:
                                response = _context.sent;
                                self = this;

                                this.errorMessage = '메세지 전송이 완료되었습니다';
                                this.initForm();
                                setTimeout(function () {
                                    self.errorMessage = '';
                                    $('.' + self.popupId).removeClass('open');
                                }, 3000);

                            case 12:
                            case 'end':
                                return _context.stop();
                        }
                    }
                }, _callee, this);
            }));

            function sendMessage() {
                return _ref.apply(this, arguments);
            }

            return sendMessage;
        }(),
        initForm: function initForm() {
            this.name = '';
            this.email = '';
            this.subject = '';
            this.message = '';
        },
        validate: function validate() {
            this.errorMessage = '';
            var error = false;
            if (!this.userLogedin) {
                if (this.name == '') {
                    this.errorMessage = '이름을 작성해주세요';
                    return false;
                }

                if (this.email == '') {
                    this.errorMessage = '이메일을 작성해주세요';
                    return false;
                }
            }

            if (this.subject == '') {
                this.errorMessage = '제목을 작성해주세요';
                return false;
            }

            if (this.message == '') {
                this.errorMessage = '메세지를 작성해주세요';
                return false;
            }

            return true;
        }
    }
});

/***/ }),

/***/ 11:
/***/ (function(module, exports, __webpack_require__) {

/**
 * Copyright (c) 2014-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

// This method of obtaining a reference to the global object needs to be
// kept identical to the way it is obtained in runtime.js
var g = (function() { return this })() || Function("return this")();

// Use `getOwnPropertyNames` because not all browsers support calling
// `hasOwnProperty` on the global `self` object in a worker. See #183.
var hadRuntime = g.regeneratorRuntime &&
  Object.getOwnPropertyNames(g).indexOf("regeneratorRuntime") >= 0;

// Save the old regeneratorRuntime in case it needs to be restored later.
var oldRuntime = hadRuntime && g.regeneratorRuntime;

// Force reevalutation of runtime.js.
g.regeneratorRuntime = undefined;

module.exports = __webpack_require__(12);

if (hadRuntime) {
  // Restore the original runtime.
  g.regeneratorRuntime = oldRuntime;
} else {
  // Remove the global property added by runtime.js.
  try {
    delete g.regeneratorRuntime;
  } catch(e) {
    g.regeneratorRuntime = undefined;
  }
}


/***/ }),

/***/ 12:
/***/ (function(module, exports) {

/**
 * Copyright (c) 2014-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

!(function(global) {
  "use strict";

  var Op = Object.prototype;
  var hasOwn = Op.hasOwnProperty;
  var undefined; // More compressible than void 0.
  var $Symbol = typeof Symbol === "function" ? Symbol : {};
  var iteratorSymbol = $Symbol.iterator || "@@iterator";
  var asyncIteratorSymbol = $Symbol.asyncIterator || "@@asyncIterator";
  var toStringTagSymbol = $Symbol.toStringTag || "@@toStringTag";

  var inModule = typeof module === "object";
  var runtime = global.regeneratorRuntime;
  if (runtime) {
    if (inModule) {
      // If regeneratorRuntime is defined globally and we're in a module,
      // make the exports object identical to regeneratorRuntime.
      module.exports = runtime;
    }
    // Don't bother evaluating the rest of this file if the runtime was
    // already defined globally.
    return;
  }

  // Define the runtime globally (as expected by generated code) as either
  // module.exports (if we're in a module) or a new, empty object.
  runtime = global.regeneratorRuntime = inModule ? module.exports : {};

  function wrap(innerFn, outerFn, self, tryLocsList) {
    // If outerFn provided and outerFn.prototype is a Generator, then outerFn.prototype instanceof Generator.
    var protoGenerator = outerFn && outerFn.prototype instanceof Generator ? outerFn : Generator;
    var generator = Object.create(protoGenerator.prototype);
    var context = new Context(tryLocsList || []);

    // The ._invoke method unifies the implementations of the .next,
    // .throw, and .return methods.
    generator._invoke = makeInvokeMethod(innerFn, self, context);

    return generator;
  }
  runtime.wrap = wrap;

  // Try/catch helper to minimize deoptimizations. Returns a completion
  // record like context.tryEntries[i].completion. This interface could
  // have been (and was previously) designed to take a closure to be
  // invoked without arguments, but in all the cases we care about we
  // already have an existing method we want to call, so there's no need
  // to create a new function object. We can even get away with assuming
  // the method takes exactly one argument, since that happens to be true
  // in every case, so we don't have to touch the arguments object. The
  // only additional allocation required is the completion record, which
  // has a stable shape and so hopefully should be cheap to allocate.
  function tryCatch(fn, obj, arg) {
    try {
      return { type: "normal", arg: fn.call(obj, arg) };
    } catch (err) {
      return { type: "throw", arg: err };
    }
  }

  var GenStateSuspendedStart = "suspendedStart";
  var GenStateSuspendedYield = "suspendedYield";
  var GenStateExecuting = "executing";
  var GenStateCompleted = "completed";

  // Returning this object from the innerFn has the same effect as
  // breaking out of the dispatch switch statement.
  var ContinueSentinel = {};

  // Dummy constructor functions that we use as the .constructor and
  // .constructor.prototype properties for functions that return Generator
  // objects. For full spec compliance, you may wish to configure your
  // minifier not to mangle the names of these two functions.
  function Generator() {}
  function GeneratorFunction() {}
  function GeneratorFunctionPrototype() {}

  // This is a polyfill for %IteratorPrototype% for environments that
  // don't natively support it.
  var IteratorPrototype = {};
  IteratorPrototype[iteratorSymbol] = function () {
    return this;
  };

  var getProto = Object.getPrototypeOf;
  var NativeIteratorPrototype = getProto && getProto(getProto(values([])));
  if (NativeIteratorPrototype &&
      NativeIteratorPrototype !== Op &&
      hasOwn.call(NativeIteratorPrototype, iteratorSymbol)) {
    // This environment has a native %IteratorPrototype%; use it instead
    // of the polyfill.
    IteratorPrototype = NativeIteratorPrototype;
  }

  var Gp = GeneratorFunctionPrototype.prototype =
    Generator.prototype = Object.create(IteratorPrototype);
  GeneratorFunction.prototype = Gp.constructor = GeneratorFunctionPrototype;
  GeneratorFunctionPrototype.constructor = GeneratorFunction;
  GeneratorFunctionPrototype[toStringTagSymbol] =
    GeneratorFunction.displayName = "GeneratorFunction";

  // Helper for defining the .next, .throw, and .return methods of the
  // Iterator interface in terms of a single ._invoke method.
  function defineIteratorMethods(prototype) {
    ["next", "throw", "return"].forEach(function(method) {
      prototype[method] = function(arg) {
        return this._invoke(method, arg);
      };
    });
  }

  runtime.isGeneratorFunction = function(genFun) {
    var ctor = typeof genFun === "function" && genFun.constructor;
    return ctor
      ? ctor === GeneratorFunction ||
        // For the native GeneratorFunction constructor, the best we can
        // do is to check its .name property.
        (ctor.displayName || ctor.name) === "GeneratorFunction"
      : false;
  };

  runtime.mark = function(genFun) {
    if (Object.setPrototypeOf) {
      Object.setPrototypeOf(genFun, GeneratorFunctionPrototype);
    } else {
      genFun.__proto__ = GeneratorFunctionPrototype;
      if (!(toStringTagSymbol in genFun)) {
        genFun[toStringTagSymbol] = "GeneratorFunction";
      }
    }
    genFun.prototype = Object.create(Gp);
    return genFun;
  };

  // Within the body of any async function, `await x` is transformed to
  // `yield regeneratorRuntime.awrap(x)`, so that the runtime can test
  // `hasOwn.call(value, "__await")` to determine if the yielded value is
  // meant to be awaited.
  runtime.awrap = function(arg) {
    return { __await: arg };
  };

  function AsyncIterator(generator) {
    function invoke(method, arg, resolve, reject) {
      var record = tryCatch(generator[method], generator, arg);
      if (record.type === "throw") {
        reject(record.arg);
      } else {
        var result = record.arg;
        var value = result.value;
        if (value &&
            typeof value === "object" &&
            hasOwn.call(value, "__await")) {
          return Promise.resolve(value.__await).then(function(value) {
            invoke("next", value, resolve, reject);
          }, function(err) {
            invoke("throw", err, resolve, reject);
          });
        }

        return Promise.resolve(value).then(function(unwrapped) {
          // When a yielded Promise is resolved, its final value becomes
          // the .value of the Promise<{value,done}> result for the
          // current iteration. If the Promise is rejected, however, the
          // result for this iteration will be rejected with the same
          // reason. Note that rejections of yielded Promises are not
          // thrown back into the generator function, as is the case
          // when an awaited Promise is rejected. This difference in
          // behavior between yield and await is important, because it
          // allows the consumer to decide what to do with the yielded
          // rejection (swallow it and continue, manually .throw it back
          // into the generator, abandon iteration, whatever). With
          // await, by contrast, there is no opportunity to examine the
          // rejection reason outside the generator function, so the
          // only option is to throw it from the await expression, and
          // let the generator function handle the exception.
          result.value = unwrapped;
          resolve(result);
        }, reject);
      }
    }

    var previousPromise;

    function enqueue(method, arg) {
      function callInvokeWithMethodAndArg() {
        return new Promise(function(resolve, reject) {
          invoke(method, arg, resolve, reject);
        });
      }

      return previousPromise =
        // If enqueue has been called before, then we want to wait until
        // all previous Promises have been resolved before calling invoke,
        // so that results are always delivered in the correct order. If
        // enqueue has not been called before, then it is important to
        // call invoke immediately, without waiting on a callback to fire,
        // so that the async generator function has the opportunity to do
        // any necessary setup in a predictable way. This predictability
        // is why the Promise constructor synchronously invokes its
        // executor callback, and why async functions synchronously
        // execute code before the first await. Since we implement simple
        // async functions in terms of async generators, it is especially
        // important to get this right, even though it requires care.
        previousPromise ? previousPromise.then(
          callInvokeWithMethodAndArg,
          // Avoid propagating failures to Promises returned by later
          // invocations of the iterator.
          callInvokeWithMethodAndArg
        ) : callInvokeWithMethodAndArg();
    }

    // Define the unified helper method that is used to implement .next,
    // .throw, and .return (see defineIteratorMethods).
    this._invoke = enqueue;
  }

  defineIteratorMethods(AsyncIterator.prototype);
  AsyncIterator.prototype[asyncIteratorSymbol] = function () {
    return this;
  };
  runtime.AsyncIterator = AsyncIterator;

  // Note that simple async functions are implemented on top of
  // AsyncIterator objects; they just return a Promise for the value of
  // the final result produced by the iterator.
  runtime.async = function(innerFn, outerFn, self, tryLocsList) {
    var iter = new AsyncIterator(
      wrap(innerFn, outerFn, self, tryLocsList)
    );

    return runtime.isGeneratorFunction(outerFn)
      ? iter // If outerFn is a generator, return the full iterator.
      : iter.next().then(function(result) {
          return result.done ? result.value : iter.next();
        });
  };

  function makeInvokeMethod(innerFn, self, context) {
    var state = GenStateSuspendedStart;

    return function invoke(method, arg) {
      if (state === GenStateExecuting) {
        throw new Error("Generator is already running");
      }

      if (state === GenStateCompleted) {
        if (method === "throw") {
          throw arg;
        }

        // Be forgiving, per 25.3.3.3.3 of the spec:
        // https://people.mozilla.org/~jorendorff/es6-draft.html#sec-generatorresume
        return doneResult();
      }

      context.method = method;
      context.arg = arg;

      while (true) {
        var delegate = context.delegate;
        if (delegate) {
          var delegateResult = maybeInvokeDelegate(delegate, context);
          if (delegateResult) {
            if (delegateResult === ContinueSentinel) continue;
            return delegateResult;
          }
        }

        if (context.method === "next") {
          // Setting context._sent for legacy support of Babel's
          // function.sent implementation.
          context.sent = context._sent = context.arg;

        } else if (context.method === "throw") {
          if (state === GenStateSuspendedStart) {
            state = GenStateCompleted;
            throw context.arg;
          }

          context.dispatchException(context.arg);

        } else if (context.method === "return") {
          context.abrupt("return", context.arg);
        }

        state = GenStateExecuting;

        var record = tryCatch(innerFn, self, context);
        if (record.type === "normal") {
          // If an exception is thrown from innerFn, we leave state ===
          // GenStateExecuting and loop back for another invocation.
          state = context.done
            ? GenStateCompleted
            : GenStateSuspendedYield;

          if (record.arg === ContinueSentinel) {
            continue;
          }

          return {
            value: record.arg,
            done: context.done
          };

        } else if (record.type === "throw") {
          state = GenStateCompleted;
          // Dispatch the exception by looping back around to the
          // context.dispatchException(context.arg) call above.
          context.method = "throw";
          context.arg = record.arg;
        }
      }
    };
  }

  // Call delegate.iterator[context.method](context.arg) and handle the
  // result, either by returning a { value, done } result from the
  // delegate iterator, or by modifying context.method and context.arg,
  // setting context.delegate to null, and returning the ContinueSentinel.
  function maybeInvokeDelegate(delegate, context) {
    var method = delegate.iterator[context.method];
    if (method === undefined) {
      // A .throw or .return when the delegate iterator has no .throw
      // method always terminates the yield* loop.
      context.delegate = null;

      if (context.method === "throw") {
        if (delegate.iterator.return) {
          // If the delegate iterator has a return method, give it a
          // chance to clean up.
          context.method = "return";
          context.arg = undefined;
          maybeInvokeDelegate(delegate, context);

          if (context.method === "throw") {
            // If maybeInvokeDelegate(context) changed context.method from
            // "return" to "throw", let that override the TypeError below.
            return ContinueSentinel;
          }
        }

        context.method = "throw";
        context.arg = new TypeError(
          "The iterator does not provide a 'throw' method");
      }

      return ContinueSentinel;
    }

    var record = tryCatch(method, delegate.iterator, context.arg);

    if (record.type === "throw") {
      context.method = "throw";
      context.arg = record.arg;
      context.delegate = null;
      return ContinueSentinel;
    }

    var info = record.arg;

    if (! info) {
      context.method = "throw";
      context.arg = new TypeError("iterator result is not an object");
      context.delegate = null;
      return ContinueSentinel;
    }

    if (info.done) {
      // Assign the result of the finished delegate to the temporary
      // variable specified by delegate.resultName (see delegateYield).
      context[delegate.resultName] = info.value;

      // Resume execution at the desired location (see delegateYield).
      context.next = delegate.nextLoc;

      // If context.method was "throw" but the delegate handled the
      // exception, let the outer generator proceed normally. If
      // context.method was "next", forget context.arg since it has been
      // "consumed" by the delegate iterator. If context.method was
      // "return", allow the original .return call to continue in the
      // outer generator.
      if (context.method !== "return") {
        context.method = "next";
        context.arg = undefined;
      }

    } else {
      // Re-yield the result returned by the delegate method.
      return info;
    }

    // The delegate iterator is finished, so forget it and continue with
    // the outer generator.
    context.delegate = null;
    return ContinueSentinel;
  }

  // Define Generator.prototype.{next,throw,return} in terms of the
  // unified ._invoke helper method.
  defineIteratorMethods(Gp);

  Gp[toStringTagSymbol] = "Generator";

  // A Generator should always return itself as the iterator object when the
  // @@iterator function is called on it. Some browsers' implementations of the
  // iterator prototype chain incorrectly implement this, causing the Generator
  // object to not be returned from this call. This ensures that doesn't happen.
  // See https://github.com/facebook/regenerator/issues/274 for more details.
  Gp[iteratorSymbol] = function() {
    return this;
  };

  Gp.toString = function() {
    return "[object Generator]";
  };

  function pushTryEntry(locs) {
    var entry = { tryLoc: locs[0] };

    if (1 in locs) {
      entry.catchLoc = locs[1];
    }

    if (2 in locs) {
      entry.finallyLoc = locs[2];
      entry.afterLoc = locs[3];
    }

    this.tryEntries.push(entry);
  }

  function resetTryEntry(entry) {
    var record = entry.completion || {};
    record.type = "normal";
    delete record.arg;
    entry.completion = record;
  }

  function Context(tryLocsList) {
    // The root entry object (effectively a try statement without a catch
    // or a finally block) gives us a place to store values thrown from
    // locations where there is no enclosing try statement.
    this.tryEntries = [{ tryLoc: "root" }];
    tryLocsList.forEach(pushTryEntry, this);
    this.reset(true);
  }

  runtime.keys = function(object) {
    var keys = [];
    for (var key in object) {
      keys.push(key);
    }
    keys.reverse();

    // Rather than returning an object with a next method, we keep
    // things simple and return the next function itself.
    return function next() {
      while (keys.length) {
        var key = keys.pop();
        if (key in object) {
          next.value = key;
          next.done = false;
          return next;
        }
      }

      // To avoid creating an additional object, we just hang the .value
      // and .done properties off the next function object itself. This
      // also ensures that the minifier will not anonymize the function.
      next.done = true;
      return next;
    };
  };

  function values(iterable) {
    if (iterable) {
      var iteratorMethod = iterable[iteratorSymbol];
      if (iteratorMethod) {
        return iteratorMethod.call(iterable);
      }

      if (typeof iterable.next === "function") {
        return iterable;
      }

      if (!isNaN(iterable.length)) {
        var i = -1, next = function next() {
          while (++i < iterable.length) {
            if (hasOwn.call(iterable, i)) {
              next.value = iterable[i];
              next.done = false;
              return next;
            }
          }

          next.value = undefined;
          next.done = true;

          return next;
        };

        return next.next = next;
      }
    }

    // Return an iterator with no values.
    return { next: doneResult };
  }
  runtime.values = values;

  function doneResult() {
    return { value: undefined, done: true };
  }

  Context.prototype = {
    constructor: Context,

    reset: function(skipTempReset) {
      this.prev = 0;
      this.next = 0;
      // Resetting context._sent for legacy support of Babel's
      // function.sent implementation.
      this.sent = this._sent = undefined;
      this.done = false;
      this.delegate = null;

      this.method = "next";
      this.arg = undefined;

      this.tryEntries.forEach(resetTryEntry);

      if (!skipTempReset) {
        for (var name in this) {
          // Not sure about the optimal order of these conditions:
          if (name.charAt(0) === "t" &&
              hasOwn.call(this, name) &&
              !isNaN(+name.slice(1))) {
            this[name] = undefined;
          }
        }
      }
    },

    stop: function() {
      this.done = true;

      var rootEntry = this.tryEntries[0];
      var rootRecord = rootEntry.completion;
      if (rootRecord.type === "throw") {
        throw rootRecord.arg;
      }

      return this.rval;
    },

    dispatchException: function(exception) {
      if (this.done) {
        throw exception;
      }

      var context = this;
      function handle(loc, caught) {
        record.type = "throw";
        record.arg = exception;
        context.next = loc;

        if (caught) {
          // If the dispatched exception was caught by a catch block,
          // then let that catch block handle the exception normally.
          context.method = "next";
          context.arg = undefined;
        }

        return !! caught;
      }

      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        var record = entry.completion;

        if (entry.tryLoc === "root") {
          // Exception thrown outside of any try block that could handle
          // it, so set the completion value of the entire function to
          // throw the exception.
          return handle("end");
        }

        if (entry.tryLoc <= this.prev) {
          var hasCatch = hasOwn.call(entry, "catchLoc");
          var hasFinally = hasOwn.call(entry, "finallyLoc");

          if (hasCatch && hasFinally) {
            if (this.prev < entry.catchLoc) {
              return handle(entry.catchLoc, true);
            } else if (this.prev < entry.finallyLoc) {
              return handle(entry.finallyLoc);
            }

          } else if (hasCatch) {
            if (this.prev < entry.catchLoc) {
              return handle(entry.catchLoc, true);
            }

          } else if (hasFinally) {
            if (this.prev < entry.finallyLoc) {
              return handle(entry.finallyLoc);
            }

          } else {
            throw new Error("try statement without catch or finally");
          }
        }
      }
    },

    abrupt: function(type, arg) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.tryLoc <= this.prev &&
            hasOwn.call(entry, "finallyLoc") &&
            this.prev < entry.finallyLoc) {
          var finallyEntry = entry;
          break;
        }
      }

      if (finallyEntry &&
          (type === "break" ||
           type === "continue") &&
          finallyEntry.tryLoc <= arg &&
          arg <= finallyEntry.finallyLoc) {
        // Ignore the finally entry if control is not jumping to a
        // location outside the try/catch block.
        finallyEntry = null;
      }

      var record = finallyEntry ? finallyEntry.completion : {};
      record.type = type;
      record.arg = arg;

      if (finallyEntry) {
        this.method = "next";
        this.next = finallyEntry.finallyLoc;
        return ContinueSentinel;
      }

      return this.complete(record);
    },

    complete: function(record, afterLoc) {
      if (record.type === "throw") {
        throw record.arg;
      }

      if (record.type === "break" ||
          record.type === "continue") {
        this.next = record.arg;
      } else if (record.type === "return") {
        this.rval = this.arg = record.arg;
        this.method = "return";
        this.next = "end";
      } else if (record.type === "normal" && afterLoc) {
        this.next = afterLoc;
      }

      return ContinueSentinel;
    },

    finish: function(finallyLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.finallyLoc === finallyLoc) {
          this.complete(entry.completion, entry.afterLoc);
          resetTryEntry(entry);
          return ContinueSentinel;
        }
      }
    },

    "catch": function(tryLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.tryLoc === tryLoc) {
          var record = entry.completion;
          if (record.type === "throw") {
            var thrown = record.arg;
            resetTryEntry(entry);
          }
          return thrown;
        }
      }

      // The context.catch method must only be called with a location
      // argument that corresponds to a known catch block.
      throw new Error("illegal catch attempt");
    },

    delegateYield: function(iterable, resultName, nextLoc) {
      this.delegate = {
        iterator: values(iterable),
        resultName: resultName,
        nextLoc: nextLoc
      };

      if (this.method === "next") {
        // Deliberately forget the last sent value so that we don't
        // accidentally pass it on to the delegate.
        this.arg = undefined;
      }

      return ContinueSentinel;
    }
  };
})(
  // In sloppy mode, unbound `this` refers to the global object, fallback to
  // Function constructor if we're in global strict mode. That is sadly a form
  // of indirect eval which violates Content Security Policy.
  (function() { return this })() || Function("return this")()
);


/***/ }),

/***/ 13:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "window-popup", class: _vm.popupId }, [
    _c(
      "a",
      {
        staticClass: "popup-close js-popup-close cd-nav-trigger",
        attrs: { href: "#" }
      },
      [
        _c("svg", { staticClass: "utouch-icon utouch-icon-cancel-1" }, [
          _c("use", { attrs: { "xlink:href": "#utouch-icon-cancel-1" } })
        ])
      ]
    ),
    _vm._v(" "),
    _c("div", { staticClass: "send-message-popup" }, [
      _c("h5", [_vm._v("Send a Message")]),
      _vm._v(" "),
      _c("form", { staticClass: "contact-form" }, [
        !_vm.userLogedin
          ? _c("div", {}, [
              _c("div", { staticClass: "with-icon" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model.trim",
                      value: _vm.name,
                      expression: "name",
                      modifiers: { trim: true }
                    }
                  ],
                  attrs: {
                    name: "name",
                    placeholder: "Your Name",
                    type: "text",
                    required: "required"
                  },
                  domProps: { value: _vm.name },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.name = $event.target.value.trim()
                    },
                    blur: function($event) {
                      _vm.$forceUpdate()
                    }
                  }
                }),
                _vm._v(" "),
                _c("svg", { staticClass: "utouch-icon utouch-icon-user" }, [
                  _c("use", { attrs: { "xlink:href": "#utouch-icon-user" } })
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "with-icon" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model.trim",
                      value: _vm.email,
                      expression: "email",
                      modifiers: { trim: true }
                    }
                  ],
                  attrs: {
                    name: "email",
                    placeholder: "Email Adress",
                    type: "text",
                    required: "required"
                  },
                  domProps: { value: _vm.email },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.email = $event.target.value.trim()
                    },
                    blur: function($event) {
                      _vm.$forceUpdate()
                    }
                  }
                }),
                _vm._v(" "),
                _c(
                  "svg",
                  {
                    staticClass:
                      "utouch-icon utouch-icon-message-closed-envelope-1"
                  },
                  [
                    _c("use", {
                      attrs: {
                        "xlink:href": "#utouch-icon-message-closed-envelope-1"
                      }
                    })
                  ]
                )
              ])
            ])
          : _vm._e(),
        _vm._v(" "),
        _c("div", { staticClass: "with-icon" }, [
          _c("input", {
            directives: [
              {
                name: "model",
                rawName: "v-model.trim",
                value: _vm.subject,
                expression: "subject",
                modifiers: { trim: true }
              }
            ],
            staticClass: "with-icon",
            attrs: {
              name: "subject",
              placeholder: "Subject",
              type: "text",
              required: "required"
            },
            domProps: { value: _vm.subject },
            on: {
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.subject = $event.target.value.trim()
              },
              blur: function($event) {
                _vm.$forceUpdate()
              }
            }
          }),
          _vm._v(" "),
          _c("svg", { staticClass: "utouch-icon utouch-icon-icon-1" }, [
            _c("use", { attrs: { "xlink:href": "#utouch-icon-icon-1" } })
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "with-icon" }, [
          _c("textarea", {
            directives: [
              {
                name: "model",
                rawName: "v-model.trim",
                value: _vm.message,
                expression: "message",
                modifiers: { trim: true }
              }
            ],
            staticStyle: { height: "180px" },
            attrs: {
              name: "message",
              required: "",
              placeholder: "Your Message"
            },
            domProps: { value: _vm.message },
            on: {
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.message = $event.target.value.trim()
              },
              blur: function($event) {
                _vm.$forceUpdate()
              }
            }
          }),
          _vm._v(" "),
          _c("svg", { staticClass: "utouch-icon utouch-icon-edit" }, [
            _c("use", { attrs: { "xlink:href": "#utouch-icon-edit" } })
          ])
        ]),
        _vm._v(" "),
        _vm._m(0),
        _vm._v(" "),
        _c(
          "button",
          {
            staticClass: "btn btn--green btn--with-shadow full-width",
            on: {
              click: function($event) {
                $event.preventDefault()
                return _vm.sendMessage($event)
              }
            }
          },
          [_vm._v("\n\t\t\t\t메세지 전송\n\t\t\t")]
        ),
        _vm._v(" "),
        this.errorMessage != ""
          ? _c(
              "div",
              {
                staticClass: "summit-message",
                staticStyle: { "margin-top": "20px" }
              },
              [
                _c("p", {
                  staticClass: "summit-error",
                  domProps: { innerHTML: _vm._s(this.errorMessage) }
                })
              ]
            )
          : _vm._e()
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      {
        staticStyle: {
          "text-align": "initial",
          "font-size": "13px",
          "margin-bottom": "20px"
        }
      },
      [
        _vm._v("마크다운(Markdown "),
        _c("i", { staticClass: "fab fa-markdown" }),
        _vm._v(
          ") 지원합니다. # ~ ######: 헤더 h1 ~ h6, >: 인용 Blockquotes, ~~~ 코드블록 ~~~, ` 인라인 코드 블럭 `, * 기울여쓰기(italic) *, ** 굵게쓰기(bold) **, --- 수평선, [Google](http://www.google.co.kr “구글”) : 인라인 링크, <http://google.com/>: URl 링크, 1. list item : 리스트, * list item : 리스트, ![alt text](image_URL): 링크 이미지\n            "
        )
      ]
    )
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-c4573c2c", module.exports)
  }
}

/***/ }),

/***/ 14:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(16)
/* template */
var __vue_template__ = __webpack_require__(17)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/components/Pagination.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-3895afde", Component.options)
  } else {
    hotAPI.reload("data-v-3895afde", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 16:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__event_js__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__event_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__event_js__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
    props: {
        pagination: {
            type: Object,
            required: true
        },
        for: {
            type: String,
            default: 'default'
        }
    },
    data: function data() {
        return {};
    },
    mounted: function mounted() {},

    methods: {
        pageChange: function pageChange(page) {
            //console.log(page)
            if (page === parseInt(this.pagination.current_page) - 4 || page === parseInt(this.pagination.current_page) + 4 || page == this.pagination.current_page) {
                return;
            } else if (page == 'next') {
                console.log(this.for);
                __WEBPACK_IMPORTED_MODULE_0__event_js___default.a.$emit(this.for + '-pagination', this.pagination.current_page + 1, this.pagination.path);
            } else if (page == 'prev') {
                console.log(this.for);
                __WEBPACK_IMPORTED_MODULE_0__event_js___default.a.$emit(this.for + '-pagination', this.pagination.current_page - 1, this.pagination.path);
            } else {
                console.log(this.for);
                __WEBPACK_IMPORTED_MODULE_0__event_js___default.a.$emit(this.for + '-pagination', page, this.pagination.path);
            }
        }
    }
});

/***/ }),

/***/ 17:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.pagination.last_page > 1
    ? _c(
        "nav",
        { staticClass: "navigation", attrs: { role: "navigation" } },
        [
          _vm.pagination.current_page <= 1
            ? _c(
                "a",
                {
                  staticClass: "disabled page-numbers",
                  attrs: {
                    "aria-aria-disabled": "true",
                    "aria-label": "previous"
                  }
                },
                [_c("span", { staticClass: "xi-angle-left" })]
              )
            : _c(
                "a",
                {
                  staticClass: "page-numbers",
                  attrs: { rel: "prev", "aria-label": "previous" },
                  on: {
                    click: function($event) {
                      $event.preventDefault()
                      _vm.pageChange("prev")
                    }
                  }
                },
                [_c("span", { staticClass: "xi-angle-left" })]
              ),
          _vm._v(" "),
          _vm.pagination.current_page > 0
            ? _vm._l(parseInt(_vm.pagination.last_page), function(page) {
                return page === 1 ||
                  page === _vm.pagination.last_page ||
                  (page > parseInt(_vm.pagination.current_page) - 5 &&
                    page < parseInt(_vm.pagination.current_page) + 5)
                  ? _c(
                      "a",
                      {
                        key: page,
                        staticClass: "page-numbers",
                        class: {
                          "current disabled":
                            _vm.pagination.current_page === page,
                          disabled:
                            page ===
                              parseInt(_vm.pagination.current_page) - 4 ||
                            page === parseInt(_vm.pagination.current_page) + 4
                        },
                        on: {
                          click: function($event) {
                            $event.preventDefault()
                            _vm.pageChange(page)
                          }
                        }
                      },
                      [
                        page === parseInt(_vm.pagination.current_page) - 4 ||
                        page === parseInt(_vm.pagination.current_page) + 4
                          ? _c("span", [_vm._v("...")])
                          : _c("span", [_vm._v(_vm._s(page))])
                      ]
                    )
                  : _vm._e()
              })
            : _vm._e(),
          _vm._v(" "),
          _vm.pagination.current_page < _vm.pagination.last_page
            ? _c(
                "a",
                {
                  staticClass: "page-numbers",
                  attrs: { rel: "next", "aria-label": "next" },
                  on: {
                    click: function($event) {
                      $event.preventDefault()
                      _vm.pageChange("next")
                    }
                  }
                },
                [_c("span", { staticClass: "xi-angle-right" })]
              )
            : _c(
                "a",
                {
                  staticClass: "page-numbers disabled",
                  attrs: { "aria-disabled": "true", "aria-label": "next" }
                },
                [_c("span", { staticClass: "xi-angle-right" })]
              )
        ],
        2
      )
    : _vm._e()
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-3895afde", module.exports)
  }
}

/***/ }),

/***/ 19:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _vueFroala = __webpack_require__(20);

var _vueFroala2 = _interopRequireDefault(_vueFroala);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _vueFroala2.default;

/***/ }),

/***/ 2:
/***/ (function(module, exports) {

/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file.
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier /* server only */
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = injectStyles
  }

  if (hook) {
    var functional = options.functional
    var existing = functional
      ? options.render
      : options.beforeCreate

    if (!functional) {
      // inject component registration as beforeCreate hook
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    } else {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functioal component in vue file
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return existing(h, context)
      }
    }
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 20:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _stringify = __webpack_require__(21);

var _stringify2 = _interopRequireDefault(_stringify);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (Vue) {
  var Options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};


  var froalaEditorFunctionality = {

    props: ['tag', 'value', 'config', 'onManualControllerReady'],

    watch: {
      value: function value() {
        this.model = this.value;
        this.updateValue();
      }
    },

    render: function render(createElement) {
      return createElement(this.currentTag, [this.$slots.default]);
    },

    created: function created() {
      this.currentTag = this.tag || this.currentTag;
      this.model = this.value;
    },

    mounted: function mounted() {
      if (this.SPECIAL_TAGS.indexOf(this.currentTag) != -1) {

        this.hasSpecialTag = true;
      }

      if (this.onManualControllerReady) {
        this.generateManualController();
      } else {
        this.createEditor();
      }
    },

    beforeDestroy: function beforeDestroy() {
      this.destroyEditor();
    },

    data: function data() {

      return {
        currentTag: 'div',
        listeningEvents: [],

        _$element: null,

        _$editor: null,

        currentConfig: null,

        defaultConfig: {
          immediateVueModelUpdate: false,
          vueIgnoreAttrs: null
        },

        editorInitialized: false,

        SPECIAL_TAGS: ['img', 'button', 'input', 'a'],
        INNER_HTML_ATTR: 'innerHTML',
        hasSpecialTag: false,

        model: null,
        oldModel: null
      };
    },
    methods: {
      updateValue: function updateValue() {
        if ((0, _stringify2.default)(this.oldModel) == (0, _stringify2.default)(this.model)) {
          return;
        }

        this.setContent();
      },

      createEditor: function createEditor() {

        if (this.editorInitialized) {
          return;
        }

        this.currentConfig = this.config || this.defaultConfig;

        this._$element = $(this.$el);

        this.setContent(true);

        this.registerEvents();
        this._$editor = this._$element.froalaEditor(this.currentConfig).data('froala.editor').$el;
        this.initListeners();

        this.editorInitialized = true;
      },

      setContent: function setContent(firstTime) {

        if (!this.editorInitialized && !firstTime) {
          return;
        }

        if (this.model || this.model == '') {

          this.oldModel = this.model;

          if (this.hasSpecialTag) {
            this.setSpecialTagContent();
          } else {
            this.setNormalTagContent(firstTime);
          }
        }
      },

      setNormalTagContent: function setNormalTagContent(firstTime) {

        var self = this;

        function htmlSet() {

          self._$element.froalaEditor('html.set', self.model || '', true);

          self._$element.froalaEditor('undo.reset');
          self._$element.froalaEditor('undo.saveStep');
        }

        if (firstTime) {
          this.registerEvent(this._$element, 'froalaEditor.initialized', function () {
            htmlSet();
          });
        } else {
          htmlSet();
        }
      },

      setSpecialTagContent: function setSpecialTagContent() {

        var tags = this.model;

        if (tags) {

          for (var attr in tags) {
            if (tags.hasOwnProperty(attr) && attr != this.INNER_HTML_ATTR) {
              this._$element.attr(attr, tags[attr]);
            }
          }

          if (tags.hasOwnProperty(this.INNER_HTML_ATTR)) {
            this._$element[0].innerHTML = tags[this.INNER_HTML_ATTR];
          }
        }
      },

      destroyEditor: function destroyEditor() {

        if (this._$element) {

          this.listeningEvents && this._$element.off(this.listeningEvents.join(" "));
          this._$editor.off('keyup');
          this._$element.froalaEditor('destroy');
          this.listeningEvents.length = 0;
          this._$element = null;
          this.editorInitialized = false;
        }
      },

      getEditor: function getEditor() {

        if (this._$element) {
          return this._$element.froalaEditor.bind(this._$element);
        }
        return null;
      },

      generateManualController: function generateManualController() {

        var self = this;
        var controls = {
          initialize: this.createEditor,
          destroy: this.destroyEditor,
          getEditor: this.getEditor
        };

        this.onManualControllerReady(controls);
      },

      updateModel: function updateModel() {

        var modelContent = '';

        if (this.hasSpecialTag) {

          var attributeNodes = this._$element[0].attributes;
          var attrs = {};

          for (var i = 0; i < attributeNodes.length; i++) {

            var attrName = attributeNodes[i].name;
            if (this.currentConfig.vueIgnoreAttrs && this.currentConfig.vueIgnoreAttrs.indexOf(attrName) != -1) {
              continue;
            }
            attrs[attrName] = attributeNodes[i].value;
          }

          if (this._$element[0].innerHTML) {
            attrs[this.INNER_HTML_ATTR] = this._$element[0].innerHTML;
          }

          modelContent = attrs;
        } else {

          var returnedHtml = this._$element.froalaEditor('html.get');
          if (typeof returnedHtml === 'string') {
            modelContent = returnedHtml;
          }
        }

        this.oldModel = modelContent;
        this.$emit('input', modelContent);
      },

      initListeners: function initListeners() {
        var self = this;

        this.registerEvent(this._$element, 'froalaEditor.contentChanged', function () {
          self.updateModel();
        });
        if (this.currentConfig.immediateVueModelUpdate) {
          this.registerEvent(this._$editor, 'keyup', function () {
            self.updateModel();
          });
        }
      },

      registerEvent: function registerEvent(element, eventName, callback) {

        if (!element || !eventName || !callback) {
          return;
        }

        this.listeningEvents.push(eventName);
        element.on(eventName, callback);
      },

      registerEvents: function registerEvents() {

        var events = this.currentConfig.events;
        if (!events) {
          return;
        }

        for (var event in events) {
          if (events.hasOwnProperty(event)) {
            this.registerEvent(this._$element, event, events[event]);
          }
        }
      }
    }
  };

  Vue.component('froala', froalaEditorFunctionality);

  var froalaViewFunctionality = {

    props: ['tag', 'value'],

    watch: {
      value: function value(newValue) {
        this._element.innerHTML = newValue;
      }
    },

    created: function created() {
      this.currentTag = this.tag || this.currentTag;
    },

    render: function render(createElement) {
      return createElement(this.currentTag, {
        class: 'fr-view'
      });
    },

    mounted: function mounted() {
      this._element = this.$el;

      if (this.value) {
        this._element.innerHTML = this.value;
      }
    },

    data: function data() {

      return {
        currentTag: 'div',
        _element: null
      };
    }
  };

  Vue.component('froalaView', froalaViewFunctionality);
};

/***/ }),

/***/ 21:
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(22), __esModule: true };

/***/ }),

/***/ 22:
/***/ (function(module, exports, __webpack_require__) {

var core = __webpack_require__(23);
var $JSON = core.JSON || (core.JSON = { stringify: JSON.stringify });
module.exports = function stringify(it) { // eslint-disable-line no-unused-vars
  return $JSON.stringify.apply($JSON, arguments);
};


/***/ }),

/***/ 23:
/***/ (function(module, exports) {

var core = module.exports = { version: '2.5.7' };
if (typeof __e == 'number') __e = core; // eslint-disable-line no-undef


/***/ }),

/***/ 24:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(25)
/* template */
var __vue_template__ = __webpack_require__(26)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/components/mywave/MyTutorials.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-26a76254", Component.options)
  } else {
    hotAPI.reload("data-v-26a76254", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 25:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__event_js__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__event_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__event_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Pagination__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Pagination___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__Pagination__);


function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
    data: function data() {
        return {
            tutorials: [],
            meta: null,
            links: null,
            pagination: {
                'current_page': null,
                'first': null,
                'from': null,
                'last': null,
                'last_page': null,
                'next': null,
                'path': null,
                'per_page': null,
                'prev': null,
                'to': null,
                'total': null
            },
            loadFlag: false,
            backgroundColors: null,
            progressStatus: null,
            viewOptions: {
                deleteBtn: false,
                editBtn: false,
                viewType: 'block',
                noDataCommentOn: false,
                noDataComment: '',
                gridClass: 'col-lg-6 col-md-6'
            },
            orderType: null,
            category: null
        };
    },

    computed: {},
    components: {
        Pagination: __WEBPACK_IMPORTED_MODULE_2__Pagination___default.a
    },
    props: {
        backgroundColorsProp: {
            required: true,
            type: String
        },
        progressStatusProp: {
            required: true
        },
        urlPath: {
            required: true,
            type: String
        },
        compName: {
            required: true,
            type: String
        },
        viewOptionsProp: {
            type: Object
        },
        thumbnailDefault: {
            type: String
        },
        channelLogoDefault: {
            type: String
        }
    },
    updated: function updated() {
        var self = this;
        this.$nextTick(function () {
            //console.log('this is updated')
            if (self.viewOptions.viewType == 'block') {
                $('.curriculum-event[data-mh="' + self.compName + '"]').matchHeight();
            }
        });
    },
    mounted: function mounted() {
        this.backgroundColors = JSON.parse(this.backgroundColorsProp);
        this.progressStatus = JSON.parse(this.progressStatusProp);

        var self = this;
        if (this.viewOptionsProp) {
            _.forEach(this.viewOptionsProp, function (value, key) {
                Vue.set(self.viewOptions, key, value);
            });
        }

        this.initLoadTutorials(1);

        __WEBPACK_IMPORTED_MODULE_1__event_js___default.a.$on(this.compName + '-pagination', this.pageChange);

        __WEBPACK_IMPORTED_MODULE_1__event_js___default.a.$on(this.compName + '-list-view', this.listViewChange);

        __WEBPACK_IMPORTED_MODULE_1__event_js___default.a.$on('orderByChange', this.orderByChange);

        __WEBPACK_IMPORTED_MODULE_1__event_js___default.a.$on(this.compName + '-cateroy-change', this.interestCategoryChange);
    },

    methods: {
        interestCategoryChange: function interestCategoryChange(category) {
            //console.log(category)
            this.category = category;
            this.loadTutorials(1);
        },
        orderByChange: function orderByChange(type) {
            //console.log(type)
            this.orderType = type;
            this.loadTutorials(1);
        },
        transVideoPlayTime: function transVideoPlayTime(tseconds) {
            var timeString = '';
            var hours = Math.floor(tseconds / 3600);
            var minutes = Math.floor((tseconds - hours * 3600) / 60);
            //let seconds = tseconds % 60
            if (hours > 0) {
                // hour
                timeString += hours + ' 시간 ';
            }
            if (minutes > 0) {
                timeString += minutes + ' 분';
            }
            return timeString;
        },
        listViewChange: function listViewChange(type) {
            if (type != this.viewOptions.viewType) {
                this.viewOptions.viewType = type;
            }
        },

        classObject: function classObject(index, type) {
            var color = '';
            if (type == 'background') {
                return color = 'c-' + this.backgroundColors[index % 12]['background'];
            } else if (type == 'text') {
                return color = 'c-' + this.backgroundColors[index % 12]['text'];
            } else if (type == 'overlay') {
                return color = 'overlay--' + this.backgroundColors[index % 12]['overlay'];
            }
        },
        initLoadTutorials: function () {
            var _ref = _asyncToGenerator( /*#__PURE__*/__WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default.a.mark(function _callee() {
                return __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default.a.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                _context.next = 2;
                                return this.loadTutorials(1);

                            case 2:
                            case 'end':
                                return _context.stop();
                        }
                    }
                }, _callee, this);
            }));

            function initLoadTutorials() {
                return _ref.apply(this, arguments);
            }

            return initLoadTutorials;
        }(),
        loadTutorials: function () {
            var _ref2 = _asyncToGenerator( /*#__PURE__*/__WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default.a.mark(function _callee2() {
                var page = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;
                var tutorials, url;
                return __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default.a.wrap(function _callee2$(_context2) {
                    while (1) {
                        switch (_context2.prev = _context2.next) {
                            case 0:
                                tutorials = void 0;
                                url = this.urlPath + '?page=' + page;

                                if (this.orderType) {
                                    url = url + '&order=' + this.orderType;
                                }
                                if (this.category) {
                                    url = url + '&category=' + this.category;
                                }

                                _context2.next = 6;
                                return axios.get(url);

                            case 6:
                                tutorials = _context2.sent;


                                if ('channel' in tutorials.data) {
                                    _.forEach(tutorials.data.data, function (value, key) {
                                        tutorials.data.data[key]['channel'] = tutorials.data.channel;
                                    });
                                }
                                this.tutorials = tutorials.data.data;
                                this.meta = tutorials.data.meta;
                                this.links = tutorials.data.links;

                                this.paginationSet();

                            case 12:
                            case 'end':
                                return _context2.stop();
                        }
                    }
                }, _callee2, this);
            }));

            function loadTutorials() {
                return _ref2.apply(this, arguments);
            }

            return loadTutorials;
        }(),
        paginationSet: function paginationSet() {
            Vue.set(this.pagination, 'current_page', this.meta.current_page);
            Vue.set(this.pagination, 'first', this.links.first);
            Vue.set(this.pagination, 'from', this.meta.from);
            Vue.set(this.pagination, 'last', this.links.last);
            Vue.set(this.pagination, 'last_page', this.meta.last_page);
            Vue.set(this.pagination, 'next', this.links.next);
            Vue.set(this.pagination, 'path', this.meta.path);
            Vue.set(this.pagination, 'per_page', this.meta.per_page);
            Vue.set(this.pagination, 'prev', this.links.prev);
            Vue.set(this.pagination, 'to', this.meta.to);
            Vue.set(this.pagination, 'total', this.meta.total);
            this.loadFlag = true;
        },
        pageChange: function pageChange(page, path) {
            this.loadTutorials(page);
        }
    }
});

/***/ }),

/***/ 26:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _vm.viewOptions.viewType == "block"
        ? _c(
            "div",
            {},
            [
              _vm.viewOptions.noDataCommentOn && _vm.tutorials.length == 0
                ? _c(
                    "div",
                    { staticClass: "col-lg-12 col-md-12 col-sm-12 col-xs-12" },
                    [_c("p", [_vm._v(_vm._s(_vm.viewOptions.noDataComment))])]
                  )
                : _vm._e(),
              _vm._v(" "),
              _vm._l(_vm.tutorials, function(tutorial, index) {
                return _c(
                  "div",
                  {
                    key: tutorial.id,
                    staticClass: "tutorial-item col-sm-12 col-xs-12",
                    class: _vm.viewOptions.gridClass
                  },
                  [
                    _c(
                      "div",
                      {
                        staticClass: "curriculum-event",
                        class: _vm.classObject(index, "background"),
                        attrs: { "data-mh": _vm.compName }
                      },
                      [
                        _c("div", { staticClass: "curriculum-event-thumb" }, [
                          _c("img", {
                            attrs: {
                              src: tutorial.thumbnail
                                ? tutorial.thumbnail
                                : _vm.thumbnailDefault,
                              alt: "image"
                            }
                          }),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              staticClass: "category-link",
                              class: _vm.classObject(index, "text")
                            },
                            [
                              _vm._v(
                                _vm._s(
                                  _vm.progressStatus[tutorial.progress_status]
                                )
                              )
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "curriculum-event-content" },
                            [
                              _c(
                                "div",
                                { staticClass: "author-block inline-items" },
                                [
                                  _c("div", { staticClass: "author-avatar" }, [
                                    _c("img", {
                                      attrs: {
                                        src: tutorial.channel.logo_image
                                          ? tutorial.channel.logo_image
                                          : _vm.channelLogoDefault,
                                        alt: "author"
                                      }
                                    })
                                  ]),
                                  _vm._v(" "),
                                  _c("div", { staticClass: "author-info" }, [
                                    _c("div", { staticClass: "author-prof" }, [
                                      _vm._v("Channel")
                                    ]),
                                    _vm._v(" "),
                                    _c(
                                      "a",
                                      {
                                        staticClass: "h6 author-name",
                                        attrs: {
                                          href:
                                            "/channel/" + tutorial.channel.slug
                                        }
                                      },
                                      [_vm._v(_vm._s(tutorial.channel.name))]
                                    )
                                  ])
                                ]
                              )
                            ]
                          ),
                          _vm._v(" "),
                          _c("div", {
                            staticClass: "overlay-standard",
                            class: _vm.classObject(index, "overlay")
                          }),
                          _vm._v(" "),
                          _c("div", { staticClass: "action-button-group" }, [
                            _vm.viewOptions.editBtn
                              ? _c(
                                  "a",
                                  {
                                    staticClass: "edit-button",
                                    attrs: { title: "수정" }
                                  },
                                  [
                                    _c("i", {
                                      staticClass: "fas fa-pen-square"
                                    })
                                  ]
                                )
                              : _vm._e(),
                            _vm._v(" "),
                            _vm.viewOptions.deleteBtn
                              ? _c(
                                  "a",
                                  {
                                    staticClass: "delete-button",
                                    attrs: { title: "삭제" }
                                  },
                                  [
                                    _c("i", {
                                      staticClass: "fas fa-minus-square"
                                    })
                                  ]
                                )
                              : _vm._e()
                          ])
                        ]),
                        _vm._v(" "),
                        _c(
                          "div",
                          {
                            staticClass:
                              "curriculum-event-content tutorial-info"
                          },
                          [
                            _c(
                              "div",
                              { staticClass: "icon-text-item display-flex" },
                              [
                                _c(
                                  "div",
                                  {
                                    staticClass: "text",
                                    attrs: { title: "수정일" }
                                  },
                                  [
                                    _c("i", {
                                      staticClass: "fas fa-calendar-alt"
                                    }),
                                    _vm._v(
                                      " " +
                                        _vm._s(
                                          _vm
                                            .moment(tutorial.updated_at.date)
                                            .format("MMM Do YYYY")
                                        )
                                    )
                                  ]
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              {
                                staticClass:
                                  "icon-text-item display-flex video-info"
                              },
                              [
                                _c(
                                  "div",
                                  {
                                    staticClass: "text",
                                    attrs: { title: "총 비디오 수" }
                                  },
                                  [
                                    _c("i", {
                                      staticClass: "fas fa-play-circle"
                                    }),
                                    _vm._v(
                                      " " +
                                        _vm._s(tutorial.totalVideos) +
                                        " 비디오\n                        "
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  {
                                    staticClass: "text",
                                    attrs: { title: "총 플레이 시간" }
                                  },
                                  [
                                    _c("i", { staticClass: "fas fa-clock" }),
                                    _vm._v(
                                      " " +
                                        _vm._s(
                                          _vm.transVideoPlayTime(
                                            tutorial.totalVideoPlayTime
                                          )
                                        ) +
                                        "\n                        "
                                    )
                                  ]
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              {
                                staticClass:
                                  "icon-text-item display-flex video-info"
                              },
                              [
                                _c(
                                  "div",
                                  {
                                    staticClass: "text",
                                    attrs: { title: "수강자 수" }
                                  },
                                  [
                                    _c("i", { staticClass: "fas fa-swimmer" }),
                                    _vm._v(
                                      " " +
                                        _vm._s(tutorial.totalRegisterd) +
                                        " 명\n                        "
                                    )
                                  ]
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "a",
                              {
                                staticClass: "h5 title",
                                attrs: { href: "/tutorial/" + tutorial.slug }
                              },
                              [_vm._v(_vm._s(tutorial.title))]
                            )
                          ]
                        )
                      ]
                    )
                  ]
                )
              })
            ],
            2
          )
        : _c(
            "div",
            { staticClass: "col-lg-12 col-md-12 col-sm-12 col-xs-12" },
            [
              _vm.viewOptions.noDataCommentOn && _vm.tutorials.length == 0
                ? _c("p", [_vm._v(_vm._s(_vm.viewOptions.noDataComment))])
                : _vm._e(),
              _vm._v(" "),
              _c(
                "ul",
                { staticClass: "teammember-list" },
                _vm._l(_vm.tutorials, function(tutorial) {
                  return _c(
                    "li",
                    {
                      key: tutorial.id,
                      staticClass:
                        "tutorial-item crumina-module crumina-teammembers-item teammember-item--author-in-round"
                    },
                    [
                      _c("div", { staticClass: "teammembers-thumb" }, [
                        _c("img", {
                          attrs: {
                            src: tutorial.thumbnail
                              ? tutorial.thumbnail
                              : _vm.thumbnailDefault,
                            alt: "team member"
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "teammember-content" }, [
                        _c("div", { staticClass: "teammembers-item-prof" }, [
                          _vm._v(
                            _vm._s(_vm.progressStatus[tutorial.progress_status])
                          )
                        ]),
                        _vm._v(" "),
                        _c(
                          "a",
                          {
                            staticClass: "h5 teammembers-item-name",
                            attrs: { href: "#" }
                          },
                          [
                            _vm._v(
                              _vm._s(tutorial.title) + "\n                    "
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          {
                            staticClass: "action-button-group c-gray",
                            staticStyle: {
                              top: "0",
                              right: "0",
                              "margin-bottom": "10px"
                            }
                          },
                          [
                            _vm.viewOptions.editBtn
                              ? _c(
                                  "a",
                                  {
                                    staticClass: "edit-button",
                                    attrs: { title: "수정" }
                                  },
                                  [
                                    _c("i", {
                                      staticClass: "fas fa-pen-square"
                                    })
                                  ]
                                )
                              : _vm._e(),
                            _vm._v(" "),
                            _vm.viewOptions.deleteBtn
                              ? _c(
                                  "a",
                                  {
                                    staticClass: "delete-button",
                                    attrs: { title: "삭제" }
                                  },
                                  [
                                    _c("i", {
                                      staticClass: "fas fa-minus-square"
                                    })
                                  ]
                                )
                              : _vm._e()
                          ]
                        ),
                        _vm._v(" "),
                        _c("div", { staticClass: "tutorial-updated" }, [
                          _c("i", { staticClass: "fas fa-calendar-alt" }),
                          _vm._v(
                            " " +
                              _vm._s(
                                _vm
                                  .moment(tutorial.updated_at.date)
                                  .format("MMM Do YYYY")
                              ) +
                              "\n                    "
                          )
                        ]),
                        _vm._v(" "),
                        _c("p", [
                          _c("i", { staticClass: "fas fa-play-circle" }),
                          _vm._v(
                            " " +
                              _vm._s(tutorial.totalVideos) +
                              " 비디오 \n                        "
                          ),
                          _c("i", { staticClass: "fas fa-clock" }),
                          _vm._v(
                            " " +
                              _vm._s(
                                _vm.transVideoPlayTime(
                                  tutorial.totalVideoPlayTime
                                )
                              ) +
                              " \n                        "
                          ),
                          _c("i", { staticClass: "fas fa-swimmer" }),
                          _vm._v(
                            " " +
                              _vm._s(tutorial.totalRegisterd) +
                              " 명\n                    "
                          )
                        ])
                      ])
                    ]
                  )
                })
              )
            ]
          ),
      _vm._v(" "),
      _vm.loadFlag &&
      "last_page" in _vm.pagination &&
      _vm.pagination.last_page > 1
        ? [
            _c(
              "div",
              { staticClass: "col-lg-12 col-md-12 col-sm-12 col-xs-12" },
              [
                _c("pagination", {
                  attrs: { for: _vm.compName, pagination: _vm.pagination }
                })
              ],
              1
            )
          ]
        : _vm._e()
    ],
    2
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-26a76254", module.exports)
  }
}

/***/ }),

/***/ 29:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(30)
/* template */
var __vue_template__ = __webpack_require__(31)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/components/common/Confirm.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-851b935a", Component.options)
  } else {
    hotAPI.reload("data-v-851b935a", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 3:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(11);


/***/ }),

/***/ 30:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__event_js__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__event_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__event_js__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
    data: function data() {
        return {
            content: {
                title: null,
                body: null,
                cancel: '취소',
                submit: '확인'
            },
            receivedData: null
        };
    },

    props: {
        modalId: {
            type: String
        },
        contentProp: {
            type: Object
        },
        eventEmitName: {
            type: String
        },
        eventOnName: {
            type: String
        }
    },
    mounted: function mounted() {
        var self = this;
        if (this.contentProp) {
            _.forEach(this.contentProp, function (value, key) {
                Vue.set(self.content, key, value);
            });
        }

        __WEBPACK_IMPORTED_MODULE_0__event_js___default.a.$on(this.eventOnName, this.setData);
    },

    methods: {
        setData: function setData(data) {
            this.receivedData = data;
        },
        confirm: function confirm() {
            __WEBPACK_IMPORTED_MODULE_0__event_js___default.a.$emit(this.eventEmitName, { modalID: this.modalId, data: this.receivedData });
        }
    }
});

/***/ }),

/***/ 31:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {
      staticClass: "modal fade",
      attrs: {
        tabindex: "-1",
        role: "dialog",
        id: _vm.modalId ? _vm.modalId : "confirmModal"
      }
    },
    [
      _c("div", { staticClass: "modal-dialog", attrs: { role: "document" } }, [
        _c("div", { staticClass: "modal-content" }, [
          _c("div", { staticClass: "modal-header" }, [
            _vm._m(0),
            _vm._v(" "),
            _c("h4", { staticClass: "modal-title" }, [
              _vm._v(_vm._s(_vm.content.title))
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "modal-body" }, [
            _c("p", [_vm._v(_vm._s(_vm.content.body))])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "modal-footer" }, [
            _c(
              "button",
              {
                staticClass: "btn btn-default",
                attrs: { type: "button", "data-dismiss": "modal" }
              },
              [_vm._v(_vm._s(_vm.content.cancel))]
            ),
            _vm._v(" "),
            _c(
              "button",
              {
                staticClass: "btn btn-primary",
                attrs: { type: "button" },
                on: {
                  click: function($event) {
                    $event.preventDefault()
                    return _vm.confirm($event)
                  }
                }
              },
              [_vm._v(_vm._s(_vm.content.submit))]
            )
          ])
        ])
      ])
    ]
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "button",
      {
        staticClass: "close",
        attrs: {
          type: "button",
          "data-dismiss": "modal",
          "aria-label": "Close"
        }
      },
      [_c("span", { attrs: { "aria-hidden": "true" } }, [_vm._v("×")])]
    )
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-851b935a", module.exports)
  }
}

/***/ }),

/***/ 4:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(6)
}
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(10)
/* template */
var __vue_template__ = __webpack_require__(13)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/components/common/sendMessagePopup.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-c4573c2c", Component.options)
  } else {
    hotAPI.reload("data-v-c4573c2c", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 42:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(43)
/* template */
var __vue_template__ = __webpack_require__(44)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/components/channel/UsersList.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-ad952ada", Component.options)
  } else {
    hotAPI.reload("data-v-ad952ada", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 43:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__event_js__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__event_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__event_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Pagination__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Pagination___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__Pagination__);


function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
    data: function data() {
        return {
            users: [],
            meta: null,
            links: null,
            pagination: {
                'current_page': null,
                'first': null,
                'from': null,
                'last': null,
                'last_page': null,
                'next': null,
                'path': null,
                'per_page': null,
                'prev': null,
                'to': null,
                'total': null
            },
            loadFlag: false,
            viewOptions: {
                channelAdminAdd: false,
                channelAdminDelete: false,
                channelAdminLevel: false,
                deleteBtn: false,
                noDataCommentOn: false,
                noDataComment: '',
                type: 'list'
            },
            superAdmin: false,
            superAdminList: [],
            userID: null,
            errorMessage: '',
            pageLoad: false
        };
    },

    components: {
        Pagination: __WEBPACK_IMPORTED_MODULE_2__Pagination___default.a
    },
    props: {
        userPhotoDefault: {
            required: true,
            type: String
        },
        fromComp: {
            type: String,
            required: true
        },
        urlPath: {
            required: true,
            type: String
        },
        superAdminProp: {
            type: Boolean
        },
        viewOptionsProp: {
            type: Object
        },
        channelInfoProp: {
            type: Object
        }
    },
    updated: function updated() {
        var self = this;
        this.$nextTick(function () {
            console.log('this is updated');
            if (self.viewOptions.type == 'block') {
                $('.crumina-testimonial-item[data-mh="user-block-item"]').matchHeight();
            }
        });
    },
    mounted: function mounted() {
        var self = this;
        if (this.viewOptionsProp) {
            _.forEach(this.viewOptionsProp, function (value, key) {
                Vue.set(self.viewOptions, key, value);
            });
        }

        __WEBPACK_IMPORTED_MODULE_1__event_js___default.a.$on(this.fromComp + '-pagination', this.pageChange);

        __WEBPACK_IMPORTED_MODULE_1__event_js___default.a.$on('userStore', this.userStore);

        __WEBPACK_IMPORTED_MODULE_1__event_js___default.a.$on('deleteChannelAdmin', this.deleteChannelAdmin);

        if (this.superAdminProp) {
            this.superAdmin = true;
        } else {
            this.superAdmin = false;
        }

        if (this.channelInfoProp) {
            var temp = JSON.parse(this.channelInfoProp.settings);
            this.superAdminList = temp.admins.super;
        }

        this.initLoadUsers(1);

        this.pageLoad = true;

        console.log('this ssss moounted');

        setTimeout(function () {
            if (self.viewOptions.type == 'block') {
                $('.crumina-testimonial-item[data-mh="user-block-item"]').matchHeight();
            }
        }, 2000);
    },

    methods: {
        userStore: function userStore(data) {
            data.options = JSON.parse(data.options);
            this.users.push(data);
        },
        initLoadUsers: function () {
            var _ref = _asyncToGenerator( /*#__PURE__*/__WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default.a.mark(function _callee(page) {
                return __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default.a.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                _context.next = 2;
                                return this.loadUsers(page);

                            case 2:
                            case 'end':
                                return _context.stop();
                        }
                    }
                }, _callee, this);
            }));

            function initLoadUsers(_x) {
                return _ref.apply(this, arguments);
            }

            return initLoadUsers;
        }(),
        loadUsers: function () {
            var _ref2 = _asyncToGenerator( /*#__PURE__*/__WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default.a.mark(function _callee2() {
                var page = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;
                var self, users;
                return __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default.a.wrap(function _callee2$(_context2) {
                    while (1) {
                        switch (_context2.prev = _context2.next) {
                            case 0:
                                self = this;
                                _context2.next = 3;
                                return axios.get(this.urlPath + '?without=channels&page=' + page);

                            case 3:
                                users = _context2.sent;


                                _.forEach(users.data.data, function (value, key) {
                                    value.options = JSON.parse(value.options);
                                    self.users.unshift(value);
                                });

                                this.meta = users.data.meta;
                                this.links = users.data.links;

                                this.paginationSet();

                            case 8:
                            case 'end':
                                return _context2.stop();
                        }
                    }
                }, _callee2, this);
            }));

            function loadUsers() {
                return _ref2.apply(this, arguments);
            }

            return loadUsers;
        }(),
        paginationSet: function paginationSet() {
            Vue.set(this.pagination, 'current_page', this.meta.current_page);
            Vue.set(this.pagination, 'first', this.links.first);
            Vue.set(this.pagination, 'from', this.meta.from);
            Vue.set(this.pagination, 'last', this.links.last);
            Vue.set(this.pagination, 'last_page', this.meta.last_page);
            Vue.set(this.pagination, 'next', this.links.next);
            Vue.set(this.pagination, 'path', this.meta.path);
            Vue.set(this.pagination, 'per_page', this.meta.per_page);
            Vue.set(this.pagination, 'prev', this.links.prev);
            Vue.set(this.pagination, 'to', this.meta.to);
            Vue.set(this.pagination, 'total', this.meta.total);
            this.loadFlag = true;
        },
        pageChange: function pageChange(page, path) {
            this.loadUsers(page);
        },
        deleteChannelAdminConfirm: function deleteChannelAdminConfirm(userID, index) {
            this.errorMessage = '';
            this.userID = userID;
            var self = this;
            var check = false;

            if (this.users.length < 2) {
                this.errorMessage = '채널의 관리자는 최소 최소 한명이상이어야 합니다';
                setTimeout(function () {
                    self.errorMessage = '';
                }, 5000);
                check = true;
            }
            if (check) {
                return false;
            }

            var isSuperAdmin = this.superAdminList.indexOf(userID) > -1;
            if (isSuperAdmin) {
                if (this.superAdminList.length < 2) {
                    this.errorMessage = '채널의 슈퍼관리자는 최소 한명이상이어야 합니다';
                    setTimeout(function () {
                        self.errorMessage = '';
                    }, 5000);
                    check = true;
                }
            }
            if (check) {
                return false;
            }

            $('#deleteChannelAdminConfirm').modal('show');
            __WEBPACK_IMPORTED_MODULE_1__event_js___default.a.$emit('deleteChannelAdminConfirm', { userID: userID, index: index });
        },
        deleteChannelAdmin: function () {
            var _ref3 = _asyncToGenerator( /*#__PURE__*/__WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default.a.mark(function _callee3(data) {
                var self, userID, index, response;
                return __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default.a.wrap(function _callee3$(_context3) {
                    while (1) {
                        switch (_context3.prev = _context3.next) {
                            case 0:
                                self = this;
                                userID = data.data.userID;
                                index = data.data.index;

                                $('#deleteChannelAdminConfirm').modal('hide');

                                _context3.next = 6;
                                return axios.post('/channel/' + this.channelInfoProp.slug + '/admin/delete', { userid: userID });

                            case 6:
                                response = _context3.sent;

                                if ('result' in response.data) {
                                    if (response.data.result == 'success') {
                                        this.users.splice(index, 1);
                                    }
                                } else {
                                    this.errorMessage = '시스템에 에러가 발생하였습니다. 관리자에게 문의하세요.';
                                    setTimeout(function () {
                                        self.errorMessage = '';
                                    }, 5000);
                                }

                            case 8:
                            case 'end':
                                return _context3.stop();
                        }
                    }
                }, _callee3, this);
            }));

            function deleteChannelAdmin(_x3) {
                return _ref3.apply(this, arguments);
            }

            return deleteChannelAdmin;
        }(),
        addChannelSuperAdmin: function () {
            var _ref4 = _asyncToGenerator( /*#__PURE__*/__WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default.a.mark(function _callee4(userID) {
                var isSuperAdmin, self, response, index, _response;

                return __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default.a.wrap(function _callee4$(_context4) {
                    while (1) {
                        switch (_context4.prev = _context4.next) {
                            case 0:
                                this.errorMessage = '';
                                this.userID = userID;
                                isSuperAdmin = this.superAdminList.indexOf(userID) > -1;
                                self = this;

                                if (!isSuperAdmin) {
                                    _context4.next = 16;
                                    break;
                                }

                                if (!(this.superAdminList.length > 1)) {
                                    _context4.next = 12;
                                    break;
                                }

                                _context4.next = 8;
                                return axios.post('/channel/' + this.channelInfoProp.slug + '/superadmin/delete', { userid: userID });

                            case 8:
                                response = _context4.sent;

                                if (response.data.result == 'error') {
                                    this.errorMessage = response.data.message;
                                    setTimeout(function () {
                                        self.errorMessage = '';
                                    }, 5000);
                                } else {
                                    index = this.superAdminList.indexOf(response.data.userID);

                                    if (index !== -1) this.superAdminList.splice(index, 1);
                                }
                                _context4.next = 14;
                                break;

                            case 12:
                                this.errorMessage = '채널의 슈퍼관리자는 최소 한명이상이어야 합니다';
                                setTimeout(function () {
                                    self.errorMessage = '';
                                }, 5000);

                            case 14:
                                _context4.next = 20;
                                break;

                            case 16:
                                _context4.next = 18;
                                return axios.post('/channel/' + this.channelInfoProp.slug + '/superadmin/store', { userid: userID });

                            case 18:
                                _response = _context4.sent;

                                if (_response.data.result == 'error') {
                                    this.errorMessage = _response.data.message;
                                    setTimeout(function () {
                                        self.errorMessage = '';
                                    }, 5000);
                                } else {
                                    this.superAdminList.unshift(_response.data.userID);
                                }

                            case 20:
                            case 'end':
                                return _context4.stop();
                        }
                    }
                }, _callee4, this);
            }));

            function addChannelSuperAdmin(_x4) {
                return _ref4.apply(this, arguments);
            }

            return addChannelSuperAdmin;
        }()
    }
});

/***/ }),

/***/ 44:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "row user-list" },
    [
      _vm.pageLoad && _vm.viewOptions.type == "list"
        ? _c(
            "div",
            { staticClass: "col-lg-12 col-md-12 col-sm-12 col-xs-12" },
            [
              _c(
                "ul",
                { staticClass: "teammember-list" },
                _vm._l(_vm.users, function(user, index) {
                  return _c(
                    "li",
                    {
                      key: user.id,
                      staticClass:
                        "crumina-module crumina-teammembers-item teammember-item--author-in-round",
                      staticStyle: { position: "relative" }
                    },
                    [
                      _c(
                        "div",
                        { staticClass: "user-image-thumb teammembers-thumb" },
                        [
                          _c("img", {
                            attrs: {
                              src: user.photo
                                ? user.photo
                                : _vm.userPhotoDefault,
                              alt: "team member"
                            }
                          })
                        ]
                      ),
                      _vm._v(" "),
                      _c("div", { staticClass: "teammember-content" }, [
                        user.job
                          ? _c(
                              "div",
                              { staticClass: "teammembers-item-prof" },
                              [_vm._v(_vm._s(user.job))]
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        _c(
                          "a",
                          {
                            staticClass: "h5 teammembers-item-name",
                            staticStyle: {
                              "margin-bottom": "10px",
                              "margin-top": "0"
                            },
                            attrs: { href: "/wave/" + user.slug }
                          },
                          [_vm._v(_vm._s(user.name))]
                        ),
                        _vm._v(" "),
                        user.options.viewoptions.contacts
                          ? _c("div", {}, [
                              user.options.phone
                                ? _c(
                                    "div",
                                    {
                                      staticClass: "contact-item display-flex"
                                    },
                                    [
                                      _c("i", {
                                        staticClass:
                                          "contact-icon fas fa-mobile-alt"
                                      }),
                                      _vm._v(" "),
                                      _c("span", { staticClass: "info" }, [
                                        _vm._v(_vm._s(user.options.phone))
                                      ])
                                    ]
                                  )
                                : _vm._e(),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "contact-item display-flex" },
                                [
                                  _c("i", {
                                    staticClass: "contact-icon fas fa-envelope"
                                  }),
                                  _vm._v(" "),
                                  _c("span", { staticClass: "info" }, [
                                    _vm._v(_vm._s(user.email))
                                  ])
                                ]
                              ),
                              _vm._v(" "),
                              user.options.site
                                ? _c(
                                    "div",
                                    {
                                      staticClass: "contact-item display-flex"
                                    },
                                    [
                                      _c("i", {
                                        staticClass:
                                          "fas fa-archway contact-icon"
                                      }),
                                      _vm._v(" "),
                                      _c("span", { staticClass: "info" }, [
                                        _c(
                                          "a",
                                          {
                                            attrs: {
                                              target: "_blank",
                                              href: user.options.site
                                            }
                                          },
                                          [_vm._v(_vm._s(user.options.site))]
                                        )
                                      ])
                                    ]
                                  )
                                : _vm._e()
                            ])
                          : _vm._e(),
                        _vm._v(" "),
                        user.options.viewoptions.sns
                          ? _c(
                              "div",
                              {
                                staticClass: "social-btn-group",
                                staticStyle: { "margin-top": "10px" }
                              },
                              _vm._l(user.options.sns, function(sns) {
                                return _c(
                                  "a",
                                  {
                                    staticClass: "social-icon",
                                    style: { color: sns.color },
                                    attrs: {
                                      target: "_blank",
                                      href: sns.link,
                                      title: sns.name
                                    }
                                  },
                                  [_c("span", { class: sns.class })]
                                )
                              })
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        user.options.viewoptions.tech
                          ? _c("div", { staticClass: "w-tags tech-tags" }, [
                              _c(
                                "ul",
                                { staticClass: "tags-list" },
                                _vm._l(user.options.tech, function(
                                  tech,
                                  index
                                ) {
                                  return _c("li", { key: tech.name }, [
                                    _c(
                                      "a",
                                      {
                                        attrs: { title: "레벨: " + tech.level }
                                      },
                                      [
                                        _c("i", { class: tech.class }),
                                        _vm._v(" " + _vm._s(tech.name))
                                      ]
                                    )
                                  ])
                                })
                              )
                            ])
                          : _vm._e(),
                        _vm._v(" "),
                        _vm.superAdmin && _vm.viewOptions.channelAdminLevel
                          ? _c(
                              "a",
                              {
                                staticClass: "btn btn-small btn--with-shadow",
                                class: {
                                  "btn-border c-green":
                                    _vm.superAdminList.indexOf(user.id) > -1,
                                  "btn--green":
                                    _vm.superAdminList.indexOf(user.id) == -1
                                },
                                staticStyle: { "margin-top": "20px" },
                                attrs: { title: "슈퍼 관리자 설정" },
                                on: {
                                  click: function($event) {
                                    $event.preventDefault()
                                    _vm.addChannelSuperAdmin(user.id)
                                  }
                                }
                              },
                              [
                                _c("i", {
                                  staticClass: "fas fa-user-astronaut"
                                }),
                                _vm._v(
                                  " " +
                                    _vm._s(
                                      _vm.superAdminList.indexOf(user.id) > -1
                                        ? "슈퍼관리자 해제"
                                        : "슈퍼관리자 설정"
                                    ) +
                                    "\n                    "
                                )
                              ]
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        _vm.errorMessage != "" && user.id == _vm.userID
                          ? _c(
                              "div",
                              {
                                staticClass: "summit-message",
                                staticStyle: { "margin-top": "20px" }
                              },
                              [
                                _c("p", {
                                  staticClass: "summit-error",
                                  domProps: {
                                    innerHTML: _vm._s(_vm.errorMessage)
                                  }
                                })
                              ]
                            )
                          : _vm._e()
                      ]),
                      _vm._v(" "),
                      _vm.superAdmin
                        ? _c(
                            "div",
                            { staticClass: "action-button-group c-gray" },
                            [
                              _vm.viewOptions.channelAdminAdd
                                ? _c(
                                    "a",
                                    {
                                      staticClass: "edit-button",
                                      attrs: { title: "관리자 등록" }
                                    },
                                    [
                                      _c("i", {
                                        staticClass: "fas fa-plus-square"
                                      })
                                    ]
                                  )
                                : _vm._e(),
                              _vm._v(" "),
                              _vm.viewOptions.channelAdminDelete
                                ? _c(
                                    "a",
                                    {
                                      staticClass: "delete-button",
                                      attrs: { title: "관리자 삭제" },
                                      on: {
                                        click: function($event) {
                                          $event.preventDefault()
                                          _vm.deleteChannelAdminConfirm(
                                            user.id,
                                            index
                                          )
                                        }
                                      }
                                    },
                                    [
                                      _c("i", {
                                        staticClass: "fas fa-minus-square"
                                      })
                                    ]
                                  )
                                : _vm._e()
                            ]
                          )
                        : _vm._e()
                    ]
                  )
                })
              )
            ]
          )
        : _vm.pageLoad && _vm.viewOptions.type == "block"
          ? _c(
              "div",
              {},
              _vm._l(_vm.users, function(user, index) {
                return _c(
                  "div",
                  {
                    key: user.id,
                    staticClass: "col-lg-4 col-md-6 col-sm-12 col-xs-12"
                  },
                  [
                    _c(
                      "div",
                      {
                        staticClass:
                          "crumina-module crumina-testimonial-item testimonial-item-quote-right",
                        attrs: { "data-mh": "user-block-item" }
                      },
                      [
                        _c(
                          "div",
                          {
                            staticClass:
                              "user-image-thumb testimonial-img-author"
                          },
                          [
                            _c("img", {
                              attrs: {
                                src: user.photo
                                  ? user.photo
                                  : _vm.userPhotoDefault,
                                alt: "author"
                              }
                            }),
                            _vm._v(" "),
                            _c(
                              "a",
                              {
                                staticClass: "social__item main item--small",
                                attrs: { href: "#" }
                              },
                              [
                                _c(
                                  "svg",
                                  {
                                    staticClass:
                                      "utouch-icon utouch-icon-1496680146-share"
                                  },
                                  [
                                    _c("use", {
                                      attrs: {
                                        "xlink:href":
                                          "#utouch-icon-1496680146-share"
                                      }
                                    })
                                  ]
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              {
                                staticClass:
                                  "share-product share-product--item-right"
                              },
                              [
                                user.options.viewoptions.sns
                                  ? _c(
                                      "ul",
                                      { staticClass: "socials " },
                                      _vm._l(user.options.sns, function(sns) {
                                        return _c(
                                          "li",
                                          {
                                            staticStyle: {
                                              "margin-right": "10px"
                                            }
                                          },
                                          [
                                            _c(
                                              "a",
                                              {
                                                staticClass: "social__item ttt",
                                                style: {
                                                  backgroundColor: sns.color
                                                },
                                                attrs: {
                                                  target: "_blank",
                                                  href: sns.link,
                                                  title: sns.name
                                                }
                                              },
                                              [
                                                _c("span", {
                                                  staticClass: "utouch-icon",
                                                  class: sns.class
                                                })
                                              ]
                                            )
                                          ]
                                        )
                                      })
                                    )
                                  : _vm._e()
                              ]
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c("div", { staticClass: "author-info" }, [
                          _c(
                            "a",
                            {
                              staticClass: "h6 author-name",
                              attrs: { href: "/wave/" + user.slug }
                            },
                            [_vm._v(_vm._s(user.name))]
                          ),
                          _vm._v(" "),
                          _c("div", { staticClass: "author-company" }, [
                            _vm._v(_vm._s(user.job))
                          ])
                        ]),
                        _vm._v(" "),
                        user.options.viewoptions.contacts
                          ? _c("div", {}, [
                              user.options.phone
                                ? _c(
                                    "div",
                                    {
                                      staticClass: "contact-item display-flex"
                                    },
                                    [
                                      _c("span", { staticClass: "info" }, [
                                        _c("i", {
                                          staticClass:
                                            "contact-icon fas fa-mobile-alt"
                                        }),
                                        _vm._v(_vm._s(user.options.phone))
                                      ])
                                    ]
                                  )
                                : _vm._e(),
                              _vm._v(" "),
                              _c("div", { staticClass: "contact-item " }, [
                                _c("span", { staticClass: "info" }, [
                                  _c("i", {
                                    staticClass: "contact-icon fas fa-envelope"
                                  }),
                                  _vm._v(_vm._s(user.email))
                                ])
                              ]),
                              _vm._v(" "),
                              user.options.site
                                ? _c(
                                    "div",
                                    {
                                      staticClass: "contact-item display-flex"
                                    },
                                    [
                                      _c("span", { staticClass: "info" }, [
                                        _c("i", {
                                          staticClass:
                                            "contact-icon fas fa-archway"
                                        }),
                                        _c(
                                          "a",
                                          {
                                            attrs: {
                                              target: "_blank",
                                              href: user.options.site
                                            }
                                          },
                                          [_vm._v(_vm._s(user.options.site))]
                                        )
                                      ])
                                    ]
                                  )
                                : _vm._e()
                            ])
                          : _vm._e()
                      ]
                    )
                  ]
                )
              })
            )
          : _vm._e(),
      _vm._v(" "),
      _vm.loadFlag &&
      "last_page" in _vm.pagination &&
      _vm.pagination.last_page > 1
        ? [
            _c(
              "div",
              { staticClass: "col-lg-12 col-md-12 col-sm-12 col-xs-12" },
              [
                _c("pagination", {
                  attrs: { for: _vm.fromComp, pagination: _vm.pagination }
                })
              ],
              1
            )
          ]
        : _vm._e()
    ],
    2
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-ad952ada", module.exports)
  }
}

/***/ }),

/***/ 45:
/***/ (function(module, exports, __webpack_require__) {

!function(t,e){ true?module.exports=e():"function"==typeof define&&define.amd?define([],e):"object"==typeof exports?exports.VueSelect=e():t.VueSelect=e()}(this,function(){return function(t){function e(r){if(n[r])return n[r].exports;var o=n[r]={exports:{},id:r,loaded:!1};return t[r].call(o.exports,o,o.exports,e),o.loaded=!0,o.exports}var n={};return e.m=t,e.c=n,e.p="/",e(0)}([function(t,e,n){"use strict";function r(t){return t&&t.__esModule?t:{default:t}}Object.defineProperty(e,"__esModule",{value:!0}),e.mixins=e.VueSelect=void 0;var o=n(83),i=r(o),a=n(42),s=r(a);e.default=i.default,e.VueSelect=i.default,e.mixins=s.default},function(t,e){var n=t.exports="undefined"!=typeof window&&window.Math==Math?window:"undefined"!=typeof self&&self.Math==Math?self:Function("return this")();"number"==typeof __g&&(__g=n)},function(t,e,n){t.exports=!n(9)(function(){return 7!=Object.defineProperty({},"a",{get:function(){return 7}}).a})},function(t,e){var n={}.hasOwnProperty;t.exports=function(t,e){return n.call(t,e)}},function(t,e,n){var r=n(11),o=n(33),i=n(25),a=Object.defineProperty;e.f=n(2)?Object.defineProperty:function(t,e,n){if(r(t),e=i(e,!0),r(n),o)try{return a(t,e,n)}catch(t){}if("get"in n||"set"in n)throw TypeError("Accessors not supported!");return"value"in n&&(t[e]=n.value),t}},function(t,e){var n=t.exports={version:"2.5.2"};"number"==typeof __e&&(__e=n)},function(t,e,n){var r=n(4),o=n(14);t.exports=n(2)?function(t,e,n){return r.f(t,e,o(1,n))}:function(t,e,n){return t[e]=n,t}},function(t,e,n){var r=n(59),o=n(16);t.exports=function(t){return r(o(t))}},function(t,e,n){var r=n(23)("wks"),o=n(15),i=n(1).Symbol,a="function"==typeof i,s=t.exports=function(t){return r[t]||(r[t]=a&&i[t]||(a?i:o)("Symbol."+t))};s.store=r},function(t,e){t.exports=function(t){try{return!!t()}catch(t){return!0}}},function(t,e){t.exports=function(t){return"object"==typeof t?null!==t:"function"==typeof t}},function(t,e,n){var r=n(10);t.exports=function(t){if(!r(t))throw TypeError(t+" is not an object!");return t}},function(t,e,n){var r=n(1),o=n(5),i=n(56),a=n(6),s="prototype",u=function(t,e,n){var l,c,f,p=t&u.F,d=t&u.G,h=t&u.S,b=t&u.P,v=t&u.B,g=t&u.W,y=d?o:o[e]||(o[e]={}),m=y[s],x=d?r:h?r[e]:(r[e]||{})[s];d&&(n=e);for(l in n)c=!p&&x&&void 0!==x[l],c&&l in y||(f=c?x[l]:n[l],y[l]=d&&"function"!=typeof x[l]?n[l]:v&&c?i(f,r):g&&x[l]==f?function(t){var e=function(e,n,r){if(this instanceof t){switch(arguments.length){case 0:return new t;case 1:return new t(e);case 2:return new t(e,n)}return new t(e,n,r)}return t.apply(this,arguments)};return e[s]=t[s],e}(f):b&&"function"==typeof f?i(Function.call,f):f,b&&((y.virtual||(y.virtual={}))[l]=f,t&u.R&&m&&!m[l]&&a(m,l,f)))};u.F=1,u.G=2,u.S=4,u.P=8,u.B=16,u.W=32,u.U=64,u.R=128,t.exports=u},function(t,e,n){var r=n(38),o=n(17);t.exports=Object.keys||function(t){return r(t,o)}},function(t,e){t.exports=function(t,e){return{enumerable:!(1&t),configurable:!(2&t),writable:!(4&t),value:e}}},function(t,e){var n=0,r=Math.random();t.exports=function(t){return"Symbol(".concat(void 0===t?"":t,")_",(++n+r).toString(36))}},function(t,e){t.exports=function(t){if(void 0==t)throw TypeError("Can't call method on  "+t);return t}},function(t,e){t.exports="constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf".split(",")},function(t,e){t.exports={}},function(t,e){t.exports=!0},function(t,e){e.f={}.propertyIsEnumerable},function(t,e,n){var r=n(4).f,o=n(3),i=n(8)("toStringTag");t.exports=function(t,e,n){t&&!o(t=n?t:t.prototype,i)&&r(t,i,{configurable:!0,value:e})}},function(t,e,n){var r=n(23)("keys"),o=n(15);t.exports=function(t){return r[t]||(r[t]=o(t))}},function(t,e,n){var r=n(1),o="__core-js_shared__",i=r[o]||(r[o]={});t.exports=function(t){return i[t]||(i[t]={})}},function(t,e){var n=Math.ceil,r=Math.floor;t.exports=function(t){return isNaN(t=+t)?0:(t>0?r:n)(t)}},function(t,e,n){var r=n(10);t.exports=function(t,e){if(!r(t))return t;var n,o;if(e&&"function"==typeof(n=t.toString)&&!r(o=n.call(t)))return o;if("function"==typeof(n=t.valueOf)&&!r(o=n.call(t)))return o;if(!e&&"function"==typeof(n=t.toString)&&!r(o=n.call(t)))return o;throw TypeError("Can't convert object to primitive value")}},function(t,e,n){var r=n(1),o=n(5),i=n(19),a=n(27),s=n(4).f;t.exports=function(t){var e=o.Symbol||(o.Symbol=i?{}:r.Symbol||{});"_"==t.charAt(0)||t in e||s(e,t,{value:a.f(t)})}},function(t,e,n){e.f=n(8)},function(t,e){"use strict";t.exports={props:{loading:{type:Boolean,default:!1},onSearch:{type:Function,default:function(t,e){}}},data:function(){return{mutableLoading:!1}},watch:{search:function(){this.search.length>0&&(this.onSearch(this.search,this.toggleLoading),this.$emit("search",this.search,this.toggleLoading))},loading:function(t){this.mutableLoading=t}},methods:{toggleLoading:function(){var t=arguments.length>0&&void 0!==arguments[0]?arguments[0]:null;return null==t?this.mutableLoading=!this.mutableLoading:this.mutableLoading=t}}}},function(t,e){"use strict";t.exports={watch:{typeAheadPointer:function(){this.maybeAdjustScroll()}},methods:{maybeAdjustScroll:function(){var t=this.pixelsToPointerTop(),e=this.pixelsToPointerBottom();return t<=this.viewport().top?this.scrollTo(t):e>=this.viewport().bottom?this.scrollTo(this.viewport().top+this.pointerHeight()):void 0},pixelsToPointerTop:function t(){var t=0;if(this.$refs.dropdownMenu)for(var e=0;e<this.typeAheadPointer;e++)t+=this.$refs.dropdownMenu.children[e].offsetHeight;return t},pixelsToPointerBottom:function(){return this.pixelsToPointerTop()+this.pointerHeight()},pointerHeight:function(){var t=!!this.$refs.dropdownMenu&&this.$refs.dropdownMenu.children[this.typeAheadPointer];return t?t.offsetHeight:0},viewport:function(){return{top:this.$refs.dropdownMenu?this.$refs.dropdownMenu.scrollTop:0,bottom:this.$refs.dropdownMenu?this.$refs.dropdownMenu.offsetHeight+this.$refs.dropdownMenu.scrollTop:0}},scrollTo:function(t){return this.$refs.dropdownMenu?this.$refs.dropdownMenu.scrollTop=t:null}}}},function(t,e){"use strict";t.exports={data:function(){return{typeAheadPointer:-1}},watch:{filteredOptions:function(){this.typeAheadPointer=0}},methods:{typeAheadUp:function(){this.typeAheadPointer>0&&(this.typeAheadPointer--,this.maybeAdjustScroll&&this.maybeAdjustScroll())},typeAheadDown:function(){this.typeAheadPointer<this.filteredOptions.length-1&&(this.typeAheadPointer++,this.maybeAdjustScroll&&this.maybeAdjustScroll())},typeAheadSelect:function(){this.filteredOptions[this.typeAheadPointer]?this.select(this.filteredOptions[this.typeAheadPointer]):this.taggable&&this.search.length&&this.select(this.search),this.clearSearchOnSelect&&(this.search="")}}}},function(t,e){var n={}.toString;t.exports=function(t){return n.call(t).slice(8,-1)}},function(t,e,n){var r=n(10),o=n(1).document,i=r(o)&&r(o.createElement);t.exports=function(t){return i?o.createElement(t):{}}},function(t,e,n){t.exports=!n(2)&&!n(9)(function(){return 7!=Object.defineProperty(n(32)("div"),"a",{get:function(){return 7}}).a})},function(t,e,n){"use strict";var r=n(19),o=n(12),i=n(39),a=n(6),s=n(3),u=n(18),l=n(61),c=n(21),f=n(67),p=n(8)("iterator"),d=!([].keys&&"next"in[].keys()),h="@@iterator",b="keys",v="values",g=function(){return this};t.exports=function(t,e,n,y,m,x,w){l(n,e,y);var S,O,_,j=function(t){if(!d&&t in A)return A[t];switch(t){case b:return function(){return new n(this,t)};case v:return function(){return new n(this,t)}}return function(){return new n(this,t)}},k=e+" Iterator",P=m==v,C=!1,A=t.prototype,M=A[p]||A[h]||m&&A[m],L=M||j(m),T=m?P?j("entries"):L:void 0,E="Array"==e?A.entries||M:M;if(E&&(_=f(E.call(new t)),_!==Object.prototype&&_.next&&(c(_,k,!0),r||s(_,p)||a(_,p,g))),P&&M&&M.name!==v&&(C=!0,L=function(){return M.call(this)}),r&&!w||!d&&!C&&A[p]||a(A,p,L),u[e]=L,u[k]=g,m)if(S={values:P?L:j(v),keys:x?L:j(b),entries:T},w)for(O in S)O in A||i(A,O,S[O]);else o(o.P+o.F*(d||C),e,S);return S}},function(t,e,n){var r=n(11),o=n(64),i=n(17),a=n(22)("IE_PROTO"),s=function(){},u="prototype",l=function(){var t,e=n(32)("iframe"),r=i.length,o="<",a=">";for(e.style.display="none",n(58).appendChild(e),e.src="javascript:",t=e.contentWindow.document,t.open(),t.write(o+"script"+a+"document.F=Object"+o+"/script"+a),t.close(),l=t.F;r--;)delete l[u][i[r]];return l()};t.exports=Object.create||function(t,e){var n;return null!==t?(s[u]=r(t),n=new s,s[u]=null,n[a]=t):n=l(),void 0===e?n:o(n,e)}},function(t,e,n){var r=n(38),o=n(17).concat("length","prototype");e.f=Object.getOwnPropertyNames||function(t){return r(t,o)}},function(t,e){e.f=Object.getOwnPropertySymbols},function(t,e,n){var r=n(3),o=n(7),i=n(55)(!1),a=n(22)("IE_PROTO");t.exports=function(t,e){var n,s=o(t),u=0,l=[];for(n in s)n!=a&&r(s,n)&&l.push(n);for(;e.length>u;)r(s,n=e[u++])&&(~i(l,n)||l.push(n));return l}},function(t,e,n){t.exports=n(6)},function(t,e,n){var r=n(16);t.exports=function(t){return Object(r(t))}},function(t,e,n){"use strict";function r(t){return t&&t.__esModule?t:{default:t}}Object.defineProperty(e,"__esModule",{value:!0});var o=n(44),i=r(o),a=n(47),s=r(a),u=n(48),l=r(u),c=n(29),f=r(c),p=n(30),d=r(p),h=n(28),b=r(h);e.default={mixins:[f.default,d.default,b.default],props:{value:{default:null},options:{type:Array,default:function(){return[]}},disabled:{type:Boolean,default:!1},maxHeight:{type:String,default:"400px"},searchable:{type:Boolean,default:!0},multiple:{type:Boolean,default:!1},placeholder:{type:String,default:""},transition:{type:String,default:"fade"},clearSearchOnSelect:{type:Boolean,default:!0},closeOnSelect:{type:Boolean,default:!0},label:{type:String,default:"label"},getOptionLabel:{type:Function,default:function(t){return"object"===("undefined"==typeof t?"undefined":(0,l.default)(t))&&this.label&&t[this.label]?t[this.label]:t}},onChange:{type:Function,default:function(t){this.$emit("input",t)}},taggable:{type:Boolean,default:!1},tabindex:{type:Number,default:null},pushTags:{type:Boolean,default:!1},filterable:{type:Boolean,default:!0},createOption:{type:Function,default:function(t){return"object"===(0,l.default)(this.mutableOptions[0])&&(t=(0,s.default)({},this.label,t)),this.$emit("option:created",t),t}},resetOnOptionsChange:{type:Boolean,default:!1},noDrop:{type:Boolean,default:!1},inputId:{type:String},dir:{type:String,default:"auto"}},data:function(){return{search:"",open:!1,mutableValue:null,mutableOptions:[]}},watch:{value:function(t){this.mutableValue=t},mutableValue:function(t,e){this.multiple?this.onChange?this.onChange(t):null:this.onChange&&t!==e?this.onChange(t):null},options:function(t){this.mutableOptions=t},mutableOptions:function(){!this.taggable&&this.resetOnOptionsChange&&(this.mutableValue=this.multiple?[]:null)},multiple:function(t){this.mutableValue=t?[]:null}},created:function(){this.mutableValue=this.value,this.mutableOptions=this.options.slice(0),this.mutableLoading=this.loading,this.$on("option:created",this.maybePushTag)},methods:{select:function(t){this.isOptionSelected(t)?this.deselect(t):(this.taggable&&!this.optionExists(t)&&(t=this.createOption(t)),this.multiple&&!this.mutableValue?this.mutableValue=[t]:this.multiple?this.mutableValue.push(t):this.mutableValue=t),this.onAfterSelect(t)},deselect:function(t){var e=this;if(this.multiple){var n=-1;this.mutableValue.forEach(function(r){(r===t||"object"===("undefined"==typeof r?"undefined":(0,l.default)(r))&&r[e.label]===t[e.label])&&(n=r)});var r=this.mutableValue.indexOf(n);this.mutableValue.splice(r,1)}else this.mutableValue=null},clearSelection:function(){this.mutableValue=this.multiple?[]:null},onAfterSelect:function(t){this.closeOnSelect&&(this.open=!this.open,this.$refs.search.blur()),this.clearSearchOnSelect&&(this.search="")},toggleDropdown:function(t){t.target!==this.$refs.openIndicator&&t.target!==this.$refs.search&&t.target!==this.$refs.toggle&&t.target!==this.$el||(this.open?this.$refs.search.blur():this.disabled||(this.open=!0,this.$refs.search.focus()))},isOptionSelected:function(t){var e=this;if(this.multiple&&this.mutableValue){var n=!1;return this.mutableValue.forEach(function(r){"object"===("undefined"==typeof r?"undefined":(0,l.default)(r))&&r[e.label]===t[e.label]?n=!0:"object"===("undefined"==typeof r?"undefined":(0,l.default)(r))&&r[e.label]===t?n=!0:r===t&&(n=!0)}),n}return this.mutableValue===t},onEscape:function(){this.search.length?this.search="":this.$refs.search.blur()},onSearchBlur:function(){this.clearSearchOnBlur&&(this.search=""),this.open=!1,this.$emit("search:blur")},onSearchFocus:function(){this.open=!0,this.$emit("search:focus")},maybeDeleteValue:function(){if(!this.$refs.search.value.length&&this.mutableValue)return this.multiple?this.mutableValue.pop():this.mutableValue=null},optionExists:function(t){var e=this,n=!1;return this.mutableOptions.forEach(function(r){"object"===("undefined"==typeof r?"undefined":(0,l.default)(r))&&r[e.label]===t?n=!0:r===t&&(n=!0)}),n},maybePushTag:function(t){this.pushTags&&this.mutableOptions.push(t)}},computed:{dropdownClasses:function(){return{open:this.dropdownOpen,single:!this.multiple,searching:this.searching,searchable:this.searchable,unsearchable:!this.searchable,loading:this.mutableLoading,rtl:"rtl"===this.dir,disabled:this.disabled}},clearSearchOnBlur:function(){return this.clearSearchOnSelect&&!this.multiple},searching:function(){return!!this.search},dropdownOpen:function(){return!this.noDrop&&(this.open&&!this.mutableLoading)},searchPlaceholder:function(){if(this.isValueEmpty&&this.placeholder)return this.placeholder},filteredOptions:function(){var t=this;if(!this.filterable&&!this.taggable)return this.mutableOptions.slice();var e=this.mutableOptions.filter(function(e){return"object"===("undefined"==typeof e?"undefined":(0,l.default)(e))&&e.hasOwnProperty(t.label)?e[t.label].toLowerCase().indexOf(t.search.toLowerCase())>-1:"object"!==("undefined"==typeof e?"undefined":(0,l.default)(e))||e.hasOwnProperty(t.label)?e.toLowerCase().indexOf(t.search.toLowerCase())>-1:console.warn('[vue-select warn]: Label key "option.'+t.label+'" does not exist in options object.\nhttp://sagalbot.github.io/vue-select/#ex-labels')});return this.taggable&&this.search.length&&!this.optionExists(this.search)&&e.unshift(this.search),e},isValueEmpty:function(){return!this.mutableValue||("object"===(0,l.default)(this.mutableValue)?!(0,i.default)(this.mutableValue).length:!this.mutableValue.length)},valueAsArray:function(){return this.multiple?this.mutableValue:this.mutableValue?[this.mutableValue]:[]},showClearButton:function(){return!this.multiple&&!this.open&&null!=this.mutableValue}}}},function(t,e,n){"use strict";function r(t){return t&&t.__esModule?t:{default:t}}Object.defineProperty(e,"__esModule",{value:!0});var o=n(28),i=r(o),a=n(30),s=r(a),u=n(29),l=r(u);e.default={ajax:i.default,pointer:s.default,pointerScroll:l.default}},function(t,e,n){t.exports={default:n(49),__esModule:!0}},function(t,e,n){t.exports={default:n(50),__esModule:!0}},function(t,e,n){t.exports={default:n(51),__esModule:!0}},function(t,e,n){t.exports={default:n(52),__esModule:!0}},function(t,e,n){"use strict";function r(t){return t&&t.__esModule?t:{default:t}}e.__esModule=!0;var o=n(43),i=r(o);e.default=function(t,e,n){return e in t?(0,i.default)(t,e,{value:n,enumerable:!0,configurable:!0,writable:!0}):t[e]=n,t}},function(t,e,n){"use strict";function r(t){return t&&t.__esModule?t:{default:t}}e.__esModule=!0;var o=n(46),i=r(o),a=n(45),s=r(a),u="function"==typeof s.default&&"symbol"==typeof i.default?function(t){return typeof t}:function(t){return t&&"function"==typeof s.default&&t.constructor===s.default&&t!==s.default.prototype?"symbol":typeof t};e.default="function"==typeof s.default&&"symbol"===u(i.default)?function(t){return"undefined"==typeof t?"undefined":u(t)}:function(t){return t&&"function"==typeof s.default&&t.constructor===s.default&&t!==s.default.prototype?"symbol":"undefined"==typeof t?"undefined":u(t)}},function(t,e,n){n(73);var r=n(5).Object;t.exports=function(t,e,n){return r.defineProperty(t,e,n)}},function(t,e,n){n(74),t.exports=n(5).Object.keys},function(t,e,n){n(77),n(75),n(78),n(79),t.exports=n(5).Symbol},function(t,e,n){n(76),n(80),t.exports=n(27).f("iterator")},function(t,e){t.exports=function(t){if("function"!=typeof t)throw TypeError(t+" is not a function!");return t}},function(t,e){t.exports=function(){}},function(t,e,n){var r=n(7),o=n(71),i=n(70);t.exports=function(t){return function(e,n,a){var s,u=r(e),l=o(u.length),c=i(a,l);if(t&&n!=n){for(;l>c;)if(s=u[c++],s!=s)return!0}else for(;l>c;c++)if((t||c in u)&&u[c]===n)return t||c||0;return!t&&-1}}},function(t,e,n){var r=n(53);t.exports=function(t,e,n){if(r(t),void 0===e)return t;switch(n){case 1:return function(n){return t.call(e,n)};case 2:return function(n,r){return t.call(e,n,r)};case 3:return function(n,r,o){return t.call(e,n,r,o)}}return function(){return t.apply(e,arguments)}}},function(t,e,n){var r=n(13),o=n(37),i=n(20);t.exports=function(t){var e=r(t),n=o.f;if(n)for(var a,s=n(t),u=i.f,l=0;s.length>l;)u.call(t,a=s[l++])&&e.push(a);return e}},function(t,e,n){var r=n(1).document;t.exports=r&&r.documentElement},function(t,e,n){var r=n(31);t.exports=Object("z").propertyIsEnumerable(0)?Object:function(t){return"String"==r(t)?t.split(""):Object(t)}},function(t,e,n){var r=n(31);t.exports=Array.isArray||function(t){return"Array"==r(t)}},function(t,e,n){"use strict";var r=n(35),o=n(14),i=n(21),a={};n(6)(a,n(8)("iterator"),function(){return this}),t.exports=function(t,e,n){t.prototype=r(a,{next:o(1,n)}),i(t,e+" Iterator")}},function(t,e){t.exports=function(t,e){return{value:e,done:!!t}}},function(t,e,n){var r=n(15)("meta"),o=n(10),i=n(3),a=n(4).f,s=0,u=Object.isExtensible||function(){return!0},l=!n(9)(function(){return u(Object.preventExtensions({}))}),c=function(t){a(t,r,{value:{i:"O"+ ++s,w:{}}})},f=function(t,e){if(!o(t))return"symbol"==typeof t?t:("string"==typeof t?"S":"P")+t;if(!i(t,r)){if(!u(t))return"F";if(!e)return"E";c(t)}return t[r].i},p=function(t,e){if(!i(t,r)){if(!u(t))return!0;if(!e)return!1;c(t)}return t[r].w},d=function(t){return l&&h.NEED&&u(t)&&!i(t,r)&&c(t),t},h=t.exports={KEY:r,NEED:!1,fastKey:f,getWeak:p,onFreeze:d}},function(t,e,n){var r=n(4),o=n(11),i=n(13);t.exports=n(2)?Object.defineProperties:function(t,e){o(t);for(var n,a=i(e),s=a.length,u=0;s>u;)r.f(t,n=a[u++],e[n]);return t}},function(t,e,n){var r=n(20),o=n(14),i=n(7),a=n(25),s=n(3),u=n(33),l=Object.getOwnPropertyDescriptor;e.f=n(2)?l:function(t,e){if(t=i(t),e=a(e,!0),u)try{return l(t,e)}catch(t){}if(s(t,e))return o(!r.f.call(t,e),t[e])}},function(t,e,n){var r=n(7),o=n(36).f,i={}.toString,a="object"==typeof window&&window&&Object.getOwnPropertyNames?Object.getOwnPropertyNames(window):[],s=function(t){try{return o(t)}catch(t){return a.slice()}};t.exports.f=function(t){return a&&"[object Window]"==i.call(t)?s(t):o(r(t))}},function(t,e,n){var r=n(3),o=n(40),i=n(22)("IE_PROTO"),a=Object.prototype;t.exports=Object.getPrototypeOf||function(t){return t=o(t),r(t,i)?t[i]:"function"==typeof t.constructor&&t instanceof t.constructor?t.constructor.prototype:t instanceof Object?a:null}},function(t,e,n){var r=n(12),o=n(5),i=n(9);t.exports=function(t,e){var n=(o.Object||{})[t]||Object[t],a={};a[t]=e(n),r(r.S+r.F*i(function(){n(1)}),"Object",a)}},function(t,e,n){var r=n(24),o=n(16);t.exports=function(t){return function(e,n){var i,a,s=String(o(e)),u=r(n),l=s.length;return u<0||u>=l?t?"":void 0:(i=s.charCodeAt(u),i<55296||i>56319||u+1===l||(a=s.charCodeAt(u+1))<56320||a>57343?t?s.charAt(u):i:t?s.slice(u,u+2):(i-55296<<10)+(a-56320)+65536)}}},function(t,e,n){var r=n(24),o=Math.max,i=Math.min;t.exports=function(t,e){return t=r(t),t<0?o(t+e,0):i(t,e)}},function(t,e,n){var r=n(24),o=Math.min;t.exports=function(t){return t>0?o(r(t),9007199254740991):0}},function(t,e,n){"use strict";var r=n(54),o=n(62),i=n(18),a=n(7);t.exports=n(34)(Array,"Array",function(t,e){this._t=a(t),this._i=0,this._k=e},function(){var t=this._t,e=this._k,n=this._i++;return!t||n>=t.length?(this._t=void 0,o(1)):"keys"==e?o(0,n):"values"==e?o(0,t[n]):o(0,[n,t[n]])},"values"),i.Arguments=i.Array,r("keys"),r("values"),r("entries")},function(t,e,n){var r=n(12);r(r.S+r.F*!n(2),"Object",{defineProperty:n(4).f})},function(t,e,n){var r=n(40),o=n(13);n(68)("keys",function(){return function(t){return o(r(t))}})},function(t,e){},function(t,e,n){"use strict";var r=n(69)(!0);n(34)(String,"String",function(t){this._t=String(t),this._i=0},function(){var t,e=this._t,n=this._i;return n>=e.length?{value:void 0,done:!0}:(t=r(e,n),this._i+=t.length,{value:t,done:!1})})},function(t,e,n){"use strict";var r=n(1),o=n(3),i=n(2),a=n(12),s=n(39),u=n(63).KEY,l=n(9),c=n(23),f=n(21),p=n(15),d=n(8),h=n(27),b=n(26),v=n(57),g=n(60),y=n(11),m=n(10),x=n(7),w=n(25),S=n(14),O=n(35),_=n(66),j=n(65),k=n(4),P=n(13),C=j.f,A=k.f,M=_.f,L=r.Symbol,T=r.JSON,E=T&&T.stringify,V="prototype",B=d("_hidden"),F=d("toPrimitive"),$={}.propertyIsEnumerable,N=c("symbol-registry"),D=c("symbols"),I=c("op-symbols"),R=Object[V],z="function"==typeof L,H=r.QObject,G=!H||!H[V]||!H[V].findChild,U=i&&l(function(){return 7!=O(A({},"a",{get:function(){return A(this,"a",{value:7}).a}})).a})?function(t,e,n){var r=C(R,e);r&&delete R[e],A(t,e,n),r&&t!==R&&A(R,e,r)}:A,W=function(t){var e=D[t]=O(L[V]);return e._k=t,e},J=z&&"symbol"==typeof L.iterator?function(t){return"symbol"==typeof t}:function(t){return t instanceof L},K=function(t,e,n){return t===R&&K(I,e,n),y(t),e=w(e,!0),y(n),o(D,e)?(n.enumerable?(o(t,B)&&t[B][e]&&(t[B][e]=!1),n=O(n,{enumerable:S(0,!1)})):(o(t,B)||A(t,B,S(1,{})),t[B][e]=!0),U(t,e,n)):A(t,e,n)},Y=function(t,e){y(t);for(var n,r=v(e=x(e)),o=0,i=r.length;i>o;)K(t,n=r[o++],e[n]);return t},q=function(t,e){return void 0===e?O(t):Y(O(t),e)},Q=function(t){var e=$.call(this,t=w(t,!0));return!(this===R&&o(D,t)&&!o(I,t))&&(!(e||!o(this,t)||!o(D,t)||o(this,B)&&this[B][t])||e)},Z=function(t,e){if(t=x(t),e=w(e,!0),t!==R||!o(D,e)||o(I,e)){var n=C(t,e);return!n||!o(D,e)||o(t,B)&&t[B][e]||(n.enumerable=!0),n}},X=function(t){for(var e,n=M(x(t)),r=[],i=0;n.length>i;)o(D,e=n[i++])||e==B||e==u||r.push(e);return r},tt=function(t){for(var e,n=t===R,r=M(n?I:x(t)),i=[],a=0;r.length>a;)!o(D,e=r[a++])||n&&!o(R,e)||i.push(D[e]);return i};z||(L=function(){if(this instanceof L)throw TypeError("Symbol is not a constructor!");var t=p(arguments.length>0?arguments[0]:void 0),e=function(n){this===R&&e.call(I,n),o(this,B)&&o(this[B],t)&&(this[B][t]=!1),U(this,t,S(1,n))};return i&&G&&U(R,t,{configurable:!0,set:e}),W(t)},s(L[V],"toString",function(){return this._k}),j.f=Z,k.f=K,n(36).f=_.f=X,n(20).f=Q,n(37).f=tt,i&&!n(19)&&s(R,"propertyIsEnumerable",Q,!0),h.f=function(t){return W(d(t))}),a(a.G+a.W+a.F*!z,{Symbol:L});for(var et="hasInstance,isConcatSpreadable,iterator,match,replace,search,species,split,toPrimitive,toStringTag,unscopables".split(","),nt=0;et.length>nt;)d(et[nt++]);for(var rt=P(d.store),ot=0;rt.length>ot;)b(rt[ot++]);a(a.S+a.F*!z,"Symbol",{for:function(t){return o(N,t+="")?N[t]:N[t]=L(t)},keyFor:function(t){if(!J(t))throw TypeError(t+" is not a symbol!");for(var e in N)if(N[e]===t)return e},useSetter:function(){G=!0},useSimple:function(){G=!1}}),a(a.S+a.F*!z,"Object",{create:q,defineProperty:K,defineProperties:Y,getOwnPropertyDescriptor:Z,getOwnPropertyNames:X,getOwnPropertySymbols:tt}),T&&a(a.S+a.F*(!z||l(function(){var t=L();return"[null]"!=E([t])||"{}"!=E({a:t})||"{}"!=E(Object(t))})),"JSON",{stringify:function(t){for(var e,n,r=[t],o=1;arguments.length>o;)r.push(arguments[o++]);if(n=e=r[1],(m(e)||void 0!==t)&&!J(t))return g(e)||(e=function(t,e){if(n&&(e=n.call(this,t,e)),!J(e))return e}),r[1]=e,E.apply(T,r)}}),L[V][F]||n(6)(L[V],F,L[V].valueOf),f(L,"Symbol"),f(Math,"Math",!0),f(r.JSON,"JSON",!0)},function(t,e,n){n(26)("asyncIterator")},function(t,e,n){n(26)("observable")},function(t,e,n){n(72);for(var r=n(1),o=n(6),i=n(18),a=n(8)("toStringTag"),s="CSSRuleList,CSSStyleDeclaration,CSSValueList,ClientRectList,DOMRectList,DOMStringList,DOMTokenList,DataTransferItemList,FileList,HTMLAllCollection,HTMLCollection,HTMLFormElement,HTMLSelectElement,MediaList,MimeTypeArray,NamedNodeMap,NodeList,PaintRequestList,Plugin,PluginArray,SVGLengthList,SVGNumberList,SVGPathSegList,SVGPointList,SVGStringList,SVGTransformList,SourceBufferList,StyleSheetList,TextTrackCueList,TextTrackList,TouchList".split(","),u=0;u<s.length;u++){var l=s[u],c=r[l],f=c&&c.prototype;f&&!f[a]&&o(f,a,l),i[l]=i.Array}},function(t,e,n){e=t.exports=n(82)(),e.push([t.id,'.v-select{position:relative;font-family:sans-serif}.v-select,.v-select *{box-sizing:border-box}.v-select.rtl .open-indicator{left:10px;right:auto}.v-select.rtl .selected-tag{float:right;margin-right:3px;margin-left:1px}.v-select.rtl .dropdown-menu{text-align:right}.v-select.rtl .dropdown-toggle .clear{left:30px;right:auto}.v-select .open-indicator{position:absolute;bottom:6px;right:10px;cursor:pointer;pointer-events:all;opacity:1;height:20px}.v-select .open-indicator,.v-select .open-indicator:before{display:inline-block;transition:all .15s cubic-bezier(1,-.115,.975,.855);transition-timing-function:cubic-bezier(1,-.115,.975,.855);width:10px}.v-select .open-indicator:before{border-color:rgba(60,60,60,.5);border-style:solid;border-width:3px 3px 0 0;content:"";height:10px;vertical-align:top;transform:rotate(133deg);box-sizing:inherit}.v-select.open .open-indicator:before{transform:rotate(315deg)}.v-select.loading .open-indicator{opacity:0}.v-select.open .open-indicator{bottom:1px}.v-select .dropdown-toggle{-webkit-appearance:none;-moz-appearance:none;appearance:none;display:block;padding:0;background:none;border:1px solid rgba(60,60,60,.26);border-radius:4px;white-space:normal}.v-select .dropdown-toggle:after{visibility:hidden;display:block;font-size:0;content:" ";clear:both;height:0}.v-select .dropdown-toggle .clear{position:absolute;bottom:9px;right:30px;font-size:23px;font-weight:700;line-height:1;color:rgba(60,60,60,.5);padding:0;border:0;background-color:transparent;cursor:pointer}.v-select.searchable .dropdown-toggle{cursor:text}.v-select.unsearchable .dropdown-toggle{cursor:pointer}.v-select.open .dropdown-toggle{border-bottom-color:transparent;border-bottom-left-radius:0;border-bottom-right-radius:0}.v-select .dropdown-menu{display:block;position:absolute;top:100%;left:0;z-index:1000;min-width:160px;padding:5px 0;margin:0;width:100%;overflow-y:scroll;border:1px solid rgba(0,0,0,.26);box-shadow:0 3px 6px 0 rgba(0,0,0,.15);border-top:none;border-radius:0 0 4px 4px;text-align:left;list-style:none;background:#fff}.v-select .no-options{text-align:center}.v-select .selected-tag{color:#333;background-color:#f0f0f0;border:1px solid #ccc;border-radius:4px;height:26px;margin:4px 1px 0 3px;padding:1px .25em;float:left;line-height:24px}.v-select.single .selected-tag{background-color:transparent;border-color:transparent}.v-select.single.open .selected-tag{position:absolute;opacity:.5}.v-select.single.loading .selected-tag,.v-select.single.open.searching .selected-tag{display:none}.v-select .selected-tag .close{float:none;margin-right:0;font-size:20px;appearance:none;padding:0;cursor:pointer;background:0 0;border:0;font-weight:700;line-height:1;color:#000;text-shadow:0 1px 0 #fff;filter:alpha(opacity=20);opacity:.2}.v-select.single.searching:not(.open):not(.loading) input[type=search]{opacity:.2}.v-select input[type=search]::-webkit-search-cancel-button,.v-select input[type=search]::-webkit-search-decoration,.v-select input[type=search]::-webkit-search-results-button,.v-select input[type=search]::-webkit-search-results-decoration{display:none}.v-select input[type=search]::-ms-clear{display:none}.v-select input[type=search],.v-select input[type=search]:focus{appearance:none;-webkit-appearance:none;-moz-appearance:none;line-height:1.42857143;font-size:1em;height:34px;display:inline-block;border:none;outline:none;margin:0;padding:0 .5em;width:10em;max-width:100%;background:none;position:relative;box-shadow:none}.v-select.unsearchable input[type=search]{opacity:0}.v-select.unsearchable input[type=search]:hover{cursor:pointer}.v-select li{line-height:1.42857143}.v-select li>a{display:block;padding:3px 20px;clear:both;color:#333;white-space:nowrap}.v-select li:hover{cursor:pointer}.v-select .dropdown-menu .active>a{color:#333;background:rgba(50,50,50,.1)}.v-select .dropdown-menu>.highlight>a{background:#5897fb;color:#fff}.v-select .highlight:not(:last-child){margin-bottom:0}.v-select .spinner{opacity:0;position:absolute;top:5px;right:10px;font-size:5px;text-indent:-9999em;overflow:hidden;border-top:.9em solid hsla(0,0%,39%,.1);border-right:.9em solid hsla(0,0%,39%,.1);border-bottom:.9em solid hsla(0,0%,39%,.1);border-left:.9em solid rgba(60,60,60,.45);transform:translateZ(0);animation:vSelectSpinner 1.1s infinite linear;transition:opacity .1s}.v-select .spinner,.v-select .spinner:after{border-radius:50%;width:5em;height:5em}.v-select.disabled .dropdown-toggle,.v-select.disabled .dropdown-toggle .clear,.v-select.disabled .dropdown-toggle input,.v-select.disabled .open-indicator,.v-select.disabled .selected-tag .close{cursor:not-allowed;background-color:#f8f8f8}.v-select.loading .spinner{opacity:1}@-webkit-keyframes vSelectSpinner{0%{transform:rotate(0deg)}to{transform:rotate(1turn)}}@keyframes vSelectSpinner{0%{transform:rotate(0deg)}to{transform:rotate(1turn)}}.fade-enter-active,.fade-leave-active{transition:opacity .15s cubic-bezier(1,.5,.8,1)}.fade-enter,.fade-leave-to{opacity:0}',""])},function(t,e){t.exports=function(){var t=[];return t.toString=function(){for(var t=[],e=0;e<this.length;e++){var n=this[e];n[2]?t.push("@media "+n[2]+"{"+n[1]+"}"):t.push(n[1])}return t.join("")},t.i=function(e,n){"string"==typeof e&&(e=[[null,e,""]]);for(var r={},o=0;o<this.length;o++){var i=this[o][0];"number"==typeof i&&(r[i]=!0)}for(o=0;o<e.length;o++){var a=e[o];"number"==typeof a[0]&&r[a[0]]||(n&&!a[2]?a[2]=n:n&&(a[2]="("+a[2]+") and ("+n+")"),t.push(a))}},t}},function(t,e,n){n(87);var r=n(84)(n(41),n(85),null,null);t.exports=r.exports},function(t,e){t.exports=function(t,e,n,r){var o,i=t=t||{},a=typeof t.default;"object"!==a&&"function"!==a||(o=t,i=t.default);var s="function"==typeof i?i.options:i;if(e&&(s.render=e.render,s.staticRenderFns=e.staticRenderFns),n&&(s._scopeId=n),r){var u=s.computed||(s.computed={});Object.keys(r).forEach(function(t){var e=r[t];u[t]=function(){return e}})}return{esModule:o,exports:i,options:s}}},function(t,e){t.exports={render:function(){var t=this,e=t.$createElement,n=t._self._c||e;return n("div",{staticClass:"dropdown v-select",class:t.dropdownClasses,attrs:{dir:t.dir}},[n("div",{ref:"toggle",class:["dropdown-toggle","clearfix"],on:{mousedown:function(e){e.preventDefault(),t.toggleDropdown(e)}}},[t._l(t.valueAsArray,function(e){return n("span",{key:e.index,staticClass:"selected-tag"},[t._t("selected-option",[t._v("\n        "+t._s(t.getOptionLabel(e))+"\n      ")],null,e),t._v(" "),t.multiple?n("button",{staticClass:"close",attrs:{disabled:t.disabled,type:"button","aria-label":"Remove option"},on:{click:function(n){t.deselect(e)}}},[n("span",{attrs:{"aria-hidden":"true"}},[t._v("×")])]):t._e()],2)}),t._v(" "),n("input",{directives:[{name:"model",rawName:"v-model",value:t.search,expression:"search"}],ref:"search",staticClass:"form-control",style:{width:t.isValueEmpty?"100%":"auto"},attrs:{type:"search",autocomplete:"false",disabled:t.disabled,placeholder:t.searchPlaceholder,tabindex:t.tabindex,readonly:!t.searchable,id:t.inputId,"aria-label":"Search for option"},domProps:{value:t.search},on:{keydown:[function(e){return"button"in e||!t._k(e.keyCode,"delete",[8,46],e.key)?void t.maybeDeleteValue(e):null},function(e){return"button"in e||!t._k(e.keyCode,"up",38,e.key)?(e.preventDefault(),void t.typeAheadUp(e)):null},function(e){return"button"in e||!t._k(e.keyCode,"down",40,e.key)?(e.preventDefault(),void t.typeAheadDown(e)):null},function(e){return"button"in e||!t._k(e.keyCode,"enter",13,e.key)?(e.preventDefault(),void t.typeAheadSelect(e)):null}],keyup:function(e){return"button"in e||!t._k(e.keyCode,"esc",27,e.key)?void t.onEscape(e):null},blur:t.onSearchBlur,focus:t.onSearchFocus,input:function(e){
e.target.composing||(t.search=e.target.value)}}}),t._v(" "),n("button",{directives:[{name:"show",rawName:"v-show",value:t.showClearButton,expression:"showClearButton"}],staticClass:"clear",attrs:{disabled:t.disabled,type:"button",title:"Clear selection"},on:{click:t.clearSelection}},[n("span",{attrs:{"aria-hidden":"true"}},[t._v("×")])]),t._v(" "),t.noDrop?t._e():n("i",{ref:"openIndicator",staticClass:"open-indicator",attrs:{role:"presentation"}}),t._v(" "),t._t("spinner",[n("div",{directives:[{name:"show",rawName:"v-show",value:t.mutableLoading,expression:"mutableLoading"}],staticClass:"spinner"},[t._v("Loading...")])])],2),t._v(" "),n("transition",{attrs:{name:t.transition}},[t.dropdownOpen?n("ul",{ref:"dropdownMenu",staticClass:"dropdown-menu",style:{"max-height":t.maxHeight}},[t._l(t.filteredOptions,function(e,r){return n("li",{key:r,class:{active:t.isOptionSelected(e),highlight:r===t.typeAheadPointer},on:{mouseover:function(e){t.typeAheadPointer=r}}},[n("a",{on:{mousedown:function(n){n.preventDefault(),t.select(e)}}},[t._t("option",[t._v("\n          "+t._s(t.getOptionLabel(e))+"\n        ")],null,e)],2)])}),t._v(" "),t.filteredOptions.length?t._e():n("li",{staticClass:"no-options"},[t._t("no-options",[t._v("Sorry, no matching options.")])],2)],2):t._e()])],1)},staticRenderFns:[]}},function(t,e,n){function r(t,e){for(var n=0;n<t.length;n++){var r=t[n],o=f[r.id];if(o){o.refs++;for(var i=0;i<o.parts.length;i++)o.parts[i](r.parts[i]);for(;i<r.parts.length;i++)o.parts.push(u(r.parts[i],e))}else{for(var a=[],i=0;i<r.parts.length;i++)a.push(u(r.parts[i],e));f[r.id]={id:r.id,refs:1,parts:a}}}}function o(t){for(var e=[],n={},r=0;r<t.length;r++){var o=t[r],i=o[0],a=o[1],s=o[2],u=o[3],l={css:a,media:s,sourceMap:u};n[i]?n[i].parts.push(l):e.push(n[i]={id:i,parts:[l]})}return e}function i(t,e){var n=h(),r=g[g.length-1];if("top"===t.insertAt)r?r.nextSibling?n.insertBefore(e,r.nextSibling):n.appendChild(e):n.insertBefore(e,n.firstChild),g.push(e);else{if("bottom"!==t.insertAt)throw new Error("Invalid value for parameter 'insertAt'. Must be 'top' or 'bottom'.");n.appendChild(e)}}function a(t){t.parentNode.removeChild(t);var e=g.indexOf(t);e>=0&&g.splice(e,1)}function s(t){var e=document.createElement("style");return e.type="text/css",i(t,e),e}function u(t,e){var n,r,o;if(e.singleton){var i=v++;n=b||(b=s(e)),r=l.bind(null,n,i,!1),o=l.bind(null,n,i,!0)}else n=s(e),r=c.bind(null,n),o=function(){a(n)};return r(t),function(e){if(e){if(e.css===t.css&&e.media===t.media&&e.sourceMap===t.sourceMap)return;r(t=e)}else o()}}function l(t,e,n,r){var o=n?"":r.css;if(t.styleSheet)t.styleSheet.cssText=y(e,o);else{var i=document.createTextNode(o),a=t.childNodes;a[e]&&t.removeChild(a[e]),a.length?t.insertBefore(i,a[e]):t.appendChild(i)}}function c(t,e){var n=e.css,r=e.media,o=e.sourceMap;if(r&&t.setAttribute("media",r),o&&(n+="\n/*# sourceURL="+o.sources[0]+" */",n+="\n/*# sourceMappingURL=data:application/json;base64,"+btoa(unescape(encodeURIComponent(JSON.stringify(o))))+" */"),t.styleSheet)t.styleSheet.cssText=n;else{for(;t.firstChild;)t.removeChild(t.firstChild);t.appendChild(document.createTextNode(n))}}var f={},p=function(t){var e;return function(){return"undefined"==typeof e&&(e=t.apply(this,arguments)),e}},d=p(function(){return/msie [6-9]\b/.test(window.navigator.userAgent.toLowerCase())}),h=p(function(){return document.head||document.getElementsByTagName("head")[0]}),b=null,v=0,g=[];t.exports=function(t,e){e=e||{},"undefined"==typeof e.singleton&&(e.singleton=d()),"undefined"==typeof e.insertAt&&(e.insertAt="bottom");var n=o(t);return r(n,e),function(t){for(var i=[],a=0;a<n.length;a++){var s=n[a],u=f[s.id];u.refs--,i.push(u)}if(t){var l=o(t);r(l,e)}for(var a=0;a<i.length;a++){var u=i[a];if(0===u.refs){for(var c=0;c<u.parts.length;c++)u.parts[c]();delete f[u.id]}}}};var y=function(){var t=[];return function(e,n){return t[e]=n,t.filter(Boolean).join("\n")}}()},function(t,e,n){var r=n(81);"string"==typeof r&&(r=[[t.id,r,""]]);n(86)(r,{});r.locals&&(t.exports=r.locals)}])});
//# sourceMappingURL=vue-select.js.map

/***/ }),

/***/ 5:
/***/ (function(module, exports) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
module.exports = function(useSourceMap) {
	var list = [];

	// return the list of modules as css string
	list.toString = function toString() {
		return this.map(function (item) {
			var content = cssWithMappingToString(item, useSourceMap);
			if(item[2]) {
				return "@media " + item[2] + "{" + content + "}";
			} else {
				return content;
			}
		}).join("");
	};

	// import a list of modules into the list
	list.i = function(modules, mediaQuery) {
		if(typeof modules === "string")
			modules = [[null, modules, ""]];
		var alreadyImportedModules = {};
		for(var i = 0; i < this.length; i++) {
			var id = this[i][0];
			if(typeof id === "number")
				alreadyImportedModules[id] = true;
		}
		for(i = 0; i < modules.length; i++) {
			var item = modules[i];
			// skip already imported module
			// this implementation is not 100% perfect for weird media query combinations
			//  when a module is imported multiple times with different media queries.
			//  I hope this will never occur (Hey this way we have smaller bundles)
			if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
				if(mediaQuery && !item[2]) {
					item[2] = mediaQuery;
				} else if(mediaQuery) {
					item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
				}
				list.push(item);
			}
		}
	};
	return list;
};

function cssWithMappingToString(item, useSourceMap) {
	var content = item[1] || '';
	var cssMapping = item[3];
	if (!cssMapping) {
		return content;
	}

	if (useSourceMap && typeof btoa === 'function') {
		var sourceMapping = toComment(cssMapping);
		var sourceURLs = cssMapping.sources.map(function (source) {
			return '/*# sourceURL=' + cssMapping.sourceRoot + source + ' */'
		});

		return [content].concat(sourceURLs).concat([sourceMapping]).join('\n');
	}

	return [content].join('\n');
}

// Adapted from convert-source-map (MIT)
function toComment(sourceMap) {
	// eslint-disable-next-line no-undef
	var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));
	var data = 'sourceMappingURL=data:application/json;charset=utf-8;base64,' + base64;

	return '/*# ' + data + ' */';
}


/***/ }),

/***/ 507:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(508);


/***/ }),

/***/ 508:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__event_js__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__event_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__event_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_vue_select__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_vue_select___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_vue_select__);
Vue.component('tutorials', __webpack_require__(24));
Vue.component('userlist', __webpack_require__(42));
Vue.component('channelinfo', __webpack_require__(509));
Vue.component('channelcontact', __webpack_require__(512));
Vue.component('searchlist', __webpack_require__(515));
Vue.component('confirm', __webpack_require__(29));
Vue.component('settings', __webpack_require__(518));
Vue.component('sendmessagepopup', __webpack_require__(4));



Vue.component('v-select', __WEBPACK_IMPORTED_MODULE_1_vue_select___default.a);

var app = new Vue({
    el: '#app',
    data: {
        user: userInfo,
        channel: channelInfo,
        settings: JSON.parse(channelInfo.settings),
        subscriptionCount: subscriptionCount,
        tutorialsOptions: {
            viewType: 'block',
            editBtn: true,
            deleteBtn: true,
            noDataCommentOn: true,
            noDataComment: '운영중인 강좌가 없습니다'
        },
        channelUsersOptions: {
            channelAdminAdd: false,
            channelAdminDelete: true,
            channelAdminLevel: true,
            deleteBtn: false,
            noDataCommentOn: false,
            noDataComment: ''
        },
        searchListOptions: {
            requestBtn: true,
            requestBtnText: '관리자 등록 요청하기'
        },
        confirmModalContent: {
            title: '채널 관리자 해제',
            body: '채널 관리자 해제를 원하시면 아래 확인 버튼을 클릭하세요',
            cancel: '취소',
            submit: '확인'
        },
        view: 'tutorials',
        snsLists: [],
        superAdmin: superAdmin
    },
    mounted: function mounted() {
        var self = this;

        _.forEach(this.settings.sns, function (value, key) {
            self.snsLists.unshift(value);
        });

        __WEBPACK_IMPORTED_MODULE_0__event_js___default.a.$on('channelInfoUpdate', this.channelInfoUpdate);

        __WEBPACK_IMPORTED_MODULE_0__event_js___default.a.$on('channelContactUpdate', this.channelContactUpdate);

        __WEBPACK_IMPORTED_MODULE_0__event_js___default.a.$on('snsUpdate', this.snsUpdate);

        __WEBPACK_IMPORTED_MODULE_0__event_js___default.a.$on('channelSettingsUpdate', this.settingsUpdate);
    },

    methods: {
        channelCreated: function channelCreated() {
            return moment(this.channel.created_at).format("MMM Do YYYY");
        },
        viewChange: function viewChange(view) {
            this.view = view;
        },
        channelInfoUpdate: function channelInfoUpdate(channel) {
            this.channel = channel;
        },
        channelContactUpdate: function channelContactUpdate(data) {
            this.settings.phone = data.phone;
            this.settings.site = data.site;
            this.settings.email = data.email;
        },
        snsUpdate: function snsUpdate(snsLists) {
            this.snsLists = snsLists;
        },
        settingsUpdate: function settingsUpdate(settings) {
            console.log(settings);
            this.settings.viewoptions = settings;
        }
    }
});

/***/ }),

/***/ 509:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(510)
/* template */
var __vue_template__ = __webpack_require__(511)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/components/channel/ChannelInfo.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-18f5215e", Component.options)
  } else {
    hotAPI.reload("data-v-18f5215e", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 510:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__event_js__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__event_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__event_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_vue_froala_wysiwyg__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_vue_froala_wysiwyg___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_vue_froala_wysiwyg__);


function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



Vue.use(__WEBPACK_IMPORTED_MODULE_2_vue_froala_wysiwyg___default.a);

/* harmony default export */ __webpack_exports__["default"] = ({
    data: function data() {
        return {
            channel: null,
            images: [],
            croppers: [],
            inputImage: [],
            defaultImage: [],
            waveHeadImage: null,
            waveLogoImage: null,
            cropOptions: {},
            getOptions: {},
            cropperImages: ['waveHeadImage', 'waveLogoImage'],
            headDefault: '/img/channels/head_image.png',
            logoDefault: '/img/channels/logo_image.png',
            cropperLoad: false,
            errorMessage: '',
            froalaConfig: {
                imageDefaultWidth: 0,
                requestHeaders: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                saveInterval: 0,
                saveURL: '/channel/updateDescription/' + this.channelProp.slug,
                saveParams: { id: this.channelProp.id },
                language: 'ko',
                imageUploadParam: 'imageFile',
                imageUploadURL: '/editor/uploadToS3',
                imageUploadMethod: 'POST',
                imageMaxSize: 2 * 1024 * 1024,
                imageUploadParams: { id: channelInfo.id, type: 'channels', width: 800, mode: 'resize', limitWidth: 1920, limitHeight: 1000, fileSize: 2 },
                imageAllowedTypes: ['jpeg', 'jpg', 'png', 'gif', 'svg'],
                heightMin: 500,
                heightMax: 1000,
                fontFamily: {
                    'Arial,Helvetica,sans-serif': 'Arial',
                    'Georgia,serif': 'Georgia',
                    'Impact,Charcoal,sans-serif': 'Impact',
                    'Tahoma,Geneva,sans-serif': 'Tahoma',
                    "'Times New Roman',Times,serif": 'Times New Roman',
                    'Verdana,Geneva,sans-serif': 'Verdana',
                    'Helvetica,sans-serif': 'Helvetica',
                    "'Helvetica Neue',sans-serif": 'Helvetica Neue',
                    "'Roboto', sans-serif": 'Roboto',
                    "'Nunito', sans-serif": 'Nunito',
                    "'Nunito Sans', sans-serif": 'Nunito Sans',
                    "'Montserrat', sans-serif": 'Montserrat',
                    "'Oswald', sans-serif": 'Oswald',
                    "'Raleway', sans-serif": 'Raleway',
                    "'Nanum Gothic', sans-serif": '나눔 고딕 Nanum Gothic',
                    "'Nanum Myeongjo', serif": '나눔 명조 Nanum Myeongjo',
                    "'Nanum Pen Script', cursive": '나눔 펜 스크립트 Nanum Pen Script',
                    "'Nanum Gothic Coding', monospace": '나눔 고딕 코딩 Nanum Gothic Coding',
                    "'Nanum Brush Script', cursive": '나눔 브러쉬 스크립트 Nanum Brush Script',
                    "'Song Myung', serif": '송명 Song Myung',
                    "'Poor Story', cursive": '푸어 스토리 Poor Story',
                    "'Gugi', cursive": '구기 Gugi',
                    "'Gothic A1', sans-serif": '고딕 A1 Gothic A1',
                    "'Black Han Sans', sans-serif": '블랙 한글 Sans Black Han Sans',
                    "'Do Hyeon', sans-serif": '도현 Do Hyeon',
                    "'Hi Melody', cursive": '하이 멜로디 Hi Melody',
                    "'Sunflower', sans-serif": '썬프라워 Sunflower',
                    "'Jua', sans-serif": '주아 Jua',
                    "'Gamja Flower', cursive": '감자 플라워 Gamja Flower',
                    "'East Sea Dokdo', cursive": '동해 독도 East Sea Dokdo',
                    "'Gaegu', cursive": '개구 Gaegu',
                    "'Stylish', sans-serif": '스타일리쉬 Stylish',
                    "'Black And White Picture', sans-serif": '블랙 앤 화이트 픽쳐 Black And White Picture',
                    "'Kirang Haerang', cursive": '키랑 해랑 Kirang Haerang',
                    "'Cute Font', cursive": '큐트 폰트 Cute Font',
                    "'Yeon Sung', cursive": '연성 Yeon Sung',
                    "'Dokdo', cursive": '독도 Dokdo'
                },
                //fontFamilySelection: true,
                toolbarButtons: ['fullscreen', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '|', 'fontFamily', 'fontSize', 'color', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', '|', 'insertLink', 'insertImage', 'insertVideo', 'insertTable', '|', 'emoticons', 'specialCharacters', 'insertHR', 'selectAll', 'clearFormatting', '|', 'print', 'help', 'html', '|', 'undo', 'redo'],
                events: {
                    'froalaEditor.image.error': function froalaEditorImageError(e, editor, error, response) {
                        console.log(error);
                        console.log(response);
                        $('#alarm #title').text('이미지 업로드 에러');
                        $('#alarm #content').html('이미지 업로드에 실패하였습니다');
                        // Bad link.
                        if (error.code == 1) {
                            $('#alarm #content').html('유효한 링크가 아닙니다');
                        }

                        // No link in upload response.
                        else if (error.code == 2 || error.code == 3 || error.code == 4 || error.code == 7) {
                                $('#alarm #content').html('업로드 가능한 본문 이미지는<br>사이즈 2Mbytes 이하<br>폭 1920픽셀 이하<br>높이 1000픽셀 이하입니다');
                                if (response) {
                                    var re = JSON.parse(response);
                                    if ('message' in re) {
                                        var message = '';
                                        _.forEach(re.message, function (value, key) {
                                            message += value;
                                            message += '<br>';
                                        });
                                        $('#alarm #content').html(message);
                                    }
                                }
                            }

                            // Image too text-large.
                            else if (error.code == 5) {
                                    $('#alarm #content').html('업로드 가능한 본문 이미지의 사이즈는 2Mbytes 이하입니다');
                                }

                                // Invalid image type.
                                else if (error.code == 6) {
                                        $('#alarm #content').html('업로드 가능한 이미지 타입은 jpeg, jpg, png, gif, svg 입니다');
                                    }
                        $('#alarm').modal('show');
                    },
                    'froalaEditor.image.uploaded': function froalaEditorImageUploaded(e, editor, response) {
                        // Image was uploaded to the server.
                        console.log('froalaEditor.image.uploaded');
                    },
                    'froalaEditor.image.inserted': function froalaEditorImageInserted(e, editor, $img, response) {
                        // Image was inserted in the editor.
                        console.log('froalaEditor.image.inserted');
                        editor.save.save();
                    },
                    'froalaEditor.image.replaced': function froalaEditorImageReplaced(e, editor, $img, response) {
                        // Image was replaced in the editor.
                        console.log('froalaEditor.image.replaced');
                    },
                    'froalaEditor.image.removed': function () {
                        var _ref = _asyncToGenerator( /*#__PURE__*/__WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default.a.mark(function _callee(e, editor, $img) {
                            var response;
                            return __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default.a.wrap(function _callee$(_context) {
                                while (1) {
                                    switch (_context.prev = _context.next) {
                                        case 0:
                                            console.log('froalaEditor.image.removed');
                                            _context.next = 3;
                                            return axios.post('/editor/deleteImage', { src: $img.attr('src'), type: 's3' });

                                        case 3:
                                            response = _context.sent;

                                            editor.save.save();

                                        case 5:
                                        case 'end':
                                            return _context.stop();
                                    }
                                }
                            }, _callee, this);
                        }));

                        function froalaEditorImageRemoved(_x, _x2, _x3) {
                            return _ref.apply(this, arguments);
                        }

                        return froalaEditorImageRemoved;
                    }()
                }
            }
        };
    },

    props: {
        channelProp: {
            type: Object,
            required: true
        }
    },
    updated: function updated() {
        var self = this;
        this.$nextTick(function () {
            //console.log('Channel Info updated')
            //self.runCropInit()
        });
    },
    beforeMount: function beforeMount() {
        this.channel = this.channelProp;
        if (this.channel.head_image) this.defaultImage.push(this.channel.head_image);else this.defaultImage.push(this.headDefault);
        if (this.channel.logo_image) this.defaultImage.push(this.channel.logo_image);else this.defaultImage.push(this.logoDefault);
    },
    mounted: function mounted() {
        var self = this;

        // set options
        this.getOptions = {
            waveHeadImage: {
                width: 1920,
                height: 622,
                minWidth: 256,
                minHeight: 256,
                maxWidth: 2000,
                maxHeight: 1500,
                imageSmoothingEnabled: false,
                imageSmoothingQuality: 'high'
            },
            waveLogoImage: {
                width: 200,
                height: 200,
                minWidth: 30,
                minHeight: 30,
                maxWidth: 1000,
                maxHeight: 1000,
                imageSmoothingEnabled: false,
                imageSmoothingQuality: 'high'
            }
        };
        this.cropOptions = {
            waveHeadImage: {
                aspectRatio: 3.08,
                viewMode: 2,
                highlight: true,
                modal: false,
                autoCropArea: 1,
                minCanvasWidth: 100,
                minCanvasHeight: 100,
                minCropBoxWidth: 100,
                minCropBoxHeight: 100,
                ready: function ready(e) {
                    self.getImageCrop(0, self.getOptions['waveHeadImage']);
                },
                cropstart: function cropstart(e) {},
                cropmove: function cropmove(e) {},
                cropend: function cropend(e) {
                    self.getImageCrop(0, self.getOptions['waveHeadImage']);
                },
                crop: function crop(e) {},
                zoom: function zoom(e) {}
            },
            waveLogoImage: {
                aspectRatio: 1,
                viewMode: 2,
                highlight: true,
                modal: false,
                autoCropArea: 1,
                minCanvasWidth: 30,
                minCanvasHeight: 30,
                minCropBoxWidth: 30,
                minCropBoxHeight: 30,
                ready: function ready(e) {
                    self.getImageCrop(1, self.getOptions['waveLogoImage']);
                },
                cropstart: function cropstart(e) {},
                cropmove: function cropmove(e) {},
                cropend: function cropend(e) {
                    self.getImageCrop(1, self.getOptions['waveLogoImage']);
                },
                crop: function crop(e) {},
                zoom: function zoom(e) {}
            }
        };
        console.log('channel info mounted');
        this.runCropInit();
    },

    methods: {
        updateProfile: function () {
            var _ref2 = _asyncToGenerator( /*#__PURE__*/__WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default.a.mark(function _callee2() {
                var data, response;
                return __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default.a.wrap(function _callee2$(_context2) {
                    while (1) {
                        switch (_context2.prev = _context2.next) {
                            case 0:

                                this.errorMessage = '';

                                if (this.formValid()) {
                                    _context2.next = 3;
                                    break;
                                }

                                return _context2.abrupt('return', false);

                            case 3:
                                data = new FormData();

                                data.append('name', this.channel.name);
                                data.append('status_message', this.channel.status_message);
                                data.append('description', this.channel.description);
                                // image set
                                if (this.inputImage[0].value && this.waveHeadImage != null) {
                                    //head image
                                    data.append('head_image', this.waveHeadImage);
                                }

                                if (this.inputImage[1].value && this.waveLogoImage != null) {
                                    //logo image
                                    data.append('photo', this.waveLogoImage);
                                }

                                _context2.next = 11;
                                return axios.post('/channel/' + this.channel.slug + '/updateProfile', data, {
                                    headers: {
                                        'Content-Type': 'multipart/form-data'
                                    }
                                });

                            case 11:
                                response = _context2.sent;


                                this.afterUpdate(response);

                            case 13:
                            case 'end':
                                return _context2.stop();
                        }
                    }
                }, _callee2, this);
            }));

            function updateProfile() {
                return _ref2.apply(this, arguments);
            }

            return updateProfile;
        }(),
        afterUpdate: function afterUpdate(response) {
            if (response.data.result == 'success') {
                if (this.channelProp.slug != response.data.channel.slug) {
                    this.errorMessage = '업데이트 완료. 페이지가 다시 열립니다. 기다려주세요.';
                    setTimeout(function () {
                        window.location.href = "/channel/" + response.data.channel.slug + '/edit';
                    }, 1500);
                } else {
                    this.channel = response.data.channel;
                    this.errorMessage = '업데이트 완료';
                    // 업데이트 이벤트 발생
                    __WEBPACK_IMPORTED_MODULE_1__event_js___default.a.$emit('channelInfoUpdate', this.channel);
                }
            } else if (response.data.result == 'error') {
                this.errorMessage = '업데이트 실패. 다시 시도해 주세요.';

                if ('errors' in response.data) {
                    this.errorMessage = '';
                    var message = '';
                    _.forEach(response.data.errors, function (value, key) {
                        message += value;
                        message += '<br>';
                    });
                    this.errorMessage = message;
                }
            }
        },
        formValid: function formValid() {
            var errorChk = true;
            this.errorMessage = '';

            this.channel.name = _.trim(this.channel.name);
            if (this.channel.name == '') {
                errorChk = false;
                this.errorMessage = '채널 이름을 작성해주세요';
                return false;
            }

            if (this.channel.description == '') {
                errorChk = false;
                this.errorMessage = '채널 소개글을 작성해주세요';
                return false;
            }

            this.errorMessage = '채널 업데이트 중 ...';
            return errorChk;
        },
        runCropInit: function runCropInit() {
            var self = this;
            //self.runCrop()
            setTimeout(function () {
                self.runCrop();
            }, 1500);
        },
        runCrop: function runCrop() {
            var _this = this;

            //if(!this.cropperLoad){
            // Images get
            this.images = document.querySelectorAll('.crop-image');
            // get Imput images
            this.inputImage = document.querySelectorAll('.input-image-file');

            var self = this;
            // cropper set
            var length = this.images.length;

            var _loop = function _loop(i) {

                _this.croppers[i] = new Cropper(_this.images[i], _this.cropOptions[_this.cropperImages[i]]);

                _this.inputImage[i].onchange = function () {
                    var files = this.files;
                    var file = void 0;
                    var URL = window.URL || window.webkitURL;

                    if (self.croppers[i] && files && files.length) {
                        file = files[0];

                        if (/^image\/\w+/.test(file.type)) {
                            console.log(file.size);
                            console.log(file.size / (1024 * 1024));
                            self.images[i].src = URL.createObjectURL(file);
                            self.croppers[i].destroy();
                            self.croppers[i] = new Cropper(self.images[i], self.cropOptions[self.cropperImages[i]]);
                        } else {
                            window.alert('이미지 파일을 선택해 주세요.');
                        }
                    }
                };
            };

            for (var i = 0; i < length; i++) {
                _loop(i);
            }
            this.cropperLoad = true;
            //}
        },
        getImageCrop: function getImageCrop(index, option) {
            var self = this;
            this.croppers[index].getCroppedCanvas(option).toBlob(function (blob) {
                if (index == 0) self.waveHeadImage = blob;else self.waveLogoImage = blob;
            });
        },
        imageCancel: function imageCancel(num) {
            this.resetInputImage(num);
        },
        resetInputImage: function resetInputImage(num) {
            this.images[num].src = this.defaultImage[num];
            this.croppers[num].destroy();
            this.croppers[num] = new Cropper(this.images[num], this.cropOptions[this.cropperImages[num]]);
            this.inputImage[num].value = null;
            if (num == 0) this.waveHeadImage = null;else this.waveLogoImage = null;
        },
        getHeadImage: function getHeadImage() {
            return this.channel.head_image ? this.channel.head_image : this.headDefault;
        },
        getLogoImage: function getLogoImage() {
            return this.channel.logo_image ? this.channel.logo_image : this.logoDefault;
        }
    }
});

/***/ }),

/***/ 511:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", {}, [
    _c(
      "div",
      { staticClass: "form-group", staticStyle: { "margin-top": "30px" } },
      [
        _c("label", { attrs: { for: "title" } }, [_vm._v("채널명 변경")]),
        _vm._v(" "),
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.channel.name,
              expression: "channel.name"
            }
          ],
          attrs: { type: "text" },
          domProps: { value: _vm.channel.name },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.$set(_vm.channel, "name", $event.target.value)
            }
          }
        }),
        _vm._v(" "),
        _c("p", { staticClass: "help-block" }, [
          _vm._v("채널명 변경시 페이지가 다시 열립니다")
        ])
      ]
    ),
    _vm._v(" "),
    _c("div", { staticClass: "form-group" }, [
      _c("label", { attrs: { for: "headImage" } }, [
        _vm._v("채널 상단 이미지")
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "img-container" }, [
        _c("img", {
          staticClass: "crop-image",
          attrs: { src: _vm.getHeadImage(), id: "headCrop" }
        })
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "btn-group image-button-group" }, [
        _c(
          "a",
          {
            staticClass: "btn btn-small btn--orange-light",
            staticStyle: { "margin-bottom": "10px" },
            on: {
              click: function($event) {
                $event.preventDefault()
                _vm.imageCancel(0)
              }
            }
          },
          [_vm._v("이미지 취소\n            ")]
        ),
        _vm._v(" "),
        _vm._m(0)
      ]),
      _vm._v(" "),
      _c("p", { staticClass: "help-block" }, [
        _vm._v("옵션사항입니다. 미지정 시 기본 이미지가 보여집니다.")
      ])
    ]),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "form-group", staticStyle: { "margin-top": "30px" } },
      [
        _c("label", { attrs: { for: "title" } }, [_vm._v("채널 상단 메세지")]),
        _vm._v(" "),
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.channel.status_message,
              expression: "channel.status_message"
            }
          ],
          attrs: { type: "text", maxlength: 255 },
          domProps: { value: _vm.channel.status_message },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.$set(_vm.channel, "status_message", $event.target.value)
            }
          }
        })
      ]
    ),
    _vm._v(" "),
    _c("div", { staticClass: "form-group" }, [
      _c("label", { attrs: { for: "logoImage" } }, [_vm._v("채널 로고")]),
      _vm._v(" "),
      _c("div", { staticClass: "img-container" }, [
        _c("img", {
          staticClass: "crop-image",
          attrs: { src: _vm.getLogoImage(), id: "logoCrop" }
        })
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "btn-group image-button-group" }, [
        _c(
          "a",
          {
            staticClass: "btn btn-small btn--orange-light",
            staticStyle: { "margin-bottom": "10px" },
            on: {
              click: function($event) {
                $event.preventDefault()
                _vm.imageCancel(1)
              }
            }
          },
          [_vm._v("이미지 취소\n            ")]
        ),
        _vm._v(" "),
        _vm._m(1)
      ]),
      _vm._v(" "),
      _c("p", { staticClass: "help-block" }, [
        _vm._v("옵션사항입니다. 미지정 시 기본 이미지가 보여집니다.")
      ])
    ]),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "form-group" },
      [
        _c("label", { attrs: { for: "description" } }, [_vm._v("채널 소개")]),
        _vm._v(" "),
        _c("froala", {
          attrs: { tag: "textarea", config: _vm.froalaConfig },
          model: {
            value: _vm.channel.description,
            callback: function($$v) {
              _vm.$set(_vm.channel, "description", $$v)
            },
            expression: "channel.description"
          }
        })
      ],
      1
    ),
    _vm._v(" "),
    _c(
      "a",
      {
        staticClass:
          "btn btn-border btn--with-shadow c-orange-light full-width",
        staticStyle: { "margin-bottom": "10px", "margin-top": "30px" },
        on: {
          click: function($event) {
            $event.preventDefault()
            return _vm.updateProfile($event)
          }
        }
      },
      [_vm._v("\n        업데이트\n    ")]
    ),
    _vm._v(" "),
    this.errorMessage != ""
      ? _c(
          "div",
          {
            staticClass: "summit-message",
            staticStyle: { "margin-top": "50px" }
          },
          [
            _c("p", {
              staticClass: "summit-error",
              domProps: { innerHTML: _vm._s(this.errorMessage) }
            })
          ]
        )
      : _vm._e()
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "label",
      {
        staticClass: "btn btn-small btn--primary",
        attrs: { title: "Upload image file", for: "headImage" }
      },
      [
        _c("input", {
          staticClass: "hide input-image-file",
          attrs: {
            type: "file",
            accept: "image/*",
            name: "headImage",
            id: "headImage"
          }
        }),
        _vm._v("\n                이미지 선택\n            ")
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "label",
      {
        staticClass: "btn btn-small btn--primary",
        attrs: { title: "Upload image file", for: "logoImage" }
      },
      [
        _c("input", {
          staticClass: "hide input-image-file",
          attrs: {
            type: "file",
            accept: "image/*",
            name: "logoImage",
            id: "logoImage"
          }
        }),
        _vm._v("\n                이미지 선택\n            ")
      ]
    )
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-18f5215e", module.exports)
  }
}

/***/ }),

/***/ 512:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(513)
/* template */
var __vue_template__ = __webpack_require__(514)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/components/channel/ChannelContact.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-404e02c0", Component.options)
  } else {
    hotAPI.reload("data-v-404e02c0", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 513:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__event_js__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__event_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__event_js__);


function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
    data: function data() {
        return {
            snsBrands: [],
            snsLists: [],
            errorMessage: '',
            settings: null,
            snsSelect: null,
            snsNameView: false,
            snsName: null,
            snsClass: null,
            snsLink: null,
            snsColor: null,
            addSNSMessage: '',
            snsEditIndex: null,
            snslinkEdit: null
        };
    },

    props: {
        channelProp: {
            type: Object,
            required: true
        },
        snsBrandsProp: {
            required: true,
            type: String
        }
    },
    updated: function updated() {
        var self = this;
        this.$nextTick(function () {});
    },
    mounted: function mounted() {
        var self = this;

        _.forEach(JSON.parse(this.snsBrandsProp), function (value, key) {
            self.snsBrands.unshift(value);
        });
        self.snsBrands.unshift({ name: '기타', class: 'fas fa-share-alt-square', color: '#495057' });

        self.settings = JSON.parse(self.channelProp.settings);

        _.forEach(self.settings.sns, function (value, key) {
            self.snsLists.unshift(value);
        });
    },

    methods: {
        cancelEdit: function cancelEdit() {
            this.snsEditIndex = null;
        },
        editRunSNS: function () {
            var _ref = _asyncToGenerator( /*#__PURE__*/__WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default.a.mark(function _callee(snsList, index) {
                var response, snsList2;
                return __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default.a.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                _context.next = 2;
                                return axios.post('/channel/' + this.channelProp.slug + '/snsEdit', { name: snsList.name, link: this.snslinkEdit });

                            case 2:
                                response = _context.sent;
                                snsList2 = this.snsLists.find(function (snsList2) {
                                    return snsList2.name === snsList.name;
                                });

                                snsList2.link = response.data.link;

                                __WEBPACK_IMPORTED_MODULE_1__event_js___default.a.$emit('snsUpdate', this.snsLists);

                                this.initComponent();

                            case 7:
                            case 'end':
                                return _context.stop();
                        }
                    }
                }, _callee, this);
            }));

            function editRunSNS(_x, _x2) {
                return _ref.apply(this, arguments);
            }

            return editRunSNS;
        }(),
        editSNS: function editSNS(snsList, index) {
            this.snsEditIndex = index;
            this.snslinkEdit = snsList.link;
        },
        deleteSNS: function () {
            var _ref2 = _asyncToGenerator( /*#__PURE__*/__WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default.a.mark(function _callee2(snsList) {
                var _this = this;

                var response;
                return __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default.a.wrap(function _callee2$(_context2) {
                    while (1) {
                        switch (_context2.prev = _context2.next) {
                            case 0:
                                _context2.next = 2;
                                return axios.post('/channel/' + this.channelProp.slug + '/snsDelete', { name: snsList.name });

                            case 2:
                                response = _context2.sent;


                                this.snsLists.map(function (snsList2, index) {
                                    if (snsList2.name === snsList.name) {
                                        _this.snsLists.splice(index, 1);
                                        return;
                                    }
                                });

                                __WEBPACK_IMPORTED_MODULE_1__event_js___default.a.$emit('snsUpdate', this.snsLists);

                                this.initComponent();

                            case 6:
                            case 'end':
                                return _context2.stop();
                        }
                    }
                }, _callee2, this);
            }));

            function deleteSNS(_x3) {
                return _ref2.apply(this, arguments);
            }

            return deleteSNS;
        }(),
        initComponent: function initComponent() {
            this.snsName = null;
            this.snsClass = null;
            this.snsLink = null;
            this.snsColor = null;
            this.addSNSMessage = '';
            this.snslinkEdit = null;
            this.snsEditIndex = null;
            this.snsSelect = null;
        },
        addSNSCancel: function addSNSCancel() {
            $('.add-form').css('display', 'none');
            this.initComponent();
        },
        addSNS: function addSNS() {
            this.addSNSMessage = '';

            if (this.existCheck()) {
                this.addSNSMessage = '이미 리스트에 있습니다';
                return false;
            }

            if (_.trim(this.snsName) == '') {
                this.addSNSMessage = '서비스명을 선택 또는 작성해주세요';
                return false;
            }
            if (_.trim(this.snsClass) == '' || _.trim(this.snsColor) == '') {
                this.addSNSMessage = '오류가 발생하였습니다. 다시 시도해 주세요';
                return false;
            }
            if (_.trim(this.snsLink) == '') {
                this.addSNSMessage = 'URL Link를 작성해주세요';
                return false;
            }

            this.sendSNSToServer();
        },
        sendSNSToServer: function () {
            var _ref3 = _asyncToGenerator( /*#__PURE__*/__WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default.a.mark(function _callee3() {
                var response, self;
                return __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default.a.wrap(function _callee3$(_context3) {
                    while (1) {
                        switch (_context3.prev = _context3.next) {
                            case 0:
                                _context3.next = 2;
                                return axios.post('/channel/' + this.channelProp.slug + '/sns', { name: this.snsName, class: this.snsClass, link: this.snsLink, color: this.snsColor });

                            case 2:
                                response = _context3.sent;


                                this.snsLists.unshift(response.data);

                                this.initComponent();

                                this.addSNSMessage = '추가되었습니다';

                                __WEBPACK_IMPORTED_MODULE_1__event_js___default.a.$emit('snsUpdate', this.snsLists);

                                self = this;

                                setTimeout(function () {
                                    self.addSNSMessage = '';
                                }, 5000);

                            case 9:
                            case 'end':
                                return _context3.stop();
                        }
                    }
                }, _callee3, this);
            }));

            function sendSNSToServer() {
                return _ref3.apply(this, arguments);
            }

            return sendSNSToServer;
        }(),
        existCheck: function existCheck() {
            var snsTemp = _.find(this.snsLists, { name: this.snsName });
            if (snsTemp) return true;else return false;
        },
        snsSelected: function snsSelected(obj) {
            if (obj) {
                if (obj.name == '기타') {
                    this.snsNameView = true;
                    this.snsName = '';
                } else {
                    this.snsNameView = false;
                    this.snsName = obj.name;
                }
                this.snsClass = obj.class;
                this.snsColor = obj.color;
            }
        },
        addSNSFormView: function addSNSFormView() {
            $('.add-form').css('display', 'block');
        },
        channelContactUpdate: function () {
            var _ref4 = _asyncToGenerator( /*#__PURE__*/__WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default.a.mark(function _callee4() {
                var response, self;
                return __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default.a.wrap(function _callee4$(_context4) {
                    while (1) {
                        switch (_context4.prev = _context4.next) {
                            case 0:
                                this.errorMessage = '';

                                if (!(this.settings.email != '' && !this.validEmail(this.settings.email))) {
                                    _context4.next = 4;
                                    break;
                                }

                                this.errorMessage = '유효한 이메일 주소가 아닙니다. 다시 확인해 주세요.';
                                return _context4.abrupt('return', false);

                            case 4:
                                _context4.next = 6;
                                return axios.post('/channel/' + this.channelProp.slug + '/contact', { phone: this.settings.phone, site: this.settings.site, email: this.settings.email });

                            case 6:
                                response = _context4.sent;


                                this.errorMessage = '업데이트가 완료되었습니다';

                                __WEBPACK_IMPORTED_MODULE_1__event_js___default.a.$emit('channelContactUpdate', response.data);

                                self = this;

                                setTimeout(function () {
                                    self.errorMessage = '';
                                }, 5000);

                            case 11:
                            case 'end':
                                return _context4.stop();
                        }
                    }
                }, _callee4, this);
            }));

            function channelContactUpdate() {
                return _ref4.apply(this, arguments);
            }

            return channelContactUpdate;
        }(),

        validEmail: function validEmail(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }
    }
});

/***/ }),

/***/ 514:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", {}, [
    _vm.settings
      ? _c("div", { staticClass: "contact-lists" }, [
          _c("div", { staticClass: "form-group" }, [
            _c("label", { attrs: { for: "email" } }, [_vm._v("이메일 주소")]),
            _vm._v(" "),
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model.trim",
                  value: _vm.settings.email,
                  expression: "settings.email",
                  modifiers: { trim: true }
                }
              ],
              attrs: { type: "email" },
              domProps: { value: _vm.settings.email },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.settings, "email", $event.target.value.trim())
                },
                blur: function($event) {
                  _vm.$forceUpdate()
                }
              }
            })
          ]),
          _vm._v(" "),
          _c(
            "div",
            {
              staticClass: "form-group",
              staticStyle: { "margin-top": "30px" }
            },
            [
              _c("label", { attrs: { for: "phone" } }, [_vm._v("휴대폰 번호")]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model.number",
                    value: _vm.settings.phone,
                    expression: "settings.phone",
                    modifiers: { number: true }
                  }
                ],
                attrs: { type: "number" },
                domProps: { value: _vm.settings.phone },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.settings, "phone", _vm._n($event.target.value))
                  },
                  blur: function($event) {
                    _vm.$forceUpdate()
                  }
                }
              })
            ]
          ),
          _vm._v(" "),
          _c(
            "div",
            {
              staticClass: "form-group",
              staticStyle: { "margin-top": "30px" }
            },
            [
              _c("label", { attrs: { for: "site" } }, [_vm._v("사이트 주소")]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model.trim",
                    value: _vm.settings.site,
                    expression: "settings.site",
                    modifiers: { trim: true }
                  }
                ],
                attrs: { type: "text" },
                domProps: { value: _vm.settings.site },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.settings, "site", $event.target.value.trim())
                  },
                  blur: function($event) {
                    _vm.$forceUpdate()
                  }
                }
              })
            ]
          ),
          _vm._v(" "),
          _c(
            "a",
            {
              staticClass: "btn btn-small btn--green btn--with-shadow ",
              staticStyle: { width: "200px" },
              attrs: { href: "" },
              on: {
                click: function($event) {
                  $event.preventDefault()
                  return _vm.channelContactUpdate($event)
                }
              }
            },
            [_c("span", { staticClass: "text" }, [_vm._v("업데이트")])]
          ),
          _vm._v(" "),
          this.errorMessage != ""
            ? _c(
                "div",
                {
                  staticClass: "summit-message",
                  staticStyle: { "margin-top": "50px" }
                },
                [
                  _c("p", {
                    staticClass: "summit-error",
                    domProps: { innerHTML: _vm._s(this.errorMessage) }
                  })
                ]
              )
            : _vm._e()
        ])
      : _vm._e(),
    _vm._v(" "),
    _c("div", { staticClass: "or none" }),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "sns-lists", staticStyle: { "margin-top": "50px" } },
      [
        _c("h3", { staticStyle: { "margin-top": "50px" } }, [
          _vm._v("채널 SNS")
        ]),
        _vm._v(" "),
        _vm.snsLists.length > 0
          ? _c(
              "div",
              [
                _c("h5", [_vm._v("SNS 리스트")]),
                _vm._v(" "),
                _vm._l(_vm.snsLists, function(snsList, index) {
                  return _c("div", { key: snsList.name, staticClass: "row" }, [
                    _c(
                      "div",
                      {
                        staticClass: "col-md-1 sns-icon",
                        style: { color: snsList.color }
                      },
                      [_c("span", [_c("i", { class: snsList.class })])]
                    ),
                    _vm._v(" "),
                    _c("div", { staticClass: "col-md-3 sns-title" }, [
                      _c("span", [_vm._v(_vm._s(snsList.name))])
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "col-md-6 sns-link" }, [
                      _c(
                        "span",
                        {
                          directives: [
                            {
                              name: "show",
                              rawName: "v-show",
                              value: index != _vm.snsEditIndex,
                              expression: "index != snsEditIndex"
                            }
                          ]
                        },
                        [
                          _c(
                            "a",
                            { attrs: { target: "_blank", href: snsList.link } },
                            [_vm._v(_vm._s(snsList.link))]
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          directives: [
                            {
                              name: "show",
                              rawName: "v-show",
                              value: index == _vm.snsEditIndex,
                              expression: "index == snsEditIndex"
                            }
                          ],
                          staticClass: "form-group"
                        },
                        [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.snslinkEdit,
                                expression: "snslinkEdit"
                              }
                            ],
                            attrs: { type: "text" },
                            domProps: { value: _vm.snslinkEdit },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.snslinkEdit = $event.target.value
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              staticClass: "tech-edit-buttons",
                              staticStyle: { "margin-top": "0" }
                            },
                            [
                              _c(
                                "a",
                                {
                                  staticClass:
                                    "btn btn-small btn--green-light btn--with-shadow",
                                  attrs: { title: "수정" },
                                  on: {
                                    click: function($event) {
                                      $event.preventDefault()
                                      _vm.editRunSNS(snsList, index)
                                    }
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n                                수정\n                            "
                                  )
                                ]
                              ),
                              _vm._v(" "),
                              _c(
                                "a",
                                {
                                  staticClass:
                                    "btn btn-small btn--secondary btn--with-shadow",
                                  attrs: { title: "삭제" },
                                  on: {
                                    click: function($event) {
                                      $event.preventDefault()
                                      return _vm.cancelEdit($event)
                                    }
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n                                취소\n                            "
                                  )
                                ]
                              )
                            ]
                          )
                        ]
                      )
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "col-md-2 sns-action-buttons" }, [
                      _c(
                        "a",
                        {
                          staticClass: "c-gray",
                          attrs: { id: "editBtn" + index, title: "수정" },
                          on: {
                            click: function($event) {
                              $event.preventDefault()
                              _vm.editSNS(snsList, index)
                            }
                          }
                        },
                        [_c("i", { staticClass: "fas fa-pen-square" })]
                      ),
                      _vm._v(" "),
                      _c(
                        "a",
                        {
                          staticClass: "c-gray",
                          attrs: { title: "삭제" },
                          on: {
                            click: function($event) {
                              $event.preventDefault()
                              _vm.deleteSNS(snsList)
                            }
                          }
                        },
                        [_c("i", { staticClass: "fas fa-window-close" })]
                      )
                    ])
                  ])
                })
              ],
              2
            )
          : _vm._e(),
        _vm._v(" "),
        _c(
          "a",
          {
            staticClass: "btn btn-small btn--yellow btn--with-shadow",
            staticStyle: { width: "250px", "margin-top": "10px" },
            attrs: { href: "" },
            on: {
              click: function($event) {
                $event.preventDefault()
                return _vm.addSNSFormView($event)
              }
            }
          },
          [_c("i", { staticClass: "fas fa-plus" }), _vm._v(" SNS 추가")]
        ),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "add-form", staticStyle: { display: "none" } },
          [
            _c(
              "div",
              {
                staticClass: "form-group",
                staticStyle: { "margin-top": "30px" }
              },
              [
                _c("label", { attrs: { for: "title" } }, [_vm._v("SNS 선택")]),
                _vm._v(" "),
                _c("v-select", {
                  attrs: { options: _vm.snsBrands, label: "name" },
                  on: { input: _vm.snsSelected },
                  scopedSlots: _vm._u([
                    {
                      key: "option",
                      fn: function(option) {
                        return [
                          _c("span", {
                            class: option.class,
                            style: { color: option.color }
                          }),
                          _vm._v(
                            "\n                        " +
                              _vm._s(option.name) +
                              "\n                    "
                          )
                        ]
                      }
                    },
                    {
                      key: "selected-option",
                      fn: function(option) {
                        return [
                          _c("div", { staticClass: "selected d-center" }, [
                            _c("span", {
                              class: option.class,
                              style: { color: option.color }
                            }),
                            _vm._v(
                              "\n                          " +
                                _vm._s(option.name) +
                                "\n                      "
                            )
                          ])
                        ]
                      }
                    }
                  ]),
                  model: {
                    value: _vm.snsSelect,
                    callback: function($$v) {
                      _vm.snsSelect = $$v
                    },
                    expression: "snsSelect"
                  }
                })
              ],
              1
            ),
            _vm._v(" "),
            _vm.snsNameView
              ? _c(
                  "div",
                  {
                    staticClass: "form-group",
                    staticStyle: { "margin-top": "30px" }
                  },
                  [
                    _c("label", { attrs: { for: "title" } }, [
                      _vm._v("SNS 서비스명")
                    ]),
                    _vm._v(" "),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.snsName,
                          expression: "snsName"
                        }
                      ],
                      attrs: { type: "text" },
                      domProps: { value: _vm.snsName },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.snsName = $event.target.value
                        }
                      }
                    })
                  ]
                )
              : _vm._e(),
            _vm._v(" "),
            _c("div", { staticClass: "form-group" }, [
              _c("label", { attrs: { for: "title" } }, [_vm._v("SNS 링크")]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.snsLink,
                    expression: "snsLink"
                  }
                ],
                attrs: {
                  type: "text",
                  placeholder: "https://www.youtube.com/channel/codewwwave"
                },
                domProps: { value: _vm.snsLink },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.snsLink = $event.target.value
                  }
                }
              })
            ]),
            _vm._v(" "),
            _c(
              "a",
              {
                staticClass: "btn btn-small btn--green-light btn--with-shadow",
                staticStyle: { width: "200px" },
                attrs: { href: "" },
                on: {
                  click: function($event) {
                    $event.preventDefault()
                    return _vm.addSNS($event)
                  }
                }
              },
              [_vm._v("저 장 ")]
            ),
            _vm._v(" "),
            _c(
              "a",
              {
                staticClass: "btn btn-small btn--secondary btn--with-shadow",
                staticStyle: { width: "200px", "margin-left": "10px" },
                attrs: { href: "" },
                on: {
                  click: function($event) {
                    $event.preventDefault()
                    return _vm.addSNSCancel($event)
                  }
                }
              },
              [_vm._v("취 소")]
            ),
            _vm._v(" "),
            _vm.addSNSMessage != ""
              ? _c("p", { staticStyle: { "margin-top": "20px" } }, [
                  _vm._v(_vm._s(_vm.addSNSMessage))
                ])
              : _vm._e()
          ]
        )
      ]
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-404e02c0", module.exports)
  }
}

/***/ }),

/***/ 515:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(516)
/* template */
var __vue_template__ = __webpack_require__(517)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/components/channel/SearchList.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-a1961f2e", Component.options)
  } else {
    hotAPI.reload("data-v-a1961f2e", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 516:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__event_js__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__event_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__event_js__);


var _this = this;

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
    data: function data() {
        return {
            itemSelect: null,
            options: [],
            viewOptions: {
                requestBtn: false,
                requestBtnText: '업데이트'
            },
            errorMessage: ''
        };
    },

    props: {
        viewOptionsProp: {
            type: Object
        },
        urlPath: {
            type: String
        },
        eventOn: {
            type: Boolean
        },
        eventName: {
            type: String
        }
    },
    mounted: function mounted() {
        var self = this;
        if (this.viewOptionsProp) {
            _.forEach(this.viewOptionsProp, function (value, key) {
                Vue.set(self.viewOptions, key, value);
            });
        }
    },

    methods: {
        uploadRequest: function () {
            var _ref = _asyncToGenerator( /*#__PURE__*/__WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default.a.mark(function _callee() {
                var response, self;
                return __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default.a.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                if (!(this.urlPath && this.itemSelect)) {
                                    _context.next = 9;
                                    break;
                                }

                                _context.next = 3;
                                return axios.post(this.urlPath, { id: this.itemSelect.id });

                            case 3:
                                response = _context.sent;
                                self = this;

                                console.log(response);
                                this.errorMessage = response.data.message;

                                if (this.eventOn && response.data.result == 'success') {
                                    __WEBPACK_IMPORTED_MODULE_1__event_js___default.a.$emit(this.eventName, this.itemSelect);
                                }

                                setTimeout(function () {
                                    self.errorMessage = '';
                                }, 5000);

                            case 9:
                            case 'end':
                                return _context.stop();
                        }
                    }
                }, _callee, this);
            }));

            function uploadRequest() {
                return _ref.apply(this, arguments);
            }

            return uploadRequest;
        }(),
        onSelected: function onSelected() {
            console.log(this.itemSelect);
        },
        onSearch: function onSearch(search, loading) {
            loading(true);
            this.search(loading, search, this);
        },

        search: _.debounce(function (loading, search, vm) {
            var self = _this;
            axios.get('/search/users?q=' + escape(search)).then(function (response) {
                vm.options = response.data.data;
                loading(false);
                //console.log(response)
                //console.log(response)
            }).catch(function (e) {
                console.log(e);
            });
        }, 350)
    }
});

/***/ }),

/***/ 517:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {},
    [
      _c(
        "v-select",
        {
          attrs: { options: _vm.options, label: "name" },
          on: { search: _vm.onSearch },
          scopedSlots: _vm._u([
            {
              key: "option",
              fn: function(option) {
                return [
                  _c("div", { staticClass: "d-center" }, [
                    _c("img", { attrs: { src: option.photo } }),
                    _vm._v(
                      "\n                  " +
                        _vm._s(option.name) +
                        "\n            "
                    )
                  ])
                ]
              }
            },
            {
              key: "selected-option",
              fn: function(option) {
                return [
                  _c("div", { staticClass: "selected d-center" }, [
                    _c("img", { attrs: { src: option.photo } }),
                    _vm._v(
                      "\n              " + _vm._s(option.name) + "\n          "
                    )
                  ])
                ]
              }
            }
          ]),
          model: {
            value: _vm.itemSelect,
            callback: function($$v) {
              _vm.itemSelect = $$v
            },
            expression: "itemSelect"
          }
        },
        [
          _c("template", { slot: "no-options" }, [
            _vm._v(
              "\n            유저의 이름 또는 이메일을 검색해보세요.\n        "
            )
          ])
        ],
        2
      ),
      _vm._v(" "),
      _vm.viewOptions.requestBtn
        ? _c(
            "a",
            {
              staticClass: "btn btn-small btn--green btn--with-shadow ",
              staticStyle: { width: "200px", "margin-top": "20px" },
              attrs: { href: "" },
              on: {
                click: function($event) {
                  $event.preventDefault()
                  return _vm.uploadRequest($event)
                }
              }
            },
            [
              _c("span", { staticClass: "text" }, [
                _vm._v(_vm._s(_vm.viewOptions.requestBtnText))
              ])
            ]
          )
        : _vm._e(),
      _vm._v(" "),
      _vm.errorMessage != ""
        ? _c(
            "div",
            {
              staticClass: "summit-message",
              staticStyle: { "margin-top": "20px" }
            },
            [
              _c("p", {
                staticClass: "summit-error",
                domProps: { innerHTML: _vm._s(_vm.errorMessage) }
              })
            ]
          )
        : _vm._e()
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-a1961f2e", module.exports)
  }
}

/***/ }),

/***/ 518:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(519)
/* template */
var __vue_template__ = __webpack_require__(520)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/components/channel/Settings.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-5faeae66", Component.options)
  } else {
    hotAPI.reload("data-v-5faeae66", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 519:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__event_js__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__event_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__event_js__);


function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
    data: function data() {
        return {
            settings: {
                contacts: true,
                messageForm: true,
                sns: true,
                tech: true
            },
            errorMessage: ''
        };
    },

    props: {
        urlPath: {
            required: true,
            type: String
        },
        settingsProp: {
            required: true,
            type: Object
        }
    },
    mounted: function mounted() {
        var self = this;
        /*let settings = JSON.parse(user.options)*/

        _.forEach(this.settingsProp.viewoptions, function (value, key) {
            Vue.set(self.settings, key, value);
        });
    },

    methods: {
        updateSettings: function () {
            var _ref = _asyncToGenerator( /*#__PURE__*/__WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default.a.mark(function _callee() {
                var result, self;
                return __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default.a.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                this.errorMessage = '';

                                _context.next = 3;
                                return axios.post(this.urlPath, this.settings);

                            case 3:
                                result = _context.sent;
                                self = this;

                                if (result.data.result == 'success') {
                                    this.errorMessage = '설정이 변경되었습니다';

                                    __WEBPACK_IMPORTED_MODULE_1__event_js___default.a.$emit('channelSettingsUpdate', this.settings);

                                    setTimeout(function () {
                                        self.errorMessage = '';
                                    }, 5000);
                                } else {
                                    this.errorMessage = '설정 변경에 실패하였습니다';
                                }

                            case 6:
                            case 'end':
                                return _context.stop();
                        }
                    }
                }, _callee, this);
            }));

            function updateSettings() {
                return _ref.apply(this, arguments);
            }

            return updateSettings;
        }()
    }
});

/***/ }),

/***/ 520:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", {}, [
    _c(
      "div",
      {
        staticClass: "description-toggle",
        staticStyle: { "margin-top": "30px" }
      },
      [
        _vm._m(0),
        _vm._v(" "),
        _c("div", { staticClass: "togglebutton" }, [
          _c("label", [
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.settings.contacts,
                  expression: "settings.contacts"
                }
              ],
              attrs: { type: "checkbox", checked: "" },
              domProps: {
                checked: Array.isArray(_vm.settings.contacts)
                  ? _vm._i(_vm.settings.contacts, null) > -1
                  : _vm.settings.contacts
              },
              on: {
                change: function($event) {
                  var $$a = _vm.settings.contacts,
                    $$el = $event.target,
                    $$c = $$el.checked ? true : false
                  if (Array.isArray($$a)) {
                    var $$v = null,
                      $$i = _vm._i($$a, $$v)
                    if ($$el.checked) {
                      $$i < 0 &&
                        _vm.$set(_vm.settings, "contacts", $$a.concat([$$v]))
                    } else {
                      $$i > -1 &&
                        _vm.$set(
                          _vm.settings,
                          "contacts",
                          $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                        )
                    }
                  } else {
                    _vm.$set(_vm.settings, "contacts", $$c)
                  }
                }
              }
            }),
            _c("span", { staticClass: "toggle" })
          ])
        ])
      ]
    ),
    _vm._v(" "),
    _c(
      "div",
      {
        staticClass: "description-toggle",
        staticStyle: { "margin-top": "30px" }
      },
      [
        _vm._m(1),
        _vm._v(" "),
        _c("div", { staticClass: "togglebutton" }, [
          _c("label", [
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.settings.messageForm,
                  expression: "settings.messageForm"
                }
              ],
              attrs: { type: "checkbox", checked: "" },
              domProps: {
                checked: Array.isArray(_vm.settings.messageForm)
                  ? _vm._i(_vm.settings.messageForm, null) > -1
                  : _vm.settings.messageForm
              },
              on: {
                change: function($event) {
                  var $$a = _vm.settings.messageForm,
                    $$el = $event.target,
                    $$c = $$el.checked ? true : false
                  if (Array.isArray($$a)) {
                    var $$v = null,
                      $$i = _vm._i($$a, $$v)
                    if ($$el.checked) {
                      $$i < 0 &&
                        _vm.$set(_vm.settings, "messageForm", $$a.concat([$$v]))
                    } else {
                      $$i > -1 &&
                        _vm.$set(
                          _vm.settings,
                          "messageForm",
                          $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                        )
                    }
                  } else {
                    _vm.$set(_vm.settings, "messageForm", $$c)
                  }
                }
              }
            }),
            _c("span", { staticClass: "toggle" })
          ])
        ])
      ]
    ),
    _vm._v(" "),
    _c(
      "div",
      {
        staticClass: "description-toggle",
        staticStyle: { "margin-top": "30px" }
      },
      [
        _vm._m(2),
        _vm._v(" "),
        _c("div", { staticClass: "togglebutton" }, [
          _c("label", [
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.settings.sns,
                  expression: "settings.sns"
                }
              ],
              attrs: { type: "checkbox", checked: "" },
              domProps: {
                checked: Array.isArray(_vm.settings.sns)
                  ? _vm._i(_vm.settings.sns, null) > -1
                  : _vm.settings.sns
              },
              on: {
                change: function($event) {
                  var $$a = _vm.settings.sns,
                    $$el = $event.target,
                    $$c = $$el.checked ? true : false
                  if (Array.isArray($$a)) {
                    var $$v = null,
                      $$i = _vm._i($$a, $$v)
                    if ($$el.checked) {
                      $$i < 0 &&
                        _vm.$set(_vm.settings, "sns", $$a.concat([$$v]))
                    } else {
                      $$i > -1 &&
                        _vm.$set(
                          _vm.settings,
                          "sns",
                          $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                        )
                    }
                  } else {
                    _vm.$set(_vm.settings, "sns", $$c)
                  }
                }
              }
            }),
            _c("span", { staticClass: "toggle" })
          ])
        ])
      ]
    ),
    _vm._v(" "),
    _c(
      "a",
      {
        staticClass: "btn btn-small btn--green btn--with-shadow ",
        staticStyle: { width: "200px" },
        attrs: { href: "" },
        on: {
          click: function($event) {
            $event.preventDefault()
            return _vm.updateSettings($event)
          }
        }
      },
      [_c("span", { staticClass: "text" }, [_vm._v("변경하기")])]
    ),
    _vm._v(" "),
    this.errorMessage != ""
      ? _c(
          "div",
          {
            staticClass: "summit-message",
            staticStyle: { "margin-top": "20px" }
          },
          [
            _c("p", {
              staticClass: "summit-error",
              domProps: { innerHTML: _vm._s(this.errorMessage) }
            })
          ]
        )
      : _vm._e()
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "description-toggle-content" }, [
      _c("div", { staticClass: "h6" }, [_vm._v("채널 연락처 정보 보이기")]),
      _vm._v(" "),
      _c("p", [_vm._v("이메일, 전화번호 및 사이트 정보가 보여집니다")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "description-toggle-content" }, [
      _c("div", { staticClass: "h6" }, [_vm._v("채널 메세지 수신 기능")]),
      _vm._v(" "),
      _c("p", [_vm._v("메세지를 받아볼 수 있습니다")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "description-toggle-content" }, [
      _c("div", { staticClass: "h6" }, [_vm._v("채널 SNS 링크 보이기")]),
      _vm._v(" "),
      _c("p", [_vm._v("등록한 SNS 리스트가 보여집니다")])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-5faeae66", module.exports)
  }
}

/***/ }),

/***/ 6:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(7);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(8)("09ee0e62", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-c4573c2c\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./sendMessagePopup.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-c4573c2c\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./sendMessagePopup.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 7:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(5)(false);
// imports


// module
exports.push([module.i, "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n", ""]);

// exports


/***/ }),

/***/ 8:
/***/ (function(module, exports, __webpack_require__) {

/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
  Modified by Evan You @yyx990803
*/

var hasDocument = typeof document !== 'undefined'

if (typeof DEBUG !== 'undefined' && DEBUG) {
  if (!hasDocument) {
    throw new Error(
    'vue-style-loader cannot be used in a non-browser environment. ' +
    "Use { target: 'node' } in your Webpack config to indicate a server-rendering environment."
  ) }
}

var listToStyles = __webpack_require__(9)

/*
type StyleObject = {
  id: number;
  parts: Array<StyleObjectPart>
}

type StyleObjectPart = {
  css: string;
  media: string;
  sourceMap: ?string
}
*/

var stylesInDom = {/*
  [id: number]: {
    id: number,
    refs: number,
    parts: Array<(obj?: StyleObjectPart) => void>
  }
*/}

var head = hasDocument && (document.head || document.getElementsByTagName('head')[0])
var singletonElement = null
var singletonCounter = 0
var isProduction = false
var noop = function () {}
var options = null
var ssrIdKey = 'data-vue-ssr-id'

// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
// tags it will allow on a page
var isOldIE = typeof navigator !== 'undefined' && /msie [6-9]\b/.test(navigator.userAgent.toLowerCase())

module.exports = function (parentId, list, _isProduction, _options) {
  isProduction = _isProduction

  options = _options || {}

  var styles = listToStyles(parentId, list)
  addStylesToDom(styles)

  return function update (newList) {
    var mayRemove = []
    for (var i = 0; i < styles.length; i++) {
      var item = styles[i]
      var domStyle = stylesInDom[item.id]
      domStyle.refs--
      mayRemove.push(domStyle)
    }
    if (newList) {
      styles = listToStyles(parentId, newList)
      addStylesToDom(styles)
    } else {
      styles = []
    }
    for (var i = 0; i < mayRemove.length; i++) {
      var domStyle = mayRemove[i]
      if (domStyle.refs === 0) {
        for (var j = 0; j < domStyle.parts.length; j++) {
          domStyle.parts[j]()
        }
        delete stylesInDom[domStyle.id]
      }
    }
  }
}

function addStylesToDom (styles /* Array<StyleObject> */) {
  for (var i = 0; i < styles.length; i++) {
    var item = styles[i]
    var domStyle = stylesInDom[item.id]
    if (domStyle) {
      domStyle.refs++
      for (var j = 0; j < domStyle.parts.length; j++) {
        domStyle.parts[j](item.parts[j])
      }
      for (; j < item.parts.length; j++) {
        domStyle.parts.push(addStyle(item.parts[j]))
      }
      if (domStyle.parts.length > item.parts.length) {
        domStyle.parts.length = item.parts.length
      }
    } else {
      var parts = []
      for (var j = 0; j < item.parts.length; j++) {
        parts.push(addStyle(item.parts[j]))
      }
      stylesInDom[item.id] = { id: item.id, refs: 1, parts: parts }
    }
  }
}

function createStyleElement () {
  var styleElement = document.createElement('style')
  styleElement.type = 'text/css'
  head.appendChild(styleElement)
  return styleElement
}

function addStyle (obj /* StyleObjectPart */) {
  var update, remove
  var styleElement = document.querySelector('style[' + ssrIdKey + '~="' + obj.id + '"]')

  if (styleElement) {
    if (isProduction) {
      // has SSR styles and in production mode.
      // simply do nothing.
      return noop
    } else {
      // has SSR styles but in dev mode.
      // for some reason Chrome can't handle source map in server-rendered
      // style tags - source maps in <style> only works if the style tag is
      // created and inserted dynamically. So we remove the server rendered
      // styles and inject new ones.
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  if (isOldIE) {
    // use singleton mode for IE9.
    var styleIndex = singletonCounter++
    styleElement = singletonElement || (singletonElement = createStyleElement())
    update = applyToSingletonTag.bind(null, styleElement, styleIndex, false)
    remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true)
  } else {
    // use multi-style-tag mode in all other cases
    styleElement = createStyleElement()
    update = applyToTag.bind(null, styleElement)
    remove = function () {
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  update(obj)

  return function updateStyle (newObj /* StyleObjectPart */) {
    if (newObj) {
      if (newObj.css === obj.css &&
          newObj.media === obj.media &&
          newObj.sourceMap === obj.sourceMap) {
        return
      }
      update(obj = newObj)
    } else {
      remove()
    }
  }
}

var replaceText = (function () {
  var textStore = []

  return function (index, replacement) {
    textStore[index] = replacement
    return textStore.filter(Boolean).join('\n')
  }
})()

function applyToSingletonTag (styleElement, index, remove, obj) {
  var css = remove ? '' : obj.css

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = replaceText(index, css)
  } else {
    var cssNode = document.createTextNode(css)
    var childNodes = styleElement.childNodes
    if (childNodes[index]) styleElement.removeChild(childNodes[index])
    if (childNodes.length) {
      styleElement.insertBefore(cssNode, childNodes[index])
    } else {
      styleElement.appendChild(cssNode)
    }
  }
}

function applyToTag (styleElement, obj) {
  var css = obj.css
  var media = obj.media
  var sourceMap = obj.sourceMap

  if (media) {
    styleElement.setAttribute('media', media)
  }
  if (options.ssrId) {
    styleElement.setAttribute(ssrIdKey, obj.id)
  }

  if (sourceMap) {
    // https://developer.chrome.com/devtools/docs/javascript-debugging
    // this makes source maps inside style tags work properly in Chrome
    css += '\n/*# sourceURL=' + sourceMap.sources[0] + ' */'
    // http://stackoverflow.com/a/26603875
    css += '\n/*# sourceMappingURL=data:application/json;base64,' + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + ' */'
  }

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = css
  } else {
    while (styleElement.firstChild) {
      styleElement.removeChild(styleElement.firstChild)
    }
    styleElement.appendChild(document.createTextNode(css))
  }
}


/***/ }),

/***/ 9:
/***/ (function(module, exports) {

/**
 * Translates the list format produced by css-loader into something
 * easier to manipulate.
 */
module.exports = function listToStyles (parentId, list) {
  var styles = []
  var newStyles = {}
  for (var i = 0; i < list.length; i++) {
    var item = list[i]
    var id = item[0]
    var css = item[1]
    var media = item[2]
    var sourceMap = item[3]
    var part = {
      id: parentId + ':' + i,
      css: css,
      media: media,
      sourceMap: sourceMap
    }
    if (!newStyles[id]) {
      styles.push(newStyles[id] = { id: id, parts: [part] })
    } else {
      newStyles[id].parts.push(part)
    }
  }
  return styles
}


/***/ })

/******/ });