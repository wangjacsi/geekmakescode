<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::get('/', function () {
    return view('app.index');
})->name('index');

Auth::routes();
//Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout')->name('logout');
Route::get('/auth/activate', 'Auth\ActivationController@activate')->name('activate');
Route::get('/auth/activate/resend', 'Auth\ActivationResendController@showResendForm')->name('auth.activate.resend');
Route::post('/auth/activate/resend', 'Auth\ActivationResendController@resend');
Route::get('/registered', '\App\Http\Controllers\Auth\RegisterController@registered');




// Search & Video
Route::get('/search', 'SearchController@index');
Route::post('/video/{video}/viewcount', 'VideoViewController@create');
Route::get('video/{video}/vote', 'VideoVoteController@show');
Route::get('/video/{video}/comments', 'VideoCommentController@index');

// Subscription
Route::get('/subscriptions/{channel}', 'ChannelSubscriptionController@show');
Route::post('/emailSubscribe', 'EmailSubscriptionController@store');

// 추천 강좌
Route::get('/getSuggestTutorials', 'TutorialController@getSuggestTutorials');

Route::group(['middleware' => ['auth']], function(){
    // Search
    Route::get('/search/users', 'SearchController@searchUsers');
    Route::get('/search/tags', 'SearchController@searchTags');

    //강좌 관리
    Route::get('/tutorial/create', 'TutorialController@create');//->name('tutorial.create');
    Route::post('/tutorial/store', 'TutorialController@store');
    Route::post('/tutorial/{tutorial}/vote', 'TutorialController@vote');
    Route::post('/tutorial/{tutorial}/interest', 'TutorialController@interest');
    Route::get('/tutorial/{tutorial}/edit', 'TutorialController@edit');
    Route::post('/tutorial/{tutorial}/updateContent', 'TutorialController@updateContent');
    Route::post('/tutorial/{tutorial}/updateSetting', 'TutorialController@updateSetting');
    Route::post('/tutorial/updateDescription/{channel}', 'TutorialController@updateDescription');
    Route::post('/tutorial/{tutorial}/updateImage', 'TutorialController@updateImage');

    // Review
    Route::post('/tutorial/{tutorial}/storeReview', 'ReviewController@storeReview');
    Route::delete('/review/{review}/deleteReview', 'ReviewController@deleteReview');
    Route::post('/tutorial/{tutorial}/reviewavg', 'ReviewController@reviewAvg');
    Route::post('/tutorial/{tutorial}/regist', 'RegistrationController@regist');

    // 비디오 강좌 관리
    Route::get('/video/upload', 'VideoController@upload')->name('video.upload');
    Route::post('/video/upload', 'VideoController@store');
    Route::post('/video/{video}/replace', 'VideoController@replace');
    Route::post('/video/getVideoTumbnails', 'VideoController@getVideoTumbnails');
    Route::post('/video/{video}/delteVideoTumbnail', 'VideoController@delteVideoTumbnail');
    Route::post('/video/{video}/setDefaultVideoTumbnail', 'VideoController@setDefaultVideoTumbnail');
    Route::post('/video/{video}/addThumbnail', 'VideoController@addThumbnail');
    Route::post('/video/{video}/watch', 'VideoViewController@watch');
    Route::post('/video/{video}/watchCurrentTime', 'VideoViewController@watchCurrentTime');



    // 동영상 관리
    Route::delete('/video/{video}', 'VideoController@delete');
    Route::get('/video/{video}/edit', 'VideoController@edit');
    Route::post('/video/updateDescription/{video}', 'VideoController@updateDescription');
    Route::post('/video/{video}/contentUpdate', 'VideoController@contentUpdate');
    Route::post('/video/{video}/vote', 'VideoVoteController@create');
    Route::delete('/video/{video}/vote', 'VideoVoteController@remove');
    Route::post('/video/{video}/comments', 'VideoCommentController@create');
    Route::patch('/video/{video}/comments/{comment}', 'VideoCommentController@update');
    Route::delete('/video/{video}/comments/{comment}', 'VideoCommentController@delete');


    // 채널 관리
    Route::get('/channel/create', 'ChannelController@create')->name('channel.create');
    Route::post('/channel/store', 'ChannelController@store');
    Route::get('/channel/{channel}/edit', 'ChannelController@edit');
    Route::post('/channel/{channel}/updateProfile', 'ChannelController@updateProfile');
    Route::post('/channel/{channel}/contact', 'ChannelController@contact');
    Route::post('/channel/{channel}/sns', 'ChannelController@snsStore');
    Route::post('/channel/{channel}/snsEdit', 'ChannelController@snsEdit');
    Route::post('/channel/{channel}/snsDelete', 'ChannelController@snsDelete');
    Route::post('/channel/updateDescription/{channel}', 'ChannelController@updateDescription');
    Route::post('/channel/{channel}/superadmin/delete', 'ChannelController@superAdminDelete');
    Route::post('/channel/{channel}/superadmin/store', 'ChannelController@superAdminStore');
    Route::post('/channel/{channel}/admin/delete', 'ChannelController@adminDelete');
    Route::post('/channel/{channel}/admin/request', 'ChannelController@adminRequest');
    Route::post('/channel/{channel}/settings/update', 'ChannelController@settingsUpdate');
    Route::get('/channel/admin/accept', 'ChannelController@adminAccept');

    // 구독 관리
    Route::post('/subscription/{channel}', 'ChannelSubscriptionController@create');
    Route::delete('/subscription/{channel}', 'ChannelSubscriptionController@delete');

    // 유저
    Route::get('/mywave', 'UserHomeController@index');
    Route::get('/messagebox', 'UserHomeController@messagebox');
    Route::post('/wave/{user}/tech', 'UserHomeController@techStore');
    Route::post('/wave/{user}/tech/delete', 'UserHomeController@techDelete');
    Route::post('/wave/{user}/tech/edit', 'UserHomeController@techEdit');
    Route::post('/wave/{user}/profile/edit', 'UserHomeController@profileEdit');
    Route::post('/wave/{user}/contact', 'UserHomeController@contact');
    Route::post('/wave/{user}/sns', 'UserHomeController@snsStore');
    Route::post('/wave/{user}/sns/delete', 'UserHomeController@snsDelete');
    Route::post('/wave/{user}/sns/edit', 'UserHomeController@snsEdit');
    Route::post('/wave/{user}/password', 'UserHomeController@passwordUpdatge');
    Route::post('/wave/{user}/settings', 'UserHomeController@settingsUpdatge');

    // API data get
    // Channel
    Route::get('/getChannelsInfoByLoginUser', 'ChannelController@getChannelsInfoByLoginUser');
    Route::get('/getSubscribedChannelsInfoByLoginUser', 'ChannelController@getSubscribedChannelsInfoByLoginUser');


    // Tutorial
    Route::get('/getRegisteredTutorialsInfoByLoginUser', 'TutorialController@getRegisteredTutorialsInfoByLoginUser');
    Route::get('/getMyTutorialsInfoByLoginUser', 'TutorialController@getMyTutorialsInfoByLoginUser');
    Route::post('/getTutorialsByChannel', 'TutorialController@getTutorialsByChannel');
    Route::get('/getInterestTutorialsInfoByLoginUser', 'TutorialController@getInterestTutorialsInfoByLoginUser');

    // Notification
    Route::get('/getNotifByUserPart', 'NotificationController@getNotifByUserPart');
    Route::post('/getNotifByUserAll', 'NotificationController@getNotifByUserAll');
    Route::post('/notifCheckUpdate', 'NotificationController@notifCheckUpdate');

    // Image Upload
    Route::post('/editor/uploadToTemp', 'ImageController@uploadToTemp');
    Route::post('/editor/uploadToS3', 'ImageController@uploadToS3');
    Route::post('/editor/deleteImage', 'ImageController@deleteImage');

    // Message
    Route::post('/sendMessage', 'MessageController@sendMessage');
    Route::post('/userSendMessage/{user}', 'MessageController@userSendMessage');
    Route::post('/setReadMarkMessge/{message}', 'MessageController@setReadMarkMessge');
    Route::post('/setUnReadMarkMessge/{message}', 'MessageController@setUnReadMarkMessge');
    Route::delete('/message/{message}', 'MessageController@delete');
    Route::post('/getUserMessages', 'MessageController@getUserMessages');
    Route::post('/getUserChannelMessages', 'MessageController@getUserChannelMessages');
});

// video
Route::get('/video/{video}', 'VideoController@show');

// channel
Route::get('/channels', 'ChannelController@index')->name('channel.index');
Route::get('/channel/{channel}', 'ChannelController@show');
Route::get('/getChannelsInfo', 'ChannelController@getChannelsInfo');
Route::get('/getChannelAdminUsers/{channel}', 'ChannelController@getChannelAdminUsers');
Route::get('/getChannelsInfoByUserId/{user}', 'ChannelController@getChannelsInfoByUserId');
Route::get('/getSubscribedChannelsInfoByUserId/{user}', 'ChannelController@getSubscribedChannelsInfoByUserId');

// tutorial
Route::get('/tutorials', 'TutorialController@index');
Route::get('/tutorial/{tutorial}', 'TutorialController@show');
Route::get('/getSearchTutorialsInfo/{search}', 'TutorialController@getSearchTutorialsInfo');
Route::get('/getTutorialsInfo', 'TutorialController@getTutorialsInfo');
Route::get('/tutorial/{tutorial}/getReviewsByTutorial', 'ReviewController@getReviewsByTutorial');
Route::get('/getTutorialsInfoByChannel/{channel}', 'TutorialController@getTutorialsInfoByChannel');
Route::get('/getMyTutorialsInfoByUserId/{user}', 'TutorialController@getMyTutorialsInfoByUserId');
Route::get('/getRegisteredTutorialsInfoByUserId/{user}', 'TutorialController@getRegisteredTutorialsInfoByUserId');
Route::get('/getInterestTutorialsInfoByUserId/{user}', 'TutorialController@getInterestTutorialsInfoByUserId');

// User
Route::get('/wave/{user}', 'UserHomeController@show');

// send message
Route::post('/globalSendMessage', 'MessageController@globalSendMessage');

// Authentication
Route::get('/login/{service}', 'Auth\SocialLoginController@redirect');
Route::get('/login/{service}/callback', 'Auth\SocialLoginController@callback');
